package com.bioserver.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;

public class MatchesResponse {

    private ReferenceOrCandidate reference;

    private ReferenceOrCandidate candidate;

    private int score;

    private int falseAcceptanceRate;

    private String correlationId;

    private String created;

    private String expires;

    private String signature;

    public ReferenceOrCandidate getReference() {
        return reference;
    }


    public ReferenceOrCandidate getCandidate() {
        return candidate;
    }

    public int getScore() {
        return score;
    }


    public int getFalseAcceptanceRate() {
        return falseAcceptanceRate;
    }


    public String getCorrelationId() {
        return correlationId;
    }


    public String getCreated() {
        return created;
    }


    public String getExpires() {
        return expires;
    }


    public String getSignature() {
        return signature;
    }
}
