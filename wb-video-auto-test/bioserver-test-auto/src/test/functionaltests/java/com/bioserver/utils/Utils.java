package com.bioserver.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Utils {

    private static final String HEX_PREFIX = "0x";

    public Utils() {
    }

    public static String hidePan(String pan) {
        return pan.substring(0, 4) + " XXXX XXXX " + pan.substring(12);
    }

    public static byte[] convertShortToByteArray(short val) {
        return new byte[]{(byte)(val >>> 8), (byte)val};
    }

    public static byte[] convertByteToByteArray(byte val) {
        return new byte[]{val};
    }

    public static byte[] convertIntToByteArray(int val) {
        return new byte[]{(byte)(val >>> 24), (byte)(val >>> 16), (byte)(val >>> 8), (byte)val};
    }

    public static ByteArray intToByteArray(int val) {
        return (new ByteArrayImpl(convertIntToByteArray(val))).trimLeftZeroes();
    }

    public static byte[] convertIntArrayToByteArray(int[] val) {
        byte[] result = new byte[val.length];

        for(int idx = 0; idx < val.length; ++idx) {
            result[idx] = convertIntToByteArray(val[idx])[0];
        }

        return result;
    }

    public static void clearByteArray(byte[] buffer) {
        if (null != buffer) {
            Arrays.fill(buffer, (byte)0);
        }
    }

    public static void clearByteArray(ByteArray buffer) {
        if (null != buffer) {
            buffer.clear();
        }
    }

    public static String toHexString(byte[] data, int length) {
        String digits = "0123456789ABCDEF";
        StringBuffer sb = new StringBuffer();

        for(int i = 0; i < length; ++i) {
            int b = data[i];
            char c = "0123456789ABCDEF".charAt(b >> 4 & 15);
            sb.append(c);
            c = "0123456789ABCDEF".charAt(b & 15);
            sb.append(c);
        }

        return sb.toString().toUpperCase();
    }

    public static String toHexString(String in) {
        byte[] inb = in.getBytes();
        return toHexString(inb, inb.length);
    }

    public static String BCDAmountarrayToString(byte[] data, int offset, int length) {
        String s = "";
        boolean maskZeros = true;
        if (offset < data.length && offset + length <= data.length) {
            for(int i = offset; i < offset + length; ++i) {
                byte hi = (byte)(data[i] >>> 4 & 15);
                byte lo = (byte)(data[i] & 15);
                if (hi > 9 || lo > 9) {
                    throw new IllegalArgumentException();
                }

                Integer hiInteger = Integer.valueOf(hi);
                Integer loInteger = Integer.valueOf(lo);
                if (!maskZeros || hi != 0) {
                    maskZeros = false;
                    s = s + hiInteger.toString();
                }

                if (!maskZeros || lo != 0) {
                    maskZeros = false;
                    s = s + loInteger.toString();
                }

                if (i == offset + length - 2) {
                    s = s + ".";
                    maskZeros = false;
                }
            }

            if (s.equalsIgnoreCase("") || s.charAt(0) == '.') {
                s = "0" + s;
            }

            return s;
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public static String BCDarrayToString(byte[] data, int offset, int length) {
        String s = "";
        boolean maskZeros = true;
        if (offset < data.length && offset + length <= data.length) {
            for(int i = offset; i < offset + length; ++i) {
                byte hi = (byte)(data[i] >>> 4 & 15);
                byte lo = (byte)(data[i] & 15);
                if (hi > 9 || lo > 9) {
                    throw new IllegalArgumentException();
                }

                Integer hiInteger = new Integer(hi);
                Integer loInteger = new Integer(lo);
                if (!maskZeros || hi != 0) {
                    maskZeros = false;
                    s = s + hiInteger.toString();
                }

                if (!maskZeros || lo != 0) {
                    maskZeros = false;
                    s = s + loInteger.toString();
                }
            }

            return s;
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public static String BCDarrayToString(byte[] data, int offset, int length, boolean maskZeroChar) {
        String s = "";
        boolean maskZeros = maskZeroChar;
        if (offset < data.length && offset + length <= data.length) {
            for(int i = offset; i < offset + length; ++i) {
                byte hi = (byte)(data[i] >>> 4 & 15);
                byte lo = (byte)(data[i] & 15);
                if (hi > 9 || lo > 9) {
                    throw new IllegalArgumentException();
                }

                Integer hiInteger = new Integer(hi);
                Integer loInteger = new Integer(lo);
                if (!maskZeros || hi != 0) {
                    maskZeros = false;
                    s = s + hiInteger.toString();
                }

                if (!maskZeros || lo != 0) {
                    maskZeros = false;
                    s = s + loInteger.toString();
                }
            }

            return s;
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public static byte[] encodeByteArray(byte[] s) {
        int i = 0;
        int j = 0;
        int originalLength = s.length;

        byte[] buf;
        for(buf = new byte[(originalLength + originalLength % 2) / 2]; i <= originalLength - 2; buf[j++] = (byte)((byte)(s[i++] - 48 << 4) | (byte)(s[i++] - 48))) {
        }

        if (originalLength % 2 == 1) {
            buf[j] = (byte)(s[i] - 48 << 4 | 15);
        }

        return buf;
    }

    public static String getAsHexString(byte[] data) {
        return getAsHexString(data, false);
    }

    public static String getAsSmallHexString(byte[] data) {
        if (data == null) {
            return null;
        } else {
            String reduced = getAsHexString(data);
            return reduced.length() > 32 ? reduced.substring(0, 8) + ".." + reduced.substring(reduced.length() - 8, reduced.length()) : reduced;
        }
    }

    public static void main(String[] args) {
        System.out.println((new ByteArrayImpl("AABB03AABBBBBBBBBBBBBBBBBBBBBBBBBBBBCCDD")).getSmallHexString());
    }

    public static short makeShort(byte byte1, byte byte2) {
        short tmp = (short)(255 & byte1);
        tmp = (short)((255 & tmp) << 8);
        short tmp2 = (short)(255 & byte2);
        return (short)(tmp | tmp2);
    }

    public static final String getAsHexString(byte[] data, boolean prefix) {
        return data == null ? null : getAsHexString(data, 0, data.length, prefix);
    }

    public static final String getAsHexString(int value, boolean prefix) {
        return (prefix ? "0x" : "") + Integer.toHexString(value);
    }

    public static final String getAsHexString(byte[] data, int offset, int len) {
        return getAsHexString(data, offset, len, false);
    }

    public static final String getAsHexString(byte[] data, int offset, int len, boolean prefix) {
        StringBuffer sb = new StringBuffer();
        if (offset + len > data.length) {
            len = data.length - offset;
        }

        for(int i = offset; i < offset + len; ++i) {
            byte b = data[i];
            appendByte(b, sb);
        }

        return sb.toString().toUpperCase();
    }

    public static final byte[] readHexString(String hexString) {
        try {
            if (hexString != null && hexString.length() != 0 && !hexString.equals("0x")) {
                if (hexString.length() % 2 != 0) {
                    hexString = "0" + hexString;
                }

                if (hexString.startsWith("0x")) {
                    hexString = hexString.substring(2);
                }

                hexString = hexString.replace("-", "");
                byte[] data = new byte[hexString.length() / 2];

                for(int i = 0; i < data.length; ++i) {
                    data[i] = (byte)(Integer.parseInt(hexString.substring(i * 2, i * 2 + 2), 16) & 255);
                }

                return data;
            } else {
                return new byte[0];
            }
        } catch (NumberFormatException var3) {
            throw new IllegalArgumentException("Unexpected character in hex string");
        }
    }

    private static final void appendByte(int byteValue, StringBuffer sb) {
        String bStr = Integer.toHexString(byteValue & 255);
        if (bStr.length() < 2) {
            sb.append('0');
        }

        sb.append(bStr);
    }

    public static final int readInt(byte[] data, int offset, boolean littleEndian) {
        int tmpLength = data.length - offset;
        if (tmpLength < 4) {
            byte[] tmpBuffer = new byte[]{0, 0, 0, 0};
            System.arraycopy(data, 0, tmpBuffer, 4 - tmpLength, tmpLength);
            data = tmpBuffer;
        }

        return littleEndian ? (data[offset + 3] & 255) << 24 | (data[offset + 2] & 255) << 16 | (data[offset + 1] & 255) << 8 | data[offset + 0] & 255 : (data[offset + 0] & 255) << 24 | (data[offset + 1] & 255) << 16 | (data[offset + 2] & 255) << 8 | data[offset + 3] & 255;
    }

    public static final int read3BytesInt(byte[] data, int offset, boolean littleEndian) {
        int tmpLength = data.length - offset;
        if (tmpLength < 3) {
            byte[] tmpBuffer = new byte[]{0, 0, 0};
            System.arraycopy(data, 0, tmpBuffer, 3 - tmpLength, tmpLength);
            data = tmpBuffer;
        }

        return littleEndian ? (data[offset + 2] & 255) << 16 | (data[offset + 1] & 255) << 8 | data[offset + 0] & 255 : (data[offset + 0] & 255) << 16 | (data[offset + 1] & 255) << 8 | data[offset + 2] & 255;
    }

    public static final int readInt(byte[] data, int offset) {
        return readInt(data, offset, false);
    }

    public static final int read3BytesInt(byte[] data, int offset) {
        return read3BytesInt(data, offset, false);
    }

    public static final int readShort(byte[] data, int offset, boolean littleEndian) {
        return littleEndian ? (data[offset + 1] << 8 | data[offset] & 255) & '\uffff' : (data[offset] << 8 | data[offset + 1] & 255) & '\uffff';
    }

    public static final short readShort(byte[] data, int offset) {
        return (short)readShort(data, offset, false);
    }

    public static final boolean arrayCompare(byte[] aArray, int aOffset, byte[] bArray, int bOffset, int length) {
        if (aOffset + length <= aArray.length && bOffset + length <= bArray.length) {
            for(int index = 0; index < length; ++index) {
                if (aArray[aOffset + index] != bArray[bOffset + index]) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public static final boolean arrayCompare(byte[] buffer, byte[] buf2) {
        int length = buffer.length;
        if (buf2.length != length) {
            for(int i = 0; i < length; ++i) {
                if (buffer[i] != buf2[i]) {
                    return false;
                }
            }
        }

        return true;
    }

    public static final boolean equals(byte[] a, byte[] b, int aOffset, int bOffset, int len) {
        if (a == null && b == null) {
            return true;
        } else if (a != null && b != null && aOffset + len <= a.length && bOffset + len <= b.length) {
            for(int i = 0; i < len; ++i) {
                if (a[aOffset + i] != b[bOffset + i]) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public static boolean equals(byte[] a, byte[] b) {
        if (a == null && b == null) {
            return true;
        } else if (a != null && b != null && a.length == b.length) {
            int size = a.length;

            for(int i = 0; i < size; ++i) {
                if (a[i] != b[i]) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public static int lastIndexOf(String input, String str) {
        int index = -1;
        int tempIndex = 0;

        while(tempIndex != -1) {
            tempIndex = input.indexOf(str, index + 1);
            if (tempIndex != -1) {
                index = tempIndex;
            }
        }

        return index;
    }

    public static ByteArray padLeft(ByteArray buffer, byte padding, int finalLength) {
        if (null == buffer) {
            return buffer;
        } else {
            int length = buffer.getLength();
            if (length >= finalLength) {
                return buffer;
            } else {
                ByteArray res = new ByteArrayImpl(finalLength);
                res.copyBytes(buffer, 0, finalLength - length, length);
                return res;
            }
        }
    }

    public static ByteArray xor(ByteArray buffer, byte val) {
        if (null == buffer) {
            return buffer;
        } else {
            int l = buffer.getLength();
            ByteArray res = buffer.clone();

            for(int i = 0; i < l; ++i) {
                res.setByte(i, (byte)(res.getByte(i) ^ val));
            }

            return res;
        }
    }

    public static byte[] readBCDString(String s) {
        if (s.length() % 2 != 0) {
            s = "0" + s;
        }

        return readHexString(s);
    }

    public static String BCDarrayToString(byte[] data) {
        return BCDarrayToString(data, 0, data.length, false);
    }

    public static String toHexString(byte[] data) {
        return data != null ? toHexString(data, data.length) : null;
    }

    public static String byte2Hex(byte b) {
        String[] HEX_DIGITS = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
        int nb = b & 255;
        int i_1 = nb >> 4 & 15;
        int i_2 = nb & 15;
        return HEX_DIGITS[i_1] + HEX_DIGITS[i_2];
    }

    public static String short2Hex(short s) {
        byte b1 = (byte)(s >> 8);
        byte b2 = (byte)(s & 255);
        return byte2Hex(b1) + byte2Hex(b2);
    }

    public static boolean validExpiration(String s) {
        DateFormat sdf = new SimpleDateFormat("MMyy");
        Calendar now = Calendar.getInstance();
        now.set(now.get(1), now.get(2), 0, 23, 59, 59);

        try {
            Date exp = sdf.parse(s);
            return !exp.before(now.getTime());
        } catch (ParseException var4) {
            return false;
        }
    }

    public static int getLastDayOfMonth(int year, int month) {
        switch(month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 2:
                return (0 != year % 4 || 0 == year % 100) && 0 != year % 400 ? 28 : 29;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            default:
                return 30;
        }
    }

    public static String getLastDay(String year, String month) {
        GregorianCalendar calendar = new GregorianCalendar();
        int yearInt = Integer.parseInt(year);
        int monthInt = Integer.parseInt(month);
        --monthInt;
        calendar.set(yearInt, monthInt, 1);
        int dayInt = calendar.getActualMaximum(5);
        return Integer.toString(dayInt);
    }

    public static ByteArray invertBits(ByteArray val) {
        ByteArray res = new ByteArrayImpl(val.getLength());

        for(int i = 0; i < res.getLength(); ++i) {
            res.setByte(i, (byte)(~val.getByte(i)));
        }

        return res;
    }

    public static ByteArray buildEMVDate(int year, int month, int day) {
        StringBuilder sb = new StringBuilder();
        if (year < 10) {
            sb.append("0");
        }

        sb.append(year);
        if (month < 10) {
            sb.append("0");
        }

        sb.append(month);
        if (day < 10) {
            sb.append("0");
        }

        sb.append(day);
        return new ByteArrayImpl(readBCDString(sb.toString()));
    }

    public static ByteArray buildTrack(String pan, String exp, String trailer) {
        String track2 = pan + "D" + exp.substring(2, 4) + exp.substring(0, 2) + trailer;
        if (track2.length() % 2 != 0) {
            track2 = track2 + "F";
        }

        return new ByteArrayImpl(readBCDString(track2));
    }

    public static ByteArray generateRandomByteArray(int size) {
        SecureRandom r = new SecureRandom(new byte[1]);
        byte[] randomBuffer = new byte[size];
        r.nextBytes(randomBuffer);
        return new ByteArrayImpl(randomBuffer);
    }

    public static String getGUID(ByteArray data) {
        if (data == null) {
            return null;
        } else {
            String s = data.getHexString();
            return (s.substring(0, 8) + "-" + s.substring(8, 12) + "-" + s.substring(12, 16) + "-" + s.substring(16, 20) + "-" + s.substring(20, 32)).toLowerCase();
        }
    }

    public static ByteArray getExpiryDate(long ttl) {
        DateTime now = new DateTime(DateTimeZone.UTC);
        long exp = now.getMillis() + ttl;
        return new ByteArrayImpl(Long.toHexString(exp));
    }
}
