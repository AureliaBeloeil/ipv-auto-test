package com.bioserver.test.replaymetadata;

import com.bioserver.data.LivenessResponse;
import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.*;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import retrofit2.Response;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

import static com.bioserver.test.core.BioserverCoreManager.bio_session_id;
import static com.bioserver.test.core.BioserverCoreManager.getBioSessionId;
import static com.bioserver.utils.ConstantTestEnvironment.biosrv_api_key;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ReplayMetadataChallengeTest extends BioServerBaseTest {

    //Replay liveness from mobile challenge meta data

    @Test
    // TODO TICKET BIO_SRV-1719
    @Order(1)
    //@DisplayName("Test that Get Challenge Response​ testcase should retrieve a challenge Replay response when valid request is sent.")
    public void bio_replay_metadata_challenge_01() {

        log.debug(">> bio_replay_metadata_challenge_01()");

        try {
            log.info("------------------------------");
            log.info("PROCESS POST REPLAY METADATA");
            log.info("-------------------------------");

            postReplayMetadataProcess();

            Thread.sleep(4000);
            log.info("------------------------------------");
            log.info("SEND GET CHALLENGE RESPONSE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> response = BioserverCoreManager.getReplayMetadataChallengeResponse(true);

            JsonObject data = response.body();

            log.info("-----------------------------------");
            log.info("GET CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            //Paths.get(ConstantTestEnvironment.RESOURCE_PATH + REPLAY_METADATA_FACE).toAbsolutePath();
            byte[] expectedFace = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH + REPLAY_METADATA_FACE).toAbsolutePath());

            getChallengeResponseAssertions(data, expectedFace, "SUCCESS");


            runOnTestLink("BioServer-TC-187", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-187", false);
            Assertions.fail("test failed", e);
        }
    }


    @Test
    @Order(2)
    //@DisplayName("Test that Get Challenge Response​ testcase should return error code 401 when bearer auth value is sent.")
    public void bio_replay_metadata_challenge_02() {

        log.debug(">> bio_replay_metadata_challenge_02()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;

        try {

            log.info("------------------------------------");
            log.info("SEND GET CHALLENGE RESPONSE REQUEST");
            log.info("-------------------------------------");

            log.info(" -- Bearer Auth is used ");
            biosrv_api_key = "Bearer ";

            BioserverCoreManager.getReplayMetadataChallengeResponse(false);

            runOnTestLink("BioServer-TC-188", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                runOnTestLink("BioServer-TC-188", true);
                //restore token
                biosrv_api_key = apiKeyRef;
            } else {
                runOnTestLink("BioServer-TC-188", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(3)
    //@DisplayName("Test that Get Challenge Response​ testcase should return error code 401 when wrong apikey value is sent.")
    public void bio_replay_metadata_challenge_03() {

        log.debug(">> bio_replay_metadata_challenge_03()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;

        try {

            log.info("------------------------------------");
            log.info("SEND GET CHALLENGE RESPONSE REQUEST");
            log.info("-------------------------------------");

            log.info(" -- Wrong Api_Key is used ");
            biosrv_api_key = " ";

            BioserverCoreManager.getReplayMetadataChallengeResponse(true);

            runOnTestLink("BioServer-TC-189", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                runOnTestLink("BioServer-TC-189", true);
                //restore token
                biosrv_api_key = apiKeyRef;
            } else {
                runOnTestLink("BioServer-TC-189", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    //@DisplayName("Test that Get Challenge Response​ testcase should return error code 404 when wrong BioSessionId value is sent.")
    public void bio_replay_metadata_challenge_04() {

        log.debug(">> bio_replay_metadata_challenge_04()");

        try {

            log.info("------------------------------------");
            log.info("SEND GET CHALLENGE RESPONSE REQUEST");
            log.info("-------------------------------------");

            log.info(" -- Wrong BioSessionId is used ");
            bio_session_id = null;

            BioserverCoreManager.getReplayMetadataChallengeResponse(true);

            runOnTestLink("BioServer-TC-190", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-190", true);

            } else {
                runOnTestLink("BioServer-TC-190", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(5)
    //TODO A RAJOUTER DANS TESTLINK
    //TODO TICKET BIO_SRV-1786
    //@DisplayName("Test that replay metadata testcase should return success with config securityLevel: LOW, type: LIVENESS_MEDIUM")
    public void bio_replay_metadata_challenge_05() {

        log.debug(">> bio_replay_metadata_challenge_05()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_MEDIUM, securityLevel: LOW");
            JSONObject livenessParams = getLivenessMediumParametersRequest();
            //livenessParams.remove("securityLevel");
            //livenessParams.put("securityLevel", "MEDIUM");

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + ENCRYPTED_METADATA_MEDIUM_LOW).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + ENCRYPTED_MASTER_SECRET_MEDIUM_LOW).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + REPLAY_METADATA_FACE_MEDIUM_LOW).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-257", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-257", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(6)
    //TODO A RAJOUTER DANS TESTLINK
    //TODO TICKET BIO_SRV-1786
    //@DisplayName("Test that replay metadata testcase should return success with config securityLevel: MEDIUM, type: LIVENESS_MEDIUM")
    public void bio_replay_metadata_challenge_06() {

        log.debug(">> bio_replay_metadata_challenge_06()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_MEDIUM, securityLevel: MEDIUM");
            JSONObject livenessParams = getLivenessMediumParametersRequest();
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "MEDIUM");

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + ENCRYPTED_METADATA_MEDIUM_MEDIUM).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + ENCRYPTED_MASTER_SECRET_MEDIUM_MEDIUM).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + REPLAY_METADATA_FACE_MEDIUM_MEDIUM).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-258", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-258", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(7)
    //TODO A RAJOUTER DANS TESTLINK
    //TODO TICKET BIO_SRV-1786
    //@DisplayName("Test that replay metadata testcase should return success when type value is set to LIVENESS_MEDIUM and securityLevel is set to HIGH")
    public void bio_replay_metadata_challenge_07() {

        log.debug(">> bio_replay_metadata_challenge_07()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_MEDIUM, securityLevel: HIGH");
            JSONObject livenessParams = getLivenessMediumParametersRequest();
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "HIGH");

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + ENCRYPTED_METADATA_MEDIUM_HIGH).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + ENCRYPTED_MASTER_SECRET_MEDIUM_HIGH).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + REPLAY_METADATA_FACE_MEDIUM_HIGH).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-259", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-259", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(8)
    //TODO A RAJOUTER DANS TESTLINK
    //@DisplayName("Test that replay metadata testcase should return success when type value is set to LIVENESS_HIGH and securityLevel is set to LOW")
    public void bio_replay_metadata_challenge_08() {

        log.debug(">> bio_replay_metadata_challenge_08()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_HIGH, securityLevel: LOW");
            JSONObject livenessParams = getLivenessHighParametersRequest();

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + ENCRYPTED_METADATA_HIGH_LOW).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + ENCRYPTED_MASTER_SECRET_HIGH_LOW).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + REPLAY_METADATA_FACE_HIGH_LOW).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-260", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-260", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(9)
    //TODO A RAJOUTER DANS TESTLINK
    //@DisplayName("Test that replay metadata testcase should return success when type value is set to LIVENESS_HIGH and securityLevel is set to MEDIUM")
    public void bio_replay_metadata_challenge_09() {

        log.debug(">> bio_replay_metadata_challenge_09()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_HIGH, securityLevel: MEDIUM");
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "MEDIUM");

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + ENCRYPTED_METADATA_HIGH_MEDIUM).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + ENCRYPTED_MASTER_SECRET_HIGH_MEDIUM).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + REPLAY_METADATA_FACE_HIGH_MEDIUM).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-261", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-261", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(10)
    //TODO A RAJOUTER DANS TESTLINK
    //@DisplayName("Test that replay metadata testcase should return success when type value is set to LIVENESS_HIGH and securityLevel is set to HIGH")
    public void bio_replay_metadata_challenge_10() {

        log.debug(">> bio_replay_metadata_challenge_10()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_HIGH, securityLevel: HIGH");
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "HIGH");

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + ENCRYPTED_METADATA_HIGH_HIGH).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + ENCRYPTED_MASTER_SECRET_HIGH_HIGH).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + REPLAY_METADATA_FACE_HIGH_HIGH).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-262", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-262", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(11)
    //TODO A RAJOUTER DANS TESTLINK
    //@DisplayName("Test that replay metadata testcase should return success when type value is set to LIVENESS_HIGH and securityLevel is set to VERY_HIGH")
    public void bio_replay_metadata_challenge_11() {

        log.debug(">> bio_replay_metadata_challenge_11()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_HIGH, securityLevel: VERY_HIGH");
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "VERY_HIGH");

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + ENCRYPTED_METADATA_HIGH_VERYHIGH).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + ENCRYPTED_MASTER_SECRET_HIGH_VERYHIGH).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + REPLAY_METADATA_FACE_HIGH_VERYHIGH).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-263", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-263", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(12)
    //TODO A RAJOUTER DANS TESTLINK
    //TODO TICKET BIO_SRV-1786
    //@DisplayName("Test that replay metadata testcase should return success when type value is set to LIVENESS_MEDIUM and securityLevel is set to VERY_HIGH")
    public void bio_replay_metadata_challenge_12() {

        log.debug(">> bio_replay_metadata_challenge_12()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_MEDIUM, securityLevel: VERY_HIGH");
            JSONObject livenessParams = getLivenessMediumParametersRequest();
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "VERY_HIGH");

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + ENCRYPTED_METADATA_MEDIUM_VERYHIGH).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + ENCRYPTED_MASTER_SECRET_MEDIUM_VERYHIGH).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + REPLAY_METADATA_FACE_MEDIUM_VERYHIGH).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-264", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-264", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(13)
    //TODO A RAJOUTER DANS TESTLINK
    //TODO TICKET BIO_SRV-1797
    //@DisplayName("Test that replay metadata testcase should return SUCCESS when matchThreshold is bigger than the expected threshold(PASSIVE LIVENESS MODE)")
    public void bio_replay_metadata_challenge_13() {
        log.debug(">> bio_replay_metadata_challenge_13()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_PASSIVE, securityLevel: VERY_HIGH");
            JSONObject livenessParams = getLivenessPassiveParametersRequest();
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "VERY_HIGH");
            livenessParams.remove("matchThreshold");
            livenessParams.put("matchThreshold", 100000000);

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_METADATA_PASSIVE_VERYHIGH).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_MASTER_SECRET_PASSIVE_VERYHIGH).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + REPLAY_METADATA_FACE_PASSIVE_VERYHIGH).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-265", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-265", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(14)
    //TODO A RAJOUTER DANS TESTLINK
    //@DisplayName("Test that replay metadata testcase should return SPOOF when NO_LIVENESS type is used")
    public void bio_replay_metadata_challenge_14() {
        log.debug(">> bio_replay_metadata_challenge_14()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: NO_LIVENESS, securityLevel: VERY_HIGH");
            JSONObject livenessParams = getLivenessPassiveParametersRequest();
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "VERY_HIGH");
            livenessParams.remove("type");
            livenessParams.put("type", "NO_LIVENESS");

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_METADATA_PASSIVE_VERYHIGH).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_MASTER_SECRET_PASSIVE_VERYHIGH).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + REPLAY_METADATA_FACE_PASSIVE_VERYHIGH).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SPOOF");

            runOnTestLink("BioServer-TC-266", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-266", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(15)
    //TODO A RAJOUTER DANS TESTLINK
    //@DisplayName("Test that replay metadata testcase should return SPOOF when wrong nbChallenge is used")
    public void bio_replay_metadata_challenge_15() {
        log.debug(">> bio_replay_metadata_challenge_15()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_PASSIVE, securityLevel: VERY_HIGH");
            JSONObject livenessParams = getLivenessPassiveParametersRequest();
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "VERY_HIGH");
            livenessParams.remove("nbChallenge");
            livenessParams.put("nbChallenge", 3);

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_METADATA_PASSIVE_VERYHIGH).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_MASTER_SECRET_PASSIVE_VERYHIGH).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + REPLAY_METADATA_FACE_PASSIVE_VERYHIGH).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SPOOF");

            runOnTestLink("BioServer-TC-267", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-267", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(16)
    //TODO A RAJOUTER DANS TESTLINK
    //@DisplayName("Test that replay metadata testcase should return SUCCESS when useAccurateMatch is set to false and matchThreshold too big than the expected threshold")
    public void bio_replay_metadata_challenge_16() {
        log.debug(">> bio_replay_metadata_challenge_16()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_PASSIVE, securityLevel: VERY_HIGH");
            JSONObject livenessParams = getLivenessPassiveParametersRequest();
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "VERY_HIGH");
            livenessParams.remove("useAccurateMatch");
            livenessParams.put("useAccurateMatch", false);
            livenessParams.remove("matchThreshold");
            livenessParams.put("matchThreshold", 100000000);

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_METADATA_PASSIVE_VERYHIGH).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_MASTER_SECRET_PASSIVE_VERYHIGH).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + REPLAY_METADATA_FACE_PASSIVE_VERYHIGH).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-268", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-268", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(17)
    //@DisplayName("Test that replay metadata testcase should return SPOOF when matchThreshold is bigger than the expected threshold(HIGH LIVENESS MODE)")
    public void bio_replay_metadata_challenge_17() {
        log.debug(">> bio_replay_metadata_challenge_17()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_HIGH, securityLevel: VERY_HIGH");
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("matchThreshold");
            livenessParams.put("matchThreshold", 100000000);

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + ENCRYPTED_METADATA_HIGH_LOW).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + ENCRYPTED_MASTER_SECRET_HIGH_LOW).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_HIGH_PATH + REPLAY_METADATA_FACE_HIGH_LOW).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SPOOF");

            runOnTestLink("BioServer-TC-484", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-484", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(18)
    //@DisplayName("Test that replay metadata testcase should return SPOOF when matchThreshold is bigger than the expected threshold(MEDIUM LIVENESS MODE)")
    public void bio_replay_metadata_challenge_18() {
        log.debug(">> bio_replay_metadata_challenge_18()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_MEDIUM, securityLevel: LOW");
            JSONObject livenessParams = getLivenessMediumParametersRequest();
            livenessParams.remove("matchThreshold");
            livenessParams.put("matchThreshold", 100000000);

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + ENCRYPTED_METADATA_MEDIUM_LOW).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + ENCRYPTED_MASTER_SECRET_MEDIUM_LOW).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_MEDIUM_PATH + REPLAY_METADATA_FACE_MEDIUM_LOW).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SPOOF");

            runOnTestLink("BioServer-TC-485", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-485", false);
            Assertions.fail("test failed", e);
        }
    }

    public void postReplayMetadataProcess() {

        log.debug(">> postReplayMetadataProcess()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            JSONObject liveness = getLivenessParametersRequest();

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(liveness, false);

            log.info("--------------------------------------");
            log.info("INIT LIVENESS PARAMS: PARSE RESPONSE");
            log.info("--------------------------------------");

            LivenessResponse livenessResponse = response.body();

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            //assert response
            replayLivenessResponseAssertions(liveness, livenessResponse);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND POST REPLAY METADATA REQUEST");
            log.info("--------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_METADATA).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_MASTER_SECRET).toFile();
            // get device info file
            File deviceInfoRequest = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + DEVICE_INFO).toFile();

            Response<Void> response1 = BioserverCoreManager.postReplayMetadata(encMetadata, encMasterSecret, deviceInfoRequest, true);

            log.info("-----------------------------------------");
            log.info("POST REPLAY METADATA RESPONSE ASSERTIONS");
            log.info("-----------------------------------------");

            log.info(" -- assert status code = {}", response1.code());
            org.assertj.core.api.Assertions.assertThat(response1.code()).isEqualTo(202);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            Assertions.fail("test failed", e);
        }
    }


    private void replayLivenessResponseProcessing(JSONObject livenessParams, Response<LivenessResponse> response) {
        log.info("--------------------------------------");
        log.info("INIT LIVENESS PARAMS: PARSE RESPONSE");
        log.info("--------------------------------------");

        LivenessResponse livenessResponse = response.body();

        log.info(" -- assert status code = {}", response.code());
        Assertions.assertThat(response.code()).isEqualTo(200);

        //Assert content type
        log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
        Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

        replayLivenessResponseAssertions(livenessParams, livenessResponse);
    }

    private JSONObject getLivenessMediumParametersRequest() {
        JSONObject liveness = new JSONObject();
        liveness.put("type", "LIVENESS_MEDIUM");
        liveness.put("timeout", 0);
        liveness.put("securityLevel", "LOW");
        liveness.put("nbChallenge", 1);
        liveness.put("useAccurateMatch", true);
        liveness.put("matchThreshold", 2500);
        liveness.put("imageRetrievalDisabled", false);

        return liveness;
    }

    private JSONObject getLivenessPassiveParametersRequest() {
        JSONObject liveness = new JSONObject();
        liveness.put("type", "LIVENESS_PASSIVE");
        liveness.put("timeout", 0);
        liveness.put("securityLevel", "MEDIUM");
        liveness.put("nbChallenge", 1);
        liveness.put("useAccurateMatch", true);
        liveness.put("matchThreshold", 2500);
        liveness.put("imageRetrievalDisabled", false);

        return liveness;
    }

    private JSONObject getLivenessParametersRequest() {
        JSONObject liveness = new JSONObject();
        liveness.put("type", "HIGH");
        liveness.put("timeout", 0);
        liveness.put("securityLevel", "LOW");
        liveness.put("nbChallenge", 2);
        liveness.put("useAccurateMatch", true);
        liveness.put("matchThreshold", 2500);
        liveness.put("imageRetrievalDisabled", false);

        return liveness;
    }

    private JSONObject getLivenessHighParametersRequest() {
        JSONObject liveness = new JSONObject();
        liveness.put("type", "LIVENESS_HIGH");
        liveness.put("timeout", 0);
        liveness.put("securityLevel", "LOW");
        liveness.put("nbChallenge", 2);
        liveness.put("useAccurateMatch", true);
        liveness.put("matchThreshold", 2500);
        liveness.put("imageRetrievalDisabled", false);

        return liveness;
    }

    private void replayLivenessResponseAssertions(JSONObject livenessParams, LivenessResponse livenessResponse) {
        log.info("-----------------------------------------");
        log.info("INIT LIVENESS PARAMS RESPONSE ASSERTIONS");
        log.info("-----------------------------------------");

        log.info(" -- assert LivenessResponse.seed = {}", livenessResponse.getSeed());
        Assertions.assertThat(livenessResponse.getSeed()).isEqualTo(42);

        log.info(" -- assert LivenessResponse.id = {}", livenessResponse.getId());
        Assertions.assertThat(isUUID(livenessResponse.getId())).isTrue();

        if(livenessParams.get("timeout") != null) {
            log.info(" -- assert LivenessResponse.timeout = {}", livenessResponse.getTimeout());
            Assertions.assertThat(livenessResponse.getTimeout()).isEqualTo(livenessParams.get("timeout"));
        }

        log.info(" -- assert decoded LivenessResponse.serverRandom = {}", livenessResponse.getServerRandom());
        String decodedRandom = new String(Base64.getDecoder().decode(livenessResponse.getServerRandom()), StandardCharsets.UTF_8);
        log.info("decoded ServerRandom: "+decodedRandom);
        Assertions.assertThat(decodedRandom).isNotNull();

        log.info(" -- assert LivenessResponse.certificates = {}", livenessResponse.getCertificates());
        for(String certificate: livenessResponse.getCertificates()) {

            String decodedCert = new String(Base64.getDecoder().decode(certificate), StandardCharsets.UTF_8);
            log.info(" -- check decoded certificate = {}", decodedCert);

            Assertions.assertThat(decodedCert).isNotNull();
        }

        if(livenessParams.get("timeout") != null) {
            log.info(" -- assert LivenessResponse.type = {}", livenessResponse.getType());
            Assertions.assertThat(livenessResponse.getType()).isEqualTo(livenessParams.get("type"));
        }

        if(livenessParams.get("securityLevel") != null) {
            log.info(" -- assert LivenessResponse.securityLevel = {}", livenessResponse.getSecurityLevel());
            Assertions.assertThat(livenessResponse.getSecurityLevel()).isEqualTo(livenessParams.get("securityLevel"));
        }

        if(livenessParams.get("matchThreshold") != null) {
            log.info(" -- assert LivenessResponse.matchThreshold = {}", livenessResponse.getMatchThreshold());
            Assertions.assertThat(livenessResponse.getMatchThreshold()).isEqualTo(livenessParams.get("matchThreshold"));
        }

        if(livenessParams.get("nbChallenge") != null) {
            log.info(" -- assert LivenessResponse.nbChallenge = {}", livenessResponse.getNbChallenge());
            Assertions.assertThat(livenessResponse.getNbChallenge()).isEqualTo(livenessParams.get("nbChallenge"));
        }

        if(livenessParams.get("useAccurateMatch") != null) {
            log.info(" -- assert LivenessResponse.useAccurateMatch = {}", livenessResponse.isUseAccurateMatch());
            Assertions.assertThat(livenessResponse.isUseAccurateMatch()).isEqualTo(livenessParams.get("useAccurateMatch"));
        }

        log.info(" -- assert LivenessResponse.logLevel = {}", livenessResponse.getLogLevel());
        Assertions.assertThat(livenessResponse.getLogLevel()).isEqualTo("DEBUG");

        log.info(" -- assert LivenessResponse.imageRetrievalDisabled = {}", livenessResponse.isImageRetrievalDisabled());
        Assertions.assertThat(livenessResponse.isImageRetrievalDisabled()).isEqualTo(livenessParams.get("imageRetrievalDisabled"));

        log.info(" ---------------------------");
        log.info(" Verify Signature");
        log.info(" ---------------------------");
        log.info(" -- assert signature  = {}", livenessResponse.getSignature());

        log.info(" decode signature data");
        String dataToVerify = decodeTokenParts(livenessResponse.getSignature());
        log.info(" -- Verify data (header+payload) with the signed data");
        verifySignature(new ByteArrayImpl(Utils.toHexString(dataToVerify)).getBytes(), signatureBytes);
    }
}
