package com.bioserver.test.biosession;

import com.bioserver.data.BioSessionResponse;
import com.bioserver.utils.*;
import com.bioserver.test.core.BioserverCoreManager;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import retrofit2.Response;

import static com.bioserver.utils.ConstantTestEnvironment.*;


@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CreateBioSessionTest extends BioServerBaseTest {

    //---------------------------------------------------------------------
    // Naming Conventions used: Should_ExpectedBehavior_When_StateUnderTest
    // ex: ShouldSuccessWhenCorrectCreateBioSessionFieldIsPassed
// ShouldFailWhenWrongCreateBioSessionAuthIsPassed
    // ShouldSuccessWhenValidGetBioSessionRequestIsSent
    //---------------------------------------------------------------------

    //@Test
//    @Order(1)
//    ////@DisplayName("get token should return success")
//    public void getTokenShouldSuccess() {
//
//        try {
//            log.debug(">> getTokenShouldSuccess()");
//
//            String response = BioserverCoreManager.getToken();
//            log.info(" -- get Token  | check result");
//            Assertions.assertThat(response).isNotNull();
//            runOnTestLink("getToken", true);
//        } catch (Throwable e) {
//            if(e instanceof BiosrvException) {
//                e.printStackTrace(System.out);
//            }
//            Assertions.fail("test failed");
//        }
//    }

    @Test
    @Order(1)
    ////@DisplayName("create bio-session should return success")
    public void bio_session_01() {

        log.debug(">> bio_session_01()");

        try {

            log.info("-----------------------------------------");
            log.info("FORMAT AND SEND CREATE BIOSESSION REQUEST");
            log.info("------------------------------------------");
            Response<Void> response = BioserverCoreManager.createBioSession();

            log.info("--------------------------------");
            log.info("CREATE BIOSESSION PARSE RESPONSE");
            log.info("--------------------------------");
            log.info(" -- retrieve bio_session_id in the header response ");
            BioserverCoreManager.bio_session_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("bio_session_id: "+BioserverCoreManager.bio_session_id);

            log.info("-------------------------------------");
            log.info("CREATE BIOSESSION RESPONSE ASSERTIONS");
            log.info("-------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            Assertions.assertThat(response.code()).isEqualTo(201);
            log.info(" -- assert bio_session_id  = {}", BioserverCoreManager.bio_session_id);
            Assertions.assertThat(BioserverCoreManager.bio_session_id).isNotNull();
            runOnTestLink("BioServer-TC-8", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-8", false);
            Assertions.fail("test failed");
        }
    }

    @Test
    @Order(2)
    ////@DisplayName("Create BioSession Rest API testcase should return error code 401 in case a wrong api_key is sent in the request.")
    public void bio_session_02() {

        log.debug(">> bio_session_02()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;
        try {

            log.info(" -- Wrong api_key is used ");
            biosrv_api_key = " ";

            log.info("-----------------------------------------");
            log.info("FORMAT AND SEND CREATE BIOSESSION REQUEST");
            log.info("------------------------------------------");

            BioserverCoreManager.createBioSession();

            runOnTestLink("BioServer-TC-9", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                runOnTestLink("BioServer-TC-9", true);
                //restore token
                biosrv_api_key = apiKeyRef;
            } else {
                runOnTestLink("BioServer-TC-9", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(3)
    ////@DisplayName("Create BioSession Rest API testcase should return success in case correlationId is not passed")
    public void bio_session_03() {

        log.debug(">> bio_session_03()");

        try {

            log.info("-----------------------------------------");
            log.info("FORMAT AND SEND CREATE BIOSESSION REQUEST");
            log.info("------------------------------------------");
            JsonObject createParams = new JsonObject();
            createParams.addProperty("imageStorageEnabled", true);
            //createParams.addProperty("correlationId", correlation_id);
            Response<Void> response = BioserverCoreManager.createBioSession(createParams);

            log.info("--------------------------------");
            log.info("CREATE BIOSESSION PARSE RESPONSE");
            log.info("--------------------------------");
            log.info(" -- retrieve bio_session_id in the header response ");
            BioserverCoreManager.bio_session_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("bio_session_id: "+BioserverCoreManager.bio_session_id);

            log.info("-------------------------------------");
            log.info("CREATE BIOSESSION RESPONSE ASSERTIONS");
            log.info("-------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            Assertions.assertThat(response.code()).isEqualTo(201);
            log.info(" -- assert bio_session_id  = {}", BioserverCoreManager.bio_session_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.bio_session_id)).isTrue();
            runOnTestLink("BioServer-TC-10", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-10", false);
            Assertions.fail("test failed");
        }
    }

    @Test
    @Order(4)
    ////@DisplayName("Create BioSession Rest API testcase should return success in case imageStorageEnabled is not passed.")
    public void bio_session_04() {

        log.debug(">> bio_session_04()");

        try {

            log.info("-----------------------------------------");
            log.info("FORMAT AND SEND CREATE BIOSESSION REQUEST");
            log.info("------------------------------------------");
            JsonObject createParams = new JsonObject();
            createParams.addProperty("correlationId", correlation_id);
            Response<Void> response = BioserverCoreManager.createBioSession(createParams);

            log.info("--------------------------------");
            log.info("CREATE BIOSESSION PARSE RESPONSE");
            log.info("--------------------------------");
            log.info(" -- retrieve bio_session_id in the header response ");
            BioserverCoreManager.bio_session_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("bio_session_id: "+BioserverCoreManager.bio_session_id);

            log.info("-------------------------------------");
            log.info("CREATE BIOSESSION RESPONSE ASSERTIONS");
            log.info("-------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            Assertions.assertThat(response.code()).isEqualTo(201);
            log.info(" -- assert bio_session_id  = {}", BioserverCoreManager.bio_session_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.bio_session_id)).isTrue();
            runOnTestLink("BioServer-TC-11", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-11", false);
            Assertions.fail("test failed");
        }
    }

    @Test
    @Order(5)
    ////@DisplayName("Create BioSession Rest API testcase should return success in case all optional fields are passed")
    public void bio_session_05() {

        log.debug(">> bio_session_05()");

        try {

            log.info("-----------------------------------------");
            log.info("FORMAT AND SEND CREATE BIOSESSION REQUEST");
            log.info("------------------------------------------");
            JsonObject createParams = new JsonObject();
            createParams.addProperty("correlationId", correlation_id);
            createParams.addProperty("imageStorageEnabled", true);
            createParams.addProperty("ttlSeconds", 30);
            createParams.addProperty("callbackURL", "callback");
            Response<Void> response = BioserverCoreManager.createBioSession(createParams);

            log.info("--------------------------------");
            log.info("CREATE BIOSESSION PARSE RESPONSE");
            log.info("--------------------------------");
            log.info(" -- retrieve bio_session_id in the header response ");
            BioserverCoreManager.bio_session_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("bio_session_id: "+BioserverCoreManager.bio_session_id);

            log.info("-------------------------------------");
            log.info("CREATE BIOSESSION RESPONSE ASSERTIONS");
            log.info("-------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            Assertions.assertThat(response.code()).isEqualTo(201);
            log.info(" -- assert bio_session_id  = {}", BioserverCoreManager.bio_session_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.bio_session_id)).isTrue();
            runOnTestLink("BioServer-TC-12", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-12", false);
            Assertions.fail("test failed");
        }
    }

    @Test
    @Order(6)
    ////@DisplayName("Create BioSession Rest API testcase should return success in case the body is not present.")
    public void bio_session_06() {

        log.debug(">> bio_session_06()");

        try {

            log.info("-----------------------------------------");
            log.info("FORMAT AND SEND CREATE BIOSESSION REQUEST");
            log.info("------------------------------------------");

            JsonObject createParams = new JsonObject();
            Response<Void> response = BioserverCoreManager.createBioSession(createParams);

            log.info("--------------------------------");
            log.info("CREATE BIOSESSION PARSE RESPONSE");
            log.info("--------------------------------");
            log.info(" -- retrieve bio_session_id in the header response ");
            BioserverCoreManager.bio_session_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("bio_session_id: "+BioserverCoreManager.bio_session_id);

            log.info("-------------------------------------");
            log.info("CREATE BIOSESSION RESPONSE ASSERTIONS");
            log.info("-------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            Assertions.assertThat(response.code()).isEqualTo(201);
            log.info(" -- assert bio_session_id  = {}", BioserverCoreManager.bio_session_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.bio_session_id)).isTrue();

            runOnTestLink("BioServer-TC-13", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-13", false);
            Assertions.fail("test failed");
        }
    }

    @Test
    @Order(7)
    ////@DisplayName("Get BioSession Rest API testcase should return success and bioSession in case valid request is sent.")
    public void bio_session_07() {
        try {
            log.debug(">> bio_session_07()");

            log.info("----------------------------");
            log.info("CREATE BIOSESSION REQUEST");
            log.info("-----------------------------");

            Response<Void> response = BioserverCoreManager.createBioSession();

            log.info(" -- retrieve bio_session_id in the header response ");
            BioserverCoreManager.bio_session_id = BioserverCoreManager.readheadersResponseAndParseId(response);

            log.info("----------------------");
            log.info("GET BIOSESSION REQUEST");
            log.info("----------------------");

            Response<BioSessionResponse> getBioSessionResponse = BioserverCoreManager.getBioSession();

            log.info("-----------------------------------");
            log.info("GET BIOSESSION RESPONSE ASSERTIONS");
            log.info("-----------------------------------");
            log.info("--get BioSession  | check result");
            Assertions.assertThat(getBioSessionResponse.code()).isEqualTo(200);

            log.info(" -- assert Content-Type  = {}", getBioSessionResponse.headers().get("Content-Type"));
            Assertions.assertThat(getBioSessionResponse.headers().get("Content-Type")).isEqualTo("application/json");

            log.info(" -- assert id  = {}", getBioSessionResponse.body().getId());
            Assertions.assertThat(BioserverCoreManager.bio_session_id).isEqualTo(getBioSessionResponse.body().getId());
            Assertions.assertThat(isUUID(getBioSessionResponse.body().getId())).isTrue();


            log.info(" -- assert isImageStorageEnabled  = {}", getBioSessionResponse.body().isImageStorageEnabled());
            Assertions.assertThat(getBioSessionResponse.body().isImageStorageEnabled()).isTrue();

            log.info(" ---------------------------");
            log.info(" Check created date format");
            log.info(" ---------------------------");
            log.info(" -- assert created  = {}", getBioSessionResponse.body().getCreated());
            dateFormatCheckingProcess(getBioSessionResponse.body().getCreated());

            log.info(" ---------------------------");
            log.info(" Check expires date format");
            log.info(" ---------------------------");
            log.info(" -- assert expires  = {}", getBioSessionResponse.body().getCreated());
            dateFormatCheckingProcess(getBioSessionResponse.body().getExpires());

            log.info(" ---------------------------");
            log.info(" Verify Signature");
            log.info(" ---------------------------");
            log.info(" -- assert signature  = {}", getBioSessionResponse.body().getSignature());
            log.info(" -- decode signature data");
            String dataToVerify = decodeTokenParts(getBioSessionResponse.body().getSignature());
            log.info(" -- Verify  data (header+payload) with the signed data");
            verifySignature(new ByteArrayImpl(Utils.toHexString(dataToVerify)).getBytes(), signatureBytes);

            runOnTestLink("BioServer-TC-14", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-14", false);
            Assertions.fail("test failed");
        }
    }

    @Test
    @Order(8)
    ////@DisplayName("Get BioSession Rest API testcase should return error code 401 when wrong api_key value is sent in the request.")
    public void bio_session_08() {

        log.debug(">> bio_session_08()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;
        try {

            log.info("----------------------------");
            log.info("CREATE BIOSESSION REQUEST");
            log.info("-----------------------------");

            Response<Void> response = BioserverCoreManager.createBioSession();

            log.info(" -- retrieve bio_session_id in the header response ");
            BioserverCoreManager.bio_session_id = BioserverCoreManager.readheadersResponseAndParseId(response);

            log.info(" -- Wrong Api_key is used ");
            biosrv_api_key = " ";

            log.info("----------------------");
            log.info("GET BIOSESSION REQUEST");
            log.info("----------------------");

            BioserverCoreManager.getBioSession();
            runOnTestLink("BioServer-TC-15", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                runOnTestLink("BioServer-TC-15", true);
                //restore token
                biosrv_api_key = apiKeyRef;
            } else {
                runOnTestLink("BioServer-TC-15", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(9)
    ////@DisplayName("Get BioSession Rest API testcase should return error code 404 in case a wrong biosession_id is sent in the request.")
    public void bio_session_09() {

        log.debug(">> bio_session_09()");

        try {

            log.info("----------------------------");
            log.info("CREATE BIOSESSION REQUEST");
            log.info("-----------------------------");

            Response<Void> response = BioserverCoreManager.createBioSession();

            log.info(" -- retrieve bio_session_id in the header response ");
            BioserverCoreManager.bio_session_id = BioserverCoreManager.readheadersResponseAndParseId(response);

            log.info(" -- Wrong bioSessionId is used ");
            BioserverCoreManager.bio_session_id = wrong_biosession_id;

            log.info("----------------------");
            log.info("GET BIOSESSION REQUEST");
            log.info("----------------------");

            BioserverCoreManager.getBioSession();

            runOnTestLink("BioServer-TC-16", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-16", true);
            } else {
                runOnTestLink("BioServer-TC-16", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(10)
    ////@DisplayName("Get BioSession Rest API testcase should return error code 404 in case an old biosession_id is sent in the request.")
    public void bio_session_10() {

        log.debug(">> bio_session_10()");

        try {

            log.info("----------------------------");
            log.info("CREATE BIOSESSION REQUEST");
            log.info("-----------------------------");

            Response<Void> response = BioserverCoreManager.createBioSession();

            log.info(" -- retrieve bio_session_id in the header response ");
            BioserverCoreManager.bio_session_id = BioserverCoreManager.readheadersResponseAndParseId(response);

            log.info(" -- Old bioSessionId is used ");
            BioserverCoreManager.bio_session_id = old_biosession_id;

            log.info("----------------------");
            log.info("GET BIOSESSION REQUEST");
            log.info("----------------------");

            BioserverCoreManager.getBioSession();

            runOnTestLink("BioServer-TC-17", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-17", true);
            } else {
                runOnTestLink("BioServer-TC-17", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(11)
    ////@DisplayName("Get BioSession Rest API testcase should return error code 405 when no biosession_id is sent in the request.")
    public void bio_session_11() {

        log.debug(">> bio_session_11()");

        try {

            log.info("----------------------------");
            log.info("CREATE BIOSESSION REQUEST");
            log.info("-----------------------------");

            Response<Void> response = BioserverCoreManager.createBioSession();

            log.info(" -- retrieve bio_session_id in the header response ");
            BioserverCoreManager.bio_session_id = BioserverCoreManager.readheadersResponseAndParseId(response);

            log.info(" -- Wrong bioSessionId is used ");
            BioserverCoreManager.bio_session_id = "";

            log.info("----------------------");
            log.info("GET BIOSESSION REQUEST");
            log.info("----------------------");

            BioserverCoreManager.getBioSession();

            runOnTestLink("BioServer-TC-284", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_ALLOWED_METHOD_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_ALLOWED_METHOD_RESPONSE.name());
                runOnTestLink("BioServer-TC-284", true);
            } else {
                runOnTestLink("BioServer-TC-284", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }
}
