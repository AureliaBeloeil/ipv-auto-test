package com.bioserver.test.facemanagement;

import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.BioServerBaseTest;
import com.bioserver.utils.BioSrvErrorEnum;
import com.bioserver.utils.BiosrvException;
import com.bioserver.utils.ConstantTestEnvironment;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import retrofit2.Response;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import static com.bioserver.test.core.BioserverCoreManager.bio_session_id;
import static com.bioserver.utils.ConstantTestEnvironment.biosrv_api_key;
import static com.bioserver.utils.ConstantTestEnvironment.wrong_biosession_id;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DeleteFacesTest extends BioServerBaseTest {


    @Test
    @Order(1)
    //@DisplayName("Delete Faces testcase should return success when a valid request is sent.")
    public void bio_delete_faces_01() {

        log.debug(">> bio_delete_faces_01()");

        try {
            log.info(" -- create face 1");
            createFaceResourceProcess(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile());
            log.info(" -- create face 2");
            BioserverCoreManager.face_id = createFaceResourceProcess(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_2).toFile());

            log.info("--------------------");
            log.info("DELETE FACES REQUEST");
            log.info("---------------------");

            Response<Void> delFacesResponse = BioserverCoreManager.deleteFaces(false);

            log.info(" -- assert status code = {}", delFacesResponse.code());
            org.assertj.core.api.Assertions.assertThat(delFacesResponse.code()).isEqualTo(204);

            try{
                log.info(" -- try to send get face ==> and check that 404 is returned by the server");
                BioserverCoreManager.getFace(
                        BioserverCoreManager.face_id,
                        null,
                        null,
                        false);

            } catch (Throwable e) {
                if(e instanceof BiosrvException) {
                    //e.printStackTrace(System.out);
                    log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                    Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                    Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                    runOnTestLink("BioServer-TC-90", true);
                } else {
                    runOnTestLink("BioServer-TC-90", false);
                    Assertions.fail("TEST FAILED !!!", e);
                }
            }

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-90", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(2)
    //@DisplayName("Delete Faces testcase should return error code 401 when wrong api_key value is sent in the request.")
    public void bio_delete_faces_02() {

        log.debug(">> bio_delete_faces_02()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;
        try {
            log.info(" -- create face 1");
            createFaceResourceProcess(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile());
            log.info(" -- create face 2");
            createFaceResourceProcess(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_2).toFile());

            log.info("--------------------");
            log.info("DELETE FACES REQUEST");
            log.info("---------------------");

            log.info(" -- Wrong Api_Key is used ");
            biosrv_api_key = " ";

            BioserverCoreManager.deleteFaces(false);

            runOnTestLink("BioServer-TC-91", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                //restore token
                biosrv_api_key = apiKeyRef;
                runOnTestLink("BioServer-TC-91", true);
            } else {
                runOnTestLink("BioServer-TC-91", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(3)
    //@DisplayName("Delete Faces testcase should return error code 404 when wrong BioSession_Id is sent in the request.")
    public void bio_delete_faces_03() {

        log.debug(">> bio_delete_faces_03()");

        try {
            log.info(" -- create face 1");
            createFaceResourceProcess(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile());
            log.info(" -- create face 2");
            createFaceResourceProcess(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_2).toFile());

            log.info("--------------------");
            log.info("DELETE FACES REQUEST");
            log.info("---------------------");

            log.info(" -- Wrong BioSessionId is used ");
            bio_session_id = wrong_biosession_id;

            BioserverCoreManager.deleteFaces(false);

            runOnTestLink("BioServer-TC-92", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());

                runOnTestLink("BioServer-TC-92", true);
            } else {
                runOnTestLink("BioServer-TC-92", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    //@DisplayName("Delete Faces testcase should return error code 404 when no faces are stored.")
    public void bio_delete_faces_04() {

        log.debug(">> bio_delete_faces_04()");

        try {

            log.info("--------------------");
            log.info("DELETE FACES REQUEST");
            log.info("---------------------");

            BioserverCoreManager.deleteFaces(false);

            runOnTestLink("BioServer-TC-93", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());

                runOnTestLink("BioServer-TC-93", true);
            } else {
                runOnTestLink("BioServer-TC-93", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    //@DisplayName("Delete Faces testcase should return error code 401 when api_key is missing.")
    public void bio_delete_faces_05() {

        log.debug(">> bio_delete_faces_05()");

        try {

            log.info("--------------------");
            log.info("DELETE FACES REQUEST");
            log.info("---------------------");

            BioserverCoreManager.deleteFaces(true);

            runOnTestLink("BioServer-TC-94", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());

                runOnTestLink("BioServer-TC-94", true);
            } else {
                runOnTestLink("BioServer-TC-94", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    private String createFaceResourceProcess(File imageFile) throws IOException, BiosrvException {
        log.info("-----------------------------------");
        log.info("FORMAT AND SEND CREATE FACE REQUEST");
        log.info("------------------------------------");
        Response<Void> response = BioserverCoreManager.createFace(
                imageFile,
                Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
        );

        log.info("--------------------------");
        log.info("CREATE FACE PARSE RESPONSE");
        log.info("--------------------------");

        log.info(" -- retrieve face_id in the header response ");
        String resId = BioserverCoreManager.readheadersResponseAndParseId(response);
        log.info("face_id: "+BioserverCoreManager.face_id);

        return resId;
    }
}
