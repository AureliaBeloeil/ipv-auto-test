package com.bioserver.test.replaymetadata;

import com.bioserver.data.LivenessResponse;
import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import retrofit2.Response;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Base64;

import static com.bioserver.test.core.BioserverCoreManager.*;
import static com.bioserver.utils.ConstantTestEnvironment.biosrv_api_key;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PostReplayMetadataTest extends BioServerBaseTest {

    //Replay liveness from mobile challenge meta data

    @Test
    @Order(1)
    //@DisplayName("Test that post replay metadata​ testcase should return success when multipart form data is used in the request.")
    public void bio_replay_metadata_01() {

        log.debug(">> bio_replay_metadata_01()");

        try {
            log.info("----------------------------------");
            log.info("PROCESS INIT LIVENESS PARAMETERS");
            log.info("----------------------------------");

            replayLivenessProcess();

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND POST REPLAY METADATA REQUEST");
            log.info("--------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_METADATA).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_MASTER_SECRET).toFile();
            // get device info file
            File deviceInfoRequest = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + DEVICE_INFO).toFile();

            Response<Void> response = BioserverCoreManager.postReplayMetadata(encMetadata, encMasterSecret, deviceInfoRequest, true);

            log.info("-----------------------------------------");
            log.info("POST REPLAY METADATA RESPONSE ASSERTIONS");
            log.info("-----------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(202);


            runOnTestLink("BioServer-TC-171", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-171", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(2)
    //@DisplayName("Test that replay metadata​ testcase should return error code 401 when wrong api_key value is sent in the request.")
    public void bio_replay_metadata_02() {

        log.debug(">> bio_replay_metadata_02()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND POST REPLAY METADATA REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- wrong api_key is used ");
            biosrv_api_key = "";
            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_METADATA).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_MASTER_SECRET).toFile();
            // get device info file
            File deviceInfoRequest = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + DEVICE_INFO).toFile();

            BioserverCoreManager.postReplayMetadata(encMetadata, encMasterSecret, deviceInfoRequest, true);

            runOnTestLink("BioServer-TC-172", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info("-----------------------------------------");
                log.info("POST REPLAY METADATA RESPONSE ASSERTIONS");
                log.info("-----------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                runOnTestLink("BioServer-TC-172", true);
                //restore api_key
                biosrv_api_key = apiKeyRef;
            } else {
                runOnTestLink("BioServer-TC-172", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(3)
    //@DisplayName("Test that replay metadata​ testcase should return error code 404 when wrong BioSessionId value is sent in the request.")
    public void bio_replay_metadata_03() {

        log.debug(">> bio_replay_metadata_03()");


        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND POST REPLAY METADATA REQUEST");
            log.info("--------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_METADATA).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_MASTER_SECRET).toFile();
            // get device info file
            File deviceInfoRequest = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + DEVICE_INFO).toFile();

            BioserverCoreManager.postReplayMetadata(encMetadata, encMasterSecret, deviceInfoRequest, true);

            runOnTestLink("BioServer-TC-174", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info("-----------------------------------------");
                log.info("POST REPLAY METADATA RESPONSE ASSERTIONS");
                log.info("-----------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-174", true);

            } else {
                runOnTestLink("BioServer-TC-174", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    //@DisplayName("Test that replay metadata​ testcase should return error code 400 when wrong encryptedMetadata value is sent in the request.")
    public void bio_replay_metadata_04() {

        log.debug(">> bio_replay_metadata_04()");

        try {

            log.info("----------------------------------");
            log.info("PROCESS INIT LIVENESS PARAMETERS");
            log.info("----------------------------------");

            replayLivenessProcess();

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND POST REPLAY METADATA REQUEST");
            log.info("--------------------------------------------");

            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_MASTER_SECRET).toFile();
            // get device info file
            File deviceInfoRequest = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + DEVICE_INFO).toFile();

            BioserverCoreManager.postReplayMetadata(encMasterSecret, encMasterSecret, deviceInfoRequest, true);

            runOnTestLink("BioServer-TC-175", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info("-----------------------------------------");
                log.info("POST REPLAY METADATA RESPONSE ASSERTIONS");
                log.info("-----------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-175", true);

            } else {
                runOnTestLink("BioServer-TC-175", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(5)
    //@DisplayName("Test that replay metadata​ testcase should return error code 400 when wrong encryptedMasterSecret value is sent in the request.")
    public void bio_replay_metadata_05() {

        log.debug(">> bio_replay_metadata_05()");

        try {

            log.info("----------------------------------");
            log.info("PROCESS INIT LIVENESS PARAMETERS");
            log.info("----------------------------------");

            replayLivenessProcess();

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND POST REPLAY METADATA REQUEST");
            log.info("--------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_METADATA).toFile();

            // get device info file
            File deviceInfoRequest = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + DEVICE_INFO).toFile();

            BioserverCoreManager.postReplayMetadata(encMetadata, encMetadata, deviceInfoRequest, true);

            runOnTestLink("BioServer-TC-176", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info("-----------------------------------------");
                log.info("POST REPLAY METADATA RESPONSE ASSERTIONS");
                log.info("-----------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-176", true);

            } else {
                runOnTestLink("BioServer-TC-176", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(6)
    //@DisplayName("Test that replay metadata​ testcase should return error code 400 when no encryptedMetaData value is sent in the request.")
    public void bio_replay_metadata_10() {

        log.debug(">> bio_replay_metadata_10()");

        try {

            log.info("----------------------------------");
            log.info("PROCESS INIT LIVENESS PARAMETERS");
            log.info("----------------------------------");

            replayLivenessProcess();

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND POST REPLAY METADATA REQUEST");
            log.info("--------------------------------------------");

            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_MASTER_SECRET).toFile();
            // get device info file
            File deviceInfoRequest = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + DEVICE_INFO).toFile();

            BioserverCoreManager.postReplayMetadata(null, encMasterSecret, deviceInfoRequest, true);

            runOnTestLink("BioServer-TC-181", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info("-----------------------------------------");
                log.info("POST REPLAY METADATA RESPONSE ASSERTIONS");
                log.info("-----------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-181", true);

            } else {
                runOnTestLink("BioServer-TC-181", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(7)
    //@DisplayName("Test that replay metadata​ testcase should return error code 400 when no encryptedMasterSecret value is sent in the request.")
    public void bio_replay_metadata_11() {

        log.debug(">> bio_replay_metadata_11()");

        try {

            log.info("----------------------------------");
            log.info("PROCESS INIT LIVENESS PARAMETERS");
            log.info("----------------------------------");

            replayLivenessProcess();

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND POST REPLAY METADATA REQUEST");
            log.info("--------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_METADATA).toFile();
            //get encryptedMasterSecret file
            //File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_MASTER_SECRET).toFile();
            // get device info file
            File deviceInfoRequest = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + DEVICE_INFO).toFile();

            BioserverCoreManager.postReplayMetadata(encMetadata, null, deviceInfoRequest, true);

            runOnTestLink("BioServer-TC-182", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info("-----------------------------------------");
                log.info("POST REPLAY METADATA RESPONSE ASSERTIONS");
                log.info("-----------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-182", true);

            } else {
                runOnTestLink("BioServer-TC-182", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(8)
    //@DisplayName("Test that replay metadata​ testcase should return error code 401 when bearer authentication value is sent in the request.")
    public void bio_replay_metadata_16() {

        log.debug(">> bio_replay_metadata_16()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND POST REPLAY METADATA REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- Bearer auth is used ");
            biosrv_api_key = "Bearer ";
            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_METADATA).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_MASTER_SECRET).toFile();
            // get device info file
            File deviceInfoRequest = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + DEVICE_INFO).toFile();

            BioserverCoreManager.postReplayMetadata(encMetadata, encMasterSecret, deviceInfoRequest, false);

            runOnTestLink("BioServer-TC-169", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info("-----------------------------------------");
                log.info("POST REPLAY METADATA RESPONSE ASSERTIONS");
                log.info("-----------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).as("Wrong  Replay Metadata Auth: ").isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).as("Wrong  Replay Metadata Auth: ").isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                //restore api_key
                biosrv_api_key = apiKeyRef;

                runOnTestLink("BioServer-TC-169", true);

            } else {
                runOnTestLink("BioServer-TC-169", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    public void replayLivenessProcess() {

        log.debug(">> replayLivenessProcess()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );
            // create json request
            JSONObject liveness = getLivenessParametersRequest();

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(liveness, false);

            log.info("--------------------------------------");
            log.info("INIT LIVENESS PARAMS: PARSE RESPONSE");
            log.info("--------------------------------------");

            LivenessResponse livenessResponse = response.body();

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            //assert response
            replayLivenessResponseAssertions(liveness, livenessResponse);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            Assertions.fail("test failed", e);
        }
    }

    private JSONObject getLivenessParametersRequest() {
        JSONObject liveness = new JSONObject();
        liveness.put("type", "LIVENESS_PASSIVE");
        liveness.put("timeout", 0);
        liveness.put("securityLevel", "VERY_HIGH");
        liveness.put("nbChallenge", 1);
        liveness.put("useAccurateMatch", true);
        liveness.put("matchThreshold", 2500);
        liveness.put("imageRetrievalDisabled", true);

        return liveness;
    }

    private void replayLivenessResponseAssertions(JSONObject livenessParams, LivenessResponse livenessResponse) {
        log.info("-----------------------------------------");
        log.info("INIT LIVENESS PARAMS RESPONSE ASSERTIONS");
        log.info("-----------------------------------------");

        log.info(" -- assert LivenessResponse.seed = {}", livenessResponse.getSeed());
        Assertions.assertThat(livenessResponse.getSeed()).isEqualTo(42);

        log.info(" -- assert LivenessResponse.id = {}", livenessResponse.getId());
        Assertions.assertThat(isUUID(livenessResponse.getId())).isTrue();

        if(livenessParams.get("timeout") != null) {
            log.info(" -- assert LivenessResponse.timeout = {}", livenessResponse.getTimeout());
            Assertions.assertThat(livenessResponse.getTimeout()).isEqualTo(livenessParams.get("timeout"));
        }

        log.info(" -- assert decoded LivenessResponse.serverRandom = {}", livenessResponse.getServerRandom());
        String decodedRandom = new String(Base64.getDecoder().decode(livenessResponse.getServerRandom()), StandardCharsets.UTF_8);
        log.info("decoded ServerRandom: "+decodedRandom);
        Assertions.assertThat(decodedRandom).isNotNull();

        log.info(" -- assert LivenessResponse.certificates = {}", livenessResponse.getCertificates());
        for(String certificate: livenessResponse.getCertificates()) {

            String decodedCert = new String(Base64.getDecoder().decode(certificate), StandardCharsets.UTF_8);
            log.info(" -- check decoded certificate = {}", decodedCert);

            Assertions.assertThat(decodedCert).isNotNull();
        }

        if(livenessParams.get("timeout") != null) {
            log.info(" -- assert LivenessResponse.type = {}", livenessResponse.getType());
            Assertions.assertThat(livenessResponse.getType()).isEqualTo(livenessParams.get("type"));
        }

        if(livenessParams.get("securityLevel") != null) {
            log.info(" -- assert LivenessResponse.securityLevel = {}", livenessResponse.getSecurityLevel());
            Assertions.assertThat(livenessResponse.getSecurityLevel()).isEqualTo(livenessParams.get("securityLevel"));
        }

        if(livenessParams.get("matchThreshold") != null) {
            log.info(" -- assert LivenessResponse.matchThreshold = {}", livenessResponse.getMatchThreshold());
            Assertions.assertThat(livenessResponse.getMatchThreshold()).isEqualTo(livenessParams.get("matchThreshold"));
        }

        if(livenessParams.get("nbChallenge") != null) {
            log.info(" -- assert LivenessResponse.nbChallenge = {}", livenessResponse.getNbChallenge());
            Assertions.assertThat(livenessResponse.getNbChallenge()).isEqualTo(livenessParams.get("nbChallenge"));
        }

        if(livenessParams.get("useAccurateMatch") != null) {
            log.info(" -- assert LivenessResponse.useAccurateMatch = {}", livenessResponse.isUseAccurateMatch());
            Assertions.assertThat(livenessResponse.isUseAccurateMatch()).isEqualTo(livenessParams.get("useAccurateMatch"));
        }

        log.info(" -- assert LivenessResponse.logLevel = {}", livenessResponse.getLogLevel());
        Assertions.assertThat(livenessResponse.getLogLevel()).isEqualTo("DEBUG");

        log.info(" -- assert LivenessResponse.imageRetrievalDisabled = {}", livenessResponse.isImageRetrievalDisabled());
        Assertions.assertThat(livenessResponse.isImageRetrievalDisabled()).isEqualTo(livenessParams.get("imageRetrievalDisabled"));

        log.info(" ---------------------------");
        log.info(" Verify Signature");
        log.info(" ---------------------------");
        log.info(" -- assert signature  = {}", livenessResponse.getSignature());

        log.info(" decode signature data");
        String dataToVerify = decodeTokenParts(livenessResponse.getSignature());
        log.info(" -- Verify data (header+payload) with the signed data");
        verifySignature(new ByteArrayImpl(Utils.toHexString(dataToVerify)).getBytes(), signatureBytes);
    }
}
