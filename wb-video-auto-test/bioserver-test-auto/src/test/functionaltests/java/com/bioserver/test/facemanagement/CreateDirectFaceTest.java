package com.bioserver.test.facemanagement;

import com.bioserver.data.DirectFaceResponse;
import com.bioserver.utils.*;
import com.bioserver.test.core.BioserverCoreManager;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import retrofit2.Response;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Map;

import static com.bioserver.utils.ConstantTestEnvironment.biosrv_api_key;
import static com.bioserver.utils.ConstantTestEnvironment.correlation_id;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CreateDirectFaceTest extends BioServerBaseTest {


    @Test
    @Order(1)
    //@DisplayName("create direct face should return success")
    public void bio_create_direct_face_01() {

        log.debug(">> bio_create_direct_face_01()");
        log.info("------------------------------------------");
        log.info("FORMAT AND SEND CREATE DIRECT FACE REQUEST");
        log.info("------------------------------------------");
        try {
            Response<DirectFaceResponse> response = BioserverCoreManager.createDirectFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + "direct_face.json").toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + "image_face_1.jpg").toFile()
            );

            log.info("---------------------------------");
            log.info("read json file: direct_face.json");
            log.info("----------------------------------");

            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_DIRECT_FACE_1).toFile());
            String friendlyName = jsonNode.get("friendlyName").asText();
            String mode = jsonNode.get("mode").asText();
            boolean rotation = jsonNode.get("imageRotationEnabled").asBoolean();
            String imageType = jsonNode.get("imageType").asText();

            log.info("--------------------------------------");
            log.info("CREATE DIRECT FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------------");

            createDirectFaceResponseAssertions(response, friendlyName, mode, rotation, imageType);

            runOnTestLink("BioServer-TC-36", true);
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-36", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(2)
    //@DisplayName("Create Direct Face from image Rest API testcase should return success in case "mode" field  is not sent.")
    public void bio_create_direct_face_02() {

        log.debug(">> bio_create_direct_face_02()");
        log.info("------------------------------------------");
        log.info("FORMAT AND SEND CREATE DIRECT FACE REQUEST");
        log.info("------------------------------------------");
        try {
            JsonObject jsonObject = new JsonObject();
            //jsonObject.addProperty("mode", "F5_4_IDD75");
            jsonObject.addProperty("imageType", "SELFIE");
            jsonObject.addProperty("friendlyName", "Image from a selfie");
            jsonObject.addProperty("signatureDisabled", false);
            jsonObject.addProperty("imageRotationEnabled", true);

            File faceFile = File.createTempFile("face", ".json");
            Files.write(faceFile.toPath(), jsonObject.toString().getBytes());

            Response<DirectFaceResponse> response = BioserverCoreManager.createDirectFace(
                    faceFile,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + "image_face_1.jpg").toFile()
            );

            log.info("---------------------------------");
            log.info("read json file: direct_face.json");
            log.info("----------------------------------");

            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_DIRECT_FACE_1).toFile());
            String friendlyName = jsonNode.get("friendlyName").asText();
            String mode = jsonNode.get("mode").asText();
            boolean rotation = jsonNode.get("imageRotationEnabled").asBoolean();
            String imageType = jsonNode.get("imageType").asText();

            log.info("--------------------------------------");
            log.info("CREATE DIRECT FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------------");

            createDirectFaceResponseAssertions(response, friendlyName, mode, rotation, imageType);

            runOnTestLink("BioServer-TC-37", true);
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-37", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(2)
    //@DisplayName("Create Direct Face from image Rest API testcase should return error code 400 when "imageType" field  is not sent.")
    public void bio_create_direct_face_03() {

        log.debug(">> bio_create_direct_face_03()");
        log.info("------------------------------------------");
        log.info("FORMAT AND SEND CREATE DIRECT FACE REQUEST");
        log.info("------------------------------------------");
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mode", "F5_4_IDD75");
            //jsonObject.addProperty("imageType", "SELFIE");
            jsonObject.addProperty("friendlyName", "Image from a selfie");
            jsonObject.addProperty("signatureDisabled", false);
            jsonObject.addProperty("imageRotationEnabled", true);

            File faceFile = File.createTempFile("face", ".json");
            Files.write(faceFile.toPath(), jsonObject.toString().getBytes());

            Response<DirectFaceResponse> response = BioserverCoreManager.createDirectFace(
                    faceFile,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + "image_face_1.jpg").toFile()
            );

            log.info("---------------------------------");
            log.info("read json file: direct_face.json");
            log.info("----------------------------------");

            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_DIRECT_FACE_1).toFile());
            String friendlyName = jsonNode.get("friendlyName").asText();
            String mode = jsonNode.get("mode").asText();
            boolean rotation = jsonNode.get("imageRotationEnabled").asBoolean();
            String imageType = jsonNode.get("imageType").asText();

            log.info("--------------------------------------");
            log.info("CREATE DIRECT FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------------");

            createDirectFaceResponseAssertions(response, friendlyName, mode, rotation, imageType);

            runOnTestLink("BioServer-TC-38", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-38", true);
            } else {
                runOnTestLink("BioServer-TC-38", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    //@DisplayName("Create Direct Face from image Rest API testcase should return success even if "friendlyName" field  is not sent.")
    public void bio_create_direct_face_04() {

        log.debug(">> bio_create_direct_face_04()");
        log.info("------------------------------------------");
        log.info("FORMAT AND SEND CREATE DIRECT FACE REQUEST");
        log.info("------------------------------------------");
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mode", "F5_4_IDD75");
            jsonObject.addProperty("imageType", "SELFIE");
            //jsonObject.addProperty("friendlyName", "Image from a selfie");
            jsonObject.addProperty("signatureDisabled", false);
            jsonObject.addProperty("imageRotationEnabled", true);

            File faceFile = File.createTempFile("face", ".json");
            Files.write(faceFile.toPath(), jsonObject.toString().getBytes());

            Response<DirectFaceResponse> response = BioserverCoreManager.createDirectFace(
                    faceFile,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + "image_face_1.jpg").toFile()
            );

            log.info("---------------------------------");
            log.info("read json file: direct_face.json");
            log.info("----------------------------------");

            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_DIRECT_FACE_1).toFile());
            String friendlyName = jsonNode.get("friendlyName").asText();
            String mode = jsonNode.get("mode").asText();
            boolean rotation = jsonNode.get("imageRotationEnabled").asBoolean();
            String imageType = jsonNode.get("imageType").asText();

            log.info("--------------------------------------");
            log.info("CREATE DIRECT FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------------");

            createDirectFaceResponseAssertions(response, null, mode, rotation, imageType);

            runOnTestLink("BioServer-TC-37", true);
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-37", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(4)
    //@DisplayName("Create Direct Face from image Rest API testcase should return success even if "signatureDisabled" field  is not sent.")
    public void bio_create_direct_face_05() {

        log.debug(">> bio_create_direct_face_05()");
        log.info("------------------------------------------");
        log.info("FORMAT AND SEND CREATE DIRECT FACE REQUEST");
        log.info("------------------------------------------");
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mode", "F5_4_IDD75");
            jsonObject.addProperty("imageType", "SELFIE");
            jsonObject.addProperty("friendlyName", "Image from a selfie");
            //jsonObject.addProperty("signatureDisabled", false);
            jsonObject.addProperty("imageRotationEnabled", true);

            File faceFile = File.createTempFile("face", ".json");
            Files.write(faceFile.toPath(), jsonObject.toString().getBytes());

            Response<DirectFaceResponse> response = BioserverCoreManager.createDirectFace(
                    faceFile,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + "image_face_1.jpg").toFile()
            );

            log.info("---------------------------------");
            log.info("read json file: direct_face.json");
            log.info("----------------------------------");

            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_DIRECT_FACE_1).toFile());
            String friendlyName = jsonNode.get("friendlyName").asText();
            String mode = jsonNode.get("mode").asText();
            boolean rotation = jsonNode.get("imageRotationEnabled").asBoolean();
            String imageType = jsonNode.get("imageType").asText();

            log.info("--------------------------------------");
            log.info("CREATE DIRECT FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------------");

            createDirectFaceResponseAssertions(response, friendlyName, mode, rotation, imageType);

            runOnTestLink("BioServer-TC-40", true);
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-40", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(6)
    //@DisplayName("Create Direct Face from image Rest API testcase should return success even if "imageRotationEnabled" field  is not sent.")
    public void bio_create_direct_face_06() {

        log.debug(">> bio_create_direct_face_06()");
        log.info("------------------------------------------");
        log.info("FORMAT AND SEND CREATE DIRECT FACE REQUEST");
        log.info("------------------------------------------");
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mode", "F5_4_IDD75");
            jsonObject.addProperty("imageType", "SELFIE");
            jsonObject.addProperty("friendlyName", "Image from a selfie");
            jsonObject.addProperty("signatureDisabled", false);
            //jsonObject.addProperty("imageRotationEnabled", true);

            File faceFile = File.createTempFile("face", ".json");
            Files.write(faceFile.toPath(), jsonObject.toString().getBytes());

            Response<DirectFaceResponse> response = BioserverCoreManager.createDirectFace(
                    faceFile,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + "image_face_1.jpg").toFile()
            );

            log.info("---------------------------------");
            log.info("read json file: direct_face.json");
            log.info("----------------------------------");

            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_DIRECT_FACE_1).toFile());
            String friendlyName = jsonNode.get("friendlyName").asText();
            String mode = jsonNode.get("mode").asText();
            boolean rotation = false; //jsonNode.get("imageRotationEnabled").asBoolean();
            String imageType = jsonNode.get("imageType").asText();

            log.info("--------------------------------------");
            log.info("CREATE DIRECT FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------------");

            createDirectFaceResponseAssertions(response, friendlyName, mode, rotation, imageType);

            runOnTestLink("BioServer-TC-41", true);
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-41", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(7)
    //@DisplayName("Create Direct Face from image Rest API testcase should return success even if "correlationId" field  is not sent.")
    public void bio_create_direct_face_07() {

        log.debug(">> bio_create_direct_face_07()");
        log.info("------------------------------------------");
        log.info("FORMAT AND SEND CREATE DIRECT FACE REQUEST");
        log.info("------------------------------------------");
        String correlationRef = correlation_id;
        try {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mode", "F5_4_IDD75");
            jsonObject.addProperty("imageType", "SELFIE");
            jsonObject.addProperty("friendlyName", "Image from a selfie");
            jsonObject.addProperty("signatureDisabled", false);
            jsonObject.addProperty("imageRotationEnabled", true);

            File faceFile = File.createTempFile("face", ".json");
            Files.write(faceFile.toPath(), jsonObject.toString().getBytes());

            //RESET correlation_id
            correlation_id = null;

            Response<DirectFaceResponse> response = BioserverCoreManager.createDirectFace(
                    faceFile,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + "image_face_1.jpg").toFile()
            );

            log.info("---------------------------------");
            log.info("read json file: direct_face.json");
            log.info("----------------------------------");

            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_DIRECT_FACE_1).toFile());
            String friendlyName = jsonNode.get("friendlyName").asText();
            String mode = jsonNode.get("mode").asText();
            boolean rotation = jsonNode.get("imageRotationEnabled").asBoolean();
            String imageType = jsonNode.get("imageType").asText();

            log.info("--------------------------------------");
            log.info("CREATE DIRECT FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------------");

            createDirectFaceResponseAssertions(response, friendlyName, mode, rotation, imageType);

            runOnTestLink("BioServer-TC-42", true);

            correlation_id = correlationRef;
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-42", false);
            correlation_id = correlationRef;
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(8)
    //@DisplayName("Create Direct Face from image Rest API testcase should return error code 401 when wrong api_key value is sent in the request.")
    public void bio_create_direct_face_08() {

        log.debug(">> bio_create_direct_face_08()");
        log.info("------------------------------------------");
        log.info("FORMAT AND SEND CREATE DIRECT FACE REQUEST");
        log.info("------------------------------------------");

        String apiKeyRef = biosrv_api_key;
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mode", "F5_4_IDD75");
            jsonObject.addProperty("imageType", "SELFIE");
            jsonObject.addProperty("friendlyName", "Image from a selfie");
            jsonObject.addProperty("signatureDisabled", false);
            jsonObject.addProperty("imageRotationEnabled", true);

            File faceFile = File.createTempFile("face", ".json");
            Files.write(faceFile.toPath(), jsonObject.toString().getBytes());

            //Send wrong apiKeyRef
            biosrv_api_key = "";

            Response<DirectFaceResponse> response = BioserverCoreManager.createDirectFace(
                    faceFile,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + "image_face_1.jpg").toFile()
            );

            log.info("---------------------------------");
            log.info("read json file: direct_face.json");
            log.info("----------------------------------");

            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_DIRECT_FACE_1).toFile());
            String friendlyName = jsonNode.get("friendlyName").asText();
            String mode = jsonNode.get("mode").asText();
            boolean rotation = jsonNode.get("imageRotationEnabled").asBoolean();
            String imageType = jsonNode.get("imageType").asText();

            log.info("--------------------------------------");
            log.info("CREATE DIRECT FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------------");

            createDirectFaceResponseAssertions(response, friendlyName, mode, rotation, imageType);

            runOnTestLink("BioServer-TC-43", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                biosrv_api_key = apiKeyRef;
                runOnTestLink("BioServer-TC-43", true);
            } else {
                runOnTestLink("BioServer-TC-43", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(8)
    //TODO TICKET BIO_SRV-1824
    //@DisplayName("Create Direct Face from image Rest API testcase should return error code 413 when request is too big.")
    public void bio_create_direct_face_18() {

        log.debug(">> bio_create_direct_face_18()");
        log.info("------------------------------------------");
        log.info("FORMAT AND SEND CREATE DIRECT FACE REQUEST");
        log.info("------------------------------------------");

        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mode", "F5_4_IDD75");
            jsonObject.addProperty("imageType", "SELFIE");
            jsonObject.addProperty("friendlyName", "Image from a selfie");
            jsonObject.addProperty("signatureDisabled", false);
            jsonObject.addProperty("imageRotationEnabled", true);

            File faceFile = File.createTempFile("face", ".json");
            Files.write(faceFile.toPath(), jsonObject.toString().getBytes());

            Response<DirectFaceResponse> response = BioserverCoreManager.createDirectFace(
                    faceFile,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + BIG_IMAGE_FACE).toFile()
            );

            log.info("---------------------------------");
            log.info("read json file: direct_face.json");
            log.info("----------------------------------");

            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_DIRECT_FACE_1).toFile());
            String friendlyName = jsonNode.get("friendlyName").asText();
            String mode = jsonNode.get("mode").asText();
            boolean rotation = jsonNode.get("imageRotationEnabled").asBoolean();
            String imageType = jsonNode.get("imageType").asText();

            log.info("--------------------------------------");
            log.info("CREATE DIRECT FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------------");

            createDirectFaceResponseAssertions(response, friendlyName, mode, rotation, imageType);

            runOnTestLink("BioServer-TC-53", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.REQUEST_TOO_BIG_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.REQUEST_TOO_BIG_RESPONSE.name());
                runOnTestLink("BioServer-TC-53", true);
            } else {
                runOnTestLink("BioServer-TC-53", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    /**
     * Assert Create Direct Face response
     * @param response
     * @param friendlyName
     * @param mode
     * @param rotation
     * @param imageType
     */
    private void createDirectFaceResponseAssertions(Response<DirectFaceResponse> response, String friendlyName, String mode, boolean rotation, String imageType) throws IOException {

        log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
        org.assertj.core.api.Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

        log.info(" -- assert id ==> check format  = {}", response.body().getId());

        Assertions.assertThat(isUUID(response.body().getId())).isTrue();

        log.info(" -- assert friendlyName  = {}", friendlyName);
        Assertions.assertThat(response.body().getFriendlyName()).isEqualTo(friendlyName);

        log.info(" -- assert digest ==> SHA-256 digest of the image file  = {}", response.body().getDigest());
        Assertions.assertThat(response.body().getDigest()).isNotNull();
        byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1));
        String digest = computeDigest(imageBytes);
        Assertions.assertThat(response.body().getDigest().toUpperCase()).isEqualTo(digest);


        log.info(" -- assert mode  = {}", mode);
        Assertions.assertThat(response.body().getMode()).isEqualTo(mode);

        log.info(" -- assert imaageType  = {}", imageType);
        Assertions.assertThat(response.body().getImageType()).isEqualTo(imageType);

        log.info("***********************************");
        log.info("QUALITY CHECKING");
        log.info("If we have a face ==> quality > 0");
        log.info(" otherwise ==> quality < 0");
        log.info("***********************************");
        log.info(" -- assert quality  = {}", response.body().getQuality());
        Assertions.assertThat(response.body().getQuality()).isGreaterThan(1);

        log.info(" -- assert landmarks  = {}", response.body().getLandmarks());
        Object eyes = ((LinkedTreeMap)response.body().getLandmarks()).get("eyes");

        landmarksAssertionsProcess((Map) eyes, false);


        log.info(" -- assert imageRotationEnabled  = {}", rotation);
        Assertions.assertThat(response.body().isImageRotationEnabled()).isEqualTo(rotation);

        log.info(" -- assert template ==> base64 decoding  = {}", response.body().getTemplate());
        byte[] decodedTemplate = Base64.getDecoder().decode(response.body().getTemplate());
        log.info(" -- decoded template  = {}", new String(decodedTemplate, StandardCharsets.UTF_8));
        Assertions.assertThat(decodedTemplate).isNotNull();

        log.info(" -- assert templateVersion  = {}", response.body().getTemplateVersion());
        Assertions.assertThat(response.body().getTemplateVersion()).isNotNull();

        log.info(" ---------------------------");
        log.info(" Check created date format");
        log.info(" ---------------------------");
        log.info(" -- assert created  = {}", response.body().getCreated());

        dateFormatCheckingProcess(response.body().getCreated());

        log.info(" ---------------------------");
        log.info(" Verify Signature");
        log.info(" ---------------------------");
        log.info(" -- assert signature  = {}", response.body().getSignature());
        log.info(" -- decode signature data");
        String dataToVerify = decodeTokenParts(response.body().getSignature());
        log.info(" -- Verify  data (header+payload) with the signed data");
        verifySignature(new ByteArrayImpl(Utils.toHexString(dataToVerify)).getBytes(), signatureBytes);
    }
}
