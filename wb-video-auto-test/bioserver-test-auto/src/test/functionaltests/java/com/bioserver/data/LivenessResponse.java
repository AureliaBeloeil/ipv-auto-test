package com.bioserver.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LivenessResponse {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("seed")
    @Expose
    private int seed;

    @SerializedName("serverRandom")
    @Expose
    private String serverRandom;

    @SerializedName("certificates")
    @Expose
    private String[] certificates;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("timeout")
    @Expose
    private int timeout;

    @SerializedName("securityLevel")
    @Expose
    private String securityLevel;

    @SerializedName("nbChallenge")
    @Expose
    private int nbChallenge;

    @SerializedName("useAccurateMatch")
    @Expose
    private boolean useAccurateMatch;

    @SerializedName("matchThreshold")
    @Expose
    private Integer matchThreshold;

    @SerializedName("logLevel")
    @Expose
    private String logLevel;

    @SerializedName("imageRetrievalDisabled")
    @Expose
    private boolean imageRetrievalDisabled;

    @SerializedName("signature")
    @Expose
    private String signature;

    public int getSeed() {
        return seed;
    }

    public String getServerRandom() {
        return serverRandom;
    }

    public String[] getCertificates() {
        return certificates;
    }

    public String getType() {
        return type;
    }

    public int getTimeout() {
        return timeout;
    }

    public String getSecurityLevel() {
        return securityLevel;
    }

    public int getNbChallenge() {
        return nbChallenge;
    }

    public boolean isUseAccurateMatch() {
        return useAccurateMatch;
    }

    public Integer getMatchThreshold() {
        return matchThreshold;
    }

    public String getSignature() {
        return signature;
    }

    public String getId() {
        return id;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public boolean isImageRetrievalDisabled() {
        return imageRetrievalDisabled;
    }
}
