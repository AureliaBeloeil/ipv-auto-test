package com.bioserver.test.replayliveness;

import com.bioserver.data.*;
import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.*;
import retrofit2.Response;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

import static com.bioserver.test.core.BioserverCoreManager.*;
import static com.bioserver.test.core.BioserverCoreManager.face_id;
import static com.bioserver.utils.ConstantTestEnvironment.biosrv_api_key;
import static com.bioserver.utils.ConstantTestEnvironment.correlation_id;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class InitLivenessParametersTest extends BioServerBaseTest {


    @Test
    @Order(1)
    //TODO TICKET BIO_SRV-1746
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when valid request is sent.")
    public void bio_replay_liveness_01() {

        log.debug(">> bio_replay_liveness_01()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);


            // create json request
            JSONObject liveness = getLivenessHighParametersRequest();

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(liveness, false);

            replayLivenessResponseProcessing(liveness, response);

            runOnTestLink("BioServer-TC-149", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-149", false);
            Assertions.fail("test failed", e);
        }
    }


    @Test
    @Order(2)
    //@DisplayName("Test that Init liveness parameters​ testcase should return error code 401 when wrong api_key value is sent in the request.")
    public void bio_replay_liveness_02() {

        log.debug(">> bio_replay_liveness_02()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;
        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info(" -- wrong api_key is used ");
            biosrv_api_key = "";

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();

            BioserverCoreManager.initLivenessParameters(livenessParams, false);

            runOnTestLink("BioServer-TC-151", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                runOnTestLink("BioServer-TC-151", true);
                //restore token
                biosrv_api_key = apiKeyRef;
            } else {
                runOnTestLink("BioServer-TC-151", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(3)
    //@DisplayName("Test that Init liveness parameters testcase should return error code 415 when wrong content type value is sent in the request.")
    public void bio_replay_liveness_03() {

        log.debug(">> bio_replay_liveness_03()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");


            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();

            BioserverCoreManager.initLivenessParameters(livenessParams, true);

            runOnTestLink("BioServer-TC-152", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.UNSUPPORTED_MEDIA_TYPE_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.UNSUPPORTED_MEDIA_TYPE_RESPONSE.name());
                runOnTestLink("BioServer-TC-152", true);
            } else {
                runOnTestLink("BioServer-TC-152", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    //@DisplayName("Test that Init liveness parameters​ testcase should return error code 404 when wrong BioSessionId is sent in the request.")
    public void bio_replay_liveness_04() {

        log.debug(">> bio_replay_liveness_04()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");


            log.info(" -- wrong BioSessionId is used ");
            bio_session_id = correlation_id;

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();

            BioserverCoreManager.initLivenessParameters(livenessParams, false);

            runOnTestLink("BioServer-TC-153", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-153", true);
            } else {
                runOnTestLink("BioServer-TC-153", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(5)
    //@DisplayName("Test that Init liveness parameters testcase should return error code 400 when wrong liveness parameter value is sent in the request.")
    public void bio_replay_liveness_05() {

        log.debug(">> bio_replay_liveness_05()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            JSONObject livenessParams = new JSONObject();

            BioserverCoreManager.initLivenessParameters(livenessParams, false);

            runOnTestLink("BioServer-TC-154", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-154", true);
            } else {
                runOnTestLink("BioServer-TC-154", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(6)
    //@DisplayName("Test that Init liveness parameters testcase should return error code 400 when wrong type value is sent in the request.")
    public void bio_replay_liveness_06() {

        log.debug(">> bio_replay_liveness_06()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("type");
            //wrong type is set
            livenessParams.put("type", "type");

            BioserverCoreManager.initLivenessParameters(livenessParams, false);

            runOnTestLink("BioServer-TC-155", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-155", true);
            } else {
                runOnTestLink("BioServer-TC-155", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(7)
    //@DisplayName("Test that Init liveness parameters testcase should return error code 400 when wrong security level value is sent in the request.")
    public void bio_replay_liveness_07() {

        log.debug(">> bio_replay_liveness_07()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("securityLevel");
            //wrong securityLevel is set
            livenessParams.put("securityLevel", "securityLevel");

            BioserverCoreManager.initLivenessParameters(livenessParams, false);

            runOnTestLink("BioServer-TC-156", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-156", true);
            } else {
                runOnTestLink("BioServer-TC-156", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(8)
    //@DisplayName("Test that Init liveness parameters testcase should return error code 400 when no challenge value is sent in the request.")
    public void bio_replay_liveness_08() {

        log.debug(">> bio_replay_liveness_08()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("nbChallenge");
            //wrong securityLevel is set
            livenessParams.put("nbChallenge", "");

            BioserverCoreManager.initLivenessParameters(livenessParams, false);

            runOnTestLink("BioServer-TC-157", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-157", true);
            } else {
                runOnTestLink("BioServer-TC-157", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }


    @Test
    @Order(4)
    //@DisplayName("Test that Init liveness parameters​ testcase should not process matching when use accurate value is set to false in the request.")
    public void bio_replay_liveness_09() {

        log.debug(">> bio_replay_liveness_09()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("useAccurateMatch");
            //use accurate is set to false
            livenessParams.put("useAccurateMatch", false);

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            runOnTestLink("BioServer-TC-158", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-158", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(5)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when match threshold value is set to 0 in the request.")
    public void bio_replay_liveness_10() {

        log.debug(">> bio_replay_liveness_10()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("matchThreshold");
            //use accurate is set to false
            livenessParams.put("matchThreshold", 0);

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            runOnTestLink("BioServer-TC-159", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-159", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(6)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when use accurate value is not sent  in the request.")
    public void bio_replay_liveness_11() {

        log.debug(">> bio_replay_liveness_11()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("useAccurateMatch");

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            runOnTestLink("BioServer-TC-160", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-160", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(7)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when match threshold value is not sent in the request.")
    public void bio_replay_liveness_12() {

        log.debug(">> bio_replay_liveness_12()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("matchThreshold");

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            runOnTestLink("BioServer-TC-161", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-161", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(8)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when type value is set to LIVENESS_MEDIUM in the request.")
    public void bio_replay_liveness_13() {

        log.debug(">> bio_replay_liveness_13()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            JSONObject livenessParams = getLivenessMediumParametersRequest();


            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_METADATA_MEDIUM).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PATH + ENCRYPTED_MASTER_SECRET_MEDIUM).toFile();

            byte[] expectedFace = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH + REPLAY_METADATA_FACE_MEDIUM).toAbsolutePath());


            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-162", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-162", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(14)
    //@DisplayName("Test that Init liveness parameters testcase should return error code 400 when type value is not sent in the request.")
    public void bio_replay_liveness_14() {

        log.debug(">> bio_replay_liveness_14()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("type");

            BioserverCoreManager.initLivenessParameters(livenessParams, false);

            runOnTestLink("BioServer-TC-163", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-163", true);
            } else {
                runOnTestLink("BioServer-TC-163", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(14)
    //@DisplayName("Test that Init liveness parameters testcase should return error code 400 when timeout parameter value is not sent in the request.")
    public void bio_replay_liveness_15() {

        log.debug(">> bio_replay_liveness_14()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("timeout");

            BioserverCoreManager.initLivenessParameters(livenessParams, false);

            runOnTestLink("BioServer-TC-164", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-164", true);
            } else {
                runOnTestLink("BioServer-TC-164", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(14)
    //@DisplayName("Test that Init liveness parameters testcase should return error code 400 when security level value is not sent in the request.")
    public void bio_replay_liveness_16() {

        log.debug(">> bio_replay_liveness_16()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("securityLevel");

            BioserverCoreManager.initLivenessParameters(livenessParams, false);

            runOnTestLink("BioServer-TC-165", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-165", true);
            } else {
                runOnTestLink("BioServer-TC-165", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(14)
    //@DisplayName("Test that Init liveness parameters testcase should return error code 400 when nbChallenge value is not sent in the request.")
    public void bio_replay_liveness_17() {

        log.debug(">> bio_replay_liveness_17()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            livenessParams.remove("nbChallenge");

            BioserverCoreManager.initLivenessParameters(livenessParams, false);

            runOnTestLink("BioServer-TC-166", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-166", true);
            } else {
                runOnTestLink("BioServer-TC-166", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(9)
    //TODO TICKET BIO_SRV-1818
    //@DisplayName("Test that Init liveness parameters​ testcase should return error code 405 when  request is not allowed.")
    public void bio_replay_liveness_18() {

        log.debug(">> bio_replay_liveness_18()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            BioserverCoreManager.initLivenessParameters2();

            runOnTestLink("BioServer-TC-167", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_ALLOWED_METHOD_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_ALLOWED_METHOD_RESPONSE.name());
                runOnTestLink("BioServer-TC-167", true);
            } else {
                runOnTestLink("BioServer-TC-167", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(10)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when type value is set to LIVENESS_HIGH.")
    public void bio_replay_liveness_20() {

        log.debug(">> bio_replay_liveness_20()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);


            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            log.info(" -- replay liveness => type: LIVENESS_HIGH, securityLevel: HIGH");
            livenessParams.remove("type");
            livenessParams.put("type", "LIVENESS_HIGH");
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "HIGH");


            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            runOnTestLink("BioServer-TC-219", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-219", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(11)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when type value is set to LIVENESS_PASSIVE and securityLevel is set to HIGH")
    public void bio_replay_liveness_21() {

        log.debug(">> bio_replay_liveness_21()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_PASSIVE, securityLevel: HIGH");
            JSONObject livenessParams = getLivenessPassiveParametersRequest();
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "HIGH");

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_METADATA_PASSIVE_HIGH).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_MASTER_SECRET_PASSIVE_HIGH).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + REPLAY_METADATA_FACE_PASSIVE_HIGH).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-220", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-220", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(12)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when type value is set to NO_LIVENESS and securityLevel is set to HIGH")
    public void bio_replay_liveness_22() {

        log.debug(">> bio_replay_liveness_22()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            log.info(" -- replay liveness => type: NO_LIVENESS, securityLevel: HIGH");
            livenessParams.remove("type");
            livenessParams.put("type", "NO_LIVENESS");
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "HIGH");


            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            log.info("--------------------------------------");
            log.info("INIT LIVENESS PARAMS: PARSE RESPONSE");
            log.info("--------------------------------------");

            LivenessResponse livenessResponse = response.body();

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(200);

            replayLivenessResponseAssertions(livenessParams, livenessResponse);

            runOnTestLink("BioServer-TC-221", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-221", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(13)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when securityLevel value is set to Low and type is set to LIVENESS_HIGH.")
    public void bio_replay_liveness_23() {

        log.debug(">> bio_replay_liveness_23()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            log.info(" -- replay liveness => type: LIVENESS_HIGH, securityLevel: LOW");
            livenessParams.remove("type");
            livenessParams.put("type", "LIVENESS_HIGH");
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "LOW");


            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            runOnTestLink("BioServer-TC-222", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-222", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(14)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when securityLevel value is set to Medium and type is set to HIGH.")
    public void bio_replay_liveness_24() {

        log.debug(">> bio_replay_liveness_23()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            log.info(" -- replay liveness => type: HIGH, securityLevel: MEDIUM");
            livenessParams.remove("type");
            livenessParams.put("type", "HIGH");
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "MEDIUM");


            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            runOnTestLink("BioServer-TC-223", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-223", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(15)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when securityLevel value is set to High and type is set to LIVENESS_MEDIUM.")
    public void bio_replay_liveness_25() {

        log.debug(">> bio_replay_liveness_25()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            log.info(" -- replay liveness => type: LIVENESS_MEDIUM, securityLevel: High");
            livenessParams.remove("type");
            livenessParams.put("type", "LIVENESS_MEDIUM");
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "HIGH");


            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            runOnTestLink("BioServer-TC-224", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-224", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(16)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when securityLevel value is set to VeryHigh and type to LIVENESS_HIGH")
    public void bio_replay_liveness_26() {

        log.debug(">> bio_replay_liveness_26()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            log.info(" -- replay liveness => type: LIVENESS_HIGH, securityLevel: VERY_HIGH");
            livenessParams.remove("type");
            livenessParams.put("type", "LIVENESS_HIGH");
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "VERY_HIGH");


            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            runOnTestLink("BioServer-TC-225", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-225", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(17)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when securityLevel value is set to VeryHigh2 and type to LIVENESS_HIGH")
    public void bio_replay_liveness_27() {

        log.debug(">> bio_replay_liveness_27()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            log.info(" -- replay liveness => type: LIVENESS_HIGH, securityLevel: VERY_HIGH2");
            livenessParams.remove("type");
            livenessParams.put("type", "LIVENESS_HIGH");
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "VERY_HIGH2");


            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            runOnTestLink("BioServer-TC-226", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-226", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(18)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when securityLevel value is set to VeryHigh3 and type set to LIVENESS_HIGH.")
    public void bio_replay_liveness_28() {

        log.debug(">> bio_replay_liveness_28()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            log.info(" -- replay liveness => type: LIVENESS_HIGH, securityLevel: VERY_HIGH3");
            livenessParams.remove("type");
            livenessParams.put("type", "LIVENESS_HIGH");
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "VERY_HIGH3");


            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            runOnTestLink("BioServer-TC-227", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-227", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(19)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when securityLevel: VeryHigh4, type: LIVENESS_HIGH.")
    public void bio_replay_liveness_29() {

        log.debug(">> bio_replay_liveness_29()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            log.info(" -- replay liveness => type: LIVENESS_HIGH, securityLevel: VERY_HIGH4");
            livenessParams.remove("type");
            livenessParams.put("type", "LIVENESS_HIGH");
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "VERY_HIGH4");


            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            runOnTestLink("BioServer-TC-228", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-228", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(20)
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when securityLevel: VeryHigh5, type: LIVENESS_HIGH.")
    public void bio_replay_liveness_30() {

        log.debug(">> bio_replay_liveness_30()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            log.info(" -- replay liveness => type: LIVENESS_HIGH, securityLevel: VERY_HIGH5");
            livenessParams.remove("type");
            livenessParams.put("type", "LIVENESS_HIGH");
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "VERY_HIGH5");


            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            runOnTestLink("BioServer-TC-229", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-229", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(21)
    //TODO A RAJOUTER DANS TESTLINK
    //@DisplayName("Test that Init liveness parameters​ testcase should return success when imageRetrievalDisabled is set to true")
    public void bio_replay_liveness_31() {

        log.debug(">> bio_replay_liveness_30()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            // create json request
            JSONObject livenessParams = getLivenessHighParametersRequest();
            log.info(" -- replay liveness => imageRetrievalDisabled: true");
            livenessParams.remove("imageRetrievalDisabled");
            livenessParams.put("imageRetrievalDisabled", true);


            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            runOnTestLink("BioServer-TC-253", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-253", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(22)
    //TODO A RAJOUTER DANS TESTLINK
    //@DisplayName("Test that init liveness + replay metadata testcase should return success when type value is set to LIVENESS_PASSIVE and securityLevel is set to LOW")
    public void bio_replay_liveness_32() {

        log.debug(">> bio_replay_liveness_32()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_PASSIVE, securityLevel: LOW");
            JSONObject livenessParams = getLivenessPassiveParametersRequest();
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "LOW");

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_METADATA_PASSIVE_LOW).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_MASTER_SECRET_PASSIVE_LOW).toFile();

            //FileInputStream fis = new FileInputStream(ConstantTestEnvironment.KEYS_PASSIVE_PATH +"replay_metadata_face_passive_low.txt");
            //byte[] expectedFace = IOUtils.toByteArray(fis);
            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + REPLAY_METADATA_FACE_PASSIVE_LOW).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-254", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-254", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(23)
    //TODO A RAJOUTER DANS TESTLINK
    //@DisplayName("Test that init liveness + replay metadata testcase should return success when type value is set to LIVENESS_PASSIVE and securityLevel is set to MEDIUM")
    public void bio_replay_liveness_33() {

        log.debug(">> bio_replay_liveness_33()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_PASSIVE, securityLevel: MEDIUM");
            JSONObject livenessParams = getLivenessPassiveParametersRequest();
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "MEDIUM");

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_METADATA_PASSIVE_MEDIUM).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_MASTER_SECRET_PASSIVE_MEDIUM).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + REPLAY_METADATA_FACE_PASSIVE_MEDIUM).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-255", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-255", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(24)
    //TODO A RAJOUTER DANS TESTLINK
    //@DisplayName("Test that init liveness + replay metadata testcase should return success when type value is set to LIVENESS_PASSIVE and securityLevel is set to VERY_HIGH")
    public void bio_replay_liveness_34() {

        log.debug(">> bio_replay_liveness_34()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND INIT LIVENESS PARAMS REQUEST");
            log.info("--------------------------------------------");

            // create json request
            log.info(" -- replay liveness => type: LIVENESS_PASSIVE, securityLevel: VERY_HIGH");
            JSONObject livenessParams = getLivenessPassiveParametersRequest();
            livenessParams.remove("securityLevel");
            livenessParams.put("securityLevel", "VERY_HIGH");

            Response<LivenessResponse> response = BioserverCoreManager.initLivenessParameters(livenessParams, false);

            replayLivenessResponseProcessing(livenessParams, response);

            log.info("----------------------------------------------------");
            log.info("SEND POST_REPLAY_METADATA AND GET_CHALLENGE_RESPONSE");
            log.info("----------------------------------------------------");

            //get encryptedMetaData file
            File encMetadata = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_METADATA_PASSIVE_VERYHIGH).toFile();
            //get encryptedMasterSecret file
            File encMasterSecret = Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + ENCRYPTED_MASTER_SECRET_PASSIVE_VERYHIGH).toFile();

            byte[] expectedFace = Base64.getDecoder().decode(Files.readAllBytes(Paths.get(ConstantTestEnvironment.KEYS_PASSIVE_PATH + REPLAY_METADATA_FACE_PASSIVE_VERYHIGH).toAbsolutePath()));

            postReplayMetadataAndGetChallengeProcess(encMetadata, encMasterSecret, expectedFace, "SUCCESS");

            runOnTestLink("BioServer-TC-256", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-256", false);
            Assertions.fail("test failed", e);
        }
    }

    private void replayLivenessResponseProcessing(JSONObject livenessParams, Response<LivenessResponse> response) {
        log.info("--------------------------------------");
        log.info("INIT LIVENESS PARAMS: PARSE RESPONSE");
        log.info("--------------------------------------");

        LivenessResponse livenessResponse = response.body();

        log.info(" -- assert status code = {}", response.code());
        Assertions.assertThat(response.code()).isEqualTo(200);

        //Assert content type
        log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
        Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

        replayLivenessResponseAssertions(livenessParams, livenessResponse);
    }

    private JSONObject getLivenessHighParametersRequest() {
        JSONObject liveness = new JSONObject();
        liveness.put("type", "HIGH");
        liveness.put("timeout", 0);
        liveness.put("securityLevel", "LOW");
        liveness.put("nbChallenge", 2);
        liveness.put("useAccurateMatch", true);
        liveness.put("matchThreshold", 2500);
        liveness.put("imageRetrievalDisabled", false);

        return liveness;
    }

    private JSONObject getLivenessMediumParametersRequest() {
        JSONObject liveness = new JSONObject();
        liveness.put("type", "MEDIUM");
        liveness.put("timeout", 0);
        liveness.put("securityLevel", "MEDIUM");
        liveness.put("nbChallenge", 1);
        liveness.put("useAccurateMatch", true);
        liveness.put("matchThreshold", 2500);
        liveness.put("imageRetrievalDisabled", false);

        return liveness;
    }

    private JSONObject getLivenessPassiveParametersRequest() {
        JSONObject liveness = new JSONObject();
        liveness.put("type", "LIVENESS_PASSIVE");
        liveness.put("timeout", 0);
        liveness.put("securityLevel", "MEDIUM");
        liveness.put("nbChallenge", 1);
        liveness.put("useAccurateMatch", true);
        liveness.put("matchThreshold", 2500);
        liveness.put("imageRetrievalDisabled", false);

        return liveness;
    }
    private JSONObject getLivenessPassiveParametersRequest2() {
        JSONObject liveness = new JSONObject();
        liveness.put("type", "LIVENESS_PASSIVE");
        liveness.put("timeout", 0);
        liveness.put("securityLevel", "HIGH");
        liveness.put("nbChallenge", 1);
        liveness.put("useAccurateMatch", true);
        liveness.put("matchThreshold", 2500);
        liveness.put("imageRetrievalDisabled", false);

        return liveness;
    }

    private void replayLivenessResponseAssertions(JSONObject livenessParams, LivenessResponse livenessResponse) {
        log.info("-----------------------------------------");
        log.info("INIT LIVENESS PARAMS RESPONSE ASSERTIONS");
        log.info("-----------------------------------------");

        log.info(" -- assert LivenessResponse.seed = {}", livenessResponse.getSeed());
        Assertions.assertThat(livenessResponse.getSeed()).isEqualTo(42);

        log.info(" -- assert LivenessResponse.id = {}", livenessResponse.getId());
        Assertions.assertThat(isUUID(livenessResponse.getId())).isTrue();

        if(livenessParams.get("timeout") != null) {
            log.info(" -- assert LivenessResponse.timeout = {}", livenessResponse.getTimeout());
            Assertions.assertThat(livenessResponse.getTimeout()).isEqualTo(livenessParams.get("timeout"));
        }

        log.info(" -- assert decoded LivenessResponse.serverRandom = {}", livenessResponse.getServerRandom());
        String decodedRandom = new String(Base64.getDecoder().decode(livenessResponse.getServerRandom()), StandardCharsets.UTF_8);
        log.info("decoded ServerRandom: "+decodedRandom);
        Assertions.assertThat(decodedRandom).isNotNull();

        log.info(" -- assert LivenessResponse.certificates = {}", livenessResponse.getCertificates());
        for(String certificate: livenessResponse.getCertificates()) {

            String decodedCert = new String(Base64.getDecoder().decode(certificate), StandardCharsets.UTF_8);
            log.info(" -- check decoded certificate = {}", decodedCert);

            Assertions.assertThat(decodedCert).isNotNull();
        }

        if(livenessParams.get("timeout") != null) {
            log.info(" -- assert LivenessResponse.type = {}", livenessResponse.getType());
            Assertions.assertThat(livenessResponse.getType()).isEqualTo(livenessParams.get("type"));
        }

        if(livenessParams.get("securityLevel") != null) {
            log.info(" -- assert LivenessResponse.securityLevel = {}", livenessResponse.getSecurityLevel());
            Assertions.assertThat(livenessResponse.getSecurityLevel()).isEqualTo(livenessParams.get("securityLevel"));
        }

        if(livenessParams.get("matchThreshold") != null) {
            log.info(" -- assert LivenessResponse.matchThreshold = {}", livenessResponse.getMatchThreshold());
            Assertions.assertThat(livenessResponse.getMatchThreshold()).isEqualTo(livenessParams.get("matchThreshold"));
        }

        if(livenessParams.get("nbChallenge") != null) {
            log.info(" -- assert LivenessResponse.nbChallenge = {}", livenessResponse.getNbChallenge());
            Assertions.assertThat(livenessResponse.getNbChallenge()).isEqualTo(livenessParams.get("nbChallenge"));
        }

        if(livenessParams.get("useAccurateMatch") != null) {
            log.info(" -- assert LivenessResponse.useAccurateMatch = {}", livenessResponse.isUseAccurateMatch());
            Assertions.assertThat(livenessResponse.isUseAccurateMatch()).isEqualTo(livenessParams.get("useAccurateMatch"));
        }

        log.info(" -- assert LivenessResponse.logLevel = {}", livenessResponse.getLogLevel());
        Assertions.assertThat(livenessResponse.getLogLevel()).isEqualTo("DEBUG");

        log.info(" -- assert LivenessResponse.imageRetrievalDisabled = {}", livenessResponse.isImageRetrievalDisabled());
        Assertions.assertThat(livenessResponse.isImageRetrievalDisabled()).isEqualTo(livenessParams.get("imageRetrievalDisabled"));

        log.info(" ---------------------------");
        log.info(" Verify Signature");
        log.info(" ---------------------------");
        log.info(" -- assert signature  = {}", livenessResponse.getSignature());

        log.info(" decode signature data");
        String dataToVerify = decodeTokenParts(livenessResponse.getSignature());
        log.info(" -- Verify data (header+payload) with the signed data");
        verifySignature(new ByteArrayImpl(Utils.toHexString(dataToVerify)).getBytes(), signatureBytes);
    }

    @AfterEach
    public void tearDown() {
        candidateResources = null;
        referenceResources = null;
        bio_session_id = null;
        face_id = null;
    }
}
