package com.bioserver.utils;

public enum LivenessStatusEnum {
    SUCCESS,
    FAILED,
    SPOOF,
    SPOOF_JS,
    ERROR,
    TIMEOUT
}
