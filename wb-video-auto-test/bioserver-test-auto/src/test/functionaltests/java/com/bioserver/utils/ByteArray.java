package com.bioserver.utils;

import com.google.protobuf.ByteString;

public interface ByteArray {

    byte[] getBytes();

    ByteArray copyOfRange(int var1, int var2);

    byte getByte(int var1);

    void setByte(int var1, byte var2);

    void setShort(int var1, short var2);

    boolean isEqual(ByteArray var1);

    int getLength();

    void copyBytes(ByteArray var1, int var2, int var3, int var4);

    void copyBytes(byte[] var1, int var2, int var3, int var4);

    ByteArray appendTLV(short var1, ByteArray var2);

    ByteArray appendTLV(byte var1, short var2);

    ByteArray appendTLV(byte var1, byte var2);

    ByteArray appendTLV(short var1, byte[] var2);

    ByteArray appendTLV(ByteArray var1, ByteArray var2);

    ByteArray appendTLV(byte var1, ByteArray var2);

    ByteArray appendTLV(byte var1, String var2);

    ByteArray appendTLV(short var1, String var2);

    ByteArray appendTLV(byte[] var1, ByteArray var2);

    ByteArray appendTLV(byte var1, byte[] var2);

    ByteArray appendTLV(byte var1, ByteString var2);

    ByteArray appendTLV(ByteArray var1, byte[] var2);

    ByteArray append(ByteArray var1);

    void fill(byte var1);

    ByteArray appendByte(byte var1);

    ByteArray appendBytes(byte[] var1);

    ByteArray appendBytes(byte[] var1, int var2);

    ByteArray append(String var1);

    short getShort(int var1);

    int getInt(int var1);

    String getHexString();

    String getSmallHexString();

    ByteArray clone();

    void clear();

    void appendByteArrayAsLV(ByteArray var1);

    void appendBuffer(byte[] var1, int var2, int var3);

    ByteArray makeXor(ByteArray var1);

    ByteArray bitWiseAnd(ByteArray var1);

    void bitAnd(int var1, byte var2);

    void setBit(int var1, byte var2);

    void resetBit(int var1, byte var2);

    boolean isGreaterThan(ByteArray var1);

    String toAsciiString();

    ByteArray appendTLV(byte[] var1, byte var2);

    ByteString toByteString();
}
