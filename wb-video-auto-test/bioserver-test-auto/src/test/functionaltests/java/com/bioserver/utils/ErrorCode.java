package com.bioserver.utils;

public interface ErrorCode {

    int getNumber();

    /**
     * Returns the key to use when accessing the i18n message resources
     *
     * @return
     */
    String getMessageKey();
}
