package com.bioserver.test.matches;

import com.bioserver.data.CreateFaceJsonParams;
import com.bioserver.data.ReferenceOrCandidate;
import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import retrofit2.Response;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.bioserver.utils.ConstantTestEnvironment.biosrv_api_key;
import static com.bioserver.utils.ConstantTestEnvironment.correlation_id;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DirectMatchTest extends BioServerBaseTest {

    //Return a single match object (see Get matches) computed from a reference face (a previous face JSON) and a candidate face image.
    // The threshold that we do want to use is driven by the expected FAR (False Acceptance Rate)
    //                      FAR	        Matching threshold
    //                     0.0001%	        4500
    //                     0.001%	        4000
    //                     0.01%	        3500
    //                     0.1%	            3000
    //                     1%	            2500
    //    Matching Document /Selfie : recommanded threashold is 2500 or 3000.
    //    Matching Selfie /Selfie : recommanded threashold is 3000, 3500 or higher depending on the use case
    @Test
    @Order(1)
    //@DisplayName("Test that Direct match​ testcase should return a single match object computed from a reference face and a candidate face image when a valid request is sent.")
    public void bio_direct_match_01() {

        log.debug(">> bio_direct_match_01()");

        log.info("---------------------------------------");
        log.info("FORMAT AND SEND DIRECT MATCH REQUEST");
        log.info("---------------------------------------");
        try {

            //get reference json file
            File refJson = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            Response<JsonObject> response = BioserverCoreManager.directMatch(refJson, candImage, SELFIE_IMAGE_TYPE, false, true, true);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, false, true, false);

            runOnTestLink("BioServer-TC-128", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-128", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(2)
    //@DisplayName("Test that Direct match​ testcase should return success when no apiKey value is sent in the request.")
    public void bio_direct_match_02() {

        log.debug(">> bio_direct_match_02()");

        log.info("---------------------------------------");
        log.info("FORMAT AND SEND DIRECT MATCH REQUEST");
        log.info("---------------------------------------");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;

        try {
            log.info(" -- wrong api_key is used ");
            biosrv_api_key = "";

            //get reference json file
            File refJson = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            BioserverCoreManager.directMatch(refJson, candImage, SELFIE_IMAGE_TYPE, false, true, true);

            runOnTestLink("BioServer-TC-129", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                log.info("---------------------------------------");
                log.info("DIRECT MATCH RESPONSE ASSERTIONS");
                log.info("---------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                //restore value
                biosrv_api_key = apiKeyRef;
                runOnTestLink("BioServer-TC-129", true);
            } else {
                runOnTestLink("BioServer-TC-129", false);
                Assertions.fail("test failed", e);
            }
        }
    }

    @Test
    @Order(3)
    //@DisplayName("Test that Direct match​ testcase should return error code 401 when auth bearer is sent in the request.")
    public void bio_direct_match_03() {

        log.debug(">> bio_direct_match_03()");

        log.info("---------------------------------------");
        log.info("FORMAT AND SEND DIRECT MATCH REQUEST");
        log.info("---------------------------------------");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;

        try {
            log.info(" -- Auth bearer is used ");
            biosrv_api_key = "Bearer ";

            //get reference json file
            File refJson = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            BioserverCoreManager.directMatch(refJson, candImage, SELFIE_IMAGE_TYPE, false, true, false);

            runOnTestLink("BioServer-TC-130", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                log.info("---------------------------------------");
                log.info("DIRECT MATCH RESPONSE ASSERTIONS");
                log.info("---------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                //restore value
                biosrv_api_key = apiKeyRef;
                runOnTestLink("BioServer-TC-130", true);
            } else {
                runOnTestLink("BioServer-TC-130", false);
                Assertions.fail("test failed", e);
            }
        }
    }

    @Test
    @Order(4)
    //@DisplayName("Test that Direct match​ testcase should return error code 400 when wrong candidate image value is sent in the request.")
    //TODO Ticket BIO_SRV-1743
    public void bio_direct_match_07() {

        log.debug(">> bio_direct_match_07()");

        log.info("---------------------------------------");
        log.info("FORMAT AND SEND DIRECT MATCH REQUEST");
        log.info("---------------------------------------");

        try {

            //get reference json file
            File refJson = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_1).toFile();

            BioserverCoreManager.directMatch(refJson, candImage, SELFIE_IMAGE_TYPE, false, true, true);

            runOnTestLink("BioServer-TC-134", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                log.info("---------------------------------------");
                log.info("DIRECT MATCH RESPONSE ASSERTIONS");
                log.info("---------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());

                runOnTestLink("BioServer-TC-134", true);
            } else {
                runOnTestLink("BioServer-TC-134", false);
                Assertions.fail("test failed", e);
            }
        }
    }

    @Test
    @Order(5)
    //@DisplayName("Test that Direct match​ testcase should return error code 400 when wrong candidate image type value is sent in the request.")
    public void bio_direct_match_08() {

        log.debug(">> bio_direct_match_08()");

        log.info("---------------------------------------");
        log.info("FORMAT AND SEND DIRECT MATCH REQUEST");
        log.info("---------------------------------------");

        try {

            //get reference json file
            File refJson = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            BioserverCoreManager.directMatch(refJson, candImage, "TEST", false, true, true);

            runOnTestLink("BioServer-TC-135", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                log.info("---------------------------------------");
                log.info("DIRECT MATCH RESPONSE ASSERTIONS");
                log.info("---------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());

                runOnTestLink("BioServer-TC-135", true);
            } else {
                runOnTestLink("BioServer-TC-135", false);
                Assertions.fail("test failed", e);
            }
        }
    }

    @Test
    @Order(6)
    //@DisplayName("Test that Direct match​ testcase should return error code 400 when wrong reference face value is sent in the request.")
    //TODO Ticket BIO_SRV-1743
    public void bio_direct_match_09() {

        log.debug(">> bio_direct_match_09()");

        log.info("---------------------------------------");
        log.info("FORMAT AND SEND DIRECT MATCH REQUEST");
        log.info("---------------------------------------");

        try {

            //get reference json file
            File refJson = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            BioserverCoreManager.directMatch(refJson, candImage, SELFIE_IMAGE_TYPE, false, true, true);

            runOnTestLink("BioServer-TC-136", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                log.info("---------------------------------------");
                log.info("DIRECT MATCH RESPONSE ASSERTIONS");
                log.info("---------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());

                runOnTestLink("BioServer-TC-136", true);
            } else {
                runOnTestLink("BioServer-TC-136", false);
                Assertions.fail("test failed", e);
            }
        }
    }

    @Test
    @Order(7)
    //@DisplayName("Test that Direct match​ testcase should return success when no image rotation value is sent in the request.")
    public void bio_direct_match_10() {

        log.debug(">> bio_direct_match_10()");

        log.info("---------------------------------------");
        log.info("FORMAT AND SEND DIRECT MATCH REQUEST");
        log.info("---------------------------------------");
        try {

            //get reference json file
            File refJson = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            Response<JsonObject> response = BioserverCoreManager.directMatch(refJson, candImage, SELFIE_IMAGE_TYPE, false, false, true);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, false, true, false);

            runOnTestLink("BioServer-TC-137", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-137", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(8)
    //@DisplayName("Test that Direct match​ testcase should return success when no correlationId value is sent in the request.")
    public void bio_direct_match_11() {

        log.debug(">> bio_direct_match_10()");

        log.info("---------------------------------------");
        log.info("FORMAT AND SEND DIRECT MATCH REQUEST");
        log.info("---------------------------------------");

        String correlation = correlation_id;
        try {

            log.info("no correlationId is sent ");
            correlation_id = null;

            //get reference json file
            File refJson = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            Response<JsonObject> response = BioserverCoreManager.directMatch(refJson, candImage, SELFIE_IMAGE_TYPE, false, false, true);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, false, true, false);

            //RESTORE CORRELATION ID
            correlation_id = correlation;

            runOnTestLink("BioServer-TC-138", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-138", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(9)
    //TODO Ticket BIO_SRV-1744
    //@DisplayName("Test that Direct match​ testcase should return error code 400 when wrong correlationId value is sent in the request.")
    public void bio_direct_match_12() {

        log.debug(">> bio_direct_match_12()");

        log.info("---------------------------------------");
        log.info("FORMAT AND SEND DIRECT MATCH REQUEST");
        log.info("---------------------------------------");

        String correlation = correlation_id;
        try {

            log.info("empty correlationId is sent ");
            correlation_id = "";

            //get reference json file
            File refJson = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            BioserverCoreManager.directMatch(refJson, candImage, SELFIE_IMAGE_TYPE, false, false, true);

            //RESTORE CORRELATION ID
            correlation_id = correlation;

            runOnTestLink("BioServer-TC-139", false);
            Assertions.fail("test failed");

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                log.info("---------------------------------------");
                log.info("DIRECT MATCH RESPONSE ASSERTIONS");
                log.info("---------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());

                runOnTestLink("BioServer-TC-139", true);
            } else {
                runOnTestLink("BioServer-TC-139", false);
                Assertions.fail("test failed", e);
            }
        }
    }

    @Test
    @Order(10)
    //@DisplayName("Test that Direct match​ testcase should return error code 413 when request is too big.")
    public void bio_direct_match_18() {

        log.debug(">> bio_direct_match_18()");

        log.info("---------------------------------------");
        log.info("FORMAT AND SEND DIRECT MATCH REQUEST");
        log.info("---------------------------------------");

        try {

            //get reference json file
            File refJson = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + BIG_IMAGE_FACE).toFile();

            BioserverCoreManager.directMatch(refJson, candImage, SELFIE_IMAGE_TYPE, false, false, true);


            runOnTestLink("BioServer-TC-146", false);
            Assertions.fail("test failed");

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                log.info("---------------------------------------");
                log.info("DIRECT MATCH RESPONSE ASSERTIONS");
                log.info("---------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.REQUEST_TOO_BIG_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.REQUEST_TOO_BIG_RESPONSE.name());

                runOnTestLink("BioServer-TC-146", true);
            } else {
                runOnTestLink("BioServer-TC-146", false);
                Assertions.fail("test failed", e);
            }
        }
    }

    @Test
    @Order(11)
    //@DisplayName("Test that Direct match​ testcase should return a success matching score when a candidate face image is rotated 90 degrees.")
    public void bio_direct_match_19() {

        log.debug(">> bio_direct_match_19()");

        log.info("---------------------------------------");
        log.info("FORMAT AND SEND DIRECT MATCH REQUEST");
        log.info("---------------------------------------");
        try {

            //get reference json file
            File refJson = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_6).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6_ROT90).toFile();

            Response<JsonObject> response = BioserverCoreManager.directMatch(refJson, candImage, SELFIE_IMAGE_TYPE, true, true, true);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, true, true, true);

            runOnTestLink("BioServer-TC-250", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-250", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(12)
    //@DisplayName("Test that Direct match​ testcase should return a success matching score when a candidate face image is rotated 180 degrees.")
    public void bio_direct_match_20() {

        log.debug(">> bio_direct_match_20()");

        log.info("---------------------------------------");
        log.info("FORMAT AND SEND DIRECT MATCH REQUEST");
        log.info("---------------------------------------");
        try {

            //get reference json file
            File refJson = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_6).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6_ROT180).toFile();

            Response<JsonObject> response = BioserverCoreManager.directMatch(refJson, candImage, SELFIE_IMAGE_TYPE, true, true, true);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, true, true, true);

            runOnTestLink("BioServer-TC-251", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-251", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(13)
    //@DisplayName("Test that Direct match​ testcase should return a not matching score when a candidate face image is rotated 90 degrees and imageRotationEnabled is set to false.")
    public void bio_direct_match_21() {

        log.debug(">> bio_direct_match_21()");

        log.info("---------------------------------------");
        log.info("FORMAT AND SEND DIRECT MATCH REQUEST");
        log.info("---------------------------------------");
        try {

            //get reference json file
            File refJson = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_6).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6_ROT90).toFile();

            Response<JsonObject> response = BioserverCoreManager.directMatch(refJson, candImage, SELFIE_IMAGE_TYPE, false, true, true);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, false, true, true);

            runOnTestLink("BioServer-TC-252", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-252", false);
            Assertions.fail("test failed", e);
        }
    }

//    @Test
//    @Order(10)
//    //@DisplayName("Test that Direct match​ testcase should return a success matching score when a candidate face image is rotated 90 degrees.")
//    public void bio_direct_match_22() {
//
//    }

    private void matchesResponseAssertionsProcess(JsonArray response, boolean rotation, boolean samePerson, boolean isImageRotated) throws IOException {

        JsonNode responseAsJson = mapper.readTree(response.toString()).get(0);

        //MAP Json file to create face object
        CreateFaceJsonParams faceJsonParams = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

        ReferenceOrCandidate reference = mapper.readValue(responseAsJson.get("reference").toString(), ReferenceOrCandidate.class);
        ReferenceOrCandidate candidate = mapper.readValue(responseAsJson.get("candidate").toString(), ReferenceOrCandidate.class);

        log.info("---------------------------------------");
        log.info("DIRECT IMAGES MATCH RESPONSE ASSERTIONS");
        log.info("----------------------------------------");

        log.info(" --------------------------------------------");
        log.info("   Assert Reference Json & Candidate Image Object");
        log.info(" --------------------------------------------");
        log.info(" -- assert reference.id  = {}", reference.getId());
        Assertions.assertThat(isUUID(reference.getId())).isTrue();
        log.info(" -- assert candidate.id  = {}", candidate.getId());
        Assertions.assertThat(isUUID(candidate.getId())).isTrue();
        log.info(" -- assert friendlyName  = {}", reference.getFriendlyName());
        Assertions.assertThat(reference.getFriendlyName()).isEqualTo(faceJsonParams.getFriendlyName());
        log.info(" -- assert mode  = {}", reference.getMode());
        Assertions.assertThat(reference.getMode()).isEqualTo(faceJsonParams.getMode());
        log.info(" -- assert imageType  = {}", reference.getImageType());
        Assertions.assertThat(reference.getImageType()).isEqualTo(faceJsonParams.getImageType());
        log.info(" -- assert quality  = {}", candidate.getQuality());
        Assertions.assertThat(candidate.getQuality()).isGreaterThan(0);

        log.info(" -- assert candidate.landmarks  = {}", candidate.getLandmarks());
        Object eyes1 = ((LinkedHashMap)candidate.getLandmarks()).get("eyes");

        landmarksAssertionsProcess((Map) eyes1, rotation);

        log.info(" -- assert candidate.imageRotationEnabled  = {}", candidate.isImageRotationEnabled());
        Assertions.assertThat(candidate.isImageRotationEnabled()).isEqualTo(rotation);

        log.info(" -- assert score  = {}", responseAsJson.get("score"));
        log.info(" -----------------------------------------------------------------------");
        log.info(" -- Score Matching Threshold SELFIE/ SELFIE must higher or equal to 3000");
        log.info(" -----------------------------------------------------------------------");
        log.info(" -- Score Matching Threshold DOCUMENT/ SELFIE is 2500 or 3000");
        log.info(" -----------------------------------------------------------");
        log.info(" -----------------------------------------------------------");
        log.info(" -- SELFIE/ SELFIE ==> FAR Threshold must less than  0.0001%");
        log.info(" -----------------------------------------------------------");
        log.info(" -------------------------------------------------------------------");
        log.info(" -- DOCUMENT/ SELFIE ==> FAR Threshold must less or equal to  0.01%");
        log.info(" -------------------------------------------------------------------");

        if(reference.getImageType().equals("SELFIE") && candidate.getImageType().equals("SELFIE") && samePerson) {

            if(isImageRotated && ! rotation) {
                log.info(" --------------------------------------------------------");
                log.info(" -- ONE IMAGE AT LEAST ARE ROTATED && ROTATION FIELD NOT SENT ");
                log.info(" --------------------------------------------------------");
                Assertions.assertThat(responseAsJson.get("score").asInt()).isLessThan(score_reference_ds_1);
                log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
                Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isBetween(far_reference_ds_2, far_ko);
            } else {
                log.info(" -------------------------------");
                log.info(" -- SELFIE/ SELFIE: SAME PERSON ");
                log.info(" -------------------------------");
                org.assertj.core.api.Assertions.assertThat(responseAsJson.get("score").asInt()).isGreaterThan(score_reference_ss);
                log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
                org.assertj.core.api.Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isLessThan(far_reference_ss);
            }

        } else if (reference.getImageType().equals("SELFIE") && candidate.getImageType().equals("SELFIE") && !samePerson) {

            log.info(" -----------------------------------");
            log.info(" -- SELFIE/ SELFIE: NOT SAME PERSON ");
            log.info(" -----------------------------------");
            Assertions.assertThat(responseAsJson.get("score").asInt()).isLessThan(score_reference_ds_1);
            log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
            Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isBetween(far_reference_ds_2, far_ko);
        } else if(reference.getImageType().equals("SELFIE") && candidate.getImageType().equals("ID_DOCUMENT") && samePerson) {
            log.info(" -----------------------------------");
            log.info(" -- SELFIE/ DOCUMENT: SAME PERSON ");
            log.info(" -----------------------------------");

            org.assertj.core.api.Assertions.assertThat(responseAsJson.get("score").asInt()).isGreaterThanOrEqualTo(score_reference_ds_2);
            log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
            org.assertj.core.api.Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isLessThan(far_reference_ds_1);
        } else if(reference.getImageType().equals("SELFIE") && candidate.getImageType().equals("ID_DOCUMENT") && !samePerson) {

            log.info(" -----------------------------------");
            log.info(" -- SELFIE/ DOCUMENT: NOT SAME PERSON ");
            log.info(" -----------------------------------");
            Assertions.assertThat(responseAsJson.get("score").asInt()).isLessThan(score_reference_ds_3);
            log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
            Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isBetween(far_reference_ds_2, far_ko);
        }

        log.info(" -- assert correlationId  = {}", responseAsJson.get("correlationId"));
        if(correlation_id != null && responseAsJson.get("correlationId") != null) {
            Assertions.assertThat(responseAsJson.get("correlationId").asText()).isEqualTo(correlation_id);
        }

        log.info(" ---------------------------");
        log.info(" Check created date format");
        log.info(" ---------------------------");
        dateFormatCheckingProcess(responseAsJson.get("created").asText());

        if(responseAsJson.get("signature") != null) {
            log.info(" ---------------------------");
            log.info(" Verify Signature");
            log.info(" ---------------------------");
            log.info(" -- assert signature  = {}", responseAsJson.get("signature").asText());

            log.info(" decode signature data");
            String dataToVerify = decodeTokenParts(responseAsJson.get("signature").asText());
            log.info(" -- Verify data (header+payload) with the signed data");
            verifySignature(new ByteArrayImpl(Utils.toHexString(dataToVerify)).getBytes(), signatureBytes);
        }
    }
}
