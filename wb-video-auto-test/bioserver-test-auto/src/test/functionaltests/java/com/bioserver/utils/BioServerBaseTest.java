package com.bioserver.utils;

import com.bioserver.data.CreateFaceJsonParams;
import com.bioserver.data.CreateFaceResponse;
import com.bioserver.data.ReferenceOrCandidate;
import com.bioserver.test.core.BioserverCoreManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.cxf.rs.security.jose.jwa.SignatureAlgorithm;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import retrofit2.Response;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.text.ParseException;
import java.util.*;

import static com.bioserver.test.core.BioserverCoreManager.*;
import static com.bioserver.test.core.BioserverCoreManager.face_id;

@Slf4j
public class BioServerBaseTest {
    protected ObjectMapper mapper = new ObjectMapper();
    protected TimeParser timeParser = new TimeParser();
    protected static KeyStore keyStore;
    //protected static byte[] headerBytes;
    //protected static byte[] payloadBytes;
    protected static byte[] signatureBytes;
    protected static String keystore_path = "src/test/keystores/bioserver-response-signer-dev.jceks";
    protected static char[] keystore_password = "pX%w6-r7+Kq#wP>c".toCharArray();
    protected static char[] privateKey_password = "pay+%n&6;%8?PMJR".toCharArray();
    protected static String keystore_alias = "bioserver-response-signer-certificate";
    protected static int score_reference_ss = 3000;
    protected static int score_reference_ss_2 = 4500;
    protected static int score_reference_ds_1 = 2500;
    protected static int score_reference_ds_2 = 3000;
    protected static int score_reference_ds_3 = 1000;
    protected static double far_reference_ss = Double.parseDouble("0.001");
    protected static double far_reference_ss_2 = Double.parseDouble("0.000001");
    protected static double far_reference_ds_1 = Double.parseDouble("0.001");
    protected static double far_reference_ds_2 = Double.parseDouble("0.01");
    protected static double far_ko = Double.parseDouble("100");
    public static String BIG_IMAGE_FACE = "big_photo.bmp";

    //METADATA
    public static String REPLAY_METADATA_FACE = "replay_metadata_face.jpg";
    public static String REPLAY_METADATA_FACE_MEDIUM = "replay_metadata_face_medium.jpg";
    public static String REPLAY_METADATA_FACE_PASSIVE = "replay_metadata_face_passive.jpg";
    public static String ENCRYPTED_METADATA = "encryptedMetaData.enc";
    public static String ENCRYPTED_MASTER_SECRET = "encryptedMasterSecret.enc";

    // LIVENESS PASSIVE MODE
    public static String REPLAY_METADATA_FACE_PASSIVE_LOW = "replay_metadata_face_passive_low.txt";
    public static String REPLAY_METADATA_FACE_PASSIVE_MEDIUM = "replay_metadata_face_passive_medium.txt";
    public static String REPLAY_METADATA_FACE_PASSIVE_HIGH = "replay_metadata_face_passive_high.txt";
    public static String REPLAY_METADATA_FACE_PASSIVE_VERYHIGH = "replay_metadata_face_passive_vhigh.txt";

    public static String ENCRYPTED_METADATA_PASSIVE_MEDIUM = "encryptedMetadata_passive_medium.enc";
    public static String ENCRYPTED_MASTER_SECRET_PASSIVE_MEDIUM = "encryptedMasterSecret_passive_medium.enc";
    public static String ENCRYPTED_METADATA_PASSIVE_LOW = "encryptedMetadata_passive_low.enc";
    public static String ENCRYPTED_MASTER_SECRET_PASSIVE_LOW = "encryptedMasterSecret_passive_low.enc";
    public static String ENCRYPTED_METADATA_PASSIVE_HIGH = "encryptedMetadata_passive_high.enc";
    public static String ENCRYPTED_MASTER_SECRET_PASSIVE_HIGH = "encryptedMasterSecret_passive_high.enc";

    public static String ENCRYPTED_METADATA_PASSIVE_VERYHIGH = "encryptedMetadata_passive_vhigh.enc";
    public static String ENCRYPTED_MASTER_SECRET_PASSIVE_VERYHIGH = "encryptedMasterSecret_passive_vhigh.enc";

    //LIVENESS MEDIUM MODE
    public static String REPLAY_METADATA_FACE_MEDIUM_LOW = "replay_metadata_face_medium_low.txt";
    public static String REPLAY_METADATA_FACE_MEDIUM_MEDIUM = "replay_metadata_face_medium_medium.txt";
    public static String REPLAY_METADATA_FACE_MEDIUM_HIGH = "replay_metadata_face_medium_high.txt";
    public static String REPLAY_METADATA_FACE_MEDIUM_VERYHIGH = "replay_metadata_face_medium_vhigh.txt";
    public static String ENCRYPTED_METADATA_MEDIUM = "encryptedMetadataMedium.enc";
    public static String ENCRYPTED_MASTER_SECRET_MEDIUM = "encryptedMasterSecretMedium.enc";
    public static String ENCRYPTED_METADATA_MEDIUM_LOW = "encryptedMetadata_medium_low.enc";
    public static String ENCRYPTED_MASTER_SECRET_MEDIUM_LOW = "encryptedMasterSecret_medium_low.enc";
    public static String ENCRYPTED_METADATA_MEDIUM_MEDIUM = "encryptedMetadata_medium_medium.enc";
    public static String ENCRYPTED_MASTER_SECRET_MEDIUM_MEDIUM = "encryptedMasterSecret_medium_medium.enc";
    public static String ENCRYPTED_METADATA_MEDIUM_HIGH = "encryptedMetadata_medium_high.enc";
    public static String ENCRYPTED_MASTER_SECRET_MEDIUM_HIGH = "encryptedMasterSecret_medium_high.enc";
    public static String ENCRYPTED_METADATA_MEDIUM_VERYHIGH = "encryptedMetadata_medium_vhigh.enc";
    public static String ENCRYPTED_MASTER_SECRET_MEDIUM_VERYHIGH = "encryptedMasterSecret_medium_vhigh.enc";

    //LIVENESS HIGH MODE
    public static String REPLAY_METADATA_FACE_HIGH_LOW = "replay_metadata_face_high_low.txt";
    public static String REPLAY_METADATA_FACE_HIGH_MEDIUM = "replay_metadata_face_high_medium.txt";
    public static String REPLAY_METADATA_FACE_HIGH_HIGH = "replay_metadata_face_high_high.txt";
    public static String REPLAY_METADATA_FACE_HIGH_VERYHIGH = "replay_metadata_face_high_vhigh.txt";
    public static String ENCRYPTED_METADATA_HIGH_LOW = "encryptedMetadata_high_low.enc";
    public static String ENCRYPTED_MASTER_SECRET_HIGH_LOW = "encryptedMasterSecret_high_low.enc";
    public static String ENCRYPTED_METADATA_HIGH_MEDIUM = "encryptedMetadata_high_medium.enc";
    public static String ENCRYPTED_MASTER_SECRET_HIGH_MEDIUM = "encryptedMasterSecret_high_medium.enc";
    public static String ENCRYPTED_METADATA_HIGH_HIGH = "encryptedMetadata_high_high.enc";
    public static String ENCRYPTED_MASTER_SECRET_HIGH_HIGH = "encryptedMasterSecret_high_high.enc";
    public static String ENCRYPTED_METADATA_HIGH_VERYHIGH = "encryptedMetadata_high_vhigh.enc";
    public static String ENCRYPTED_MASTER_SECRET_HIGH_VERYHIGH = "encryptedMasterSecret_high_vhigh.enc";


    public static String DEVICE_INFO = "device_info.json";
    //No exifs data
    public static String IMAGE_FACE_NO_EXIF_4 = "image_face_4_no_exif.jpg";
    public static String IMAGE_FACE_NO_EXIF_4_ROT90 = "image_face_4_rotation_90_no_exif.jpg";
    public static String IMAGE_FACE_NO_EXIF_4_ROT180 = "image_face_4_rotation_180_no_exif.jpg";
    public static String IMAGE_FACE_NO_EXIF_5 = "image_face_5_no_exif.jpg";
    public static String IMAGE_FACE_NO_EXIF_5_ROT90 = "image_face_5_rotation_90_no_exif.jpg";
    public static String IMAGE_FACE_NO_EXIF_5_ROT180 = "image_face_5_rotation_180_no_exif.jpg";
    public static String IMAGE_FACE_NO_EXIF_6 = "image_face_6_no_exif.jpg";
    public static String IMAGE_FACE_NO_EXIF_6_ROT90 = "image_face_6_rotation_90_no_exif.jpg";
    public static String IMAGE_FACE_NO_EXIF_6_ROT180 = "image_face_6_rotation_180_no_exif.jpg";

    //with exifs data
    public static String IMAGE_FACE_4 = "image_face_4.jpg";
    public static String IMAGE_FACE_4_ROT90 = "image_face_4_rotation_90.jpg";
    public static String IMAGE_FACE_4_ROT180 = "image_face_4_rotation_180.jpg";
    public static String IMAGE_FACE_5 = "image_face_5.jpg";
    public static String IMAGE_FACE_5_ROT90 = "image_face_5_rotation_90.jpg";
    public static String IMAGE_FACE_5_ROT180 = "image_face_5_rotation_180.jpg";
    public static String IMAGE_FACE_1 = "image_face_1.jpg";
    public static String IMAGE_FACE_2 = "image_face_2.png";
    public static String IMAGE_FACE_3 = "image_face_3.png";
    public static String IMAGE_DOC_1 = "driver_license_1.png";
    public static String IMAGE_DOC_2 = "driver_license_2.png";
    public static String JSON_FACE_1 = "create_face_from_json.json";
    public static String JSON_FACE_6 = "create_face6_from_json.json";
    public static String JSON_FACE_6ROT90 = "create_face6Rot_from_json.json";
    public static String JSON_DIRECT_FACE_1 = "direct_face.json";
    public static String JSON_FACE_2 = "face_1.json";
    public static String JSON_FACE_3 = "face_2.json";
    public static String JSON_FACE_2_1 = "face_1_1.json";
    public static String JSON_FACE_2_2 = "face_1_2.json";
    protected String SELFIE_IMAGE_TYPE = "SELFIE";
    static String buildName;
    public static boolean isTestSuite = true;
    public static boolean init = false;
    //static String buildName;
    static boolean isTestLinkEnabled = true;


    @BeforeAll
    static void setUp() throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException {
        if(isTestSuite && !init) {
            keyStore = createKeystore();
            if(isTestLinkEnabled) {
                TestLinkCoreManager.connect();
                buildName = TestLinkCoreManager.createTestLinkBuild(ConstantTestEnvironment.testlink_projet_name, ConstantTestEnvironment.testlink_plan_name);
            }
            init = true;
        }
    }


    public static void runOnTestLink(String testClass, boolean success) {

        if(isTestLinkEnabled) {
            if(success) {
                log.info(" -- Test Case: {}  | Test Success Reported", testClass);
                TestLinkCoreManager.pass(testClass, ConstantTestEnvironment.testlink_projet_name, ConstantTestEnvironment.testlink_plan_name, buildName);
            } else {
                log.info(" -- Test Case: {}  | Test Failed Reported", testClass);
                TestLinkCoreManager.fail(testClass, ConstantTestEnvironment.testlink_projet_name, ConstantTestEnvironment.testlink_plan_name, buildName);
            }
        }
    }

    protected static KeyStore createKeystore() throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException {
        log.info("------------------------");
        log.info(" Load Keystore ");
        log.info("-------------------------");
        keyStore = KeyStore.getInstance("JCEKS");
        FileInputStream inputStream = new FileInputStream(Paths.get(keystore_path).toFile());
        keyStore.load(inputStream, keystore_password);
        return keyStore;
    }

    protected Certificate getBioServerCertificate() throws KeyStoreException {
        log.info(" Get BioServer Certificate >> ");
        return keyStore.getCertificate(keystore_alias);
    }

    protected void verifySignature(byte[] data, byte[] signedBytes) {
        try {
            String dataString = new String(data, StandardCharsets.UTF_8);
            log.info(" Data to verify >> "+dataString);
            Signature signature = Signature.getInstance(SignatureAlgorithm.RS256.getJavaAlgoName());
            signature.initVerify(getBioServerCertificate());
            signature.update(data);
            Assertions.assertThat(signature.verify(signedBytes)).isTrue();

        } catch(NoSuchAlgorithmException | KeyStoreException | InvalidKeyException | SignatureException e) {
            e.printStackTrace();
        }
    }

    public static String decodeTokenParts(String token)
    {
        log.info(">> decodeTokenParts");
        String[] parts = token.split("\\.", 0);

        byte[]  headerBytes = Base64.getUrlDecoder().decode(parts[0]);
        String decodedString = new String(headerBytes, StandardCharsets.UTF_8);
        log.info("Decoded header part  ==> {} ", decodedString);

        byte[]   payloadBytes = Base64.getUrlDecoder().decode(parts[1]);
        decodedString = new String(payloadBytes, StandardCharsets.UTF_8);
        log.info("Decoded payload part  ==> {} ", decodedString);

        signatureBytes = Base64.getUrlDecoder().decode(parts[2]);
        decodedString = new String(signatureBytes, StandardCharsets.UTF_8);
        //log.info("Decoded signed part  ==> {} ", decodedString);

        return parts[0]+"."+parts[1];
    }

    protected void dateFormatCheckingProcess(String dateAsString) {
        log.info(">> dateFormatCheckingProcess");
        List<Date> date = null;
        try {
            log.info(" -- parse date");
            date = timeParser.parse(dateAsString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        date.get(0);
        Assertions.assertThat(date.size()).isEqualTo(1);
        log.info(" -- Display parsed date  = {}", date.get(0));
    }

    protected static boolean isUUID(String string) {
        try {
            UUID.fromString(string);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    protected String computeDigest(byte[] data) {
        try{
            log.info(">> computeDigest of the image file");
            //Creating the MessageDigest object
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            //Passing data to the created MessageDigest Object
            md.update(data);
            //Compute the message digest
            String digest = new ByteArrayImpl(md.digest()).getHexString();
            log.info(" -- computed digest: "+digest);
            return digest;
        } catch (NoSuchAlgorithmException e) {
            return null;
        }

    }

    protected void landmarksAssertionsProcess(Map eyes, boolean isRotatedImage) {
        log.info("------------------------------------------------------------------------------");
        log.info(" x1: x-coordinate of the first eye < x2: x-coordinate of the second eye > 0 ");
        log.info(" y1: y-coordinate of the first eye> 0 ");
        log.info(" y2: y-coordinate of the second eye> 0 ");
        log.info("------------------------------------------------------------------------------");

        log.info(" -- assert landmarks => x1, x2  = {} , {}", eyes.get("x1"), eyes.get("x1"));
        if(!isRotatedImage) {
            Assertions.assertThat(Double.parseDouble(eyes.get("x1").toString())).isLessThan(Double.parseDouble(eyes.get("x2").toString()));
        } else {
            Assertions.assertThat(Double.parseDouble(eyes.get("x1").toString())).isGreaterThan(0);
        }

        log.info(" -- assert landmarks => x2>0  = {}", eyes.get("x2"));
        Assertions.assertThat(Double.parseDouble(eyes.get("x2").toString())).isGreaterThan(0);


        log.info(" -- assert landmarks => y1 >0 = {}", eyes.get("y1"));
        Assertions.assertThat(Double.parseDouble(eyes.get("y1").toString())).isGreaterThan(0);

        log.info(" -- assert landmarks => y2 >0  = {}", eyes.get("y2"));
        Assertions.assertThat(Double.parseDouble(eyes.get("y2").toString())).isGreaterThan(0);
    }

    protected void faceResourcesResponseAssertions(Response<CreateFaceResponse> response, CreateFaceJsonParams faceJsonParams, byte[] imageBytes, JsonNode jsonNode) {

        log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
        org.assertj.core.api.Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

        log.info(" -- assert id ==> check format  = {}", response.body().getId());
        Assertions.assertThat(isUUID(response.body().getId())).isTrue();

        log.info(" -- assert friendlyName  = {}", response.body().getFriendlyName());
        if(faceJsonParams != null) {
            Assertions.assertThat(response.body().getFriendlyName()).isEqualTo(faceJsonParams.getFriendlyName());
        } else {
            Assertions.assertThat(response.body().getFriendlyName()).isEqualTo(jsonNode.get("friendlyName").asText());
        }


        if(faceJsonParams != null) {
            Assertions.assertThat(response.body().getDigest()).isNull();
        } else {
            log.info(" -- assert digest ==> SHA-256 digest of the image file  = {}", response.body().getDigest());
            //byte[] imageBytes = Files.readAllBytes(Paths.get("src/test/resources/driver_license_us_face.jpg"));
            String digest = computeDigest(imageBytes);
            Assertions.assertThat(response.body().getDigest().toUpperCase()).isEqualTo(digest);

        }


        log.info(" -- assert mode  = {}", response.body().getMode());
        if(faceJsonParams != null) {
            Assertions.assertThat(response.body().getMode()).isEqualTo(faceJsonParams.getMode());
        } else {
            //TODO REMOVE FIX  ALGO ON THE BIOSRV ENV
            Assertions.assertThat(response.body().getMode()).isEqualTo("F5_4_IDD75");
        }

        log.info(" -- assert imageType  = {}", response.body().getImageType());
        if(faceJsonParams != null) {
            Assertions.assertThat(response.body().getImageType()).isEqualTo(faceJsonParams.getImageType());
        } else {
            Assertions.assertThat(response.body().getImageType()).isEqualTo(jsonNode.get("imageType").asText());
        }

        log.info("***********************************");
        log.info("QUALITY CEHCKING");
        log.info("If we have a face ==> quality > 1");
        log.info(" otherwise ==> quality < 1");
        log.info("***********************************");
        log.info(" -- assert quality  = {}", response.body().getQuality());
        if(faceJsonParams != null) {
            Assertions.assertThat(response.body().getQuality()).isEqualTo(0);
        } else {
            Assertions.assertThat(response.body().getQuality()).isGreaterThan(1);
        }

        if(faceJsonParams != null) {
            Assertions.assertThat(response.body().getLandmarks()).isNull();
        } else{
            log.info(" -- assert landmarks  = {}", response.body().getLandmarks());
            Object eyes = ((LinkedHashMap)response.body().getLandmarks()).get("eyes");
            landmarksAssertionsProcess((Map) eyes, false);
        }

        log.info(" -- assert template ==> base64 decoding  = {}", response.body().getTemplate());
        byte[] decodedTemplate = Base64.getDecoder().decode(response.body().getTemplate());
        log.info(" -- decoded template  = {}", new String(decodedTemplate, StandardCharsets.UTF_8));
        if(faceJsonParams != null) {
            Assertions.assertThat(decodedTemplate).isEqualTo(Base64.getDecoder().decode(faceJsonParams.getTemplate()));

            log.info(" -- assert templateVersion  = {}", response.body().getTemplateVersion());
            Assertions.assertThat(response.body().getTemplateVersion()).isEqualTo(faceJsonParams.getTemplateVersion());
        } else {
            Assertions.assertThat(decodedTemplate).isNotNull();

            log.info(" -- assert templateVersion  = {}", response.body().getTemplateVersion());
            Assertions.assertThat(response.body().getTemplateVersion()).isNotNull();
        }


        log.info(" ---------------------------");
        log.info(" Check created date format");
        log.info(" ---------------------------");
        log.info(" -- assert created  = {}", response.body().getCreated());
        dateFormatCheckingProcess(response.body().getCreated());
        if(faceJsonParams != null) {
            Assertions.assertThat(response.body().getCreated().equals(faceJsonParams.getCreated())).isFalse();
        }

        log.info(" ---------------------------");
        log.info(" Check expires date format");
        log.info(" ---------------------------");
        log.info(" -- assert expires  = {}", response.body().getExpires());

        dateFormatCheckingProcess(response.body().getExpires());

        log.info(" ---------------------------");
        log.info(" Verify Signature");
        log.info(" ---------------------------");
        log.info(" -- assert signature  = {}", response.body().getSignature());
        log.info(" -- decode signature data");
        String dataToVerify = decodeTokenParts(response.body().getSignature());
        log.info(" -- Verify  data (header+payload) with the signed data");
        verifySignature(new ByteArrayImpl(Utils.toHexString(dataToVerify)).getBytes(), signatureBytes);

        if(faceJsonParams != null) {
            Assertions.assertThat(response.body().getSignature().equals(faceJsonParams.getSignature())).isFalse();
        }
    }

    protected Response<CreateFaceResponse> createAndGetFaceSuccess(File image, File jsonFile) throws IOException, BiosrvException {

        Response<Void> response = BioserverCoreManager.createFace(image, jsonFile);
        log.info(" -- get reference_id");
        String reference_id = BioserverCoreManager.readheadersResponseAndParseId(response);
        log.info("reference_id: " + reference_id);
        log.info(" -- get reference resources");
        Response<JsonObject> res = BioserverCoreManager.getFace(
                reference_id,
                image,
                jsonFile,
                false);
        CreateFaceResponse mappedResponse = mapper.readValue(res.body().toString(), CreateFaceResponse.class);
        return Response.success(mappedResponse, res.headers());
    }

    public void postReplayMetadataAndGetChallengeProcess(File encMetadata, File encMasterSecret, byte[] expectedFace, String expectedStatus) {

        log.debug(">> postReplayMetadataAndGetChallengeProcess()");

        try {

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND POST REPLAY METADATA REQUEST");
            log.info("--------------------------------------------");

            // get device info file
            File deviceInfoRequest = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + DEVICE_INFO).toFile();

            BioserverCoreManager.postReplayMetadata(encMetadata, encMasterSecret, deviceInfoRequest, true);

            Thread.sleep(4000);

            log.info("------------------------------------");
            log.info("SEND GET CHALLENGE RESPONSE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> response = BioserverCoreManager.getReplayMetadataChallengeResponse(true);

            JsonObject data = response.body();

            getChallengeResponseAssertions(data, expectedFace, expectedStatus);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            Assertions.fail("test failed", e);
        }
    }

    protected void getChallengeResponseAssertions(JsonObject data, byte[] expectedFace, String expectedStatus) {
        log.info(" -- assert ChallengeResponse.status = {}", data.get("status"));
        Assertions.assertThat(data.get("status").getAsString()).isEqualTo(expectedStatus);

        if(data.get("image") != null) {
            log.info(" -- assert ChallengeResponse.image = {}", data.get("image"));
            byte[] imageBytes = Base64.getDecoder().decode(data.get("image").getAsString());
            Assertions.assertThat(imageBytes).
                    isEqualTo(expectedFace);

        }

        log.info(" -- assert ChallengeResponse.signature = {}", data.get("signature").getAsString());
        log.info(" ---------------------------");
        log.info(" Verify Signature");
        log.info(" ---------------------------");

        log.info(" decode signature data");
        String dataToVerify = decodeTokenParts(data.get("signature").getAsString());

        //remove signature
        data.remove("signature");

        //minify json data
        String minifiedPayload = new Minify().minify(data.toString());

        log.info("display payload: {}", minifiedPayload);
        String payload = Base64.getUrlEncoder().withoutPadding().encodeToString(new ByteArrayImpl(Utils.toHexString(minifiedPayload)).getBytes());
        log.info("display encoded payload: {}", payload);

        log.info(" -- Verify data (header+payload) with the signed data");
        verifySignature(new ByteArrayImpl(Utils.toHexString(dataToVerify+payload)).getBytes(), signatureBytes.clone());
    }

    protected void getMatchesResponseAssertionsProcess(JsonArray response, boolean isRefRotated, boolean isCandRotated, boolean samePerson, boolean isRotationEnabled) throws JsonProcessingException {

        JsonNode responseAsJson = mapper.readTree(response.toString()).get(0);

        ReferenceOrCandidate reference = mapper.readValue(responseAsJson.get("reference").toString(), ReferenceOrCandidate.class);
        ReferenceOrCandidate candidate = mapper.readValue(responseAsJson.get("candidate").toString(), ReferenceOrCandidate.class);

        log.info("----------------------------------");
        log.info("GET MATCHES RESPONSE ASSERTIONS");
        log.info("-----------------------------------");

        log.info(" --------------------------------");
        log.info("   Assert Reference Image Object");
        log.info(" ---------------------------------");
        referenceOrCandidateObjectAssertion(reference, referenceResources);

        log.info(" --------------------------------");
        log.info("   Assert Candidate Image Object");
        log.info(" ---------------------------------");
        referenceOrCandidateObjectAssertion(candidate, candidateResources);

        if(isRefRotated) {
            log.info(" -- assert reference.imageRotationEnabled  = {}", reference.isImageRotationEnabled());
            Assertions.assertThat(reference.isImageRotationEnabled()).isEqualTo(isRotationEnabled);
        }

        if(isCandRotated) {
            log.info(" -- assert candidate.imageRotationEnabled  = {}", candidate.isImageRotationEnabled());
            Assertions.assertThat(candidate.isImageRotationEnabled()).isEqualTo(isRotationEnabled);
        }

        log.info(" -- assert score  = {}", responseAsJson.get("score"));
        log.info(" -----------------------------------------------------------------------");
        log.info(" -- Score Matching Threshold SELFIE/ SELFIE equal to 3000");
        log.info(" -----------------------------------------------------------------------");
        log.info(" -- Score Matching Threshold DOCUMENT/ SELFIE is 2500");
        log.info(" -----------------------------------------------------------");
        log.info(" -----------------------------------------------------------");
        log.info(" -- SELFIE/ SELFIE ==> FAR Threshold must less than  0.0001%");
        log.info(" -----------------------------------------------------------");
        log.info(" -------------------------------------------------------------------");
        log.info(" -- DOCUMENT/ SELFIE ==> FAR Threshold must less or equal to  0.01%");
        log.info(" -------------------------------------------------------------------");

        if(reference.getImageType().equals("SELFIE") && candidate.getImageType().equals("SELFIE") && samePerson) {

            if((isCandRotated || isRefRotated) && !isRotationEnabled) {
                log.info(" --------------------------------------------------------");
                log.info(" -- ONE IMAGE AT LEAST ARE ROTATED && ROTATION FIELD NOT SENT ");
                log.info(" --------------------------------------------------------");
                Assertions.assertThat(responseAsJson.get("score").asInt()).isLessThan(score_reference_ds_1);
                log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
                Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isBetween(far_reference_ds_2, far_ko);
            } else {
                log.info(" -------------------------------");
                log.info(" -- SELFIE/ SELFIE: SAME PERSON ");
                log.info(" -------------------------------");
                org.assertj.core.api.Assertions.assertThat(responseAsJson.get("score").asInt()).isGreaterThan(score_reference_ss);
                log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
                org.assertj.core.api.Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isLessThan(far_reference_ss);
            }

        } else if (reference.getImageType().equals("SELFIE") && candidate.getImageType().equals("SELFIE") && !samePerson) {

            log.info(" -----------------------------------");
            log.info(" -- SELFIE/ SELFIE: NOT SAME PERSON ");
            log.info(" -----------------------------------");
            Assertions.assertThat(responseAsJson.get("score").asInt()).isLessThan(score_reference_ds_1);
            log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
            Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isBetween(far_reference_ds_2, far_ko);
        } else if(reference.getImageType().equals("SELFIE") && candidate.getImageType().equals("ID_DOCUMENT") && samePerson) {
            log.info(" -----------------------------------");
            log.info(" -- SELFIE/ DOCUMENT: SAME PERSON ");
            log.info(" -----------------------------------");

            org.assertj.core.api.Assertions.assertThat(responseAsJson.get("score").asInt()).isGreaterThanOrEqualTo(score_reference_ds_2);
            log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
            org.assertj.core.api.Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isLessThan(far_reference_ds_1);
        } else if(reference.getImageType().equals("SELFIE") && candidate.getImageType().equals("ID_DOCUMENT") && !samePerson) {

            log.info(" -----------------------------------");
            log.info(" -- SELFIE/ DOCUMENT: NOT SAME PERSON ");
            log.info(" -----------------------------------");
            Assertions.assertThat(responseAsJson.get("score").asInt()).isLessThan(score_reference_ds_3);
            log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
            Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isBetween(far_reference_ds_2, far_ko);
        }


        log.info(" -- assert correlationId  = {}", responseAsJson.get("correlationId"));
        Assertions.assertThat(responseAsJson.get("correlationId").asText()).isEqualTo("testlink");

        log.info(" ---------------------------");
        log.info(" Check created date format");
        log.info(" ---------------------------");
        dateFormatCheckingProcess(responseAsJson.get("created").asText());

        log.info(" ---------------------------");
        log.info(" Check expires date format");
        log.info(" ---------------------------");

        dateFormatCheckingProcess(responseAsJson.get("expires").asText());

        //check that expires date is greater than the created
        Assertions.assertThat(responseAsJson.get("created").asText()).isLessThan(responseAsJson.get("expires").asText());

        log.info(" ---------------------------");
        log.info(" Verify Signature");
        log.info(" ---------------------------");
        log.info(" -- assert signature  = {}", responseAsJson.get("signature").asText());

        log.info(" decode signature data");
        String dataToVerify = decodeTokenParts(responseAsJson.get("signature").asText());
        log.info(" -- Verify data (header+payload) with the signed data");
        verifySignature(new ByteArrayImpl(Utils.toHexString(dataToVerify)).getBytes(), signatureBytes);
    }

    protected void referenceOrCandidateObjectAssertion(ReferenceOrCandidate reference, Response<CreateFaceResponse> resource) {
        log.info(" -- assert reference.id  = {}", reference.getId());
        Assertions.assertThat(reference.getId()).isEqualTo(resource.body().getId());
        log.info(" -- assert reference.friendlyName  = {}", reference.getFriendlyName());
        Assertions.assertThat(reference.getFriendlyName()).isEqualTo(resource.body().getFriendlyName());
        log.info(" -- assert reference.digest  = {}", reference.getDigest());
        Assertions.assertThat(reference.getDigest()).isEqualTo(resource.body().getDigest());
        log.info(" -- assert reference.mode  = {}", reference.getMode());
        Assertions.assertThat(reference.getMode()).isEqualTo(resource.body().getMode());
        log.info(" -- assert reference.imageType  = {}", reference.getImageType());
        Assertions.assertThat(reference.getImageType()).isEqualTo(resource.body().getImageType());
        log.info(" -- assert reference.quality  = {}", reference.getQuality());
        Assertions.assertThat(reference.getQuality()).isEqualTo(resource.body().getQuality());
        log.info(" -- assert reference.landmarks  = {}", reference.getLandmarks());
        Assertions.assertThat(reference.getLandmarks()).isEqualTo(resource.body().getLandmarks());
    }

    @AfterEach
    public void tearDown() {
        candidateResources = null;
        referenceResources = null;
        bio_session_id = null;
        face_id = null;
    }

}
