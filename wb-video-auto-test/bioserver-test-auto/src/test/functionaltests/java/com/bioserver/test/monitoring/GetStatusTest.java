package com.bioserver.test.monitoring;

import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.BioServerBaseTest;
import com.bioserver.utils.BiosrvException;
import com.bioserver.utils.Mode;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import retrofit2.Response;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GetStatusTest extends BioServerBaseTest {

    @Test
    @Order(1)
    //TODO TICKET BIO_SRV-1817
    ////@DisplayName("Test that Get status​ testcase should return success and the bioserver status when valid request is sent.")
    public void bio_monitoring_01() {

        log.debug(">> bio_monitoring_01()");

        try {

            String expectedVersion = System.getProperty("env.version");
            log.info("------------System.getProperty--------------"+expectedVersion);
            String[] splitExpectedVersion = expectedVersion.split("\\.");

            log.info("--------------------------");
            log.info("SEND GET STATUS REQUEST");
            log.info("--------------------------");
            Response<JsonObject> response = BioserverCoreManager.getStatus();

            log.info("--------------------------------");
            log.info("GET STATUS RESPONSE ASSERTIONS");
            log.info("--------------------------------");

            log.info(" -- assert status code = {}", response.code());
            Assertions.assertThat(response.code()).isEqualTo(200);

            log.info(" -- assert version  = {}", response.body().get("version"));

            String version = response.body().get("version").getAsString();
            String[] spplitedVersion = version.split("\\.");
            Assertions.assertThat(spplitedVersion.length).isEqualTo(3);
            Assertions.assertThat(spplitedVersion[0]).isEqualTo(splitExpectedVersion[0]);
            Assertions.assertThat(spplitedVersion[1]).isEqualTo(splitExpectedVersion[1]);
            Assertions.assertThat(spplitedVersion[2]).isEqualTo(splitExpectedVersion[2]);

            log.info(" -- assert modes  = {}", response.body().get("acceptedModes").getAsString());
            Assertions.assertThat(response.body().get("acceptedModes").getAsString()).isEqualTo(Mode.F5_4_IDD75.name());

            log.info(" -- assert status  = {}", response.body().get("status").getAsString());
            Assertions.assertThat(response.body().get("status").getAsString()).isEqualTo("ok");

            runOnTestLink("BioServer-TC-147", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-147", false);
            Assertions.fail("test failed");
        }
    }
}
