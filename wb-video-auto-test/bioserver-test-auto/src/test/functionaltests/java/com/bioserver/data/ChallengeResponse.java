package com.bioserver.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChallengeResponse {


    private String status;

    private String signature;

    private String image;

    public String getStatus() {
        return status;
    }

    public String getSignature() {
        return signature;
    }

    public String getImage() {
        return image;
    }

}
