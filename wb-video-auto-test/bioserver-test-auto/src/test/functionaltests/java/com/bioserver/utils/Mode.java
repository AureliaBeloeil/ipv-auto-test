package com.bioserver.utils;

public enum Mode implements ErrorCode  {
    F5_1_VID60(500),
    F5_2_VID75(600),
    F5_3_VID60(700),
    F5_4_IDD60(802),
    F5_4_IDD75(804),
    F5_4_LOW75(804);

    private int number = 0;

    Mode(int code) {
        this.number = code;
    }

    @Override
    public int getNumber() {
        return this.number;
    }

    public static Mode map(int i) {
        for (Mode re : Mode.values()) {
            if (i == re.getNumber()) {
                return re;
            }
        }
        return null;
    }

    @Override
    public String getMessageKey() {
        return this.getClass().getSimpleName() + "__" + this;
    }
}
