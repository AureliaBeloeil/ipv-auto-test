package com.bioserver.test.facemanagement;

import com.bioserver.data.CreateFaceResponse;
import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.BioServerBaseTest;
import com.bioserver.utils.BioSrvErrorEnum;
import com.bioserver.utils.BiosrvException;
import com.bioserver.utils.ConstantTestEnvironment;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import retrofit2.Response;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.bioserver.test.core.BioserverCoreManager.bio_session_id;
import static com.bioserver.test.core.BioserverCoreManager.getBioSessionId;
import static com.bioserver.utils.ConstantTestEnvironment.biosrv_api_key;
import static com.bioserver.utils.ConstantTestEnvironment.wrong_biosession_id;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GetFacesTest extends BioServerBaseTest {


    @Test
    @Order(1)
    //@DisplayName("Get Faces testcase should retrieve a list of created faces when a valid request is sent.")
    public void bio_get_faces_01() {

        log.debug(">> bio_get_faces_01()");

        try {
            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            createFaceSuccess();

            log.info("-------------------");
            log.info("GET FACES REQUEST");
            log.info("-------------------");

            //get face server response
            Response<JsonArray> faceResponse = BioserverCoreManager.getFaces(false);

            log.info("------------------------------");
            log.info("GET FACES RESPONSE ASSERTIONS");
            log.info("-----------------------------");

            log.info(" -- assert content type  "+faceResponse.headers().get("Content-Type"));
            Assertions.assertThat(faceResponse.headers().get("Content-Type")).isEqualTo("application/json");

            //read image file
            byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1));
            //read json file
            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile());
            CreateFaceResponse mappedResponse = mapper.readValue(faceResponse.body().get(0).toString(), CreateFaceResponse.class);


            faceResourcesResponseAssertions(Response.success(mappedResponse, faceResponse.headers()), null, imageBytes, jsonNode);

            runOnTestLink("BioServer-TC-79", true);
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-79", true);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(2)
    //@DisplayName("Get Faces testcase should return error 401 when an invalid api_key value is sent.")
    public void bio_get_faces_02() {

        log.debug(">> bio_get_faces_02()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;

        try {
            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            createFaceSuccess();

            log.info(" -- Wrong Api_Key is used ");
            biosrv_api_key = " ";

            log.info("-------------------");
            log.info("GET FACES REQUEST");
            log.info("-------------------");

            //get face server response
            BioserverCoreManager.getFaces(false);

            runOnTestLink("BioServer-TC-80", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                //restore token
                biosrv_api_key = apiKeyRef;
                runOnTestLink("BioServer-TC-80", true);
            } else {
                runOnTestLink("BioServer-TC-80", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(3)
    //@DisplayName("Get Faces testcase should return error 404 when an invalid BioSession_Id value is sent.")
    public void bio_get_faces_03() {

        log.debug(">> bio_get_faces_03()");

        try {
            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            createFaceSuccess();

            log.info(" -- Wrong BioSessionId is used ");
            bio_session_id = wrong_biosession_id;

            log.info("-------------------");
            log.info("GET FACES REQUEST");
            log.info("-------------------");

            //get face server response
            BioserverCoreManager.getFaces(false);

            runOnTestLink("BioServer-TC-81", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-81", true);
            } else {
                runOnTestLink("BioServer-TC-81", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(3)
    //@DisplayName("Get Faces testcase should return error 404 when no face is found.")
    public void bio_get_faces_04() {

        log.debug(">> bio_get_faces_04()");

        try {
            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            log.info("-------------------");
            log.info("GET FACES REQUEST");
            log.info("-------------------");

            //get face server response
            Response<JsonArray> faceResponse = BioserverCoreManager.getFaces(false);

            Assertions.assertThat(faceResponse.body().size()).isEqualTo(0);


            runOnTestLink("BioServer-TC-82", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-82", true);
            Assertions.fail("Test failed: ",  e);
        }
    }

    private void createFaceSuccess() throws IOException, BiosrvException {

        Response<Void> response = BioserverCoreManager.createFace2(
                Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
        );

        log.info("--------------------------");
        log.info("CREATE FACE PARSE RESPONSE");
        log.info("--------------------------");

        log.info(" -- retrieve face_id in the header response ");
        BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
        log.info("face_id: "+BioserverCoreManager.face_id);

        log.info("--------------------------------");
        log.info("CREATE FACE RESPONSE ASSERTIONS");
        log.info("--------------------------------");

        log.info(" -- assert status code = {}", response.code());
        Assertions.assertThat(response.code()).isEqualTo(201);

        log.info(" -- assert face_id format  = {}", BioserverCoreManager.face_id);
        Assertions.assertThat(isUUID(BioserverCoreManager.face_id)).isTrue();
    }
}
