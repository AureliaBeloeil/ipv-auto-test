package com.bioserver.test.facemanagement;

import com.bioserver.data.CreateFaceResponse;
import com.bioserver.utils.BioServerBaseTest;
import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.BioSrvErrorEnum;
import com.bioserver.utils.BiosrvException;
import com.bioserver.utils.ConstantTestEnvironment;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import okhttp3.ResponseBody;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import retrofit2.Response;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.bioserver.test.core.BioserverCoreManager.bio_session_id;
import static com.bioserver.test.core.BioserverCoreManager.getBioSessionId;
import static com.bioserver.utils.ConstantTestEnvironment.*;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CreateFaceTest extends BioServerBaseTest {


    @Test
    @Order(1)
    //@DisplayName("create face should return success")
    public void bio_create_face_01() {

        log.debug(">> bio_create_face_01()");
        log.info("-----------------------------------");
        log.info("FORMAT AND SEND CREATE FACE REQUEST");
        log.info("------------------------------------");
        try {
            Response<Void> response = BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
            );

            log.info("--------------------------");
            log.info("CREATE FACE PARSE RESPONSE");
            log.info("--------------------------");

            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("--------------------------------");
            log.info("CREATE FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(201);

            log.info(" -- assert face_id format  = {}", BioserverCoreManager.face_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.face_id)).isTrue();

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //get face server response
            Response<JsonObject>  faceResponse = BioserverCoreManager.getFace(
                    BioserverCoreManager.face_id,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile(),
                    false);

            CreateFaceResponse mappedResponse = mapper.readValue(faceResponse.body().toString(), CreateFaceResponse.class);

            log.info("-----------------------------");
            log.info("GET FACE RESPONSE ASSERTIONS");
            log.info("------------------------------");

            //read image file
            byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1));
            //read json file
            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile());

            faceResourcesResponseAssertions(Response.success(mappedResponse, faceResponse.headers()), null, imageBytes, jsonNode);

            log.info("-----------------");
            log.info("GET FACE IMAGE");
            log.info("-----------------");

            Response<ResponseBody>  faceImageResponse = BioserverCoreManager.getFaceImage(BioserverCoreManager.face_id, Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(), Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile(), false, false);

            log.info("---------------------------------");
            log.info("GET FACE IMAGE RESPONSE ASSERTIONS");
            log.info("----------------------------------");
            log.info(" -- assert content type  "+faceImageResponse.headers().get("Content-Type"));
            Assertions.assertThat(faceImageResponse.headers().get("Content-Type")).isEqualTo("image/jpeg");
            log.info(" -- assert image  ");
            Assertions.assertThat(faceImageResponse.body().bytes()).isEqualTo(imageBytes);

            runOnTestLink("BioServer-TC-21", true);
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-21", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(2)
    ////@DisplayName("Create Face from image Rest API testcase should return error code 401 when invalid api_key is sent in the request.")
    public void bio_create_face_02() {

        log.debug(">> bio_create_face_02()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info(" -- Wrong api_key is used ");
            biosrv_api_key = " ";

            log.info("-----------------------------------");
            log.info("FORMAT AND SEND CREATE FACE REQUEST");
            log.info("------------------------------------");
            BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
            );

            runOnTestLink("BioServer-TC-22", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                runOnTestLink("BioServer-TC-22", true);
                //restore token
                biosrv_api_key = apiKeyRef;
            } else {
                runOnTestLink("BioServer-TC-22", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(3)
    ////@DisplayName("Create Face from image Rest API testcase should return error code 404 when an invalid bioSession_id value is sent in the request.")
    public void bio_create_face_04() {

        log.debug(">> bio_create_face_04()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info(" -- Wrong BioSessionId is used ");
            bio_session_id = wrong_biosession_id;

            log.info("-----------------------------------");
            log.info("FORMAT AND SEND CREATE FACE REQUEST");
            log.info("------------------------------------");
            BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
            );

            runOnTestLink("BioServer-TC-24", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-24", true);
            } else {
                runOnTestLink("BioServer-TC-24", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    ////@DisplayName("Create Face from image Rest API testcase should return error code 404 when an old bioSession_id value is sent in the request.")
    public void bio_create_face_05() {

        log.debug(">> bio_create_face_05()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info(" -- Wrong BioSessionId is used ");
            bio_session_id = old_biosession_id;

            log.info("-----------------------------------");
            log.info("FORMAT AND SEND CREATE FACE REQUEST");
            log.info("------------------------------------");
            BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
            );

            runOnTestLink("BioServer-TC-25", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-25", true);
            } else {
                runOnTestLink("BioServer-TC-25", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(5)
    ////@DisplayName("Create Face from image Rest API testcase should return error code 400 when no image is sent in the request.")
    public void bio_create_face_06() {

        log.debug(">> bio_create_face_06()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("-----------------------------------");
            log.info("FORMAT AND SEND CREATE FACE REQUEST");
            log.info("------------------------------------");
            BioserverCoreManager.createFace(
                    null,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
            );

            runOnTestLink("BioServer-TC-25", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-25", true);
            } else {
                runOnTestLink("BioServer-TC-25", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(6)
    ////@DisplayName("Create Face from image Rest API testcase should return error code 400 when wrong image name field is sent in the request.")
    public void bio_create_face_07() {

        log.debug(">> bio_create_face_07()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("-----------------------------------");
            log.info("FORMAT AND SEND CREATE FACE REQUEST");
            log.info("------------------------------------");
            BioserverCoreManager.createFace(
                    "wrongField",
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                    "face",
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
            );

            runOnTestLink("BioServer-TC-27", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-27", true);
            } else {
                runOnTestLink("BioServer-TC-27", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(7)
    ////@DisplayName("Create Face from image Rest API testcase should return 201 when "friendlyName" field  is not sent in the request.")
    public void bio_create_face_13() {

        log.debug(">> bio_create_face_13()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("-----------------------------------");
            log.info("FORMAT AND SEND CREATE FACE REQUEST");
            log.info("------------------------------------");
            Response<Void> response = BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2_2).toFile()
            );

            log.info("--------------------------");
            log.info("CREATE FACE PARSE RESPONSE");
            log.info("--------------------------");

            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("--------------------------------");
            log.info("CREATE FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(201);

            log.info(" -- assert face_id format  = {}", BioserverCoreManager.face_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.face_id)).isTrue();

            runOnTestLink("BioServer-TC-33", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-33", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(8)
    ////@DisplayName("Create Face from image Rest API testcase should return 201 when "imageRotationEnabled" field  is not sent in the request.")
    public void bio_create_face_14() {

        log.debug(">> bio_create_face_14()");

        try {

            log.info(" -- create BioSession ");
            getBioSessionId(true);

            log.info("-----------------------------------");
            log.info("FORMAT AND SEND CREATE FACE REQUEST");
            log.info("-----------------------------------");

            JsonObject jsonObject = new JsonObject();
            //"mode": "F5_4_IDD75", "imageType": "SELFIE", "friendlyName": "Image from a selfie"
            jsonObject.addProperty("mode", "F5_4_IDD75");
            jsonObject.addProperty("imageType", "SELFIE");
            jsonObject.addProperty("friendlyName", "Image from a selfie");

            File file = File.createTempFile("face", ".json");
            Files.write(file.toPath(), jsonObject.toString().getBytes());

            Response<Void> response = BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                    file
            );

            log.info("--------------------------");
            log.info("CREATE FACE PARSE RESPONSE");
            log.info("--------------------------");

            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("--------------------------------");
            log.info("CREATE FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(201);

            log.info(" -- assert face_id format  = {}", BioserverCoreManager.face_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.face_id)).isTrue();

            runOnTestLink("BioServer-TC-34", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-34", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(9)
    ////@DisplayName("Create Face from image Rest API testcase should return error code 413 when the request is too big.")
    //TODO TICKET BIO_SRV-1824
    public void bio_create_face_15() {

        log.debug(">> bio_create_face_15()");

        try {

            log.info("-----------------------------------");
            log.info("FORMAT AND SEND CREATE FACE REQUEST");
            log.info("------------------------------------");
            BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + BIG_IMAGE_FACE).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
            );

            runOnTestLink("BioServer-TC-35", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.REQUEST_TOO_BIG_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.REQUEST_TOO_BIG_RESPONSE.name());
                runOnTestLink("BioServer-TC-35", true);
            } else {
                runOnTestLink("BioServer-TC-35", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(10)
    ////@DisplayName("Create Face from image Rest API testcase should return error code 400 when wrong  mode value is sent in the request.")
    public void bio_create_face_16() {

        log.debug(">> bio_create_face_16()");

        try {

            log.info("-----------------------------------");
            log.info("FORMAT AND SEND CREATE FACE REQUEST");
            log.info("------------------------------------");

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mode", "F5_4_");
            jsonObject.addProperty("imageType", "SELFIE");
            jsonObject.addProperty("friendlyName", "Image from a selfie");

            File file = File.createTempFile("face", ".json");
            Files.write(file.toPath(), jsonObject.toString().getBytes());

            BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    file
            );

            runOnTestLink("BioServer-TC-231", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-231", true);
            } else {
                runOnTestLink("BioServer-TC-231", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Disabled("disabled due to Unsupported mode")
    @Order(11)
    ////@DisplayName("Test that Create Direct Face testcase should return success when mode is set to F5_1_VID60 in the request.")
    public void bio_create_face_18() {

        log.debug(">> bio_create_face_18()");

        try {

            log.info("-----------------------------------");
            log.info("FORMAT AND SEND CREATE FACE REQUEST");
            log.info("------------------------------------");

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mode", "F5_1_VID60");
            jsonObject.addProperty("imageType", "SELFIE");
            jsonObject.addProperty("friendlyName", "Image from a selfie");

            File file = File.createTempFile("face", ".json");
            Files.write(file.toPath(), jsonObject.toString().getBytes());

            Response<Void> response = BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    file
            );

            log.info("--------------------------");
            log.info("CREATE FACE PARSE RESPONSE");
            log.info("--------------------------");

            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("--------------------------------");
            log.info("CREATE FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(201);

            log.info(" -- assert face_id format  = {}", BioserverCoreManager.face_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.face_id)).isTrue();

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //get face server response
            Response<JsonObject>  faceResponse = BioserverCoreManager.getFace(
                    BioserverCoreManager.face_id,
                    null,null,
                    false);

            CreateFaceResponse mappedResponse = mapper.readValue(faceResponse.body().toString(), CreateFaceResponse.class);

            log.info("-----------------------------");
            log.info("GET FACE RESPONSE ASSERTIONS");
            log.info("------------------------------");

            //read image file
            byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6));
            //read json file
            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile());

            faceResourcesResponseAssertions(Response.success(mappedResponse, faceResponse.headers()), null, imageBytes, jsonNode);

            runOnTestLink("BioServer-TC-293", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-293", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Disabled("disabled due to Unsupported mode")
    @Order(12)
    ////@DisplayName("Test that Create Direct Face testcase should return success when mode is set to F5_2_VID75 in the request.")
    public void bio_create_face_19() {

        log.debug(">> bio_create_face_19()");

        try {

            log.info("-----------------------------------");
            log.info("FORMAT AND SEND CREATE FACE REQUEST");
            log.info("------------------------------------");

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mode", "F5_2_VID75");
            jsonObject.addProperty("imageType", "SELFIE");
            jsonObject.addProperty("friendlyName", "Image from a selfie");

            File file = File.createTempFile("face", ".json");
            Files.write(file.toPath(), jsonObject.toString().getBytes());

            Response<Void> response = BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    file
            );

            log.info("--------------------------");
            log.info("CREATE FACE PARSE RESPONSE");
            log.info("--------------------------");

            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("--------------------------------");
            log.info("CREATE FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(201);

            log.info(" -- assert face_id format  = {}", BioserverCoreManager.face_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.face_id)).isTrue();

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //get face server response
            Response<JsonObject>  faceResponse = BioserverCoreManager.getFace(
                    BioserverCoreManager.face_id,
                    null,null,
                    false);

            log.info("-----------------------------");
            log.info("GET FACE RESPONSE ASSERTIONS");
            log.info("------------------------------");

            //read image file
            byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6));
            //read json file
            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile());

            CreateFaceResponse mappedResponse = mapper.readValue(faceResponse.body().toString(), CreateFaceResponse.class);

            faceResourcesResponseAssertions(Response.success(mappedResponse, faceResponse.headers()), null, imageBytes, jsonNode);

            runOnTestLink("BioServer-TC-294", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-294", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Disabled("disabled due to Unsupported mode")
    @Order(13)
    ////@DisplayName("Test that Create Direct Face testcase should return success when mode is set to F5_3_VID60 in the request.")
    public void bio_create_face_20() {

        log.debug(">> bio_create_face_20()");

        try {

            log.info("-----------------------------------");
            log.info("FORMAT AND SEND CREATE FACE REQUEST");
            log.info("------------------------------------");

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mode", "F5_3_VID60");
            jsonObject.addProperty("imageType", "SELFIE");
            jsonObject.addProperty("friendlyName", "Image from a selfie");

            File file = File.createTempFile("face", ".json");
            Files.write(file.toPath(), jsonObject.toString().getBytes());

            Response<Void> response = BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    file
            );

            log.info("--------------------------");
            log.info("CREATE FACE PARSE RESPONSE");
            log.info("--------------------------");

            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("--------------------------------");
            log.info("CREATE FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(201);

            log.info(" -- assert face_id format  = {}", BioserverCoreManager.face_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.face_id)).isTrue();

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //get face server response
            Response<JsonObject>  faceResponse = BioserverCoreManager.getFace(
                    BioserverCoreManager.face_id,
                    null,null,
                    false);

            log.info("-----------------------------");
            log.info("GET FACE RESPONSE ASSERTIONS");
            log.info("------------------------------");

            //read image file
            byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6));
            //read json file
            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile());

            CreateFaceResponse mappedResponse = mapper.readValue(faceResponse.body().toString(), CreateFaceResponse.class);

            faceResourcesResponseAssertions(Response.success(mappedResponse, faceResponse.headers()), null, imageBytes, jsonNode);

            runOnTestLink("BioServer-TC-295", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-295", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Disabled("disabled due to Unsupported mode")
    @Order(14)
    ////@DisplayName("Test that Create Direct Face testcase should return success when mode is set to F5_4_IDD60 in the request.")
    public void bio_create_face_21() {

        log.debug(">> bio_create_face_21()");

        try {

            log.info("-----------------------------------");
            log.info("FORMAT AND SEND CREATE FACE REQUEST");
            log.info("------------------------------------");

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mode", "F5_4_IDD60");
            jsonObject.addProperty("imageType", "SELFIE");
            jsonObject.addProperty("friendlyName", "Image from a selfie");

            File file = File.createTempFile("face", ".json");
            Files.write(file.toPath(), jsonObject.toString().getBytes());

            Response<Void> response = BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    file
            );

            log.info("--------------------------");
            log.info("CREATE FACE PARSE RESPONSE");
            log.info("--------------------------");

            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("--------------------------------");
            log.info("CREATE FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(201);

            log.info(" -- assert face_id format  = {}", BioserverCoreManager.face_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.face_id)).isTrue();

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //get face server response
            Response<JsonObject>  faceResponse = BioserverCoreManager.getFace(
                    BioserverCoreManager.face_id,
                    null,null,
                    false);

            log.info("-----------------------------");
            log.info("GET FACE RESPONSE ASSERTIONS");
            log.info("------------------------------");

            //read image file
            byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6));
            //read json file
            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile());

            CreateFaceResponse mappedResponse = mapper.readValue(faceResponse.body().toString(), CreateFaceResponse.class);

            faceResourcesResponseAssertions(Response.success(mappedResponse, faceResponse.headers()), null, imageBytes, jsonNode);

            runOnTestLink("BioServer-TC-296", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-296", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Disabled("disabled due to Unsupported mode")
    @Order(15)
    ////@DisplayName("Test that Create Direct Face testcase should return success when mode is set to F5_4_LOW75 in the request.")
    public void bio_create_face_22() {

        log.debug(">> bio_create_face_22()");

        try {

            log.info("-----------------------------------");
            log.info("FORMAT AND SEND CREATE FACE REQUEST");
            log.info("------------------------------------");

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mode", "F5_4_LOW75");
            jsonObject.addProperty("imageType", "SELFIE");
            jsonObject.addProperty("friendlyName", "Image from a selfie");

            File file = File.createTempFile("face", ".json");
            Files.write(file.toPath(), jsonObject.toString().getBytes());

            Response<Void> response = BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    file
            );

            log.info("--------------------------");
            log.info("CREATE FACE PARSE RESPONSE");
            log.info("--------------------------");

            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("--------------------------------");
            log.info("CREATE FACE RESPONSE ASSERTIONS");
            log.info("--------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(201);

            log.info(" -- assert face_id format  = {}", BioserverCoreManager.face_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.face_id)).isTrue();

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //get face server response
            Response<JsonObject>  faceResponse = BioserverCoreManager.getFace(
                    BioserverCoreManager.face_id,
                    null,null,
                    false);

            log.info("-----------------------------");
            log.info("GET FACE RESPONSE ASSERTIONS");
            log.info("------------------------------");

            //read image file
            byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6));
            //read json file
            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile());

            CreateFaceResponse mappedResponse = mapper.readValue(faceResponse.body().toString(), CreateFaceResponse.class);

            faceResourcesResponseAssertions(Response.success(mappedResponse, faceResponse.headers()), null, imageBytes, jsonNode);

            runOnTestLink("BioServer-TC-296", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-296", false);
            Assertions.fail("Test failed: ",  e);
        }
    }
}
