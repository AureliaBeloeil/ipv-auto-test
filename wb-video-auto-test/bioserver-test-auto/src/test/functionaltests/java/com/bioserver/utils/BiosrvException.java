package com.bioserver.utils;

import okhttp3.ResponseBody;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class BiosrvException extends Exception{

    private BioSrvErrorEnum errorCode;
    private ResponseBody responseBody;
    private String errorMsg;

    private static final long serialVersionUID = 1L;

    public static BiosrvException wrap(Throwable exception, BioSrvErrorEnum errorCode) {
        if (exception instanceof BiosrvException) {
            BiosrvException se = (BiosrvException) exception;
            if (errorCode != null && errorCode != se.getErrorCode()) {
                return new BiosrvException(exception.getMessage(), exception, errorCode);
            }
            return se;
        } else {
            return new BiosrvException(exception.getMessage(), exception, errorCode);
        }
    }

    private final Map<String, Object> properties = new TreeMap<String, Object>();

    public BiosrvException(BioSrvErrorEnum errorCode) {
        this.errorCode = errorCode;
    }

    public BiosrvException(String message, BioSrvErrorEnum errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public BiosrvException(String message, ResponseBody responseBody, BioSrvErrorEnum errorCode) {
        super(message);
        this.errorMsg = message;
        this.errorCode = errorCode;
        this.responseBody = responseBody;
    }

    public BiosrvException(Throwable cause, BioSrvErrorEnum errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public BiosrvException(String message, Throwable cause, BioSrvErrorEnum errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public BioSrvErrorEnum getErrorCode() {
        return errorCode;
    }

    public  ResponseBody getResponseBody() {
        return responseBody;
    }

    public BiosrvException setErrorCode(BioSrvErrorEnum errorCode) {
        this.errorCode = errorCode;
        return this;
    }

    public void printStackTrace(StringWriter s) {
        synchronized (s) {
            printStackTrace(new PrintWriter(s));
        }
    }

    @Override
    public void printStackTrace(PrintStream s) {
        synchronized (s) {
            printStackTrace(new PrintWriter(s));
        }
    }

    @Override
    public void printStackTrace(PrintWriter s) {
        synchronized (s) {
            s.println(this);
            s.println("\t-------------------------------");
            s.println("\t Error Message Found======>:" + errorMsg);
            if (errorCode != null) {
                s.println("\t Error Code Found======>:" + errorCode.name());
            }
            if (responseBody != null) {
                if(responseBody.contentType() != null) {
                    s.println("\t" + " ErrorBody Type Found======>: " + Objects.requireNonNull(responseBody.contentType()).toString());
                    s.println("\t" + " ErrorBody Length Found======>: " + responseBody.contentLength());
                }
                try {
                    s.println("\t" + " ErrorBody Content Found======>: " + responseBody.source().readString(Charset.defaultCharset()));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            for (String key : properties.keySet()) {
                s.println("\t" + key + "=[" + properties.get(key) + "]");
            }
            s.println("\t-------------------------------");
            StackTraceElement[] trace = getStackTrace();
            for (int i = 0; i < trace.length; i++) {
                s.println("\tat " + trace[i]);
            }

            Throwable ourCause = getCause();
            if (ourCause != null) {
                ourCause.printStackTrace(s);
            }
            s.flush();
        }
    }

}
