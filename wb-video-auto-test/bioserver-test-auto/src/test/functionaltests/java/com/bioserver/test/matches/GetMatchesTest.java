package com.bioserver.test.matches;

import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.*;
import com.google.gson.JsonArray;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import retrofit2.Response;

import java.nio.file.Paths;

import static com.bioserver.test.core.BioserverCoreManager.*;
import static com.bioserver.utils.ConstantTestEnvironment.*;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GetMatchesTest extends BioServerBaseTest {


//                      FAR	        Matching threshold
//                     0.0001%	        4500
//                     0.001%	        4000
//                     0.01%	        3500
//                     0.1%	            3000
//                     1%	            2500
//    Matching Document /Selfie : recommanded threashold is 2500 or 3000.
//    Matching Selfie /Selfie : recommanded threashold is 3000, 3500 or higher depending on the use case
    @Test
    @Order(1)
    //@DisplayName("get matches should return success")
    public void bio_get_matches_01() {

        log.debug(">> bio_get_matches_01()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");
        try {

            //create face
            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info(" -- create face II");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            Response<JsonArray> response = BioserverCoreManager.getMatches(referenceResources.body().getId());

            log.info("----------------------------");
            log.info("PARSE GET MATCHES RESPONSE");
            log.info("-----------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            getMatchesResponseAssertionsProcess(response.body(), false, false, true, true);

            runOnTestLink("BioServer-TC-95", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-95", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(2)
    //@DisplayName("get matches should fail in case a wrong auth is sent")
    public void bio_get_matches_02() {

        log.debug(">> bio_get_matches_02()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;
        try {

            //create face
            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info(" -- create face II");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info(" -- wrong token is used ");
            biosrv_api_key = "";

            BioserverCoreManager.getMatches(referenceResources.body().getId());
            runOnTestLink("BioServer-TC-96", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                runOnTestLink("BioServer-TC-96", true);
                //restore token
                biosrv_api_key = apiKeyRef;
            } else {
                runOnTestLink("BioServer-TC-96", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }

    }

    @Test
    @Order(3)
    //@DisplayName("get matches should fail in case a wrong BioSessionId is sent")
    public void bio_get_matches_03() {

        log.debug(">> bio_get_matches_03()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");

        //save biosession_id
        String bioSessionRef = BioserverCoreManager.bio_session_id;

        try {

            //create face
            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info(" -- create face II");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info(" -- wrong BioSessionId is used ");
            BioserverCoreManager.bio_session_id = wrong_biosession_id;

            BioserverCoreManager.getMatches(referenceResources.body().getId());
            runOnTestLink("BioServer-TC-97", false);
            Assertions.fail("TEST FAILED !!!");
            //runOnTestLink("getBioSessionShouldSuccess", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-97", true);
                //restore biosession_id
                BioserverCoreManager.bio_session_id = bioSessionRef;
            } else {
                runOnTestLink("BioServer-TC-97", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    //@DisplayName("get matches should fail in case a wrong referenceId is sent")
    public void bio_get_matches_04() {

        log.debug(">> bio_get_matches_04()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");

        try {

            //create face
            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info(" -- create face II");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info(" -- wrong referenceId is used ");

            BioserverCoreManager.getMatches(wrong_biosession_id);
            runOnTestLink("BioServer-TC-98", false);
            Assertions.fail("TEST FAILED !!!");
            //runOnTestLink("getBioSessionShouldSuccess", true);
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-98", true);
            } else {
                runOnTestLink("BioServer-TC-98", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(5)
    //@DisplayName("get matches should fail in case a wrong BioSessionId and referenceId are sent")
    public void bio_get_matches_05() {

        log.debug(">> bio_get_matches_05()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");

        //save biosession_id
        String bioSessionRef = BioserverCoreManager.bio_session_id;

        try {

            //create face
            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info(" -- create face II");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info(" -- wrong BioSessionId and referenceId are used ");
            BioserverCoreManager.bio_session_id = wrong_biosession_id;

            BioserverCoreManager.getMatches(wrong_biosession_id);
            runOnTestLink("BioServer-TC-99", false);
            Assertions.fail("TEST FAILED !!!");
            //runOnTestLink("getBioSessionShouldSuccess", true);
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-99", true);
                //restore bioSession_id
                BioserverCoreManager.bio_session_id = bioSessionRef;
            } else {
                runOnTestLink("BioServer-TC-99", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(6)
    //TODO A rajouter
    //@DisplayName("Get matches when Document and Selfie used belongs to the same person")
    public void bio_get_matches_06() {

        log.debug(">> bio_get_matches_06()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");
        try {

            //create face
            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_DOC_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_3).toFile()
            );

            log.info(" -- create face II");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            Response<JsonArray> response = BioserverCoreManager.getMatches(referenceResources.body().getId());

            log.info("----------------------------");
            log.info("PARSE GET MATCHES RESPONSE");
            log.info("-----------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            getMatchesResponseAssertionsProcess(response.body(), false, false, true, true);

            runOnTestLink("BioServer-TC-100", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-100", false);
            Assertions.fail("test failed", e);
        }

    }


    @Test
    @Order(8)
    //@DisplayName("get matches should fail when no auth is sent")
    public void bio_get_matches_08() {

        log.debug(">> bio_get_matches_08()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;

        try {

            //create face
            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(), Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile());

            log.info(" -- create face II");
            referenceResources = createAndGetFaceSuccess(Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(), Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile());

            log.info(" -- token is empty ");
            biosrv_api_key = null;

            BioserverCoreManager.getMatches(referenceResources.body().getId());
            runOnTestLink("BioServer-TC-233", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                runOnTestLink("BioServer-TC-233", true);
                //restore token
                biosrv_api_key = apiKeyRef;
            } else {
                runOnTestLink("BioServer-TC-233", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }

    }

    @Test
    @Order(9)
    //TODO A rajouter DANS TESTLINK
    //@DisplayName("Get matches when two different images are used")
    public void bio_get_matches_09() {

        log.debug(">> bio_get_matches_09()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");

        try {

            //create face
            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info(" -- create face II");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_2).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            Response<JsonArray> response = BioserverCoreManager.getMatches(referenceResources.body().getId());

            log.info("----------------------------");
            log.info("PARSE GET MATCHES RESPONSE");
            log.info("-----------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            getMatchesResponseAssertionsProcess(response.body(), false, false, false, true);
            runOnTestLink("BioServer-TC-234", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-234", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(10)
    //TODO A rajouter DANS TESTLINK
    //@DisplayName("Get matches when Document and Selfie used not belongs to the same person")
    public void bio_get_matches_10() {

        log.debug(">> bio_get_matches_10()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");
        try {

            //create face
            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_DOC_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_3).toFile()
            );

            log.info(" -- create face II");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_2).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            Response<JsonArray> response = BioserverCoreManager.getMatches(referenceResources.body().getId());

            log.info("----------------------------");
            log.info("PARSE GET MATCHES RESPONSE");
            log.info("-----------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            getMatchesResponseAssertionsProcess(response.body(), false, false, false, true);
            runOnTestLink("BioServer-TC-235", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-235", false);
            Assertions.fail("test failed", e);
        }
    }

    //@Test
    @Order(11)
    //TODO A rajouter DANS TESTLINK
    //@DisplayName("Get matches when Document and Video Frame are used")
    public void bio_get_matches_11() {

    }

    @Test
    @Order(12)
    //TODO TEST A RAJOUTER SUR TESTLINK
    //@DisplayName("Test that Get Matches​ testcase should return success when  the candidate image is rotated 90 degrees.")
    public void bio_get_matches_12() {

        log.debug(">> bio_get_matches_12()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");
        try {

            //create face
            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_4_ROT90).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2_1).toFile()
            );

            log.info(" -- create face II");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_4).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            Response<JsonArray> response = BioserverCoreManager.getMatches(referenceResources.body().getId());

            log.info("----------------------------");
            log.info("PARSE GET MATCHES RESPONSE");
            log.info("-----------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            getMatchesResponseAssertionsProcess(response.body(), false, true, true, true);

            runOnTestLink("BioServer-TC-242", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-242", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(13)
    //TODO TEST A RAJOUTER SUR TESTLINK
    //@DisplayName("Test that Direct images match​ testcase should return success when  the reference image is rotated 90 degrees.")
    public void bio_get_matches_13() {

        log.debug(">> bio_get_matches_13()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");
        try {

            //create face
            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_4_ROT90).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2_1).toFile()
            );

            log.info(" -- create face II");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_4).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            Response<JsonArray> response = BioserverCoreManager.getMatches(referenceResources.body().getId());

            log.info("----------------------------");
            log.info("PARSE GET MATCHES RESPONSE");
            log.info("-----------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            getMatchesResponseAssertionsProcess(response.body(), true, false, true, true);

            runOnTestLink("BioServer-TC-243", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-243", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(14)
    //TODO TEST A RAJOUTER SUR TESTLINK ==> demander à lequipe les souci du score sur une image sans exif
    //@DisplayName("Test that Direct images match​ testcase should return success when  the reference image is rotated 180 degrees.")
    public void bio_get_matches_14() {

        log.debug(">> bio_get_matches_14()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");
        try {

            //create face
            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6_ROT180).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2_1).toFile()
            );

            log.info(" -- create face II");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            Response<JsonArray> response = BioserverCoreManager.getMatches(referenceResources.body().getId());

            log.info("----------------------------");
            log.info("PARSE GET MATCHES RESPONSE");
            log.info("-----------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            getMatchesResponseAssertionsProcess(response.body(), true, false, true, true);

            runOnTestLink("BioServer-TC-244", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-244", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(15)
    //TODO TEST A RAJOUTER SUR TESTLINK
    //@DisplayName("Test that Direct images match​ testcase should return success when  the candidate image is rotated 180 degrees.")
    public void bio_get_matches_15() {

        log.debug(">> bio_get_matches_15()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");
        try {

            //create face
            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6_ROT180).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2_1).toFile()
            );

            log.info(" -- create face II");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_4).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            Response<JsonArray> response = BioserverCoreManager.getMatches(referenceResources.body().getId());

            log.info("----------------------------");
            log.info("PARSE GET MATCHES RESPONSE");
            log.info("-----------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            getMatchesResponseAssertionsProcess(response.body(), false, true, true, true);

            runOnTestLink("BioServer-TC-245", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-245", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(16)
    //TODO TEST A RAJOUTER SUR TESTLINK
    //@DisplayName("Test that Direct images match​ testcase should return success when  the candidate image is rotated 90 degrees and the reference image is rotated 180 degrees.")
    public void bio_get_matches_16() {

        log.debug(">> bio_get_matches_16()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");
        try {

            //create face
            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_4_ROT90).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2_1).toFile()
            );

            log.info(" -- create face II");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6_ROT180).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2_1).toFile()
            );

            Response<JsonArray> response = BioserverCoreManager.getMatches(referenceResources.body().getId());

            log.info("----------------------------");
            log.info("PARSE GET MATCHES RESPONSE");
            log.info("-----------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            getMatchesResponseAssertionsProcess(response.body(), true, true, true, true);

            runOnTestLink("BioServer-TC-246", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-246", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(17)
    //TODO TEST A RAJOUTER SUR TESTLINK
    //@DisplayName("Test that Direct images match​ testcase should return success when  the candidate image is rotated 90 degrees.")
    public void bio_get_matches_17() {

        log.debug(">> bio_get_matches_17()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");
        try {

            //create face
            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_4_ROT90).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2_1).toFile()
            );

            log.info(" -- create face II");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_4).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            Response<JsonArray> response = BioserverCoreManager.getMatches(referenceResources.body().getId());

            log.info("----------------------------");
            log.info("PARSE GET MATCHES RESPONSE");
            log.info("-----------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            getMatchesResponseAssertionsProcess(response.body(), false, true, true, true);

            runOnTestLink("BioServer-TC-247", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-247", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(18)
    //TODO TEST A RAJOUTER SUR TESTLINK
    //@DisplayName("Test that Get matches​ testcase should not match when  the candidate image is rotated 90 degrees and rotation field not enabled")
    public void bio_get_matches_18() {

        log.debug(">> bio_get_matches_18()");

        log.info("-------------------------------------");
        log.info("FORMAT AND SEND GET MATCHES REQUEST");
        log.info("-------------------------------------");
        try {

            //create face
            log.info(" -- create face I");
            candidateResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6_ROT90).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info(" -- create face II");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            Response<JsonArray> response = BioserverCoreManager.getMatches(referenceResources.body().getId());

            log.info("----------------------------");
            log.info("PARSE GET MATCHES RESPONSE");
            log.info("-----------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            getMatchesResponseAssertionsProcess(response.body(), false, true, true, false);

            runOnTestLink("BioServer-TC-249", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-249", false);
            Assertions.fail("test failed", e);
        }
    }

    @AfterEach
    public void tearDown() {
        candidateResources = null;
        referenceResources = null;
        bio_session_id = null;
        face_id = null;
    }
}