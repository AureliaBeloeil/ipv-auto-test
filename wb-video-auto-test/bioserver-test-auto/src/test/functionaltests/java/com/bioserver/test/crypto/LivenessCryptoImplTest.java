package com.bioserver.test.crypto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

import idemia.bioserver.metadatacrypto.common.InvalidInputException;
import idemia.bioserver.metadatacrypto.common.MetaDataCrypto;
import idemia.bioserver.metadatacrypto.common.MetaDataCryptoException;
import idemia.bioserver.metadatacrypto.core.Engine;
import idemia.bioserver.metadatacrypto.core.MetaDataCryptoImpl;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class LivenessCryptoImplTest {

    protected static final String pathResourcesDir = "src/test/keystores/";


    protected final String SERVER_KEYSTOREFILE = "test.webioserver.liveness.jks";
    protected final String SERVER_KEYSTORETYPE = "JKS";
    protected final String SERVER_ALIAS = "ALIAS_WBS";
    protected final String SERVER_KEYSTOREPASSWORD = "wbs123";
    protected final String SERVER_PRIVATEKEYPASSWORD = "password";


    private SecureServer server;
    private SecureClient client;


    @BeforeEach
    public void setUp() throws Exception {
        client = new SecureClient();

        server = new SecureServer(Paths.get(pathResourcesDir + SERVER_KEYSTOREFILE).toAbsolutePath().toString(), SERVER_ALIAS, SERVER_KEYSTORETYPE, SERVER_KEYSTOREPASSWORD, SERVER_PRIVATEKEYPASSWORD);

    }


    //@Test
    public void processSimple() throws Exception {

        byte[] decrypted;
        byte[] encryptedData;
        //new String(Base64.getEncoder().encode(ks.getCertificate(alias).getEncoded()), StandardCharsets.UTF_8)
        // Generate simple TEST data
        byte[] data = Files.readAllBytes(Paths.get("C:\\Users\\G597970\\Documents\\WebBio\\metadata\\NEW\\medium_vhigh.dat"));
        encryptedData = client.encryptMetaData(data, server.getServerRandom());

        byte[] encryptedMasterSecret = client.encryptSecret(server.getServerRandom(), server.getCertificate());

        decrypted = server.decrypt(encryptedData, encryptedMasterSecret);

        Files.write(Paths.get("C:\\Users\\G597970\\Documents\\WebBio\\metadata\\test\\medium\\encryptedMetadata_medium_vhigh.enc"), encryptedData);
        Files.write(Paths.get("C:\\Users\\G597970\\Documents\\WebBio\\metadata\\test\\medium\\encryptedMasterSecret_medium_vhigh.enc"), encryptedMasterSecret);

        String encryptedDataFile  = new String(Base64.getEncoder().encode(encryptedData), StandardCharsets.UTF_8);

        String encryptedMasterSecretFile  = new String(Base64.getEncoder().encode(encryptedMasterSecret), StandardCharsets.UTF_8);

        assertThat(decrypted).isEqualTo(data);
    }

    public class SecureClient {

        private byte[] masterSecret = new byte[48];
        private          Engine engine = new Engine();

        public SecureClient(){
            new Random().nextBytes(masterSecret);
        }

        // Utils methods
        protected byte[] encryptMetaData(byte[] input, byte [] serverRandom) throws MetaDataCryptoException {
            engine.generateKeys(masterSecret, serverRandom);
            return engine.encryptAndMac(input);
        }


        // Utils methods
        protected byte[] encryptSecret(byte [] serverRandom, Certificate certificate) throws MetaDataCryptoException {


            engine.generateKeys(masterSecret, serverRandom);
            return engine.encrypt(masterSecret, certificate.getPublicKey());
        }
    }

    public class SecureServer {

        protected PrivateKey privateKey;
        protected byte[] serverRandom;
        protected Certificate certificate;


        public SecureServer(String keyStoreFile, String alias, String keyStoreType, String keyStorePassword, String privateKeyPassword) throws MetaDataCryptoException {
            assert keyStoreFile != null;
            assert alias != null;
            assert keyStoreType != null;
            assert keyStorePassword != null;
            assert privateKeyPassword != null;


            serverRandom = Base64.getDecoder().decode("4hX0FHLahDr/DPd8qUqD4J0eUI1/rV92NRrdyo81HR4=");

            try {
                KeyStore ks = KeyStore.getInstance(keyStoreType);
                FileInputStream in = new FileInputStream(keyStoreFile);
                ks.load(in, keyStorePassword.toCharArray());
                privateKey = (PrivateKey) ks.getKey(alias, privateKeyPassword.toCharArray());
                if (privateKey == null) {
                    throw new UnrecoverableEntryException("cannot find private key");
                }
                certificate = ks.getCertificate(alias);
                System.out.println(" certificate: "+new String(Base64.getEncoder().encode(ks.getCertificate(alias).getEncoded()), StandardCharsets.UTF_8));
                if (certificate == null) {
                    throw new UnrecoverableEntryException("cannot find certificate");
                }
            } catch (IOException | KeyStoreException | CertificateException | NoSuchAlgorithmException | UnrecoverableEntryException ex) {
                throw new RuntimeException(ex);
            }
        }


        /**
         * @param input
         * @return
         * @throws MetaDataCryptoException
         */
        protected byte[] encryptMetaData(byte[] input) throws MetaDataCryptoException {

            byte[] masterSecret = new byte[48];
            new Random().nextBytes(masterSecret);

            Engine engine = new Engine();

            engine.generateKeys(privateKey.getEncoded(), serverRandom);

            return engine.encryptAndMac(input);
        }


        /**
         * @param input
         * @param encryptedMasterSecret
         * @return
         * @throws MetaDataCryptoException
         * @throws InvalidInputException
         */

        protected byte[] decrypt(byte[] input, byte[] encryptedMasterSecret) throws MetaDataCryptoException, InvalidInputException {

            MetaDataCrypto metaDataCrypto = new MetaDataCryptoImpl(serverRandom, privateKey);
            return metaDataCrypto.process(encryptedMasterSecret, input);
        }


        /**
         * @param serverRandom
         * @param publicKey
         * @return
         * @throws InvalidKeySpecException
         * @throws MetaDataCryptoException
         * @throws NoSuchAlgorithmException
         */
        protected byte[] encryptPrivateKey(byte[] serverRandom, byte[] publicKey) throws InvalidKeySpecException, MetaDataCryptoException, NoSuchAlgorithmException {
            Engine engine = new Engine();
            return engine.encrypt(privateKey.getEncoded(), KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(publicKey)));

        }


        public byte[] getServerRandom() {
            return serverRandom;
        }

        public PrivateKey getPrivateKey() {
            return privateKey;
        }

        public Certificate getCertificate() {
            return certificate;
        }

        public byte[] getPublicKey() {
            if (certificate == null || certificate.getPublicKey() == null) {
                return null;
            }
            return certificate.getPublicKey().getEncoded();
        }
    }
}
