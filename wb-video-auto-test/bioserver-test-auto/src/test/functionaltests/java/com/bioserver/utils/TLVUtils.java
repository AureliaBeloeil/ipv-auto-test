package com.bioserver.utils;

public class TLVUtils {

    private static ByteArrayFactory defaultBaf = new ByteArrayFactoryImpl();

    public TLVUtils() {
    }

    private static ByteArray lengthBytes(ByteArrayFactory baf, int length) {
        ByteArray L;
        if (length <= 127) {
            L = baf.createByteArray(1);
            L.setByte(0, (byte)length);
            return L;
        } else if (length <= 255) {
            L = baf.createByteArray(2);
            L.setByte(0, (byte)-127);
            L.setByte(1, (byte)length);
            return L;
        } else if (length <= 65535) {
            L = baf.createByteArray(3);
            L.setByte(0, (byte)-126);
            L.setByte(1, (byte)(length >> 8 & 255));
            L.setByte(2, (byte)(length & 255));
            return L;
        } else if (length <= 16777215) {
            L = baf.createByteArray(4);
            L.setByte(0, (byte)-125);
            L.setByte(1, (byte)(length >> 16 & 255));
            L.setByte(2, (byte)(length >> 8 & 255));
            L.setByte(3, (byte)(length & 255));
            return L;
        } else {
            L = baf.createByteArray(4);
            L.setByte(0, (byte)-124);
            L.setByte(1, (byte)length);
            return L;
        }
    }

    public static ByteArray lengthBytes(ByteArrayFactory baf, byte[] value) {
        return lengthBytes(baf, value.length);
    }

    public static ByteArray lengthBytes(ByteArrayFactory baf, ByteArray value) {
        int length = value.getLength();
        return lengthBytes(baf, length);
    }

    public static ByteArray lengthBytes(ByteArray value) {
        int length = value.getLength();
        return lengthBytes(defaultBaf, length);
    }

    public static ByteArray create(ByteArrayFactory baf, ByteArray tag, ByteArray value) {
        return create(baf, tag.getBytes(), value.getBytes());
    }

    public static ByteArray create(ByteArray tag, ByteArray value) {
        return defaultBaf.createByteArray(tag).append(lengthBytes(defaultBaf, value)).append(value);
    }

    public static ByteArray create(byte tag, ByteArray value) {
        return create(defaultBaf, tag, value);
    }

    public static ByteArray create(short tag, String val) {
        return create((short)tag, (ByteArray)(new ByteArrayImpl(val)));
    }

    public static ByteArray create(byte tag, String val) {
        return create((byte)tag, (ByteArray)(new ByteArrayImpl(val)));
    }

    public static ByteArray create(short tag, ByteArray value) {
        return create(defaultBaf, tag, value);
    }

    public static ByteArray create(short tag, byte[] value) {
        return create(defaultBaf, tag, value);
    }

    public static ByteArray create(short tag, byte value) {
        return create(tag, ByteArrayImpl.fromByte(value));
    }

    public static ByteArray create(ByteArrayFactory baf, ByteArray tag, byte[] val) {
        return baf.createByteArray(tag).append(lengthBytes(baf, val)).appendBytes(val);
    }

    public static ByteArray create(ByteArrayFactory baf, String tag, String hexValue) {
        if (tag.length() == 2) {
            byte t = Byte.parseByte(tag, 16);
            return create(baf, t, baf.createByteArray(hexValue));
        } else if (tag.length() == 4) {
            short t = Short.parseShort(tag, 16);
            return create(baf, t, baf.createByteArray(hexValue));
        } else {
            throw new IllegalArgumentException("wrong tag length ");
        }
    }

    public static ByteArray create(byte[] tag, byte[] val) {
        return create(defaultBaf, tag, val);
    }

    public static ByteArray create(byte tag, byte[] val) {
        return create(defaultBaf, tag, val);
    }

    public static ByteArray create(byte tag, short val) {
        return create(defaultBaf, tag, Utils.convertShortToByteArray(val));
    }

    private static ByteArray create(ByteArrayFactory baf, byte[] tag, byte[] val) {
        return baf.createByteArray(tag).append(lengthBytes(baf, val)).appendBytes(val);
    }

    public static ByteArray create(ByteArrayFactory baf, byte tag, ByteArray value) {
        return create(baf, tag, value.getBytes());
    }

    public static ByteArray create(ByteArrayFactory baf, short tag, ByteArray value) {
        return create(baf, tag, value.getBytes());
    }

    public static ByteArray create(ByteArrayFactory baf, byte tag, byte[] value) {
        ByteArray L = lengthBytes(baf, value);
        int ll = L.getLength();
        int length = 1 + ll + value.length;
        ByteArray tlv = baf.createByteArray(length);
        tlv.setByte(0, tag);
        tlv.copyBytes(L, 0, 1, ll);
        tlv.copyBytes(value, 0, 1 + ll, value.length);
        return tlv;
    }

    public static ByteArray create(ByteArrayFactory baf, short tag, byte[] value) {
        ByteArray L = lengthBytes(baf, value);
        int ll = L.getLength();
        int length = 2 + ll + value.length;
        ByteArray tlv = baf.createByteArray(length);
        tlv.setShort(0, tag);
        tlv.copyBytes(L, 0, 2, ll);
        tlv.copyBytes(value, 0, 2 + ll, value.length);
        return tlv;
    }

    public static ByteArray createWithByte(ByteArrayFactory baf, byte tag, byte val) {
        return baf.createByteArray(new byte[]{tag, 1, val});
    }

    public static ByteArray createWithByte(ByteArrayFactory baf, short tag, byte val) {
        return baf.createByteArray(new byte[]{(byte)(tag >>> 8), (byte)tag, 1, val});
    }

    public static ByteArray create(ByteArray tag, byte value) {
        return defaultBaf.createByteArray(tag).appendByte((byte)1).appendByte(value);
    }
}
