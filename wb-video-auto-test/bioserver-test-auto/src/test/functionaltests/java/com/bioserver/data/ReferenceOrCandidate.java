package com.bioserver.data;

public class ReferenceOrCandidate {

    private String id;

    private String friendlyName;

    private String digest;

    private String mode;

    private String imageType;

    private int quality;

    private Object landmarks;

    private boolean imageRotationEnabled;

    public String getId() {
        return id;
    }

    public String getFriendlyName() {
        return friendlyName;
    }


    public String getDigest() {
        return digest;
    }


    public String getMode() {
        return mode;
    }

    public String getImageType() {
        return imageType;
    }


    public int getQuality() {
        return quality;
    }


    public Object getLandmarks() {
        return landmarks;
    }

    public boolean isImageRotationEnabled() {
        return imageRotationEnabled;
    }

    public void setImageRotationEnabled(boolean imageRotationEnabled) {
        this.imageRotationEnabled = imageRotationEnabled;
    }
}
