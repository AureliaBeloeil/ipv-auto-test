package com.bioserver.utils;

public enum BioSrvErrorEnum implements ErrorCode {

    OK_RESPONSE(200),
    CREATED_RESPONSE(201),
    STORAGE_NOT_ENABLED_RESPONSE(204),
    NOT_FOUND_RESPONSE(404),
    NOT_ALLOWED_METHOD_RESPONSE(405),
    UNSUPPORTED_MEDIA_TYPE_RESPONSE(415),
    AUTHENTICATION_REQUIRED_RESPONSE(401),
    INTERNAL_ERROR_RESPONSE(500),
    PERMISSIONS_MISSING_RESPONSE(403),
    REQUEST_TOO_BIG_RESPONSE(413),
    BAD_GATEWAY_RESPONSE(502),
    BAD_REQUEST_RESPONSE(400);

    private int number = 0;

    BioSrvErrorEnum(int code) {
        this.number = code;
    }

    @Override
    public int getNumber() {
        return this.number;
    }

    public static BioSrvErrorEnum map(int i) {
        for (BioSrvErrorEnum re : BioSrvErrorEnum.values()) {
            if (i == re.getNumber()) {
                return re;
            }
        }
        return null;
    }

    @Override
    public String getMessageKey() {
        return this.getClass().getSimpleName() + "__" + this;
    }
}
