package com.bioserver.test.testsuites;

import com.bioserver.test.facemanagement.CreateDirectFaceTest;
import com.bioserver.test.facemanagement.CreateFaceFromJsonTest;
import com.bioserver.test.facemanagement.CreateFaceTest;
import com.bioserver.test.facemanagement.GetFaceImageTest;
import com.bioserver.test.matches.DirectImagesMatchTest;
import com.bioserver.test.matches.GetMatchesTest;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectClasses({
        CreateDirectFaceTest.class,
        CreateFaceFromJsonTest.class,
        CreateFaceTest.class,
        GetFaceImageTest.class,
        DirectImagesMatchTest.class,
        GetMatchesTest.class,
})
public class BioServerTestSuite {
    //BioServerBaseTest.isTestSuite = true;
}
