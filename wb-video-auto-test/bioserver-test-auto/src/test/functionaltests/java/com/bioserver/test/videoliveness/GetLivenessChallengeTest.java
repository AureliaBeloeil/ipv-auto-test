package com.bioserver.test.videoliveness;

import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.*;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import retrofit2.Response;

import java.io.IOException;
import java.nio.file.Paths;

import static com.bioserver.test.core.BioserverCoreManager.bio_session_id;
import static com.bioserver.test.core.BioserverCoreManager.referenceResources;
import static com.bioserver.utils.ConstantTestEnvironment.biosrv_api_key;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GetLivenessChallengeTest extends BioServerBaseTest {


    @Test
    @Order(1)
    //TODO TICKET BIO_SRV-1804
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return success when valid request is sent.")
    public void bio_replay_liveness_challenge_result_01() {

        log.debug(">> bio_replay_liveness_challenge_result_01()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> livenessResult = BioserverCoreManager.getLivenessChallengeResult("LIVENESS_HIGH/LOW?numberOfChallenge=2", true);

            JsonObject data = livenessResult.body();

            log.info("------------------------------------------");
            log.info("GET LIVENESS CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------------");

            log.info(" -- assert status code = {}", livenessResult.code());
            org.assertj.core.api.Assertions.assertThat(livenessResult.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", livenessResult.headers().get("Content-Type"));
            Assertions.assertThat(livenessResult.headers().get("Content-Type")).isEqualTo("application/json");

            livenessChallengeResponseAssertions(livenessChallenge, data, LivenessStatusEnum.SUCCESS.name());

            runOnTestLink("BioServer-TC-192", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-192", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(2)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return error code 401 when bearer Auth value is sent.")
    public void bio_replay_liveness_challenge_result_02() {

        log.debug(">> bio_replay_liveness_challenge_result_02()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            log.info(" -- Bearer Auth is used ");
            biosrv_api_key = " ";

            BioserverCoreManager.getLivenessChallengeResult("LIVENESS_HIGH/LOW?numberOfChallenge=2", false);

            runOnTestLink("BioServer-TC-193", false);

            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                runOnTestLink("BioServer-TC-193", true);
                //restore token
                biosrv_api_key = apiKeyRef;
            } else {
                runOnTestLink("BioServer-TC-193", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(3)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return error code 401 when wrong api_key value is sent.")
    public void bio_replay_liveness_challenge_result_03() {

        log.debug(">> bio_replay_liveness_challenge_result_02()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            log.info(" -- Wrong Api_Key is used ");
            biosrv_api_key = " ";

            BioserverCoreManager.getLivenessChallengeResult("LIVENESS_HIGH/LOW?numberOfChallenge=2", true);

            runOnTestLink("BioServer-TC-194", false);

            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                runOnTestLink("BioServer-TC-194", true);
                //restore token
                biosrv_api_key = apiKeyRef;
            } else {
                runOnTestLink("BioServer-TC-194", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return error code 404 when wrong BioSessionId value is sent.")
    public void bio_replay_liveness_challenge_result_04() {

        log.debug(">> bio_replay_liveness_challenge_result_04()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            log.info(" -- Wrong BioSessionId is used ");
            bio_session_id = " ";

            BioserverCoreManager.getLivenessChallengeResult("LIVENESS_HIGH/LOW?numberOfChallenge=2", true);

            runOnTestLink("BioServer-TC-195", false);

            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-195", true);
            } else {
                runOnTestLink("BioServer-TC-195", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(5)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return Spoof_js when wrong liveness mode is sent in the url.")
    public void bio_replay_liveness_challenge_result_05() {

        log.debug(">> bio_replay_liveness_challenge_result_05()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> livenessResult = BioserverCoreManager.getLivenessChallengeResult("VERYHIGH/LOW?numberOfChallenge=2", true);

            JsonObject data = livenessResult.body();

            log.info("------------------------------------------");
            log.info("GET LIVENESS CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------------");

            log.info(" -- assert status code = {}", livenessResult.code());
            org.assertj.core.api.Assertions.assertThat(livenessResult.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", livenessResult.headers().get("Content-Type"));
            Assertions.assertThat(livenessResult.headers().get("Content-Type")).isEqualTo("application/json");

            livenessChallengeResponseAssertions(livenessChallenge, data, LivenessStatusEnum.SPOOF_JS.name());

            runOnTestLink("BioServer-TC-196", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-196", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(6)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return error code 400 when wrong Security Level is sent.")
    public void bio_replay_liveness_challenge_result_06() {

        log.debug(">> bio_replay_liveness_challenge_result_06()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> livenessResult = BioserverCoreManager.getLivenessChallengeResult("LIVENESS_HIGH/VERYHIGH?numberOfChallenge=2", true);

            JsonObject data = livenessResult.body();

            log.info("------------------------------------------");
            log.info("GET LIVENESS CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------------");

            log.info(" -- assert status code = {}", livenessResult.code());
            org.assertj.core.api.Assertions.assertThat(livenessResult.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", livenessResult.headers().get("Content-Type"));
            Assertions.assertThat(livenessResult.headers().get("Content-Type")).isEqualTo("application/json");

            livenessChallengeResponseAssertions(livenessChallenge, data, LivenessStatusEnum.SPOOF_JS.name());

            runOnTestLink("BioServer-TC-197", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-197", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(7)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return success with livenessResult when saved video liveness has livenessStatus = FAILED.")
    public void bio_replay_liveness_challenge_result_07() {

        log.debug(">> bio_replay_liveness_challenge_result_07()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();
        log.info(" -- replay liveness challenge => livenessStatus : FAILED");
        livenessChallenge.remove("livenessStatus");
        livenessChallenge.addProperty("livenessStatus", "FAILED");

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> livenessResult = BioserverCoreManager.getLivenessChallengeResult("LIVENESS_HIGH/LOW?numberOfChallenge=2", true);

            JsonObject data = livenessResult.body();

            log.info("------------------------------------------");
            log.info("GET LIVENESS CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------------");

            log.info(" -- assert status code = {}", livenessResult.code());
            org.assertj.core.api.Assertions.assertThat(livenessResult.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", livenessResult.headers().get("Content-Type"));
            Assertions.assertThat(livenessResult.headers().get("Content-Type")).isEqualTo("application/json");

            livenessChallengeResponseAssertions(livenessChallenge, data, LivenessStatusEnum.FAILED.name());

            runOnTestLink("BioServer-TC-273", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-273", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(8)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return success with livenessResult when saved video liveness has livenessStatus = SPOOF.")
    public void bio_replay_liveness_challenge_result_08() {

        log.debug(">> bio_replay_liveness_challenge_result_08()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();
        log.info(" -- replay liveness challenge => livenessStatus : SPOOF");
        livenessChallenge.remove("livenessStatus");
        livenessChallenge.addProperty("livenessStatus", "SPOOF");

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> livenessResult = BioserverCoreManager.getLivenessChallengeResult("LIVENESS_HIGH/LOW?numberOfChallenge=2", true);

            JsonObject data = livenessResult.body();

            log.info("------------------------------------------");
            log.info("GET LIVENESS CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------------");

            log.info(" -- assert status code = {}", livenessResult.code());
            org.assertj.core.api.Assertions.assertThat(livenessResult.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", livenessResult.headers().get("Content-Type"));
            Assertions.assertThat(livenessResult.headers().get("Content-Type")).isEqualTo("application/json");

            livenessChallengeResponseAssertions(livenessChallenge, data, LivenessStatusEnum.SPOOF.name());

            runOnTestLink("BioServer-TC-274", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-274", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(9)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return success with livenessResult when saved video liveness has livenessStatus = SPOOF_JS.")
    public void bio_replay_liveness_challenge_result_09() {

        log.debug(">> bio_replay_liveness_challenge_result_09()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();
        log.info(" -- replay liveness challenge => livenessStatus : SPOOF_JS");
        livenessChallenge.remove("livenessStatus");
        livenessChallenge.addProperty("livenessStatus", "SPOOF_JS");

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> livenessResult = BioserverCoreManager.getLivenessChallengeResult("LIVENESS_HIGH/LOW?numberOfChallenge=2", true);

            JsonObject data = livenessResult.body();

            log.info("------------------------------------------");
            log.info("GET LIVENESS CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------------");

            log.info(" -- assert status code = {}", livenessResult.code());
            org.assertj.core.api.Assertions.assertThat(livenessResult.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", livenessResult.headers().get("Content-Type"));
            Assertions.assertThat(livenessResult.headers().get("Content-Type")).isEqualTo("application/json");

            livenessChallengeResponseAssertions(livenessChallenge, data, LivenessStatusEnum.SPOOF_JS.name());

            runOnTestLink("BioServer-TC-275", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-275", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(10)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return success with livenessResult when saved video liveness has livenessStatus = ERROR.")
    public void bio_replay_liveness_challenge_result_10() {

        log.debug(">> bio_replay_liveness_challenge_result_10()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();
        log.info(" -- replay liveness challenge => livenessStatus : ERROR");
        livenessChallenge.remove("livenessStatus");
        livenessChallenge.addProperty("livenessStatus", "ERROR");

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> livenessResult = BioserverCoreManager.getLivenessChallengeResult("LIVENESS_HIGH/LOW?numberOfChallenge=2", true);

            JsonObject data = livenessResult.body();

            log.info("------------------------------------------");
            log.info("GET LIVENESS CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------------");

            log.info(" -- assert status code = {}", livenessResult.code());
            org.assertj.core.api.Assertions.assertThat(livenessResult.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", livenessResult.headers().get("Content-Type"));
            Assertions.assertThat(livenessResult.headers().get("Content-Type")).isEqualTo("application/json");

            livenessChallengeResponseAssertions(livenessChallenge, data, LivenessStatusEnum.ERROR.name());

            runOnTestLink("BioServer-TC-276", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-276", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(11)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return success with livenessResult when saved video liveness has livenessStatus = TIMEOUT.")
    public void bio_replay_liveness_challenge_result_11() {

        log.debug(">> bio_replay_liveness_challenge_result_11()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();
        log.info(" -- replay liveness challenge => livenessStatus : TIMEOUT");
        livenessChallenge.remove("livenessStatus");
        livenessChallenge.addProperty("livenessStatus", "TIMEOUT");

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> livenessResult = BioserverCoreManager.getLivenessChallengeResult("LIVENESS_HIGH/LOW?numberOfChallenge=2", true);

            JsonObject data = livenessResult.body();

            log.info("------------------------------------------");
            log.info("GET LIVENESS CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------------");

            log.info(" -- assert status code = {}", livenessResult.code());
            org.assertj.core.api.Assertions.assertThat(livenessResult.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", livenessResult.headers().get("Content-Type"));
            Assertions.assertThat(livenessResult.headers().get("Content-Type")).isEqualTo("application/json");

            livenessChallengeResponseAssertions(livenessChallenge, data, LivenessStatusEnum.TIMEOUT.name());

            runOnTestLink("BioServer-TC-277", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-277", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(12)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return success with livenessMode = NO_LIVENESS when saved video liveness has livenessMode = NO_LIVENESS .")
    public void bio_replay_liveness_challenge_result_12() {

        log.debug(">> bio_replay_liveness_challenge_result_12()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();
        log.info(" -- replay liveness challenge => livenessMode : NO_LIVENESS");
        livenessChallenge.remove("livenessMode");
        livenessChallenge.addProperty("livenessMode", "NO_LIVENESS");

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> livenessResult = BioserverCoreManager.getLivenessChallengeResult("NO_LIVENESS/LOW?numberOfChallenge=2", true);

            JsonObject data = livenessResult.body();

            log.info("------------------------------------------");
            log.info("GET LIVENESS CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------------");

            log.info(" -- assert status code = {}", livenessResult.code());
            org.assertj.core.api.Assertions.assertThat(livenessResult.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", livenessResult.headers().get("Content-Type"));
            Assertions.assertThat(livenessResult.headers().get("Content-Type")).isEqualTo("application/json");

            livenessChallengeResponseAssertions(livenessChallenge, data, LivenessStatusEnum.SUCCESS.name());

            runOnTestLink("BioServer-TC-278", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-278", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(13)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return success with livenessMode = LIVENESS_MEDIUM when saved video liveness has livenessMode = LIVENESS_MEDIUM .")
    public void bio_replay_liveness_challenge_result_13() {

        log.debug(">> bio_replay_liveness_challenge_result_13()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();
        log.info(" -- replay liveness challenge => livenessMode : LIVENESS_MEDIUM");
        livenessChallenge.remove("livenessMode");
        livenessChallenge.addProperty("livenessMode", "LIVENESS_MEDIUM");

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> livenessResult = BioserverCoreManager.getLivenessChallengeResult("LIVENESS_MEDIUM/LOW?numberOfChallenge=2", true);

            JsonObject data = livenessResult.body();

            log.info("------------------------------------------");
            log.info("GET LIVENESS CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------------");

            log.info(" -- assert status code = {}", livenessResult.code());
            org.assertj.core.api.Assertions.assertThat(livenessResult.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", livenessResult.headers().get("Content-Type"));
            Assertions.assertThat(livenessResult.headers().get("Content-Type")).isEqualTo("application/json");

            livenessChallengeResponseAssertions(livenessChallenge, data, LivenessStatusEnum.SUCCESS.name());

            runOnTestLink("BioServer-TC-279", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-279", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(14)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return success with livenessMode = LIVENESS_HIGH when saved video liveness has livenessMode = LIVENESS_HIGH.")
    public void bio_replay_liveness_challenge_result_14() {

        log.debug(">> bio_replay_liveness_challenge_result_14()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();
        log.info(" -- replay liveness challenge => livenessMode : LIVENESS_HIGH");
        livenessChallenge.remove("livenessMode");
        livenessChallenge.addProperty("livenessMode", "LIVENESS_HIGH");

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> livenessResult = BioserverCoreManager.getLivenessChallengeResult("LIVENESS_HIGH/LOW?numberOfChallenge=2", true);

            JsonObject data = livenessResult.body();

            log.info("------------------------------------------");
            log.info("GET LIVENESS CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------------");

            log.info(" -- assert status code = {}", livenessResult.code());
            org.assertj.core.api.Assertions.assertThat(livenessResult.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", livenessResult.headers().get("Content-Type"));
            Assertions.assertThat(livenessResult.headers().get("Content-Type")).isEqualTo("application/json");

            livenessChallengeResponseAssertions(livenessChallenge, data, LivenessStatusEnum.SUCCESS.name());

            runOnTestLink("BioServer-TC-280", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-280", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(15)
    //TODO LIVENESS_HIGH_TRAINING nest plus supporté
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return success with livenessMode = LIVENESS_HIGH_TRAINING when saved video liveness has livenessMode = LIVENESS_HIGH_TRAINING.")
    public void bio_replay_liveness_challenge_result_15() {

        log.debug(">> bio_replay_liveness_challenge_result_15()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();
        log.info(" -- replay liveness challenge => livenessMode : LIVENESS_HIGH_TRAINING");
        livenessChallenge.remove("livenessMode");
        livenessChallenge.addProperty("livenessMode", "LIVENESS_HIGH_TRAINING");

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            BioserverCoreManager.getLivenessChallengeResult("LIVENESS_HIGH_TRAINING/LOW?numberOfChallenge=2", true);

            runOnTestLink("BioServer-TC-281", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-281", true);
            } else {
                runOnTestLink("BioServer-TC-281", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(16)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return SPOOF_JS with livenessMode = LIVENESS_HIGH_TRAINING when saved video liveness has livenessMode = LIVENESS_HIGH.")
    public void bio_replay_liveness_challenge_result_16() {

        log.debug(">> bio_replay_liveness_challenge_result_16()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> livenessResult = BioserverCoreManager.getLivenessChallengeResult("LIVENESS_HIGH_TRAINING/LOW?numberOfChallenge=2", true);

            JsonObject data = livenessResult.body();

            log.info("------------------------------------------");
            log.info("GET LIVENESS CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------------");

            log.info(" -- assert status code = {}", livenessResult.code());
            org.assertj.core.api.Assertions.assertThat(livenessResult.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", livenessResult.headers().get("Content-Type"));
            Assertions.assertThat(livenessResult.headers().get("Content-Type")).isEqualTo("application/json");

            livenessChallengeResponseAssertions(livenessChallenge, data, LivenessStatusEnum.SPOOF_JS.name());

            runOnTestLink("BioServer-TC-282", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-282", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(17)
    //@DisplayName("Test that Get Liveness Challenge result​ testcase should return livenessStatus SPOOF with securityLevel= LOW when saved video liveness has securityLevel= HIGH.")
    public void bio_replay_liveness_challenge_result_17() {

        log.debug(">> bio_replay_liveness_challenge_result_17()");

        JsonObject livenessChallenge = getLivenessChallengeResultRequest();
        log.info(" -- replay liveness challenge => securityLevel : HIGH");
        livenessChallenge.remove("securityLevel");
        livenessChallenge.addProperty("securityLevel", "HIGH");

        try {
            postLivenessChallengeProcess(livenessChallenge);

            log.info("------------------------------------");
            log.info("SEND GET LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            Response<JsonObject> livenessResult = BioserverCoreManager.getLivenessChallengeResult("LIVENESS_HIGH/LOW?numberOfChallenge=2", true);

            JsonObject data = livenessResult.body();

            log.info("------------------------------------------");
            log.info("GET LIVENESS CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------------");

            log.info(" -- assert status code = {}", livenessResult.code());
            org.assertj.core.api.Assertions.assertThat(livenessResult.code()).isEqualTo(200);

            //Assert content type
            log.info(" -- assert Content-Type  = {}", livenessResult.headers().get("Content-Type"));
            Assertions.assertThat(livenessResult.headers().get("Content-Type")).isEqualTo("application/json");

            livenessChallengeResponseAssertions(livenessChallenge, data, LivenessStatusEnum.SPOOF_JS.name());

            runOnTestLink("BioServer-TC-283", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-283", false);
            Assertions.fail("test failed", e);
        }
    }


    private void livenessChallengeResponseAssertions(JsonObject livenessChallenge, JsonObject data, String status) {

        log.info(" -- assert livenessStatus = {}", data.get("livenessStatus"));
        Assertions.assertThat(data.get("livenessStatus").getAsString()).isEqualTo(status);

        log.info(" -- assert diagnostic = {}", data.get("diagnostic"));
        Assertions.assertThat(data.get("diagnostic").getAsString()).isEqualTo(livenessChallenge.get("diagnostic").getAsString());

        log.info(" -- assert fakeWebcam = {}", data.get("fakeWebcam"));
        Assertions.assertThat(data.get("fakeWebcam").getAsBoolean()).isEqualTo(livenessChallenge.get("fakeWebcam").getAsBoolean());

        log.info(" -- assert livenessMode = {}", data.get("livenessMode"));
        Assertions.assertThat(data.get("livenessMode").getAsString()).isEqualTo(livenessChallenge.get("livenessMode").getAsString());

        log.info(" -- assert numberOfChallenge = {}", data.get("numberOfChallenge"));
        Assertions.assertThat(data.get("numberOfChallenge").getAsInt()).isEqualTo(livenessChallenge.get("numberOfChallenge").getAsInt());

        log.info(" -- assert securityLevel = {}", data.get("securityLevel"));
        Assertions.assertThat(data.get("securityLevel").getAsString()).isEqualTo(livenessChallenge.get("securityLevel").getAsString());

        log.info(" -- assert bestImageId = {}", data.get("bestImageId"));
        Assertions.assertThat(data.get("bestImageId").getAsString()).isEqualTo(livenessChallenge.get("bestImageId").getAsString());

        log.info(" ---------------------------");
        log.info(" Verify Signature");
        log.info(" ---------------------------");

        log.info(" decode signature data");
        String dataToVerify = decodeTokenParts(data.get("signature").getAsString());

        log.info(" -- Verify data (header+payload) with the signed data");
        verifySignature(new ByteArrayImpl(Utils.toHexString(dataToVerify)).getBytes(), signatureBytes.clone());
    }

    private void postLivenessChallengeProcess(JsonObject livenessChallenge) throws IOException, BiosrvException {
        log.info("---------------------------------");
        log.info("CREATE BIOSESSION &  FACE RESOURCE");
        log.info("---------------------------------");

        log.info(" -- create face I");
        referenceResources = createAndGetFaceSuccess(
                Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
        );

        //add reference id
        livenessChallenge.addProperty("bestImageId",   referenceResources.body().getId());

        log.info("------------------------------------");
        log.info("SEND POST LIVENESS CHALLENGE REQUEST");
        log.info("-------------------------------------");


        Response<Void> response = BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

        log.info("-----------------------------------");
        log.info("GET CHALLENGE RESPONSE ASSERTIONS");
        log.info("------------------------------------");

        log.info(" -- assert status code = {}", response.code());
        Assertions.assertThat(response.code()).isEqualTo(204);
    }

    private JsonObject getLivenessChallengeResultRequest() {
        JsonObject livenessChallenge = new JsonObject();
        livenessChallenge.addProperty("livenessStatus",   LivenessStatusEnum.SUCCESS.name());
        livenessChallenge.addProperty("diagnostic",   "OK");
        livenessChallenge.addProperty("fakeWebcam",   false);
        livenessChallenge.addProperty("livenessMode",   "LIVENESS_HIGH");
        livenessChallenge.addProperty("numberOfChallenge",   2);
        livenessChallenge.addProperty("securityLevel", "LOW");
        return livenessChallenge;
    }

}
