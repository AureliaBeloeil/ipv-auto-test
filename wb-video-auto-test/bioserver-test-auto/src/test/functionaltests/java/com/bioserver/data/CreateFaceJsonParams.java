package com.bioserver.data;

public class CreateFaceJsonParams {
    private String id;
    private String friendlyName;
    private String digest;
    private String mode;
    private String imageType;
    private int quality;
    private Object landmarks;
    private String template;
    private int templateVersion;
    private String created;
    private String signature;

    public String getId() {
        return id;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public String getDigest() {
        return digest;
    }

    public String getMode() {
        return mode;
    }

    public String getImageType() {
        return imageType;
    }

    public int getQuality() {
        return quality;
    }

    public Object getLandmarks() {
        return landmarks;
    }

    public String getTemplate() {
        return template;
    }

    public int getTemplateVersion() {
        return templateVersion;
    }

    public String getCreated() {
        return created;
    }

    public String getSignature() {
        return signature;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public void setTemplateVersion(int templateVersion) {
        this.templateVersion = templateVersion;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    /*public CreateFaceJsonParams(String id, String friendlyName, String digest, String mode,
                                String imageType, int quality, Object landmarks, String template, int templateVersion, String created, String signature) {
        this.id = id;
        this.friendlyName = friendlyName;
        this.digest = digest;
        this.mode = mode;
        this.imageType = imageType;
        this.quality = quality;
        this.landmarks = landmarks;
        this.template = template;
        this.templateVersion = templateVersion;
        this.created = created;
        this.signature = signature;
    }*/
}
