package com.bioserver.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DirectFaceResponse {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("friendlyName")
    @Expose
    private String friendlyName;

    @SerializedName("digest")
    @Expose
    private String digest;

    @SerializedName("mode")
    @Expose
    private String mode;

    @SerializedName("imageType")
    @Expose
    private String imageType;

    @SerializedName("quality")
    @Expose
    private int quality;

    @SerializedName("landmarks")
    @Expose
    private Object landmarks;

    @SerializedName("imageRotationEnabled")
    @Expose
    private boolean imageRotationEnabled;

    @SerializedName("template")
    @Expose
    private String template;

    @SerializedName("templateVersion")
    @Expose
    private int templateVersion;

    @SerializedName("created")
    @Expose
    private String created;

    @SerializedName("signature")
    @Expose
    private String signature;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public Object getLandmarks() {
        return landmarks;
    }

    public void setLandmarks(String landmarks) {
        this.landmarks = landmarks;
    }

    public boolean isImageRotationEnabled() {
        return imageRotationEnabled;
    }

    public void setImageRotationEnabled(boolean imageRotationEnabled) {
        this.imageRotationEnabled = imageRotationEnabled;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public int getTemplateVersion() {
        return templateVersion;
    }

    public void setTemplateVersion(int templateVersion) {
        this.templateVersion = templateVersion;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
