package com.bioserver.utils;

public class ByteArrayFactoryImpl implements ByteArrayFactory {
    public ByteArrayFactoryImpl() {
    }

    public ByteArray createByteArray(int length) {
        return new ByteArrayImpl(length);
    }

    public ByteArray createByteArray(byte[] bytes) {
        return new ByteArrayImpl(bytes);
    }

    public ByteArray createByteArray(ByteArray b) {
        return new ByteArrayImpl(b);
    }

    public ByteArray getFromWord(short value) {
        return new ByteArrayImpl(Utils.convertShortToByteArray(value));
    }

    public ByteArray createByteArray(String s) {
        return new ByteArrayImpl(s);
    }
}
