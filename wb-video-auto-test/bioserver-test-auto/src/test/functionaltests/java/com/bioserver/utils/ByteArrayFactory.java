package com.bioserver.utils;

public interface ByteArrayFactory {
    ByteArray createByteArray(int var1);

    ByteArray createByteArray(byte[] var1);

    ByteArray createByteArray(ByteArray var1);

    ByteArray getFromWord(short var1);

    ByteArray createByteArray(String var1);
}
