package com.bioserver.test.core;

import com.bioserver.data.*;
import com.bioserver.utils.ConstantTestEnvironment;
import com.bioserver.utils.BioSrvErrorEnum;
import com.bioserver.utils.BiosrvException;
import com.bioserver.utils.UnsafeOkHttpClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.json.simple.JSONObject;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

import static com.bioserver.utils.ConstantTestEnvironment.*;
import static org.junit.Assert.fail;

@Slf4j
public class BioserverCoreManager {

    private static OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();
    public static String bio_session_id;
    public static String face_id;
    //public static String token;
    static ObjectMapper mapper = new ObjectMapper();
    public static Response<CreateFaceResponse> referenceResources;
    public static Response<CreateFaceResponse> candidateResources;
    //protected  static Logger log = LoggerFactory.getLogger(BioserverCoreManager.class);

//    public static String getToken() {
//
//        if(token == null) {
//            log.info(">> getToken");
//
//            Retrofit retrofit = new Retrofit.Builder()
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .baseUrl(ConstantTestEnvironment.base_url)
//                    .client(okHttpClient)
//                    .build();
//
//            BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);
//
//            //grant types = client_credentials
//            Call<TokenResponse> call = service.getToken(
//                    ConstantTestEnvironment.client_id,
//                    ConstantTestEnvironment.client_secret
//            );
//
//            try {
//                Response<TokenResponse> response = call.execute();
//
//                log.info("getToken response status : " + response.code());
//                log.info("getToken response body : " + response.body().getToken());
//
//                return response.body().getToken();
//            } catch (IOException e) {
//                fail();
//                return null;
//            }
//        } else {
//            log.info("token already exist: "+token);
//            return token;
//        }
//    }

    public static Response<JsonObject> getStatus() throws IOException, BiosrvException {
        log.info(">>  getStatus() ");

        log.info(" -- BASE URL======> "+ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        log.info("get status request");
        Call<JsonObject> call = service.getStatus();

        try {
            Response<JsonObject> getStatusResponse = call.execute();

            if(getStatusResponse.code() !=200) {
                log.info(" -- Get Status Error Message: "+getStatusResponse.message());
                throw new BiosrvException((getStatusResponse.message().equals(""))?"Get Status Error: url =>/bioserver-app/v2/monitor ":getStatusResponse.message(), getStatusResponse.errorBody(), BioSrvErrorEnum.map(getStatusResponse.code()));

            }

            log.info(" -- Get Status response code : " + getStatusResponse.code());
            return getStatusResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }
    }

    public static Response<Void> createBioSession() throws IOException, BiosrvException {
        if(bio_session_id == null) {
            log.info(">>  Create the bio-session ");

            log.info(" -- BASE URL======> " + ConstantTestEnvironment.base_url);
            Retrofit retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(ConstantTestEnvironment.base_url)
                    .client(okHttpClient)
                    .build();

            BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

            //get token
            //token = getToken();
            //
            BioSessionParams bioSessionParams = new BioSessionParams(true, "testlink");
            Call<Void> call = service.createBioSession(biosrv_api_key, bioSessionParams);

            try {
                Response<Void> response = call.execute();

                if (response.code() != 201) {
                    log.info(" -- createBioSession error message: " + response.message());
                    throw new BiosrvException((response.message().equals(""))?"Create Biosession ERROR ":response.message(), response.errorBody(), BioSrvErrorEnum.map(response.code()));

                }

                return response;
            } catch (IOException | BiosrvException e) {
                if(e instanceof BiosrvException) {
                    log.info("ERROR MESSAGE: "+e.getMessage());
                    log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
                }
                throw  e;
            }
        } else {
            log.info("bio-session already created: "+bio_session_id);
            return null;
        }
    }

    public static Response<Void> createBioSession(boolean storage) throws IOException, BiosrvException {
        if(bio_session_id == null) {
            log.info(">>  Create the bio-session ");

            log.info(" -- BASE URL======> " + ConstantTestEnvironment.base_url);
            Retrofit retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(ConstantTestEnvironment.base_url)
                    .client(okHttpClient)
                    .build();

            BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

            //get token
            //token = getToken();
            //
            BioSessionParams bioSessionParams = new BioSessionParams(storage, "testlink");
            Call<Void> call = service.createBioSession(biosrv_api_key, bioSessionParams);

            try {
                Response<Void> response = call.execute();

                if (response.code() != 201) {
                    log.info(" -- createBioSession error message: " + response.message());
                    throw new BiosrvException((response.message().equals(""))?"Create Biosession ERROR ":response.message(), response.errorBody(), BioSrvErrorEnum.map(response.code()));

                }

                return response;
            } catch (IOException | BiosrvException e) {
                if(e instanceof BiosrvException) {
                    log.info("ERROR MESSAGE: "+e.getMessage());
                    log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
                }
                throw  e;
            }
        } else {
            log.info("bio-session already created: "+bio_session_id);
            return null;
        }
    }

    public static Response<Void> createBioSession(JsonObject bioSessionParams) throws IOException, BiosrvException {
        if(bio_session_id == null) {
            log.info(">>  Create the bio-session ");

            log.info(" -- BASE URL======> " + ConstantTestEnvironment.base_url);
            Retrofit retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(ConstantTestEnvironment.base_url)
                    .client(okHttpClient)
                    .build();

            BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);


            Call<Void> call = service.createBioSession(biosrv_api_key, bioSessionParams);

            try {
                Response<Void> response = call.execute();

                if (response.code() != 201) {
                    log.info(" -- createBioSession error message: " + response.message());
                    throw new BiosrvException((response.message().equals(""))?"Create Biosession ERROR ":response.message(), response.errorBody(), BioSrvErrorEnum.map(response.code()));

                }

                return response;
            } catch (IOException | BiosrvException e) {
                if(e instanceof BiosrvException) {
                    log.info("ERROR MESSAGE: "+e.getMessage());
                    log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
                }
                throw  e;
            }
        } else {
            log.info("bio-session already created: "+bio_session_id);
            return null;
        }
    }

    public static Response<BioSessionResponse> getBioSession() throws IOException, BiosrvException {
        log.info(">>  get Bio-session ");

        log.info(" -- BASE URL======> "+ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        //create bio-session
        log.info(" -- get BioSession Id");
        getBioSessionId(true);
        // parse get bio session url
        String getBioSessionURL = ConstantTestEnvironment.bio_session_url+"/"+bio_session_id;

        log.info("get bio-session request");
        Call<BioSessionResponse> call = service.getBioSession(getBioSessionURL,biosrv_api_key);

        try {
            Response<BioSessionResponse> bioSessionResponseResponse = call.execute();

            if(bioSessionResponseResponse.code() !=200) {
                log.info(" -- getBioSession error message: "+bioSessionResponseResponse.message());
                throw new BiosrvException((bioSessionResponseResponse.message().equals(""))?"getBioSession Error: url => "+getBioSessionURL:bioSessionResponseResponse.message(), bioSessionResponseResponse.errorBody(), BioSrvErrorEnum.map(bioSessionResponseResponse.code()));

            }

            log.info(" -- get BioSession response code : " + bioSessionResponseResponse.code());
            return bioSessionResponseResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }
    }

    public static Response<JsonObject> getFace(String face_idValue, File image, File jsonFile, boolean isWrongMethod) throws IOException, BiosrvException {
        log.info(">>  get face ");

        log.info(" -- BASE URL======> "+ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        if(face_idValue == null) {
            log.info(" -- create face");
            Response<Void> response = createFace(image, jsonFile);
            log.info(" -- get face_id");
            face_idValue = readheadersResponseAndParseId(response);
        }

        // get face url
        String faceIdUrl = ConstantTestEnvironment.bio_session_url + "/" + bio_session_id + "/faces/" + face_idValue;

        log.info("get FaceId request");
        Call<JsonObject> call;
        try {
            if(!isWrongMethod) {
                call = service.getFaceId(faceIdUrl, biosrv_api_key);
            } else {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("test", "test");
                call = service.getWrongFaceId(faceIdUrl, biosrv_api_key, jsonObject);
            }
            Response<JsonObject> faceResponseResponse = call.execute();


            if(faceResponseResponse.code() !=200) {
                log.info(" -- getFaceId error message: "+faceResponseResponse.message());
                throw new BiosrvException((faceResponseResponse.message().equals(""))?"getFaceId Error: url => "+faceIdUrl:faceResponseResponse.message(), faceResponseResponse.errorBody(), BioSrvErrorEnum.map(faceResponseResponse.code()));

            }

            log.info(" -- get faceId response code : " + faceResponseResponse.code());
            return faceResponseResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }
    }

    public static Response<Void> deleteFace(boolean isWrongMethod) throws IOException, BiosrvException {
        log.info(">>  delete face ");

        log.info(" -- BASE URL======> "+ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        // get face url
        String faceIdUrl = ConstantTestEnvironment.bio_session_url + "/" + bio_session_id + "/faces/" + face_id;

        log.info("delete Face request");
        Call<Void> call = null;
        try {
            if(!isWrongMethod) {
                call = service.deleteFace(faceIdUrl, biosrv_api_key);
            } else {
                call = service.deleteFaceNoApiKey(faceIdUrl);
            }

            Response<Void> deleteFaceResponse = call.execute();

            if(deleteFaceResponse.code() !=204) {
                log.info(" -- deleteFace Error Message: "+deleteFaceResponse.message());
                throw new BiosrvException((deleteFaceResponse.message().equals(""))?"deleteFace Error: url => "+faceIdUrl:deleteFaceResponse.message(), deleteFaceResponse.errorBody(), BioSrvErrorEnum.map(deleteFaceResponse.code()));

            }

            log.info(" -- delete face response code : " + deleteFaceResponse.code());
            return deleteFaceResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }
    }

    public static Response<Void> deleteFaces(boolean isWrongMethod) throws IOException, BiosrvException {
        log.info(">>  delete faces ");

        log.info(" -- BASE URL======> "+ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        // delete faces url
        String deleteFacesUrl = ConstantTestEnvironment.bio_session_url + "/" + bio_session_id + "/faces";

        log.info("delete Faces request");
        Call<Void> call;
        try {
            if(!isWrongMethod) {
                call = service.deleteFaces(deleteFacesUrl, biosrv_api_key);
            } else {
                call = service.deleteFacesNoApiKey(deleteFacesUrl);
            }

            Response<Void> deleteFaceResponse = call.execute();

            if(deleteFaceResponse.code() !=204) {
                log.info(" -- deleteFaces Error Message: "+deleteFaceResponse.message());
                throw new BiosrvException((deleteFaceResponse.message().equals(""))?"deleteFace Error: url => "+deleteFacesUrl:deleteFaceResponse.message(), deleteFaceResponse.errorBody(), BioSrvErrorEnum.map(deleteFaceResponse.code()));

            }

            log.info(" -- delete faces response code : " + deleteFaceResponse.code());
            return deleteFaceResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }
    }

    public static Response<ResponseBody> getFaceImage(String face_idValue, File image, File jsonFile, boolean compression, boolean url2) throws IOException, BiosrvException {
        log.info(">>  get face ");

        log.info(" -- BASE URL======> "+ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        if(face_idValue == null) {
            log.info(" -- create face");
            Response<Void> response = createFace(image, jsonFile);
            log.info(" -- get face_id");
            face_idValue = readheadersResponseAndParseId(response);
        }

        String faceIdUrl = null;
        if(!url2) {
            // get face url
            faceIdUrl = ConstantTestEnvironment.bio_session_url + "/" + bio_session_id + "/faces/" + face_idValue + "/image?compression=" + compression;
        } else {
            faceIdUrl = ConstantTestEnvironment.bio_session_url + "/" + bio_session_id + "/faces/" + face_idValue + "/image";
        }

        log.info("get FaceId request: "+faceIdUrl);

        try {
            Call<ResponseBody>  call = service.getFaceImage(faceIdUrl, biosrv_api_key);
            Response<ResponseBody>  faceImageResponse = call.execute();


            if(faceImageResponse.code() !=200) {
                log.info(" -- getFaceId error message: "+faceImageResponse.message());
                throw new BiosrvException((faceImageResponse.message().equals(""))?"getFaceId Error: url => "+faceIdUrl:faceImageResponse.message(), faceImageResponse.errorBody(), BioSrvErrorEnum.map(faceImageResponse.code()));

            }

            log.info(" -- get faceId response code : " + faceImageResponse.code());
            return faceImageResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }
    }

    public static Response<JsonArray> getFaces(boolean url2) throws IOException, BiosrvException {
        log.info(">>  get faces ");

        log.info(" -- BASE URL======> "+ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        String faceIdUrl;
        if(!url2) {
            // get face url
            faceIdUrl = ConstantTestEnvironment.bio_session_url + "/" + bio_session_id + "/faces";
        } else {
            faceIdUrl = ConstantTestEnvironment.bio_session_url + "/" + bio_session_id + "/faces";
        }

        log.info("get faces request url: "+faceIdUrl);

        try {
            Call<JsonArray>  call = service.getFaces(faceIdUrl, biosrv_api_key);
            Response<JsonArray>  facesResponse = call.execute();


            if(facesResponse.code() !=200) {
                log.info(" -- getFaces Error Message: "+facesResponse.message());
                throw new BiosrvException((facesResponse.message().equals(""))?"getFaces Error: url => "+faceIdUrl:facesResponse.message(), facesResponse.errorBody(), BioSrvErrorEnum.map(facesResponse.code()));

            }

            log.info(" -- get faces response code : " + facesResponse.code());
            return facesResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }
    }

    public static Response<Void> createFace(File imageFace, File jsonFaceFile) throws IOException, BiosrvException {

        log.info(">>  Create face ");

        log.info(" -- BASE URL======> " + ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        log.info(" -- get BioSession Id");
        getBioSessionId(true);

        // parse create face url
        String createFaceURL = ConstantTestEnvironment.bio_session_url+"/"+bio_session_id+"/faces";

        //add form data parts
        MultipartBody.Builder body = new MultipartBody.Builder("AaB03x").setType(MultipartBody.FORM);
        if(imageFace != null) {
            body.addFormDataPart("image", imageFace.getName(), RequestBody.create(MediaType.parse("image/jpg"), imageFace));
        }

        if(jsonFaceFile != null) {
            body.addFormDataPart("face", jsonFaceFile.getName(), RequestBody.create(MediaType.parse("application/json"), jsonFaceFile));
        }

        Call<Void> call = service.createFace(createFaceURL,biosrv_api_key, body.build().parts());

        try {
            Response<Void> faceResponse = call.execute();

            if (faceResponse.code() != 201) {
                log.info(" --   Create Face Error message: " + faceResponse.message());
                throw new BiosrvException((faceResponse.message().equals(""))?"CreateFace Error: url => "+createFaceURL:faceResponse.message(), faceResponse.errorBody(), BioSrvErrorEnum.map(faceResponse.code()));

            }

            return faceResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;

        }

    }

    public static Response<Void> createFace2(File imageFace, File jsonFaceFile) throws IOException, BiosrvException {

        log.info(">>  Create face ");

        log.info(" -- BASE URL======> " + ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        // parse create face url
        String createFaceURL = ConstantTestEnvironment.bio_session_url+"/"+bio_session_id+"/faces";

        //add form data parts
        MultipartBody.Builder body = new MultipartBody.Builder("AaB03x").setType(MultipartBody.FORM);
        if(imageFace != null) {
            body.addFormDataPart("image", imageFace.getName(), RequestBody.create(MediaType.parse("image/jpg"), imageFace));
        }

        if(jsonFaceFile != null) {
            body.addFormDataPart("face", jsonFaceFile.getName(), RequestBody.create(MediaType.parse("application/json"), jsonFaceFile));
        }

        Call<Void> call = service.createFace(createFaceURL,biosrv_api_key, body.build().parts());

        try {
            Response<Void> faceResponse = call.execute();

            if (faceResponse.code() != 201) {
                log.info(" --   Create Face Error message: " + faceResponse.message());
                throw new BiosrvException((faceResponse.message().equals(""))?"CreateFace Error: url => "+createFaceURL:faceResponse.message(), faceResponse.errorBody(), BioSrvErrorEnum.map(faceResponse.code()));

            }

            return faceResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;

        }

    }

    public static Response<Void> createFace(String imageField, File imageFace, String faceField, File jsonFaceFile) throws IOException, BiosrvException {

        log.info(">>  Create face ");

        log.info(" -- BASE URL======> " + ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        log.info(" -- get BioSession Id");
        getBioSessionId(true);

        // parse create face url
        String createFaceURL = ConstantTestEnvironment.bio_session_url+"/"+bio_session_id+"/faces";

        //add form data parts
        MultipartBody.Builder body = new MultipartBody.Builder("AaB03x").setType(MultipartBody.FORM);
        if(imageFace != null) {
            body.addFormDataPart(imageField, imageFace.getName(), RequestBody.create(MediaType.parse("image/jpg"), imageFace));
        }

        if(jsonFaceFile != null) {
            body.addFormDataPart(faceField, jsonFaceFile.getName(), RequestBody.create(MediaType.parse("application/json"), jsonFaceFile));
        }

        Call<Void> call = service.createFace(createFaceURL,biosrv_api_key, body.build().parts());

        try {
            Response<Void> faceResponse = call.execute();

            if (faceResponse.code() != 201) {
                log.info(" --   Create Face Error message: " + faceResponse.message());
                throw new BiosrvException((faceResponse.message().equals(""))?"CreateFace Error: url => "+createFaceURL:faceResponse.message(), faceResponse.errorBody(), BioSrvErrorEnum.map(faceResponse.code()));

            }

            return faceResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;

        }

    }

    public static Response<DirectFaceResponse> createDirectFace(File jsonFile, File imageFile) throws IOException, BiosrvException {

        log.info(">>  Create Direct face Request");

        log.info(" -- Create Direct Face URL======> " + direct_face_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        // create files
        log.info(" -- Create face file");
        //File face = Paths.get(ConstantTestEnvironment.RESOURCE_PATH+"direct_face.json").toFile();
        log.info(" -- Create image file");
        //File image = Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ "image_face_1.jpg").toFile();


        //create bodies
        log.info(" -- Create face object");
        //RequestBody requestFaceFile = RequestBody.create(MediaType.parse("application/json"), jsonFile);
        log.info(" -- Create image object");
        //RequestBody requestImageFile = RequestBody.create(MediaType.parse("image/jpg"), imageFile);

        //add form data parts
//        MultipartBody body = new MultipartBody.Builder("AaB03x").setType(MultipartBody.FORM)
//                .addFormDataPart("image", imageFile.getName(), requestImageFile)
//                .addFormDataPart("face", jsonFile.getName(), requestFaceFile)
//                .addFormDataPart("correlationId", correlation_id)
//                .build();

        MultipartBody.Builder bodyBuilder = new MultipartBody.Builder("AaB03x").setType(MultipartBody.FORM);

        if(imageFile != null) {
            bodyBuilder.addFormDataPart("image", imageFile.getName(), RequestBody.create(MediaType.parse("image/jpg"), imageFile));
        }
        if(jsonFile != null) {
            bodyBuilder.addFormDataPart("face", jsonFile.getName(), RequestBody.create(MediaType.parse("application/json"), jsonFile));
        }

        if(correlation_id != null) {
            bodyBuilder
                    .addFormDataPart("correlationId", correlation_id);
        }

        Call<DirectFaceResponse> call = service.createDirectFace(biosrv_api_key, bodyBuilder.build().parts());

        try {
            Response<DirectFaceResponse> faceResponse = call.execute();

            if (faceResponse.code() != 200) {
                log.info(" --   Create Direct Face Error Message: " + faceResponse.message());
                throw new BiosrvException((faceResponse.message().equals(""))?"CreateDirectFace Error: url => "+direct_face_url:faceResponse.message(), faceResponse.errorBody(), BioSrvErrorEnum.map(faceResponse.code()));

            }

            return faceResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }

    }

    public static Response<JsonObject> directImagesMatch(File referenceImageFile, File candidateImageFile, String rImageType,
                                                         String cImageType, boolean rotation, boolean isRotationPresent) throws IOException, BiosrvException {

        log.info(">> Direct Images Match Request");

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        //add form data parts
        MultipartBody.Builder bodyBuilder = new MultipartBody.Builder("AaB03x").setType(MultipartBody.FORM);
//        MultipartBody body = new MultipartBody.Builder("AaB03x").setType(MultipartBody.FORM)
//                .addFormDataPart("referenceImage", referenceImageFile.getName(), RequestBody.create(MediaType.parse("image/*"), referenceImageFile))
//                .addFormDataPart("candidateImage", candidateImageFile.getName(), RequestBody.create(MediaType.parse("image/*"), candidateImageFile))
//                .addFormDataPart("referenceImageType", rImageType)
//                .addFormDataPart("candidateImageType", cImageType)
//                .addFormDataPart("correlationId", correlation_id)
//                .addFormDataPart("imageRotationEnabled", Boolean.toString(rotation))
//                .build();

        if(referenceImageFile != null) {
            bodyBuilder.addFormDataPart("referenceImage", referenceImageFile.getName(), RequestBody.create(MediaType.parse("image/*"), referenceImageFile));
        }
        if(candidateImageFile != null) {
            bodyBuilder.addFormDataPart("candidateImage", candidateImageFile.getName(), RequestBody.create(MediaType.parse("image/*"), candidateImageFile));
        }
        if(rImageType != null) {
            bodyBuilder.addFormDataPart("referenceImageType", rImageType);
        }
        if(cImageType != null) {
            bodyBuilder.addFormDataPart("candidateImageType", cImageType);
        }
        if(correlation_id != null) {
            bodyBuilder
                    .addFormDataPart("correlationId", correlation_id);
        }
        if(isRotationPresent) {
            bodyBuilder
                    .addFormDataPart("imageRotationEnabled", Boolean.toString(rotation));
        }

        Call<JsonObject> call = service.directImagesMatch(biosrv_api_key, bodyBuilder.build().parts());

        try {
            Response<JsonObject> directMatchResponse = call.execute();

            if (directMatchResponse.code() != 200) {
                log.info(" --   Direct Images Match Error Message: " + directMatchResponse.message());
                throw new BiosrvException((directMatchResponse.message().equals(""))?"DirectImagesMatch Error: url => "+direct_face_url:directMatchResponse.message(), directMatchResponse.errorBody(), BioSrvErrorEnum.map(directMatchResponse.code()));

            }

            return directMatchResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }

    }

    public static Response<JsonObject> directMatch(File referenceJsonFile, File candidateImageFile,
                                                   String cImageType, boolean rotation, boolean isRotationPresent, boolean isV2Version) throws IOException, BiosrvException {

        log.info(">> Direct Images Match Request");

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        //add form data parts
        MultipartBody.Builder bodyBuilder = new MultipartBody.Builder("AaB03x").setType(MultipartBody.FORM);

        if(referenceJsonFile != null) {
            bodyBuilder.addFormDataPart("referenceFace", referenceJsonFile.getName(), RequestBody.create(MediaType.parse("application/json"), referenceJsonFile));
        }
        if(candidateImageFile != null) {
            bodyBuilder.addFormDataPart("candidateImage", candidateImageFile.getName(), RequestBody.create(MediaType.parse("image/*"), candidateImageFile));
        }

        if(cImageType != null) {
            bodyBuilder.addFormDataPart("candidateImageType", cImageType);
        }
        if(correlation_id != null) {
            bodyBuilder
                    .addFormDataPart("correlationId", correlation_id);
        }
        if(isRotationPresent) {
            bodyBuilder
                    .addFormDataPart("imageRotationEnabled", Boolean.toString(rotation));
        }

        Call<JsonObject> call = null;
        if(isV2Version) {
            call = service.directMatch(biosrv_api_key, bodyBuilder.build().parts());
        } else {
            call = service.directMatchV1(biosrv_api_key, bodyBuilder.build().parts());
        }

        try {
            Response<JsonObject> directMatchResponse = call.execute();

            if (directMatchResponse.code() != 200) {
                log.info(" --   Direct Match Error Message: " + directMatchResponse.message());
                throw new BiosrvException((directMatchResponse.message().equals(""))?"DirectMatch Error: url => "+direct_face_url:directMatchResponse.message(), directMatchResponse.errorBody(), BioSrvErrorEnum.map(directMatchResponse.code()));

            }

            return directMatchResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }

    }

    public static Response<Void> postReplayMetadata(File encryptedMetadata, File encryptedMS, File deviceInfo, boolean isV2Version) throws IOException, BiosrvException {

        log.info(">> Post replay metadata Request");

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        // parse post replay metadata url
        String replayMetadataUrl = ConstantTestEnvironment.bio_session_url+"/"+bio_session_id+"/replay-liveness-metadata";
        log.info(">> Post replay metadata URL: "+replayMetadataUrl);

        //add form data parts
        MultipartBody.Builder bodyBuilder = new MultipartBody.Builder("AaB03x").setType(MultipartBody.FORM);

        if(encryptedMetadata != null) {
            bodyBuilder.addFormDataPart("encryptedMetaData", encryptedMetadata.getName(), RequestBody.create(MediaType.parse("text/plain"), encryptedMetadata));
        }
        if(encryptedMS != null) {
            bodyBuilder.addFormDataPart("encryptedMasterSecret", encryptedMS.getName(), RequestBody.create(MediaType.parse("text/plain"), encryptedMS));
        }
        if(deviceInfo != null) {
            bodyBuilder.addFormDataPart("livenessDeviceInfo", deviceInfo.getName(), RequestBody.create(MediaType.parse("application/json"), deviceInfo));
        }

        Call<Void> call = null;
        if(isV2Version) {
            call = service.postReplayMetadata(replayMetadataUrl, biosrv_api_key, bodyBuilder.build().parts());
        } else {
            call = service.postReplayMetadataV1(replayMetadataUrl, biosrv_api_key, bodyBuilder.build().parts());
        }

        try {
            Response<Void> replayMetadataResponse = call.execute();

            if (replayMetadataResponse.code() != 202) {
                log.info(" --   Post replay metadata Error Message: " + replayMetadataResponse.message());
                throw new BiosrvException((replayMetadataResponse.message().equals(""))?"Post replay metadata Error: url => "+replayMetadataUrl:replayMetadataResponse.message(), replayMetadataResponse.errorBody(), BioSrvErrorEnum.map(replayMetadataResponse.code()));

            }

            return replayMetadataResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }

    }

    public static Response<JsonObject> getReplayMetadataChallengeResponse(boolean isV2Version) throws IOException, BiosrvException {

        log.info(">> Get Replay Metadata Challenge Response");

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        // parse post replay metadata url
        String replayMetadataUrl = ConstantTestEnvironment.bio_session_url+"/"+bio_session_id+"/replay-liveness-metadata";
        log.info(">> Get Replay Metadata Challenge URL: "+replayMetadataUrl);


        Call<JsonObject> call = null;
        if(isV2Version) {
            call = service.getReplayMetadataChallengeResponse(replayMetadataUrl, biosrv_api_key);
        } else {
            call = service.getReplayMetadataChallengeResponseV1(replayMetadataUrl, biosrv_api_key);
        }

        try {
            Response<JsonObject> replayMetadataResponse = call.execute();

            if (replayMetadataResponse.code() != 200) {
                log.info(" --   Get Replay Metadata Challenge Error Message: " + replayMetadataResponse.message());
                throw new BiosrvException((replayMetadataResponse.message().equals(""))?"Get Replay Metadata Challenge Error: url => "+replayMetadataUrl:replayMetadataResponse.message(), replayMetadataResponse.errorBody(), BioSrvErrorEnum.map(replayMetadataResponse.code()));

            }

            return replayMetadataResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }

    }

    public static Response<Void> postLivenessChallengeResult(JsonObject livenessParams, boolean isV2Version) throws IOException, BiosrvException {

        log.info("------------------------------------------");
        log.info(">>  Post Liveness Challenge result Request ");
        log.info("------------------------------------------");

        log.info(" -- BASE URL======> " + ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        // parse liveness challenge params url
        String livenessUrl = ConstantTestEnvironment.bio_session_url+"/"+bio_session_id+"/liveness-challenge-result";
        log.info(">> Post Liveness Challenge result URL: "+livenessUrl);

        Call<Void> call;
        if(isV2Version) {
            call = service.postLivenessChallengeResult(livenessUrl, biosrv_api_key, livenessParams);
        } else {
            call = service.postLivenessChallengeResultV1(livenessUrl, biosrv_api_key, livenessParams);
        }

        try {
            Response<Void> response = call.execute();

            if (response.code() != 204) {
                log.info(" -- Post Liveness Challenge result Error Message: " + response.message());
                throw new BiosrvException((response.message().equals(""))?"Post Liveness Challenge result Error: "+livenessUrl:response.message(), response.errorBody(), BioSrvErrorEnum.map(response.code()));

            }

            return response;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }

    }

    public static Response<JsonObject> getLivenessChallengeResult(String livenessMode, boolean isV2Version) throws IOException, BiosrvException {

        log.info(">> Get Liveness Challenge result Response");

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        // parse post replay metadata url
        String livenessUrl = ConstantTestEnvironment.bio_session_url+"/"+bio_session_id+"/liveness-challenge-result/"+livenessMode;
        log.info(">> Get Liveness Challenge result URL: "+livenessUrl);


        Call<JsonObject> call = null;
        if(isV2Version) {
            call = service.getLivenessChallengeResult(livenessUrl, biosrv_api_key);
        } else {
            call = service.getLivenessChallengeResult(livenessUrl, biosrv_api_key);
        }

        try {
            Response<JsonObject> livenessResponse = call.execute();

            if (livenessResponse.code() != 200) {
                log.info(" --   Get Liveness Challenge result Error Message: " + livenessResponse.message());
                throw new BiosrvException((livenessResponse.message().equals(""))?"Get Liveness Challenge result Error: url => "+livenessUrl:livenessResponse.message(), livenessResponse.errorBody(), BioSrvErrorEnum.map(livenessResponse.code()));

            }

            return livenessResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }

    }


    public static Response<Void> createFaceFromJson(CreateFaceJsonParams faceJsonParams, boolean wrongMethod) throws IOException, BiosrvException {

        log.info(">>  Create face from Json ");

        log.info(" -- BASE URL======> " + ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        // parse create face url
        String faceUrl = ConstantTestEnvironment.bio_session_url+"/"+bio_session_id+"/faces";

        Call<Void> call = null;
        if(!wrongMethod) {
            call = service.createFaceFromJson(faceUrl, biosrv_api_key, faceJsonParams);
        } else{
            call = service.createFaceFromJsonWrongMethod(faceUrl, biosrv_api_key);
        }

        try {
            Response<Void> response = call.execute();

            if (response.code() != 201) {
                log.info(" -- createFaceFromJson error message: " + response.message());
                throw new BiosrvException((response.message().equals(""))?"Create Face from Json ERROR ":response.message(), response.errorBody(), BioSrvErrorEnum.map(response.code()));

            }

            return response;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }
    }

    public static Response<LivenessResponse> initLivenessParameters(JSONObject livenessParams, boolean isWrongContentType) throws IOException, BiosrvException {

        log.info("------------------------------");
        log.info(">>  Init liveness parameters ");
        log.info("------------------------------");

        log.info(" -- BASE URL======> " + ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        // parse init liveness params url
        String livenessUrl = ConstantTestEnvironment.bio_session_url+"/"+bio_session_id+"/init-liveness-parameters";
        Call<LivenessResponse> call;
        if(!isWrongContentType) {
            call = service.initLivenessParameters(livenessUrl, biosrv_api_key, livenessParams);
        } else {
            call = service.initLivenessParameters(livenessUrl, biosrv_api_key, "image/*", livenessParams);
        }

        try {
            Response<LivenessResponse> response = call.execute();

            if (response.code() != 200) {
                log.info(" -- Init liveness parameters Error Message: " + response.message());
                throw new BiosrvException((response.message().equals(""))?"Init liveness parameters Error: "+livenessUrl:response.message(), response.errorBody(), BioSrvErrorEnum.map(response.code()));

            }

            return response;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }
    }

    public static Response<LivenessResponse> initLivenessParameters2() throws IOException, BiosrvException {

        log.info("------------------------------");
        log.info(">>  Init liveness parameters ");
        log.info("------------------------------");

        log.info(" -- BASE URL======> " + ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        // parse init liveness params url
        String livenessUrl = ConstantTestEnvironment.bio_session_url+"/"+bio_session_id+"/init-liveness-parameters";

        Call<LivenessResponse> call = service.wrongInitLivenessParameters(livenessUrl, biosrv_api_key);

        try {
            Response<LivenessResponse> response = call.execute();

            if (response.code() != 200) {
                log.info(" -- Init liveness parameters Error Message: " + response.message());
                throw new BiosrvException((response.message().equals(""))?"Init liveness parameters Error: "+livenessUrl:response.message(), response.errorBody(), BioSrvErrorEnum.map(response.code()));

            }

            return response;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }
    }

    public static Response<JsonArray> getMatches(String reference_id) throws IOException, BiosrvException {
        log.info(">>  get Matches ");

        log.info(" -- BASE URL======> "+ConstantTestEnvironment.base_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ConstantTestEnvironment.base_url)
                .client(okHttpClient)
                .build();

        BioserverCoreInterface service = retrofit.create(BioserverCoreInterface.class);

        // parse get matches url
        String matchesUrl = ConstantTestEnvironment.bio_session_url+"/"+bio_session_id+"/faces/"+reference_id+"/matches";

        log.info("get matches request");
        Call<JsonArray> call = service.getMatches(matchesUrl,biosrv_api_key);

        try {
            Response<JsonArray> matchesResponseResponse = call.execute();

            if(matchesResponseResponse.code() !=200) {
                log.info(" -- getMatches error message: "+matchesResponseResponse.message());
                throw new BiosrvException((matchesResponseResponse.message().equals(""))?"getMatches Error: url => "+matchesUrl:matchesResponseResponse.message(), matchesResponseResponse.errorBody(), BioSrvErrorEnum.map(matchesResponseResponse.code()));

            }

            log.info(" -- getMatches response code : " + matchesResponseResponse.code());
            return matchesResponseResponse;
        } catch (IOException | BiosrvException e) {
            if(e instanceof BiosrvException) {
                log.info("ERROR MESSAGE: "+e.getMessage());
                log.info("ERROR CODE: "+((BiosrvException)e).getErrorCode().getNumber());
                log.info("ERROR CODE NAME: "+((BiosrvException)e).getErrorCode());
            }
            throw  e;
        }
    }

    public static void getBioSessionId(boolean storage) throws IOException, BiosrvException {
        log.info(" -- create bio-session if not exist");
        Response<Void> response = createBioSession(storage);
        if(response != null && bio_session_id == null) {
            BioserverCoreManager.bio_session_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("bio_session_id: " + BioserverCoreManager.bio_session_id);
        }
        log.info("bio_session_id already exist: " + BioserverCoreManager.bio_session_id);
    }

    public static String readheadersResponseAndParseId(Response<Void> response) {
        log.info(" -- response code : " + response.code());
        String locationUrl = response.headers().get("location");
        String id = locationUrl.substring(locationUrl.lastIndexOf('/') + 1);
        log.info(" -- parsed id: " + id);
        return id;
    }

    protected static String generateBoundary() {
        StringBuilder buffer = new StringBuilder();
        Random rand = new Random();
        int count = rand.nextInt(11) + 30; // a random size from 30 to 40
        for (int i = 0; i < count; i++) {
            buffer.append(MULTIPART_CHARS[rand.nextInt(MULTIPART_CHARS.length)]);
        }
        return buffer.toString();
    }

    protected static String generateContentType(final String boundary, final Charset charset) {
        final StringBuilder buffer = new StringBuilder();
        buffer.append("multipart/form-data; boundary=");
        buffer.append(boundary);
        if (charset != null) {
            buffer.append("; charset=");
            buffer.append(charset.name());
        }
        return buffer.toString();
    }

    private final static char[] MULTIPART_CHARS = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
}
