package com.bioserver.utils;

public class ConstantTestEnvironment {

    public static String base_url = "https://10.126.237.10";
    public static String client_id = "testuser";
    public static String client_secret = "F401MTb6QMNy8VFDMYDJ0dBQZepKDeHbTTCXRGaL+gFOCAomRITeZeJoQG3+CU2YNT2JXVzVgcZWD5LQNASqJA==";
    public static String bio_session_url = base_url+"/bioserver-app/v2/bio-sessions";
    public static String direct_face_url = base_url+"/bioserver-app/v2/faces";
    public static String wrong_token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0dXNlciIsImF1ZCI6Imh0dHBzOlwvXC9tb3JwaG8uY29tIiwiaXNzIjoiaHR0cHM6XC9cL21vcnBoby5jb20iLCJleHAiOjE2MDE5MTI4MTh9.sPMHyDCNrDt2FWzvRBPlpQjmWUfmFeVOGnwAttta_RY";
    public static String wrong_biosession_id = "aa5ad8ff-6146-4849-8fcc-39f29d7a8973";
    public static String old_biosession_id = "2a094fa9-3cc6-4d84-8a25-73dc8ed5f686";
    public static String correlation_id = "c5a0aec2-1262-41b6-82bd-e9d60da92ef2";
    public static String biosrv_api_key = "BIOSRVTEST";

    /**
     * TestLink Data
     */
    public static String testlink_url = "https://r11-testlink002.mphr11.morpho.com/tl-ced/lib/api/xmlrpc/v1/xmlrpc.php";
    public static String api_key = "9a3215648c1fbb1bcff4d227e4e7cb41";
    public static String testlink_projet_name = "BioServer";
    public static String testlink_plan_name = "BioserverTPlan";

    /**
     * Other Datas
     */
    public static String idValue = "https://r11-testlink002.mphr11.morpho.com/tl-ced/lib/api/xmlrpc/v1/xmlrpc.php";

    public static String RESOURCE_PATH = "src/test/resources/";
    public static String RESOURCE_PATH2 = "src/test/resources/noexifs/";
    public static String KEYS_PATH = "src/test/keystores/";
    public static String KEYS_PASSIVE_PATH = KEYS_PATH+"passivedata/";
    public static String KEYS_MEDIUM_PATH = KEYS_PATH+"mediumdata/";
    public static String KEYS_HIGH_PATH = KEYS_PATH+"highdata/";
}
