package com.bioserver.test.facemanagement;

import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.*;
import lombok.extern.slf4j.Slf4j;
import okhttp3.ResponseBody;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import retrofit2.Response;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.bioserver.test.core.BioserverCoreManager.bio_session_id;
import static com.bioserver.test.core.BioserverCoreManager.getBioSessionId;
import static com.bioserver.utils.ConstantTestEnvironment.biosrv_api_key;
import static com.bioserver.utils.ConstantTestEnvironment.wrong_biosession_id;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GetFaceImageTest extends BioServerBaseTest {


    @Test
    @Order(1)
    //@DisplayName("get face image should return success")
    public void bio_get_face_image_01() {

        log.debug(">> bio_get_face_image_01()");
        log.info("-----------------------------------");
        log.info(" SEND GET FACE IMAGE REQUEST");
        log.info("------------------------------------");
        try {
            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            createFaceSuccess();

            log.info("-----------------------");
            log.info("GET FACE IMAGE REQUEST");
            log.info("------------------------");

            //get face server response
            Response<ResponseBody>  faceResponse = BioserverCoreManager.getFaceImage(
                    BioserverCoreManager.face_id,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile(),
                    false, false);

            log.info("---------------------------------");
            log.info("GET FACE IMAGE RESPONSE ASSERTIONS");
            log.info("----------------------------------");

            log.info(" -- assert content type  "+faceResponse.headers().get("Content-Type"));
            Assertions.assertThat(faceResponse.headers().get("Content-Type")).isEqualTo("image/jpeg");

            //read image file
            byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1));
            log.info(" -- assert image ");
            Assertions.assertThat(faceResponse.body().bytes()).isEqualTo(imageBytes);

            runOnTestLink("BioServer-TC-70", true);
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-70", true);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(2)
    //@DisplayName("Get Face Image testcase should return error code 401 when a wrong api_key value is sent in the request.")
    public void bio_get_face_image_02() {

        log.debug(">> bio_get_face_image_02()");
        log.info("-----------------------------------");
        log.info(" SEND GET FACE IMAGE REQUEST");
        log.info("------------------------------------");
        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;
        try {

            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            createFaceSuccess();

            log.info(" -- Wrong Api_Key is used ");
            biosrv_api_key = " ";

            log.info("-----------------------");
            log.info("GET FACE IMAGE REQUEST");
            log.info("------------------------");

            //get face server response
            BioserverCoreManager.getFaceImage(
                    BioserverCoreManager.face_id,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile(),
                    false, false);

            runOnTestLink("BioServer-TC-71", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                //restore token
                biosrv_api_key = apiKeyRef;
                runOnTestLink("BioServer-TC-71", true);
            } else {
                runOnTestLink("BioServer-TC-71", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(3)
    //@DisplayName("Get Face Image testcase should return error code 400 when a wrong BioSession_Id value is sent in the request.")
    public void bio_get_face_image_03() {

        log.debug(">> bio_get_face_image_03()");
        log.info("-----------------------------------");
        log.info(" SEND GET FACE IMAGE REQUEST");
        log.info("------------------------------------");

        try {
            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            createFaceSuccess();

            log.info(" -- Wrong BioSessionId is used ");
            bio_session_id = wrong_biosession_id;

            log.info("-----------------------");
            log.info("GET FACE IMAGE REQUEST");
            log.info("------------------------");

            //get face server response
            BioserverCoreManager.getFaceImage(
                    BioserverCoreManager.face_id,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile(),
                    false, false);

            runOnTestLink("BioServer-TC-72", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());

                runOnTestLink("BioServer-TC-72", true);
            } else {
                runOnTestLink("BioServer-TC-72", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    //@DisplayName("Get Face Image testcase should return error code 400 when a wrong Face_Id value is sent in the request.")
    public void bio_get_face_image_04() {

        log.debug(">> bio_get_face_image_04()");
        log.info("-----------------------------------");
        log.info(" SEND GET FACE IMAGE REQUEST");
        log.info("------------------------------------");

        try {

            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            createFaceSuccess();

            log.info(" -- Wrong face_id is used ");

            log.info("-----------------------");
            log.info("GET FACE IMAGE REQUEST");
            log.info("------------------------");

            //get face server response
            BioserverCoreManager.getFaceImage(
                    wrong_biosession_id,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile(),
                    false, false);

            runOnTestLink("BioServer-TC-72", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());

                runOnTestLink("BioServer-TC-72", true);
            } else {
                runOnTestLink("BioServer-TC-72", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(5)
    //TODO CHECK COMPRESSION AFTER
    //@DisplayName("Get Face Image testcase should return success when the compression field is enabled in the request.")
    public void bio_get_face_image_05() {

        log.debug(">> bio_get_face_image_05()");
        log.info("-----------------------------------");
        log.info(" SEND GET FACE IMAGE REQUEST");
        log.info("------------------------------------");
        try {
            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            createFaceSuccess();

            log.info("-----------------------");
            log.info("GET FACE IMAGE REQUEST");
            log.info("------------------------");

            //get face server response
            Response<ResponseBody>  faceResponse = BioserverCoreManager.getFaceImage(
                    BioserverCoreManager.face_id,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile(),
                    true, false);

            log.info("---------------------------------");
            log.info("GET FACE IMAGE RESPONSE ASSERTIONS");
            log.info("----------------------------------");

            log.info(" -- assert content type  "+faceResponse.headers().get("Content-Type"));
            Assertions.assertThat(faceResponse.headers().get("Content-Type")).isEqualTo("image/jpeg");

            File resizedImage = File.createTempFile("compressed_image", ".jpg");
            // resize smaller by 80%
            double percent = 0.8;
            ImageResizer.resize(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile().getAbsolutePath(),
                    resizedImage.getAbsolutePath(),
                    percent
            );
            ///////////////////////////

            //read image file
            byte[] expectedImageBytes = Files.readAllBytes(resizedImage.toPath());
            log.info(" -- assert image ");
            Assertions.assertThat(faceResponse.body().bytes()).isEqualTo(expectedImageBytes);

            runOnTestLink("BioServer-TC-483", true);
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-483", true);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(6)
    //@DisplayName("Get Face Image testcase should return success and not compressed image when the compression field is disabled in the request.")
    public void bio_get_face_image_06() {

        log.debug(">> bio_get_face_image_06()");
        log.info("-----------------------------------");
        log.info(" SEND GET FACE IMAGE REQUEST");
        log.info("------------------------------------");
        try {
            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            createFaceSuccess();

            log.info("-----------------------");
            log.info("GET FACE IMAGE REQUEST");
            log.info("------------------------");

            //get face server response
            Response<ResponseBody>  faceResponse = BioserverCoreManager.getFaceImage(
                    BioserverCoreManager.face_id,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile(),
                    false, false);

            log.info("---------------------------------");
            log.info("GET FACE IMAGE RESPONSE ASSERTIONS");
            log.info("----------------------------------");

            log.info(" -- assert content type  "+faceResponse.headers().get("Content-Type"));
            Assertions.assertThat(faceResponse.headers().get("Content-Type")).isEqualTo("image/jpeg");

            File resizedImage = File.createTempFile("compressed_image", ".jpg");

            //read image file
            byte[] expectedImageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toAbsolutePath());
            log.info(" -- assert image ");
            Assertions.assertThat(faceResponse.body().bytes()).isEqualTo(expectedImageBytes);

            runOnTestLink("BioServer-TC-75", true);
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-75", true);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(7)
    //@DisplayName("Get Face Image testcase should return success when the compression field is not present in the request.")
    public void bio_get_face_image_07() {

        log.debug(">> bio_get_face_image_07()");
        log.info("-----------------------------------");
        log.info(" SEND GET FACE IMAGE REQUEST");
        log.info("------------------------------------");
        try {
            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            createFaceSuccess();

            log.info("-----------------------");
            log.info("GET FACE IMAGE REQUEST");
            log.info("------------------------");

            //get face server response
            Response<ResponseBody>  faceResponse = BioserverCoreManager.getFaceImage(
                    BioserverCoreManager.face_id,
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile(),
                    false, true);

            log.info("---------------------------------");
            log.info("GET FACE IMAGE RESPONSE ASSERTIONS");
            log.info("----------------------------------");

            log.info(" -- assert content type  "+faceResponse.headers().get("Content-Type"));
            Assertions.assertThat(faceResponse.headers().get("Content-Type")).isEqualTo("image/jpeg");

            //read image file
            byte[] expectedImageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toAbsolutePath());
            log.info(" -- assert image ");
            Assertions.assertThat(faceResponse.body().bytes()).isEqualTo(expectedImageBytes);

            runOnTestLink("BioServer-TC-76", true);
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-76", true);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(8)
    //@DisplayName("Get Face Image testcase should return error code 204 when the storage is not enabled for bioSession.")
    public void bio_get_face_image_08() {

        log.debug(">> bio_get_face_image_08()");
        log.info("-----------------------------------");
        log.info(" SEND GET FACE IMAGE REQUEST");
        log.info("------------------------------------");
        try {
            log.info(" -- get BioSession Id");
            getBioSessionId(false);

            createFaceSuccess();

            log.info("-----------------------");
            log.info("GET FACE IMAGE REQUEST");
            log.info("------------------------");

            //get face server response
            BioserverCoreManager.getFaceImage(
                    BioserverCoreManager.face_id,
                    null,
                    null,
                    false, false);

            runOnTestLink("BioServer-TC-77", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.STORAGE_NOT_ENABLED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.STORAGE_NOT_ENABLED_RESPONSE.name());
                runOnTestLink("BioServer-TC-77", true);
            } else {
                runOnTestLink("BioServer-TC-77", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    private void createFaceSuccess() throws IOException, BiosrvException {

        Response<Void> response = BioserverCoreManager.createFace2(
                Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
        );

        log.info("--------------------------");
        log.info("CREATE FACE PARSE RESPONSE");
        log.info("--------------------------");

        log.info(" -- retrieve face_id in the header response ");
        BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
        log.info("face_id: "+BioserverCoreManager.face_id);

        log.info("--------------------------------");
        log.info("CREATE FACE RESPONSE ASSERTIONS");
        log.info("--------------------------------");

        log.info(" -- assert status code = {}", response.code());
        Assertions.assertThat(response.code()).isEqualTo(201);

        log.info(" -- assert face_id format  = {}", BioserverCoreManager.face_id);
        Assertions.assertThat(isUUID(BioserverCoreManager.face_id)).isTrue();
    }
}
