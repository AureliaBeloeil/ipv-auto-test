package com.bioserver.test.core;

import com.bioserver.data.*;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.json.simple.JSONObject;
import retrofit2.Call;
import retrofit2.http.*;

import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface BioserverCoreInterface {

    @POST("/token-app/v1/authentication")
    @Deprecated
    @FormUrlEncoded
    Call<TokenResponse> getToken(@Field("client_id") String client_id, @Field("client_secret") String client_secret);

    @GET("/bioserver-app/v2/monitor")
    Call<JsonObject> getStatus();

    @POST("/bioserver-app/v2/bio-sessions")
    Call<Void> createBioSession(@Header("apikey") String apiKey, @Body BioSessionParams bioSessionParams);

    @POST("/bioserver-app/v2/bio-sessions")
    Call<Void> createBioSession(@Header("apikey") String apiKey, @Body JsonObject bioSessionParams);

    @GET
    Call<BioSessionResponse> getBioSession(@Url String url, @Header("apikey") String apiKey);

    @POST()
    @Multipart
    //@Headers("Content-Type: multipart/form-data; boundary=AaB03x")
    Call<Void> createFace(@Url String url,  /*@Header("Content-Type") String content,*/ @Header("apikey") String apiKey, @Part List<MultipartBody.Part> params/*, @Part MultipartBody.Part face*/);

    @GET
    Call<JsonObject> getFaceId(@Url String url, @Header("apikey") String apiKey);

    @POST
    Call<JsonObject> getWrongFaceId(@Url String url, @Header("apikey") String apiKey, @Body JsonObject params);

    @DELETE
    Call<Void> deleteFace(@Url String url, @Header("apikey") String apiKey);

    @DELETE
    Call<Void> deleteFaceNoApiKey(@Url String url);

    @DELETE
    Call<Void> deleteFaces(@Url String url, @Header("apikey") String apiKey);

    @DELETE
    Call<Void> deleteFacesNoApiKey(@Url String url);

    @GET
    Call<ResponseBody> getFaceImage(@Url String url, @Header("apikey") String apiKey);

    @GET
    Call<JsonArray> getFaces(@Url String url, @Header("apikey") String apiKey);

    @POST("/bioserver-app/v2/faces")
    @Multipart
    Call<DirectFaceResponse> createDirectFace(@Header("apikey") String apiKey, @Part List<MultipartBody.Part> params);

    @POST()
    Call<Void> createFaceFromJson(@Url String url, @Header("apikey") String apiKey, @Body CreateFaceJsonParams faceJsonParams);

    @GET()
    Call<Void> createFaceFromJsonWrongMethod(@Url String url, @Header("apikey") String apiKey);

    @POST()
    Call<LivenessResponse> initLivenessParameters(@Url String url, @Header("apikey") String apiKey, @Body JSONObject livenessParams);

    @POST()
    Call<LivenessResponse> initLivenessParameters(@Url String url, @Header("apikey") String apiKey, @Header("Content-Type") String type, @Body JSONObject livenessParams);

    @GET()
    Call<LivenessResponse> wrongInitLivenessParameters(@Url String url, @Header("apikey") String apiKey);

    @POST()
    Call<Void> postLivenessChallengeResult(@Url String url, @Header("apikey") String apiKey, @Body JsonObject livenessParams);

    @POST()
    Call<Void> postLivenessChallengeResultV1(@Url String url, @Header("Authorization") String apiKey, @Body JsonObject livenessParams);


    @GET
    Call<JsonArray> getMatches(@Url String url, @Header("apikey") String apiKey);

    @POST("/bioserver-app/v2/match/images")
    @Multipart
    Call<JsonObject> directImagesMatch(@Header("apikey") String apiKey, @Part List<MultipartBody.Part> params);

    @POST("/bioserver-app/v2/match")
    @Multipart
    Call<JsonObject> directMatch(@Header("apikey") String apiKey, @Part List<MultipartBody.Part> params);

    @POST("/bioserver-app/v2/match")
    @Multipart
    Call<JsonObject> directMatchV1(@Header("Authorization") String apiKey, @Part List<MultipartBody.Part> params);

    @POST()
    @Multipart
    Call<Void> postReplayMetadata(@Url String url, @Header("apikey") String apiKey, @Part List<MultipartBody.Part> params);

    @POST()
    @Multipart
    Call<Void> postReplayMetadataV1(@Url String url, @Header("Authorization") String apiKey, @Part List<MultipartBody.Part> params);

    @GET()
    Call<JsonObject> getReplayMetadataChallengeResponse(@Url String url, @Header("apikey") String apiKey);

    @GET()
    Call<JsonObject> getReplayMetadataChallengeResponseV1(@Url String url, @Header("Authorization") String apiKey);

    @GET()
    Call<JsonObject> getLivenessChallengeResult(@Url String url, @Header("apikey") String apiKey);

}
