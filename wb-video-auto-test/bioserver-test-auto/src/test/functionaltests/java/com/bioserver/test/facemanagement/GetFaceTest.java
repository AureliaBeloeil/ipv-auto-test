package com.bioserver.test.facemanagement;

import com.bioserver.data.CreateFaceResponse;
import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.BioServerBaseTest;
import com.bioserver.utils.BioSrvErrorEnum;
import com.bioserver.utils.BiosrvException;
import com.bioserver.utils.ConstantTestEnvironment;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import retrofit2.Response;

import java.nio.file.Files;
import java.nio.file.Paths;

import static com.bioserver.utils.ConstantTestEnvironment.biosrv_api_key;
import static com.bioserver.utils.ConstantTestEnvironment.wrong_biosession_id;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GetFaceTest extends BioServerBaseTest {


    @Test
    @Order(1)
    //@DisplayName("Get Face testcase should return success in case a valid request is sent.")
    public void bio_get_face_01() {

        log.debug(">> bio_get_face_01()");
        log.info("-----------------------------------");
        log.info("FORMAT AND SEND CREATE FACE REQUEST");
        log.info("------------------------------------");
        try {
            Response<Void> response = BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
            );

            log.info("--------------------------");
            log.info("CREATE FACE PARSE RESPONSE");
            log.info("--------------------------");

            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //get face server response
            Response<JsonObject>  faceResponse = BioserverCoreManager.getFace(
                    BioserverCoreManager.face_id,
                    null,
                    null,
                    false);

            log.info("-----------------------------");
            log.info("GET FACE RESPONSE ASSERTIONS");
            log.info("------------------------------");

            //read image file
            byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1));
            //read json file
            JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile());

            CreateFaceResponse mappedResponse = mapper.readValue(faceResponse.body().toString(), CreateFaceResponse.class);

            faceResourcesResponseAssertions(Response.success(mappedResponse, faceResponse.headers()), null, imageBytes, jsonNode);

            runOnTestLink("BioServer-TC-63", true);
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-63", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(2)
    //@DisplayName("Get Face testcase should return error code 401 in case a wrong api_key is passed in the request.")
    public void bio_get_face_02() {

        log.debug(">> bio_get_face_02()");
        log.info("-----------------------------------");
        log.info("FORMAT AND SEND CREATE FACE REQUEST");
        log.info("------------------------------------");
        String apiKeyRef = biosrv_api_key;
        try {

            Response<Void> response = BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
            );

            log.info("--------------------------");
            log.info("CREATE FACE PARSE RESPONSE");
            log.info("--------------------------");

            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //Send wrong apiKeyRef
            biosrv_api_key = "";

            //get face server response
            BioserverCoreManager.getFace(
                    BioserverCoreManager.face_id,
                    null,
                    null,
                    false);

            runOnTestLink("BioServer-TC-64", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                biosrv_api_key = apiKeyRef;
                runOnTestLink("BioServer-TC-64", true);
            } else {
                runOnTestLink("BioServer-TC-64", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(3)
    //@DisplayName("Get Face testcase should return error code 404 in case a wrong BioSession_Id is passed in the request.")
    public void bio_get_face_03() {

        log.debug(">> bio_get_face_03()");
        log.info("-----------------------------------");
        log.info("FORMAT AND SEND CREATE FACE REQUEST");
        log.info("------------------------------------");

        try {

            Response<Void> response = BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
            );

            log.info("--------------------------");
            log.info("CREATE FACE PARSE RESPONSE");
            log.info("--------------------------");

            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //Send wrong bioSessionId
            BioserverCoreManager.bio_session_id = wrong_biosession_id;

            //get face server response
            BioserverCoreManager.getFace(
                    BioserverCoreManager.face_id,
                    null,
                    null,
                    false);

            runOnTestLink("BioServer-TC-65", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-65", true);
            } else {
                runOnTestLink("BioServer-TC-65", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    //@DisplayName("Get Face testcase should return error code 404 in case a wrong Face_Id is passed in the request.")
    public void bio_get_face_04() {

        log.debug(">> bio_get_face_04()");
        log.info("-----------------------------------");
        log.info("FORMAT AND SEND CREATE FACE REQUEST");
        log.info("------------------------------------");

        try {

            Response<Void> response = BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
            );

            log.info("--------------------------");
            log.info("CREATE FACE PARSE RESPONSE");
            log.info("--------------------------");

            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //get face server response
            BioserverCoreManager.getFace(
                    wrong_biosession_id,
                    null,
                    null,
                    false);

            runOnTestLink("BioServer-TC-66", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-66", true);
            } else {
                runOnTestLink("BioServer-TC-66", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(5)
    //@DisplayName("Get Face testcase should return error code 404 in case an empty Face_Id is passed in the request.")
    public void bio_get_face_05() {

        log.debug(">> bio_get_face_05()");
        log.info("-----------------------------------");
        log.info("FORMAT AND SEND CREATE FACE REQUEST");
        log.info("------------------------------------");

        try {

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //get face server response
            BioserverCoreManager.getFace(
                    "",
                    null,
                    null,
                    false);

            runOnTestLink("BioServer-TC-67", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-67", true);
            } else {
                runOnTestLink("BioServer-TC-67", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(6)
    //@DisplayName("Get Face testcase should return error code 405 in case permissions are missing.")
    public void bio_get_face_07() {

        log.debug(">> bio_get_face_07()");
        log.info("-----------------------------------");
        log.info("FORMAT AND SEND CREATE FACE REQUEST");
        log.info("------------------------------------");

        try {

            Response<Void> response = BioserverCoreManager.createFace(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
            );

            log.info("--------------------------");
            log.info("CREATE FACE PARSE RESPONSE");
            log.info("--------------------------");

            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //get face server response
            BioserverCoreManager.getFace(
                    BioserverCoreManager.face_id,
                    null,
                    null,
                    true);

            runOnTestLink("BioServer-TC-69", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_ALLOWED_METHOD_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_ALLOWED_METHOD_RESPONSE.name());
                runOnTestLink("BioServer-TC-69", true);
            } else {
                runOnTestLink("BioServer-TC-69", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }
}
