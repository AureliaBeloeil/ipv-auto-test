package com.bioserver.test.videoliveness;

import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.*;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import retrofit2.Response;
import java.nio.file.Paths;

import static com.bioserver.test.core.BioserverCoreManager.*;
import static com.bioserver.utils.ConstantTestEnvironment.biosrv_api_key;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PostLivenessChallengeTest extends BioServerBaseTest {


    @Test
    @Order(1)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return success when valid request is sent.")
    public void bio_replay_liveness_challenge_01() {

        log.debug(">> bio_replay_liveness_challenge_01()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");

            JsonObject livenessChallenge = getLivenessChallengeResultRequest();


            Response<Void> response = BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            log.info("-----------------------------------");
            log.info("GET CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(204);

            runOnTestLink("BioServer-TC-199", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-199", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(2)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return error code 401 when bearer Auth value is sent.")
    public void bio_replay_liveness_challenge_02() {

        log.debug(">> bio_replay_liveness_challenge_02()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;


        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            log.info(" -- Wrong Api_Key is used ");
            biosrv_api_key = " ";

            JsonObject livenessChallenge = getLivenessChallengeResultRequest();


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, false);


            runOnTestLink("BioServer-TC-200", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                runOnTestLink("BioServer-TC-200", true);
                //restore token
                biosrv_api_key = apiKeyRef;
            } else {
                runOnTestLink("BioServer-TC-200", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(3)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return error code 401 when wrong api_key value is sent.")
    public void bio_replay_liveness_challenge_03() {

        log.debug(">> bio_replay_liveness_challenge_02()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;


        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            log.info(" -- Wrong Api_Key is used ");
            biosrv_api_key = " ";

            JsonObject livenessChallenge = getLivenessChallengeResultRequest();


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-201", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                runOnTestLink("BioServer-TC-201", true);
                //restore token
                biosrv_api_key = apiKeyRef;
            } else {
                runOnTestLink("BioServer-TC-201", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return error code 404 when wrong BioSessionId is sent.")
    public void bio_replay_liveness_challenge_05() {

        log.debug(">> bio_replay_liveness_challenge_05()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            log.info(" -- Wrong bioSessionId is used ");
            bio_session_id = " ";

            JsonObject livenessChallenge = getLivenessChallengeResultRequest();


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-203", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-203", true);

            } else {
                runOnTestLink("BioServer-TC-203", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(5)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return error code 400 when liveness result field is not sent.")
    public void bio_replay_liveness_challenge_06() {

        log.debug(">> bio_replay_liveness_challenge_06()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = new JsonObject();


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-204", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-204", true);

            } else {
                runOnTestLink("BioServer-TC-204", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(6)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return error code 400 when liveness status field is not sent.")
    public void bio_replay_liveness_challenge_07() {

        log.debug(">> bio_replay_liveness_challenge_07()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- remove liveness status field");
            livenessChallenge.remove("livenessStatus");


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-205", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-205", true);

            } else {
                runOnTestLink("BioServer-TC-205", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(7)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return success when diagnostic field is not sent.")
    public void bio_replay_liveness_challenge_08() {

        log.debug(">> bio_replay_liveness_challenge_08()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- remove diagnostic field");
            livenessChallenge.remove("diagnostic");


            Response<Void> response = BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            log.info("-----------------------------------");
            log.info("GET CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(204);

            runOnTestLink("BioServer-TC-206", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-206", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(8)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return success when bestImageId field is not sent.")
    public void bio_replay_liveness_challenge_09() {

        log.debug(">> bio_replay_liveness_challenge_09()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- remove bestImageId field");
            livenessChallenge.remove("bestImageId");


            Response<Void> response = BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            log.info("-----------------------------------");
            log.info("GET CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(204);

            runOnTestLink("BioServer-TC-207", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-207", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(9)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return success when fake webcam field is not sent.")
    public void bio_replay_liveness_challenge_10() {

        log.debug(">> bio_replay_liveness_challenge_10()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- remove fakeWebcam field");
            livenessChallenge.remove("fakeWebcam");


            Response<Void> response = BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            log.info("-----------------------------------");
            log.info("GET CHALLENGE RESPONSE ASSERTIONS");
            log.info("------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(204);

            runOnTestLink("BioServer-TC-208", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-208", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(10)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return error code 400 when liveness mode  field is not sent.")
    public void bio_replay_liveness_challenge_11() {

        log.debug(">> bio_replay_liveness_challenge_11()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- remove livenessMode field");
            livenessChallenge.remove("livenessMode");


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-209", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-209", true);

            } else {
                runOnTestLink("BioServer-TC-209", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(11)
    //TODO TICKET BIO_SRV-1816
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return error code 400 when number of challenge field is not sent.")
    public void bio_replay_liveness_challenge_12() {

        log.debug(">> bio_replay_liveness_challenge_12()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- remove numberOfChallenge field");
            livenessChallenge.remove("numberOfChallenge");


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-210", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-210", true);

            } else {
                runOnTestLink("BioServer-TC-210", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }


    @Test
    @Order(12)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return error code 400 when wrong Liveness status is sent.")
    public void bio_replay_liveness_challenge_14() {

        log.debug(">> bio_replay_liveness_challenge_14()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- add wrong livenessStatus");
            livenessChallenge.remove("livenessStatus");
            livenessChallenge.addProperty("livenessStatus", "TEST");


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-212", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-212", true);

            } else {
                runOnTestLink("BioServer-TC-212", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(13)
    //TODO TICKET BIO_SRV-1799 ==> this bug is no more relevant : this api will be removed
    //@DisplayName("Test that Post Liveness Challenge result testcase should return error code 400 when wrong bestImageId is sent.")
    public void bio_replay_liveness_challenge_16() {

        log.debug(">> bio_replay_liveness_challenge_16()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- add wrong bestImageId");
            livenessChallenge.remove("bestImageId");
            livenessChallenge.addProperty("bestImageId", 12345);


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-214", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-214", false);

            } else {
                runOnTestLink("BioServer-TC-214", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(14)
    //TODO TICKET BIO_SRV-1799
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return error code 400 when wrong fakeWebcam is sent.")
    public void bio_replay_liveness_challenge_17() {

        log.debug(">> bio_replay_liveness_challenge_17()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- add wrong fakeWebcam ");
            livenessChallenge.remove("fakeWebcam");
            livenessChallenge.addProperty("fakeWebcam", "");


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-215", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-215", true);

            } else {
                runOnTestLink("BioServer-TC-215", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(15)
    //TODO TICKET BIO_SRV-1800
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return error code 400 when wrong Liveness mode is sent.")
    public void bio_replay_liveness_challenge_18() {

        log.debug(">> bio_replay_liveness_challenge_18()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- add wrong fakeWebcam ");
            livenessChallenge.remove("livenessMode");
            livenessChallenge.addProperty("livenessMode", "PASSIVE");


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-216", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-216", true);

            } else {
                runOnTestLink("BioServer-TC-216", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(16)
    //TODO TICKET BIO_SRV-1801
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return error code 400 when wrong number of challenget is sent.")
    public void bio_replay_liveness_challenge_19() {

        log.debug(">> bio_replay_liveness_challenge_19()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- add wrong numberOfChallenge ");
            livenessChallenge.remove("numberOfChallenge");
            livenessChallenge.addProperty("numberOfChallenge", Double.parseDouble("5555555555555555"));


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-217", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-217", true);

            } else {
                runOnTestLink("BioServer-TC-217", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(17)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return the expected behavior (status code 204) when livenessStatus is set to FAILED.")
    public void bio_replay_liveness_challenge_20() {

        log.debug(">> bio_replay_liveness_challenge_20()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- Modify livenessStatus to FAILED ");
            livenessChallenge.remove("livenessStatus");
            livenessChallenge.addProperty("livenessStatus", LivenessStatusEnum.FAILED.name());


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-269", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-269", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(18)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return the expected behavior  (status code 204) when livenessStatus is set to SPOOF.")
    public void bio_replay_liveness_challenge_21() {

        log.debug(">> bio_replay_liveness_challenge_21()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- Modify livenessStatus to SPOOF ");
            livenessChallenge.remove("livenessStatus");
            livenessChallenge.addProperty("livenessStatus", LivenessStatusEnum.SPOOF.name());


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-270", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-270", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(19)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return the expected behavior  (status code 204) when livenessStatus is set to ERROR.")
    public void bio_replay_liveness_challenge_22() {

        log.debug(">> bio_replay_liveness_challenge_22()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- Modify livenessStatus to ERROR ");
            livenessChallenge.remove("livenessStatus");
            livenessChallenge.addProperty("livenessStatus", LivenessStatusEnum.ERROR.name());


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-271", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-271", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(20)
    //@DisplayName("Test that Post Liveness Challenge result​ testcase should return the expected behavior (status code 204) when livenessStatus is set to TIMEOUT.")
    public void bio_replay_liveness_challenge_23() {

        log.debug(">> bio_replay_liveness_challenge_22()");

        try {
            log.info("---------------------------------");
            log.info("CREATE BIOSESSION &  FACE RESOURCE");
            log.info("---------------------------------");

            log.info(" -- create face I");
            referenceResources = createAndGetFaceSuccess(
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH2 + IMAGE_FACE_NO_EXIF_6).toFile(),
                    Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile()
            );

            log.info("------------------------------------");
            log.info("SEND POST LIVENESS CHALLENGE REQUEST");
            log.info("-------------------------------------");


            JsonObject livenessChallenge = getLivenessChallengeResultRequest();
            log.info(" -- Modify livenessStatus to TIMEOUT ");
            livenessChallenge.remove("livenessStatus");
            livenessChallenge.addProperty("livenessStatus", LivenessStatusEnum.TIMEOUT.name());


            BioserverCoreManager.postLivenessChallengeResult(livenessChallenge, true);

            runOnTestLink("BioServer-TC-272", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-272", false);
            Assertions.fail("test failed", e);
        }
    }

    private JsonObject getLivenessChallengeResultRequest() {
        JsonObject livenessChallenge = new JsonObject();
        livenessChallenge.addProperty("livenessStatus",   "SUCCESS");
        livenessChallenge.addProperty("diagnostic",   "OK");
        livenessChallenge.addProperty("bestImageId",   referenceResources.body().getId());
        livenessChallenge.addProperty("fakeWebcam",   false);
        livenessChallenge.addProperty("livenessMode",   "LIVENESS_HIGH");
        livenessChallenge.addProperty("numberOfChallenge",   2);
        return livenessChallenge;
    }
}
