package com.bioserver.data;

public class BioSessionParams {

    boolean imageStorageEnabled;
    String correlationId;
    public BioSessionParams(boolean imageStorageEnabled, String correlationId) {
        this.imageStorageEnabled = imageStorageEnabled;
        this.correlationId = correlationId;

    }
}
