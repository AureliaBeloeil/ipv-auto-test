package com.bioserver.utils;

import com.google.protobuf.ByteString;

import java.util.Arrays;

public class ByteArrayImpl implements ByteArray {
    private byte[] buffer;
    private static ByteArrayFactory defaultByteArrayFactoryInstance = new ByteArrayFactoryImpl();

    public ByteArrayImpl(String hex) {
        this.buffer = Utils.readHexString(hex);
    }

    public ByteArrayImpl(int length) {
        this.buffer = new byte[length];
    }

    public ByteArrayImpl() {
        this(0);
    }

    public ByteArrayImpl(byte[] buf, int offset, int length) {
        this.buffer = new byte[length];
        System.arraycopy(buf, offset, this.buffer, 0, length);
    }

    public ByteArrayImpl(byte[] val) {
        this.init(val);
    }

    public ByteArrayImpl(ByteArray array) {
        if (null == array) {
            this.buffer = new byte[0];
        } else {
            this.init(array.getBytes());
        }

    }

    public static ByteArray fromByte(byte b) {
        return new ByteArrayImpl(new byte[]{b});
    }

    public static ByteArray fromByteString(ByteString bytes) {
        return new ByteArrayImpl(bytes.toByteArray());
    }

    public static ByteArray fromShort(short val) {
        ByteArray b = new ByteArrayImpl(2);
        b.setShort(0, val);
        return b;
    }

    private void init(byte[] val) {
        if (null == val) {
            this.buffer = new byte[0];
        } else {
            this.buffer = new byte[val.length];
            System.arraycopy(val, 0, this.buffer, 0, this.buffer.length);
        }
    }

    public byte[] getBytes() {
        return this.buffer;
    }

    public ByteArray copyOfRange(int from, int to) {
        int length = to - from;
        ByteArrayImpl range = new ByteArrayImpl(length);
        if (to >= this.getLength()) {
            length = this.getLength() - from;
        }

        range.copyBytes((ByteArray)this, from, 0, length);
        return range;
    }

    public byte getByte(int offset) {
        return this.buffer[offset];
    }

    public void setByte(int offset, byte val) {
        this.buffer[offset] = val;
    }

    public void setShort(int offset, short val) {
        this.buffer[offset] = (byte)(val >>> 8);
        this.buffer[offset + 1] = (byte)val;
    }

    public boolean isEqual(ByteArray val) {
        return Arrays.equals(val.getBytes(), this.buffer);
    }

    public int getLength() {
        return this.buffer.length;
    }

    public void copyBytes(ByteArray bufferToCopy, int offsetSrc, int offsetDest, int length) {
        System.arraycopy(bufferToCopy.getBytes(), offsetSrc, this.buffer, offsetDest, length);
    }

    public void copyBytes(byte[] buffer, int offsetSrc, int offsetDest, int length) {
        System.arraycopy(buffer, offsetSrc, this.buffer, offsetDest, length);
    }

    public ByteArray appendTLV(byte tag, ByteArray val) {
        this.append(TLVUtils.create(defaultByteArrayFactoryInstance, tag, val));
        return this;
    }

    public ByteArray appendTLV(byte tag, String val) {
        return this.append(TLVUtils.create(tag, val));
    }

    public ByteArray appendTLV(short tag, String val) {
        return this.append(TLVUtils.create(tag, val));
    }

    public ByteArray appendTLV(byte[] tag, ByteArray val) {
        return this.appendTLV(tag, val.getBytes());
    }

    public ByteArray append(String val) {
        return this.appendBytes(Utils.readHexString(val));
    }

    public ByteArray appendTLV(short tag, ByteArray val) {
        this.append(TLVUtils.create(defaultByteArrayFactoryInstance, tag, val));
        return this;
    }

    public ByteArray appendTLV(byte tag, short val) {
        this.append(TLVUtils.create(defaultByteArrayFactoryInstance, tag, fromShort(val)));
        return this;
    }

    public ByteArray appendTLV(byte tag, byte val) {
        this.append(TLVUtils.create(defaultByteArrayFactoryInstance, tag, fromByte(val)));
        return this;
    }

    public ByteArray appendTLV(short tag, byte[] val) {
        this.append(TLVUtils.create(defaultByteArrayFactoryInstance, tag, val));
        return this;
    }

    public ByteArray appendTLV(ByteArray tag, ByteArray val) {
        this.appendTLV(tag.getBytes(), val.getBytes());
        return this;
    }

    private ByteArray appendTLV(byte[] tag, byte[] val) {
        return this.append(TLVUtils.create(tag, val));
    }

    public ByteArray appendTLV(byte tag, byte[] val) {
        this.append(TLVUtils.create(defaultByteArrayFactoryInstance, tag, val));
        return this;
    }

    public ByteArray appendTLV(byte tag, ByteString val) {
        this.append(TLVUtils.create(defaultByteArrayFactoryInstance, tag, val.toByteArray()));
        return this;
    }

    public ByteArray appendTLV(ByteArray tag, byte[] val) {
        this.append(TLVUtils.create(defaultByteArrayFactoryInstance, tag, val));
        return this;
    }

    public ByteArray append(ByteArray val) {
        if (null != val && val.getLength() != 0) {
            int prevLength = this.buffer == null ? 0 : this.buffer.length;
            byte[] neWvalue = new byte[prevLength + val.getLength()];
            System.arraycopy(this.buffer, 0, neWvalue, 0, prevLength);
            System.arraycopy(val.getBytes(), 0, neWvalue, prevLength, val.getLength());
            this.buffer = neWvalue;
            return this;
        } else {
            return this;
        }
    }

    public void fill(byte value) {
        if (null != this.buffer) {
            int size = this.buffer.length;

            for(int i = 0; i < size; ++i) {
                this.buffer[i] = value;
            }

        }
    }

    public ByteArray appendByte(byte val) {
        this.resizeNew(this.buffer.length + 1);
        this.buffer[this.buffer.length - 1] = val;
        return this;
    }

    public ByteArray appendBytes(byte[] buffer) {
        return this.appendBytes(buffer, buffer.length);
    }

    private void resizeNew(int newCapacity) {
        byte[] newbuf = Arrays.copyOf(this.buffer, newCapacity);
        Utils.clearByteArray(newbuf);
        this.buffer = newbuf;
    }

    private void resize(int i) {
        if (null == this.buffer) {
            this.buffer = new byte[i];
        } else {
            byte[] newBuffer = new byte[i];
            System.arraycopy(this.buffer, 0, newBuffer, 0, this.buffer.length);
            Utils.clearByteArray(this.buffer);
            this.buffer = newBuffer;
        }

    }

    public ByteArray appendBytes(byte[] val, int size) {
        int old_length = this.buffer.length;
        this.resize(this.buffer.length + size);
        System.arraycopy(val, 0, this.buffer, old_length, size);
        return this;
    }

    public short getShort(int offset) {
        return Utils.readShort(this.buffer, offset);
    }

    public int getInt(int offset) {
        return Utils.readInt(this.buffer, offset);
    }

    public String getHexString() {
        return Utils.getAsHexString(this.buffer);
    }

    public String getSmallHexString() {
        return Utils.getAsSmallHexString(this.buffer);
    }

    public ByteArray clone() {
        return new ByteArrayImpl(this.getBytes());
    }

    public void clear() {
        Utils.clearByteArray(this.buffer);
    }

    public void appendByteArrayAsLV(ByteArray barray) {
        if (barray != null && barray.getLength() != 0) {
            int old_length = this.buffer.length;
            this.resize(this.buffer.length + 1 + barray.getLength());
            this.buffer[old_length] = (byte)barray.getLength();
            this.copyBytes((ByteArray)barray, 0, old_length + 1, barray.getLength());
        } else {
            this.appendByte((byte)0);
        }

    }

    public void appendBuffer(byte[] bytes, int offset, int bytes_length) {
        int old_length = this.buffer.length;
        if (offset <= bytes.length) {
            this.resize(this.buffer.length + bytes_length);
            System.arraycopy(bytes, offset, bytes, old_length, bytes_length);
        }
    }

    public ByteArray makeXor(ByteArray b) {
        ByteArray res = new ByteArrayImpl(this.getLength());

        for(int i = 0; i < res.getLength(); ++i) {
            res.setByte(i, (byte)(this.getByte(i) ^ b.getByte(i)));
        }

        return res;
    }

    public String toString() {
        return Utils.getAsHexString(this.buffer);
    }

    public ByteArray bitWiseAnd(ByteArray buffer) {
        if (buffer == null) {
            return this.clone();
        } else {
            int size = this.getLength() < buffer.getLength() ? this.getLength() : buffer.getLength();
            ByteArray res = new ByteArrayImpl(this.getLength());

            for(int i = 0; i < size; ++i) {
                res.setByte(i, (byte)(this.buffer[i] & buffer.getByte(i)));
            }

            return res;
        }
    }

    public void bitAnd(int offset, byte val) {
        byte[] var10000 = this.buffer;
        var10000[offset] &= val;
    }

    public void setBit(int offset, byte bit) {
        byte[] var10000 = this.buffer;
        var10000[offset] |= bit;
    }

    public void resetBit(int offset, byte bit) {
        byte[] var10000 = this.buffer;
        var10000[offset] = (byte)(var10000[offset] & ~bit);
    }

    public boolean isGreaterThan(ByteArray val) {
        if (null == val) {
            return true;
        } else {
            int delta = this.buffer.length - val.getLength();
            if (delta < 0) {
                return false;
            } else {
                int d;
                if (delta > 0) {
                    for(d = 0; d < delta; ++d) {
                        if (this.buffer[d] != 0) {
                            return true;
                        }
                    }
                }

                for(d = 0; d < val.getLength(); ++d) {
                    if (this.buffer[d] > val.getByte(d)) {
                        return true;
                    }

                    if (this.buffer[d] != val.getByte(d) && this.buffer[d] < val.getByte(d)) {
                        return false;
                    }
                }

                return false;
            }
        }
    }

    public String toAsciiString() {
        return new String((new ByteArrayImpl(new String(this.buffer))).getBytes());
    }

    public ByteArray appendTLV(byte[] tag, byte val) {
        return this.appendTLV((ByteArray)(new ByteArrayImpl(tag)), val);
    }

    public ByteString toByteString() {
        return ByteString.copyFrom(this.buffer);
    }

    private ByteArray appendTLV(ByteArray tag, byte val) {
        return this.append(TLVUtils.create(tag, val));
    }

    public ByteArray trimLeftZeroes() {
        int offset;
        for(offset = 0; offset < this.buffer.length && this.buffer[offset] == 0; ++offset) {
        }

        return this.copyOfRange(offset, this.buffer.length);
    }
}
