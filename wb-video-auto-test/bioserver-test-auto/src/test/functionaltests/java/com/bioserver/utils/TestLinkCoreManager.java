package com.bioserver.utils;

import br.eti.kinoshita.testlinkjavaapi.*;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.constants.ResponseDetails;
import br.eti.kinoshita.testlinkjavaapi.model.*;
import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;
import lombok.extern.slf4j.Slf4j;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.bioserver.utils.ConstantTestEnvironment.api_key;
import static com.bioserver.utils.ConstantTestEnvironment.testlink_url;

@Slf4j
public class TestLinkCoreManager {

    private static TestLinkAPI testLinkAPI;
    //private static TestLinkAPI buildName;

    public static void connect() throws TestLinkAPIException, MalformedURLException {

        testLinkAPI = new TestLinkAPI(new URL(testlink_url), api_key);
    }

    public static String createTestLinkBuild(String testProjectName, String testPlanName) {

        log.info("create new build for testlink ");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
        String timeStamp = dateFormat.format(new Date());

        Integer testPlanId = testLinkAPI.getTestPlanByName(testPlanName, testProjectName).getId();

        testLinkAPI.createBuild(testPlanId, timeStamp, "create build in order to run tests");
        log.info("create build success ");

        return timeStamp;
    }

    public static void pass(String testCaseName, String testProjectName, String testPlanName,
                             String buildName){

        log.info(">> pass(): Prepare TestCase update on testlink ");

        TestCase testCaseId = testLinkAPI.getTestCaseByExternalId(testCaseName, 1);
        log.info(" -- Testlink TestCase Name = {}  ==> TestCase Id = {}   ", testCaseId.getName(), testCaseId.getId());

        Integer testProjectId =
                testLinkAPI.getTestProjectByName(testProjectName).getId();

        Integer testPlanId =
                testLinkAPI.getTestPlanByName(testPlanName, testProjectName).getId();
        log.info(" -- Testlink TestPlan Name = {}  ==> TestPlan Id = {}   ", testPlanName, testPlanId);

//        CustomField fields = testLinkAPI.getTestCaseCustomFieldDesignValue(testCaseId.getId(), null,
//                1, testProjectId, "CF_TC_TEST_CLASS",
//                ResponseDetails.FULL);

        Platform[] platform = testLinkAPI.getProjectPlatforms(testProjectId);
        log.info(" -- Testlink TestCase Platform = {}   ", platform[0].getName());

        testLinkAPI.reportTCResult(testCaseId.getId() , null, testPlanId, ExecutionStatus.PASSED, null,
                null, buildName, "Test OK",    null, false, null, platform[0].getId(), platform[0].getName(), null/*customFields*/, false, "", null);

        log.info(" -- reportTCResult Success");
    }

    public static void fail(String testCaseName, String testProjectName, String testPlanName,
                            String buildName){

        log.info(">> fail(): Prepare TestCase update on testlink ");

        TestCase testCaseId = testLinkAPI.getTestCaseByExternalId(testCaseName, 1);
        log.info(" -- Testlink TestCase Name = {}  ==> TestCase Id = {}   ", testCaseId.getName(), testCaseId.getId());

        Integer testProjectId =
                testLinkAPI.getTestProjectByName(testProjectName).getId();

        Integer testPlanId =
                testLinkAPI.getTestPlanByName(testPlanName, testProjectName).getId();
        log.info(" -- Testlink TestPlan Name = {}  ==> TestPlan Id = {}   ", testPlanName, testPlanId);

        Platform[] platform = testLinkAPI.getProjectPlatforms(testProjectId);
        log.info(" -- Testlink TestCase Platform = {}    ", platform[0].getName());

        testLinkAPI.reportTCResult(testCaseId.getId() , null, testPlanId, ExecutionStatus.FAILED, null,
                null, buildName, "Test NOK",    null, false, null, platform[0].getId(), platform[0].getName(), null, false, "", null);
    }

    protected void addTestCaseToTestPlan(TestCase testCase, TestPlan testPlan) {
        TestProject project = testLinkAPI.getTestProjectByName(ConstantTestEnvironment.testlink_projet_name);
        testLinkAPI.addTestCaseToTestPlan(project.getId(), testPlan.getId(), testCase.getId(), testCase.getVersion(), null, null, null);
    }

    public static void displayResults(String testProjectName, String testPlanName,
                                      String buildName){

        Integer testPlanId =
                testLinkAPI.getTestPlanByName(testPlanName, testProjectName).getId();

        Build build =
                testLinkAPI.getLatestBuildForTestPlan(testPlanId);

        TestCase[] testCase =
                testLinkAPI.getTestCasesForTestPlan(testPlanId, null, build.getId(), null, null,
                        null, null, null, null, null, null);

        System.out.println("Resultat de la campagne de test demo");
        System.out.println("Titre du build: " + build.getName() + "\n");

        for(int i=0; i < testCase.length; i++){
            String testCaseResult =
                    testCase[i].getName() +": " + testCase[i].getExecutionStatus().name();
            System.out.println(testCaseResult);
        }
    }

}
