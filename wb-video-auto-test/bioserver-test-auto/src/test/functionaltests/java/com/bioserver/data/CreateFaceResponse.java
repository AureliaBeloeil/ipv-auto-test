package com.bioserver.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateFaceResponse {

    private String id;

    private String friendlyName;

    private String digest;

    private String mode;

    private String imageType;

    private int quality;

    private Object landmarks;

    private String template;

    private int templateVersion;

    private String created;

    private String expires;

    private String signature;

    private boolean imageRotationEnabled;

    public String getId() {
        return id;
    }


    public String getFriendlyName() {
        return friendlyName;
    }


    public String getDigest() {
        return digest;
    }


    public String getMode() {
        return mode;
    }


    public String getImageType() {
        return imageType;
    }


    public int getQuality() {
        return quality;
    }


    public String getTemplate() {
        return template;
    }


    public int getTemplateVersion() {
        return templateVersion;
    }


    public String getCreated() {
        return created;
    }


    public String getExpires() {
        return expires;
    }


    public String getSignature() {
        return signature;
    }


    public Object getLandmarks() {
        return landmarks;
    }

    public boolean isImageRotationEnabled() {
        return imageRotationEnabled;
    }
}
