package com.bioserver.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BioSessionResponse {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("imageStorageEnabled")
    @Expose
    private boolean imageStorageEnabled;

    @SerializedName("created")
    @Expose
    private String created;

    @SerializedName("expires")
    @Expose
    private String expires;

    @SerializedName("signature")
    @Expose
    private String signature;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isImageStorageEnabled() {
        return imageStorageEnabled;
    }

    public void setImageStorageEnabled(boolean imageStorageEnabled) {
        this.imageStorageEnabled = imageStorageEnabled;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
