package com.bioserver.data;

public class LivenessParams {
    private String type;
    private int timeout;
    private String securityLevel;
    private int nbChallenge;
    private boolean useAccurateMatch;
    private Integer matchThreshold;

    public LivenessParams() {
    }
    public LivenessParams(String type, int timeout, String securityLevel, int nbChallenge, boolean useAccurateMatch, int matchThreshold) {
        this.type = type;
        this.timeout = timeout;
        this.securityLevel = securityLevel;
        this.nbChallenge = nbChallenge;
        this.useAccurateMatch = useAccurateMatch;
        this.matchThreshold = matchThreshold;
    }

    public LivenessParams2 getLivenessObject2(String type, int timeout, String securityLevel, int nbChallenge, int matchThreshold) {
        return new LivenessParams2(type, timeout, securityLevel, nbChallenge, matchThreshold);
    }

    public LivenessParams3 getLivenessObject3(String type, int timeout, String securityLevel, int nbChallenge, boolean useAccurateMatch) {
        return new LivenessParams3(type, timeout, securityLevel, nbChallenge, useAccurateMatch);
    }

    public String getType() {
        return type;
    }

    public int getTimeout() {
        return timeout;
    }

    public String getSecurityLevel() {
        return securityLevel;
    }

    public int getNbChallenge() {
        return nbChallenge;
    }

    public boolean isUseAccurateMatch() {
        return useAccurateMatch;
    }

    public Integer getMatchThreshold() {
        return matchThreshold;
    }
}

class LivenessParams2 {
    private String type;
    private int timeout;
    private String securityLevel;
    private int nbChallenge;
    private Integer matchThreshold;


    public LivenessParams2(String type, int timeout, String securityLevel, int nbChallenge, int matchThreshold) {
        this.type = type;
        this.timeout = timeout;
        this.securityLevel = securityLevel;
        this.nbChallenge = nbChallenge;
        this.matchThreshold = matchThreshold;
    }


    public String getType() {
        return type;
    }

    public int getTimeout() {
        return timeout;
    }

    public String getSecurityLevel() {
        return securityLevel;
    }

    public int getNbChallenge() {
        return nbChallenge;
    }

    public Integer getMatchThreshold() {
        return matchThreshold;
    }
}

class LivenessParams3 {
    private String type;
    private int timeout;
    private String securityLevel;
    private int nbChallenge;
    private boolean useAccurateMatch;

    public LivenessParams3(String type, int timeout, String securityLevel, int nbChallenge, boolean useAccurateMatch) {
        this.type = type;
        this.timeout = timeout;
        this.securityLevel = securityLevel;
        this.nbChallenge = nbChallenge;
        this.useAccurateMatch = useAccurateMatch;
    }


    public String getType() {
        return type;
    }

    public int getTimeout() {
        return timeout;
    }

    public String getSecurityLevel() {
        return securityLevel;
    }

    public int getNbChallenge() {
        return nbChallenge;
    }

    public boolean isUseAccurateMatch() {
        return useAccurateMatch;
    }

}
