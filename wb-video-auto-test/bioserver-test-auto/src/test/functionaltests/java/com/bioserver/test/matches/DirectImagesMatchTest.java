package com.bioserver.test.matches;

import com.bioserver.data.ReferenceOrCandidate;
import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import retrofit2.Response;

import java.io.File;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.bioserver.test.core.BioserverCoreManager.*;
import static com.bioserver.test.core.BioserverCoreManager.face_id;
import static com.bioserver.utils.ConstantTestEnvironment.*;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DirectImagesMatchTest extends BioServerBaseTest {

    //                      FAR	        Matching threshold
    //                     0.0001%	        4500
    //                     0.001%	        4000
    //                     0.01%	        3500
    //                     0.1%	            3000
    //                     1%	            2500
    //    Matching Document /Selfie : recommanded threashold is 2500 or 3000.
    //    Matching Selfie /Selfie : recommanded threashold is 3000, 3500 or higher depending on the use case
    @Test
    @Order(1)
    //@DisplayName("Test that Direct images match testcase should return a single match object computed by 2 images.")
    public void bio_direct_images_match_01() {

        log.debug(">> bio_direct_images_match_01()");

        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get reference image file
            File refImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            Response<JsonObject> response = BioserverCoreManager.directImagesMatch(refImage, candImage, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, false, true);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, false, true, false);

            runOnTestLink("BioServer-TC-102", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-102", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(3)
    //@DisplayName("Test that Direct images match testcase should return error 401 when an invalid api_key value is sent in the request.")
    public void bio_direct_images_match_03() {

        log.debug(">> bio_direct_images_match_03()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;
        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get reference image file
            File refImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            log.info(" -- wrong token is used ");
            biosrv_api_key = "";

            BioserverCoreManager.directImagesMatch(refImage, candImage, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, false, true);

            runOnTestLink("BioServer-TC-104", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info("---------------------------------------");
                log.info("DIRECT IMAGES MATCH RESPONSE ASSERTIONS");
                log.info("---------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                //restore value
                biosrv_api_key = apiKeyRef;
                runOnTestLink("BioServer-TC-104", true);
            } else {
                runOnTestLink("BioServer-TC-104", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    //TODO TICKET BIO_SRV-1823
    //@DisplayName("Test that Direct images match testcase should return error 400 when  wrong referenceImage value is sent in the request.")
    public void bio_direct_images_match_07() {

        log.debug(">> bio_direct_images_match_07()");

        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get reference image file
            File refImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            BioserverCoreManager.directImagesMatch(refImage, candImage, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, false, true);

            runOnTestLink("BioServer-TC-108", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info("---------------------------------------");
                log.info("DIRECT IMAGES MATCH RESPONSE ASSERTIONS");
                log.info("---------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-108", true);
            } else {
                runOnTestLink("BioServer-TC-108", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(5)
    //@DisplayName("Test that Direct images match testcase should return error 400 when  no referenceImage value is sent in the request")
    public void bio_direct_images_match_08() {

        log.debug(">> bio_direct_images_match_08()");

        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            BioserverCoreManager.directImagesMatch(null, candImage, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, false, true);

            runOnTestLink("BioServer-TC-109", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info("---------------------------------------");
                log.info("DIRECT IMAGES MATCH RESPONSE ASSERTIONS");
                log.info("---------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-109", true);
            } else {
                runOnTestLink("BioServer-TC-109", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(6)
    //TODO TICKET BIO_SRV-1823
    //@DisplayName("Test that Direct images match testcase should return error 400 when  wrong candidate image value is sent in the request")
    public void bio_direct_images_match_09() {

        log.debug(">> bio_direct_images_match_09()");

        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get reference image file
            File refImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_1).toFile();

            BioserverCoreManager.directImagesMatch(refImage, candImage, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, false, true);

            runOnTestLink("BioServer-TC-110", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info("---------------------------------------");
                log.info("DIRECT IMAGES MATCH RESPONSE ASSERTIONS");
                log.info("---------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-110", true);
            } else {
                runOnTestLink("BioServer-TC-110", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(7)
    //@DisplayName("Test that Direct images match testcase should return error 400 when  no candidateImage value is sent in the request.")
    public void bio_direct_images_match_10() {

        log.debug(">> bio_direct_images_match_10()");

        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get reference image file
            File refImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            BioserverCoreManager.directImagesMatch(refImage, null, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, false, true);

            runOnTestLink("BioServer-TC-111", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info("---------------------------------------");
                log.info("DIRECT IMAGES MATCH RESPONSE ASSERTIONS");
                log.info("---------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());
                runOnTestLink("BioServer-TC-111", true);
            } else {
                runOnTestLink("BioServer-TC-111", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(8)
    //@DisplayName("Test that Direct images match testcase should return success when  no correlationId value is sent in the request")
    public void bio_direct_images_match_16() {

        log.debug(">> bio_direct_images_match_16()");

        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get reference image file
            File refImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            //send null correlation id
            log.info("no correlationId is sent ");
            correlation_id = null;

            Response<JsonObject> response = BioserverCoreManager.directImagesMatch(refImage, candImage, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, false, true);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, false, true, false);
            runOnTestLink("BioServer-TC-117", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-117", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(9)
    //@DisplayName("Test that Direct images match​ testcase should success when no imageRotation value is sent in the request.")
    public void bio_direct_images_match_18() {

        log.debug(">> bio_direct_images_match_18()");

        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get reference image file
            File refImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();

            //send null correlation id
            log.info("no correlationId is sent ");
            correlation_id = null;

            Response<JsonObject> response = BioserverCoreManager.directImagesMatch(refImage, candImage, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, false, false);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, false, true, false);
            runOnTestLink("BioServer-TC-119", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-119", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(10)
    //@DisplayName("Test that Direct images match​ testcase should return error 413 when  the request is too big.")
    public void bio_direct_images_match_26() {

        log.debug(">> bio_direct_images_match_26()");

        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get reference image file
            File refImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + BIG_IMAGE_FACE).toFile();

            BioserverCoreManager.directImagesMatch(refImage, candImage, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, false, true);

            runOnTestLink("BioServer-TC-127", false);
            Assertions.fail("TEST FAILED !!!");
        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info("---------------------------------------");
                log.info("DIRECT IMAGES MATCH RESPONSE ASSERTIONS");
                log.info("---------------------------------------");
                log.info(" -- assert status code = {}", ((BiosrvException) e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.REQUEST_TOO_BIG_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException) e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.REQUEST_TOO_BIG_RESPONSE.name());
                runOnTestLink("BioServer-TC-127", true);
            } else {
                runOnTestLink("BioServer-TC-127", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(11)
    //TODO TEST A RAJOUTER SUR TESTLINK
    //@DisplayName("Test that Direct images match​ testcase should return success when  the candidate image is rotated 90 degrees.")
    public void bio_direct_images_match_27() {

        log.debug(">> bio_direct_images_match_27()");

        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get reference image file
            File refImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_4).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_4_ROT90).toFile();

            Response<JsonObject> response = BioserverCoreManager.directImagesMatch(refImage, candImage, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, true, true);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, true, true, true);

            runOnTestLink("BioServer-TC-237", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-237", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(12)
    //TODO TEST A RAJOUTER SUR TESTLINK
    //@DisplayName("Test that Direct images match​ testcase should return success when  the reference image is rotated 90 degrees.")
    public void bio_direct_images_match_28() {

        log.debug(">> bio_direct_images_match_28()");

        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get reference image file
            File refImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_5_ROT90).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_5).toFile();

            Response<JsonObject> response = BioserverCoreManager.directImagesMatch(refImage, candImage, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, true, true);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, true, true, true);

            runOnTestLink("BioServer-TC-238", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-238", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(13)
    //TODO TEST A RAJOUTER SUR TESTLINK
    //@DisplayName("Test that Direct images match​ testcase should return success when  the reference image is rotated 180 degrees.")
    public void bio_direct_images_match_29() {

        log.debug(">> bio_direct_images_match_28()");

        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get reference image file
            File refImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_5_ROT90).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_5).toFile();

            Response<JsonObject> response = BioserverCoreManager.directImagesMatch(refImage, candImage, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, true, true);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, true, true, true);

            runOnTestLink("BioServer-TC-239", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-239", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(14)
    //TODO TEST A RAJOUTER SUR TESTLINK
    //@DisplayName("Test that Direct images match​ testcase should return success when  the candidate image is rotated 180 degrees.")
    public void bio_direct_images_match_30() {

        log.debug(">> bio_direct_images_match_30()");

        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get reference image file
            File refImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_5).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_5_ROT180).toFile();

            Response<JsonObject> response = BioserverCoreManager.directImagesMatch(refImage, candImage, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, true, true);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, true, true, true);

            runOnTestLink("BioServer-TC-240", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-240", false);
            Assertions.fail("test failed", e);
        }

    }

    @Test
    @Order(15)
    //TODO TEST A RAJOUTER SUR TESTLINK
    //@DisplayName("Test that Direct images match​ testcase should return success when  the candidate image is rotated 90 degrees and the reference image is rotated 180 degrees.")
    public void bio_direct_images_match_31() {

        log.debug(">> bio_direct_images_match_31()");

        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get reference image file
            File refImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_5_ROT180).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_5_ROT90).toFile();

            Response<JsonObject> response = BioserverCoreManager.directImagesMatch(refImage, candImage, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, true, true);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, true, true, true);

            runOnTestLink("BioServer-TC-241", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-241", false);
            Assertions.fail("test failed", e);
        }
    }
    @Test
    @Order(15)
    //TODO TEST A RAJOUTER SUR TESTLINK
    //@DisplayName("Test that Direct images match​ testcase should not match when  the candidate image is rotated 90 degrees and rotation is not enabled")
    public void bio_direct_images_match_32() {

        log.debug(">> bio_direct_images_match_32()");

        log.info("-------------------------------------------");
        log.info("FORMAT AND SEND DIRECT IMAGES MATCH REQUEST");
        log.info("-------------------------------------------");
        try {

            //get reference image file
            File refImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_5).toFile();
            //get candidate image file
            File candImage = Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_5_ROT90).toFile();

            Response<JsonObject> response = BioserverCoreManager.directImagesMatch(refImage, candImage, SELFIE_IMAGE_TYPE, SELFIE_IMAGE_TYPE, false, true);

            log.info("----------------------------------");
            log.info("PARSE DIRECT IMAGES MATCH RESPONSE");
            log.info("----------------------------------");

            //Assert content type
            log.info(" -- assert Content-Type  = {}", response.headers().get("Content-Type"));
            Assertions.assertThat(response.headers().get("Content-Type")).isEqualTo("application/json");

            JsonArray jsonResponse = new JsonArray();
            jsonResponse.add(response.body());
            matchesResponseAssertionsProcess(jsonResponse, false, true, true);

            runOnTestLink("BioServer-TC-248", true);

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-248", false);
            Assertions.fail("test failed", e);
        }
    }


    private void matchesResponseAssertionsProcess(JsonArray response, boolean rotation, boolean samePerson, boolean isImageRotated) throws JsonProcessingException {

        JsonNode responseAsJson = mapper.readTree(response.toString()).get(0);

        ReferenceOrCandidate reference = mapper.readValue(responseAsJson.get("reference").toString(), ReferenceOrCandidate.class);
        ReferenceOrCandidate candidate = mapper.readValue(responseAsJson.get("candidate").toString(), ReferenceOrCandidate.class);

        log.info("---------------------------------------");
        log.info("DIRECT IMAGES MATCH RESPONSE ASSERTIONS");
        log.info("----------------------------------------");

        log.info(" --------------------------------------------");
        log.info("   Assert Reference & Candidate Image Object");
        log.info(" --------------------------------------------");
        log.info(" -- assert reference.id  = {}", reference.getId());
        Assertions.assertThat(isUUID(reference.getId())).isTrue();
        log.info(" -- assert candidate.id  = {}", candidate.getId());
        Assertions.assertThat(isUUID(candidate.getId())).isTrue();
        log.info(" -- assert friendlyName  = {}", reference.getFriendlyName());
        Assertions.assertThat(reference.getFriendlyName()).isEqualTo(candidate.getFriendlyName());
        log.info(" -- assert digest  = {}", reference.getDigest());
        Assertions.assertThat(reference.getDigest()).isEqualTo(candidate.getDigest());
        log.info(" -- assert mode  = {}", reference.getMode());
        Assertions.assertThat(reference.getMode()).isEqualTo(candidate.getMode());
        log.info(" -- assert imageType  = {}", reference.getImageType());
        Assertions.assertThat(reference.getImageType()).isEqualTo(candidate.getImageType());
        log.info(" -- assert quality  = {}", reference.getQuality());
        Assertions.assertThat(reference.getQuality()).isGreaterThan(0);
        Assertions.assertThat(candidate.getQuality()).isGreaterThan(0);

        log.info(" -- assert reference.landmarks  = {}", reference.getLandmarks());
        Object eyes = ((LinkedHashMap)reference.getLandmarks()).get("eyes");
        landmarksAssertionsProcess((Map) eyes, rotation);

        log.info(" -- assert candidate.landmarks  = {}", candidate.getLandmarks());
        Object eyes1 = ((LinkedHashMap)candidate.getLandmarks()).get("eyes");

        landmarksAssertionsProcess((Map) eyes1, rotation);

        log.info(" -- assert reference.imageRotationEnabled  = {}", reference.isImageRotationEnabled());
        Assertions.assertThat(reference.isImageRotationEnabled()).isEqualTo(rotation);

        log.info(" -- assert candidate.imageRotationEnabled  = {}", candidate.isImageRotationEnabled());
        Assertions.assertThat(candidate.isImageRotationEnabled()).isEqualTo(rotation);

        log.info(" -- assert score  = {}", responseAsJson.get("score"));
        log.info(" -----------------------------------------------------------------------");
        log.info(" -- Score Matching Threshold SELFIE/ SELFIE must higher or equal to 3000");
        log.info(" -----------------------------------------------------------------------");
        log.info(" -- Score Matching Threshold DOCUMENT/ SELFIE is 2500 or 3000");
        log.info(" -----------------------------------------------------------");
        log.info(" -----------------------------------------------------------");
        log.info(" -- SELFIE/ SELFIE ==> FAR Threshold must less than  0.0001%");
        log.info(" -----------------------------------------------------------");
        log.info(" -------------------------------------------------------------------");
        log.info(" -- DOCUMENT/ SELFIE ==> FAR Threshold must less or equal to  0.01%");
        log.info(" -------------------------------------------------------------------");

        if(reference.getImageType().equals("SELFIE") && candidate.getImageType().equals("SELFIE") && samePerson) {

            if(isImageRotated && ! rotation) {
                log.info(" --------------------------------------------------------");
                log.info(" -- ONE IMAGE AT LEAST ARE ROTATED && ROTATION FIELD NOT SENT ");
                log.info(" --------------------------------------------------------");
                Assertions.assertThat(responseAsJson.get("score").asInt()).isLessThan(score_reference_ds_1);
                log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
                Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isBetween(far_reference_ds_2, far_ko);
            } else {
                log.info(" -------------------------------");
                log.info(" -- SELFIE/ SELFIE: SAME PERSON ");
                log.info(" -------------------------------");
                org.assertj.core.api.Assertions.assertThat(responseAsJson.get("score").asInt()).isGreaterThan(score_reference_ss);
                log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
                org.assertj.core.api.Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isLessThan(far_reference_ss);
            }

        } else if (reference.getImageType().equals("SELFIE") && candidate.getImageType().equals("SELFIE") && !samePerson) {

            log.info(" -----------------------------------");
            log.info(" -- SELFIE/ SELFIE: NOT SAME PERSON ");
            log.info(" -----------------------------------");
            Assertions.assertThat(responseAsJson.get("score").asInt()).isLessThan(score_reference_ds_1);
            log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
            Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isBetween(far_reference_ds_2, far_ko);
        } else if(reference.getImageType().equals("SELFIE") && candidate.getImageType().equals("ID_DOCUMENT") && samePerson) {
            log.info(" -----------------------------------");
            log.info(" -- SELFIE/ DOCUMENT: SAME PERSON ");
            log.info(" -----------------------------------");

            org.assertj.core.api.Assertions.assertThat(responseAsJson.get("score").asInt()).isGreaterThanOrEqualTo(score_reference_ds_2);
            log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
            org.assertj.core.api.Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isLessThan(far_reference_ds_1);
        } else if(reference.getImageType().equals("SELFIE") && candidate.getImageType().equals("ID_DOCUMENT") && !samePerson) {

            log.info(" -----------------------------------");
            log.info(" -- SELFIE/ DOCUMENT: NOT SAME PERSON ");
            log.info(" -----------------------------------");
            Assertions.assertThat(responseAsJson.get("score").asInt()).isLessThan(score_reference_ds_3);
            log.info(" -- assert falseAcceptanceRate  = {}", responseAsJson.get("falseAcceptanceRate"));
            Assertions.assertThat(responseAsJson.get("falseAcceptanceRate").asDouble()).isBetween(far_reference_ds_2, far_ko);
        }

        log.info(" -- assert correlationId  = {}", responseAsJson.get("correlationId"));
        if(correlation_id != null) {
            Assertions.assertThat(responseAsJson.get("correlationId").asText()).isEqualTo(correlation_id);
        }

        log.info(" ---------------------------");
        log.info(" Check created date format");
        log.info(" ---------------------------");
        dateFormatCheckingProcess(responseAsJson.get("created").asText());

//        log.info(" ---------------------------");
//        log.info(" Check expires date format");
//        log.info(" ---------------------------");
//
//        dateFormatCheckingProcess(responseAsJson.get("expires").asText());

        log.info(" ---------------------------");
        log.info(" Verify Signature");
        log.info(" ---------------------------");
        log.info(" -- assert signature  = {}", responseAsJson.get("signature").asText());

        log.info(" decode signature data");
        String dataToVerify = decodeTokenParts(responseAsJson.get("signature").asText());
        log.info(" -- Verify data (header+payload) with the signed data");
        verifySignature(new ByteArrayImpl(Utils.toHexString(dataToVerify)).getBytes(), signatureBytes);
    }

    @AfterEach
    public void tearDown() {
        candidateResources = null;
        referenceResources = null;
        bio_session_id = null;
        face_id = null;
    }
}
