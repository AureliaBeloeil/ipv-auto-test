package com.bioserver.test.facemanagement;

import com.bioserver.data.CreateFaceJsonParams;
import com.bioserver.data.CreateFaceResponse;
import com.bioserver.utils.BioServerBaseTest;
import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.BioSrvErrorEnum;
import com.bioserver.utils.BiosrvException;
import com.bioserver.utils.ConstantTestEnvironment;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import retrofit2.Response;

import java.nio.file.Files;
import java.nio.file.Paths;

import static com.bioserver.test.core.BioserverCoreManager.bio_session_id;
import static com.bioserver.test.core.BioserverCoreManager.getBioSessionId;
import static com.bioserver.utils.ConstantTestEnvironment.*;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CreateFaceFromJsonTest extends BioServerBaseTest {


    @Test
    @Order(1)
    //@DisplayName("create face from json should return success")
    public void bio_create_face_json_01() {

        log.debug(">> bio_create_face_json_01()");

        try {
            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND CREATE FACE FROM JSON REQUEST");
            log.info("--------------------------------------------");

            //read json file
            CreateFaceJsonParams faceJsonParams = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            Response<Void> response = BioserverCoreManager.createFaceFromJson(faceJsonParams, false);

            log.info("--------------------------------------");
            log.info("CREATE FACE FROM JSON: PARSE RESPONSE");
            log.info("--------------------------------------");
            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("-----------------------------------------");
            log.info("CREATE FACE FROM JSON RESPONSE ASSERTIONS");
            log.info("-----------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(201);

            log.info(" -- assert face_id format  = {}", BioserverCoreManager.face_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.face_id)).isTrue();

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //get face server response
            Response<JsonObject>  faceResponse = BioserverCoreManager.getFace(BioserverCoreManager.face_id, Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(), Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile(), false);

            //MAP Json file to create face object
            CreateFaceJsonParams faceJsonParams1 = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            CreateFaceResponse mappedResponse = mapper.readValue(faceResponse.body().toString(), CreateFaceResponse.class);

            log.info("-----------------------------");
            log.info("GET FACE RESPONSE ASSERTIONS");
            log.info("------------------------------");

            //read image file
            byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1));

            faceResourcesResponseAssertions(Response.success(mappedResponse, faceResponse.headers()), faceJsonParams1, imageBytes, null);

            runOnTestLink("BioServer-TC-54", true);
        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-54", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(2)
    //@DisplayName("Create Face from Json testcase should return error code 401 in case an invalid api_key is sent.")
    public void bio_create_face_json_02() {

        log.debug(">> bio_create_face_json_02()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;
        try {

            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            log.info(" -- Wrong Api_Key is used ");
            biosrv_api_key = " ";

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND CREATE FACE FROM JSON REQUEST");
            log.info("--------------------------------------------");
            //read json file
            CreateFaceJsonParams faceJsonParams = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            BioserverCoreManager.createFaceFromJson(faceJsonParams, false);

            runOnTestLink("BioServer-TC-55", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                //restore token
                biosrv_api_key = apiKeyRef;
                runOnTestLink("BioServer-TC-55", true);
            } else {
                runOnTestLink("BioServer-TC-55", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(3)
    //@DisplayName("Create Face from Json testcase should return error code 404 in case a wrong BioSession_Id is passed in the request.")
    public void bio_create_face_json_04() {

        log.debug(">> bio_create_face_json_04()");

        try {

            log.info(" -- Wrong BioSessionId is used ");
            bio_session_id = wrong_biosession_id;

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND CREATE FACE FROM JSON REQUEST");
            log.info("--------------------------------------------");
            //read json file
            CreateFaceJsonParams faceJsonParams = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            BioserverCoreManager.createFaceFromJson(faceJsonParams, false);

            runOnTestLink("BioServer-TC-57", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());

                runOnTestLink("BioServer-TC-57", true);
            } else {
                runOnTestLink("BioServer-TC-57", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    //@DisplayName("Create Face from Json testcase should return error code 404 in case an old BioSession_Id is passed in the request.")
    public void bio_create_face_json_05() {

        log.debug(">> bio_create_face_json_05()");

        try {

            log.info(" -- Old BioSessionId is used ");
            bio_session_id = old_biosession_id;

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND CREATE FACE FROM JSON REQUEST");
            log.info("--------------------------------------------");
            //read json file
            CreateFaceJsonParams faceJsonParams = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            BioserverCoreManager.createFaceFromJson(faceJsonParams, false);

            runOnTestLink("BioServer-TC-58", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());

                runOnTestLink("BioServer-TC-58", true);
            } else {
                runOnTestLink("BioServer-TC-58", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(5)
    //@DisplayName("Create Face from Json testcase should return error code 400 in case a wrong mode is passed in the request.")
    public void bio_create_face_json_07() {

        log.debug(">> bio_create_face_json_07()");

        try {

            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND CREATE FACE FROM JSON REQUEST");
            log.info("--------------------------------------------");
            //read json file
            CreateFaceJsonParams faceJsonParams = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            log.info(" -- wrong Digest is used ");
            faceJsonParams.setMode("");

            BioserverCoreManager.createFaceFromJson(faceJsonParams, false);

            runOnTestLink("BioServer-TC-60", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());

                runOnTestLink("BioServer-TC-60", true);
            } else {
                runOnTestLink("BioServer-TC-60", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(6)
    //@DisplayName("Create Face from Json testcase should return error code 400 in case a wrong Id is passed in the request.")
    public void bio_create_face_json_08() {

        log.debug(">> bio_create_face_json_08()");

        try {

            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND CREATE FACE FROM JSON REQUEST");
            log.info("--------------------------------------------");
            //read json file
            CreateFaceJsonParams faceJsonParams = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            log.info(" -- wrong signature is used ");
            faceJsonParams.setSignature("");

            Response<Void> response = BioserverCoreManager.createFaceFromJson(faceJsonParams, false);

            log.info("--------------------------------------");
            log.info("CREATE FACE FROM JSON: PARSE RESPONSE");
            log.info("--------------------------------------");
            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("-----------------------------------------");
            log.info("CREATE FACE FROM JSON RESPONSE ASSERTIONS");
            log.info("-----------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(201);

            log.info(" -- assert face_id format  = {}", BioserverCoreManager.face_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.face_id)).isTrue();

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //get face server response
            Response<JsonObject>  faceResponse = BioserverCoreManager.getFace(BioserverCoreManager.face_id, Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(), Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile(), false);

            //MAP Json file to create face object
            CreateFaceJsonParams faceJsonParams1 = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            CreateFaceResponse mappedResponse = mapper.readValue(faceResponse.body().toString(), CreateFaceResponse.class);

            log.info("-----------------------------");
            log.info("GET FACE RESPONSE ASSERTIONS");
            log.info("------------------------------");

            //read image file
            byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1));

            faceResourcesResponseAssertions(Response.success(mappedResponse, faceResponse.headers()), faceJsonParams1, imageBytes, null);

            runOnTestLink("BioServer-TC-61", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-61", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(7)
    //@DisplayName("Create Face from Json testcase should return error code 400 in case a wrong Id is passed in the request.")
    public void bio_create_face_json_09() {

        log.debug(">> bio_create_face_json_09()");

        try {

            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND CREATE FACE FROM JSON REQUEST");
            log.info("--------------------------------------------");
            //read json file
            CreateFaceJsonParams faceJsonParams = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            log.info(" -- wrong Id is used ");
            faceJsonParams.setId("");

            Response<Void> response = BioserverCoreManager.createFaceFromJson(faceJsonParams, false);

            log.info("--------------------------------------");
            log.info("CREATE FACE FROM JSON: PARSE RESPONSE");
            log.info("--------------------------------------");
            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("-----------------------------------------");
            log.info("CREATE FACE FROM JSON RESPONSE ASSERTIONS");
            log.info("-----------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(201);

            log.info(" -- assert face_id format  = {}", BioserverCoreManager.face_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.face_id)).isTrue();

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //get face server response
            Response<JsonObject>  faceResponse = BioserverCoreManager.getFace(BioserverCoreManager.face_id, Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(), Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile(), false);

            //MAP Json file to create face object
            CreateFaceJsonParams faceJsonParams1 = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            CreateFaceResponse mappedResponse = mapper.readValue(faceResponse.body().toString(), CreateFaceResponse.class);

            log.info("-----------------------------");
            log.info("GET FACE RESPONSE ASSERTIONS");
            log.info("------------------------------");

            //read image file
            byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1));

            faceResourcesResponseAssertions(Response.success(mappedResponse, faceResponse.headers()), faceJsonParams1, imageBytes, null);

            runOnTestLink("BioServer-TC-478", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-478", false);
            Assertions.fail("test failed", e);
        }
    }

    @Test
    @Order(8)
    //@DisplayName("Create Face from Json testcase should return error code 400 in case a wrong imageType is passed in the request.")
    public void bio_create_face_json_10() {

        log.debug(">> bio_create_face_json_10()");

        try {

            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND CREATE FACE FROM JSON REQUEST");
            log.info("--------------------------------------------");
            //read json file
            CreateFaceJsonParams faceJsonParams = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            log.info(" -- wrong imageType is used ");
            faceJsonParams.setImageType("");

            BioserverCoreManager.createFaceFromJson(faceJsonParams, false);

            runOnTestLink("BioServer-TC-479", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());

                runOnTestLink("BioServer-TC-479", true);
            } else {
                runOnTestLink("BioServer-TC-479", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(9)
    //@DisplayName("Create Face from Json testcase should return error code 400 in case a wrong template is passed in the request.")
    public void bio_create_face_json_11() {

        log.debug(">> bio_create_face_json_11()");

        try {

            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND CREATE FACE FROM JSON REQUEST");
            log.info("--------------------------------------------");
            //read json file
            CreateFaceJsonParams faceJsonParams = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            log.info(" -- wrong template is used ");
            faceJsonParams.setTemplate("");

            BioserverCoreManager.createFaceFromJson(faceJsonParams, false);

            runOnTestLink("BioServer-TC-480", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());

                runOnTestLink("BioServer-TC-480", true);
            } else {
                runOnTestLink("BioServer-TC-480", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(10)
    //@DisplayName("Create Face from Json testcase should return error code 400 in case a wrong templateVersion is passed in the request.")
    public void bio_create_face_json_12() {

        log.debug(">> bio_create_face_json_12()");

        try {

            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND CREATE FACE FROM JSON REQUEST");
            log.info("--------------------------------------------");
            //read json file
            CreateFaceJsonParams faceJsonParams = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            log.info(" -- wrong template is used ");
            faceJsonParams.setTemplateVersion(0);

            BioserverCoreManager.createFaceFromJson(faceJsonParams, false);

            runOnTestLink("BioServer-TC-481", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.BAD_REQUEST_RESPONSE.name());

                runOnTestLink("BioServer-TC-481", true);
            } else {
                runOnTestLink("BioServer-TC-481", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(11)
    //@DisplayName("Create Face from Json testcase should return error code 400 in case a wrong created date is passed in the request.")
    public void bio_create_face_json_13() {

        log.debug(">> bio_create_face_json_13()");

        try {

            log.info(" -- get BioSession Id");
            getBioSessionId(true);

            log.info("--------------------------------------------");
            log.info("FORMAT AND SEND CREATE FACE FROM JSON REQUEST");
            log.info("--------------------------------------------");
            //read json file
            CreateFaceJsonParams faceJsonParams = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            log.info(" -- wrong created date is used ");
            faceJsonParams.setCreated("");

            Response<Void> response = BioserverCoreManager.createFaceFromJson(faceJsonParams, false);

            log.info("--------------------------------------");
            log.info("CREATE FACE FROM JSON: PARSE RESPONSE");
            log.info("--------------------------------------");
            log.info(" -- retrieve face_id in the header response ");
            BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
            log.info("face_id: "+BioserverCoreManager.face_id);

            log.info("-----------------------------------------");
            log.info("CREATE FACE FROM JSON RESPONSE ASSERTIONS");
            log.info("-----------------------------------------");

            log.info(" -- assert status code = {}", response.code());
            org.assertj.core.api.Assertions.assertThat(response.code()).isEqualTo(201);

            log.info(" -- assert face_id format  = {}", BioserverCoreManager.face_id);
            Assertions.assertThat(isUUID(BioserverCoreManager.face_id)).isTrue();

            log.info("--------------------");
            log.info("GET FACE RESOURCES");
            log.info("---------------------");

            //get face server response
            Response<JsonObject>  faceResponse = BioserverCoreManager.getFace(BioserverCoreManager.face_id, Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1).toFile(), Paths.get(ConstantTestEnvironment.RESOURCE_PATH + JSON_FACE_2).toFile(), false);

            //MAP Json file to create face object
            CreateFaceJsonParams faceJsonParams1 = mapper.readValue(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_1).toFile(), CreateFaceJsonParams.class);

            CreateFaceResponse mappedResponse = mapper.readValue(faceResponse.body().toString(), CreateFaceResponse.class);

            log.info("-----------------------------");
            log.info("GET FACE RESPONSE ASSERTIONS");
            log.info("------------------------------");

            //read image file
            byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1));

            faceResourcesResponseAssertions(Response.success(mappedResponse, faceResponse.headers()), faceJsonParams1, imageBytes, null);

            runOnTestLink("BioServer-TC-482", true);

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-482", false);
            Assertions.fail("test failed", e);
        }
    }
}
