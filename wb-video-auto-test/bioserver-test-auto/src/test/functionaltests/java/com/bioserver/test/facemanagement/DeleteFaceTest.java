package com.bioserver.test.facemanagement;

import com.bioserver.data.CreateFaceResponse;
import com.bioserver.test.core.BioserverCoreManager;
import com.bioserver.utils.BioServerBaseTest;
import com.bioserver.utils.BioSrvErrorEnum;
import com.bioserver.utils.BiosrvException;
import com.bioserver.utils.ConstantTestEnvironment;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import retrofit2.Response;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.bioserver.test.core.BioserverCoreManager.bio_session_id;
import static com.bioserver.test.core.BioserverCoreManager.face_id;
import static com.bioserver.utils.ConstantTestEnvironment.biosrv_api_key;
import static com.bioserver.utils.ConstantTestEnvironment.wrong_biosession_id;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DeleteFaceTest extends BioServerBaseTest {


    @Test
    @Order(1)
    //@DisplayName("Delete Face testcase should return success (status code 204) when a valid request is sent.")
    public void bio_delete_face_01() {

        log.debug(">> bio_delete_face_01()");

        try {
            createFaceResourceProcess();

            log.info("--------------------");
            log.info("DELETE FACE REQUEST");
            log.info("---------------------");

            Response<Void>  delFaceResponse = BioserverCoreManager.deleteFace(false);

            log.info(" -- assert status code = {}", delFaceResponse.code());
            org.assertj.core.api.Assertions.assertThat(delFaceResponse.code()).isEqualTo(204);

            try{
                log.info(" -- try to send get face ==> and check that 404 is returned by the server");
                BioserverCoreManager.getFace(
                        BioserverCoreManager.face_id,
                        null,
                        null,
                        false);

            } catch (Throwable e) {
                if(e instanceof BiosrvException) {
                    //e.printStackTrace(System.out);
                    log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                    Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                    Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                    runOnTestLink("BioServer-TC-84", true);
                } else {
                    runOnTestLink("BioServer-TC-84", false);
                    Assertions.fail("TEST FAILED !!!", e);
                }
            }

        } catch (Throwable e) {
            if (e instanceof BiosrvException) {
                e.printStackTrace(System.out);
            }
            runOnTestLink("BioServer-TC-84", false);
            Assertions.fail("Test failed: ",  e);
        }
    }

    @Test
    @Order(2)
    //@DisplayName("Delete Face testcase should return error code 401 when an invalid api_key value is sent.")
    public void bio_delete_face_02() {

        log.debug(">> bio_delete_face_02()");

        //store biosrv_api_key
        String apiKeyRef = biosrv_api_key;
        try {
            createFaceResourceProcess();

            log.info("--------------------");
            log.info("DELETE FACE REQUEST");
            log.info("---------------------");

            log.info(" -- Wrong Api_Key is used ");
            biosrv_api_key = " ";

            BioserverCoreManager.deleteFace(false);

            runOnTestLink("BioServer-TC-85", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                //restore token
                biosrv_api_key = apiKeyRef;
                runOnTestLink("BioServer-TC-85", true);
            } else {
                runOnTestLink("BioServer-TC-85", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(3)
    //@DisplayName("Delete Face testcase should return error code 404 when a wrong BioSession_Id  is sent in the request.")
    public void bio_delete_face_03() {

        log.debug(">> bio_delete_face_03()");

        try {
            createFaceResourceProcess();

            log.info("--------------------");
            log.info("DELETE FACE REQUEST");
            log.info("---------------------");

            //use wrong biosession_id
            bio_session_id = wrong_biosession_id;

            BioserverCoreManager.deleteFace(false);

            runOnTestLink("BioServer-TC-86", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-86", true);
            } else {
                runOnTestLink("BioServer-TC-86", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(4)
    //@DisplayName("Delete Face testcase should return error code 404 when a wrong Face_Id  is sent in the request.")
    //TODO TICKET BIO_SRV-1917
    public void bio_delete_face_04() {

        log.debug(">> bio_delete_face_04()");

        try {
            createFaceResourceProcess();

            log.info("--------------------");
            log.info("DELETE FACE REQUEST");
            log.info("---------------------");

            //use wrong face_id
            face_id = wrong_biosession_id;

            BioserverCoreManager.deleteFace(false);

            runOnTestLink("BioServer-TC-87", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.NOT_FOUND_RESPONSE.name());
                runOnTestLink("BioServer-TC-87", true);
            } else {
                runOnTestLink("BioServer-TC-87", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    @Test
    @Order(5)
    //@DisplayName("Delete Face testcase should return error code 401 when no api_key value is sent in the request.")
    public void bio_delete_face_05() {

        log.debug(">> bio_delete_face_05()");

        try {
            createFaceResourceProcess();

            log.info("--------------------");
            log.info("DELETE FACE REQUEST");
            log.info("---------------------");

            BioserverCoreManager.deleteFace(true);

            runOnTestLink("BioServer-TC-88", false);
            Assertions.fail("TEST FAILED !!!");

        } catch (Throwable e) {
            if(e instanceof BiosrvException) {
                //e.printStackTrace(System.out);
                log.info(" -- assert status code = {}", ((BiosrvException)e).getErrorCode().getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().getNumber()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.getNumber());
                Assertions.assertThat(((BiosrvException)e).getErrorCode().name()).isEqualTo(BioSrvErrorEnum.AUTHENTICATION_REQUIRED_RESPONSE.name());
                runOnTestLink("BioServer-TC-88", true);
            } else {
                runOnTestLink("BioServer-TC-88", false);
                Assertions.fail("TEST FAILED !!!", e);
            }
        }
    }

    private Response<Void> createFaceResourceProcess() throws IOException, BiosrvException {
        log.info("-----------------------------------");
        log.info("FORMAT AND SEND CREATE FACE REQUEST");
        log.info("------------------------------------");
        Response<Void> response = BioserverCoreManager.createFace(
                Paths.get(ConstantTestEnvironment.RESOURCE_PATH+IMAGE_FACE_1).toFile(),
                Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile()
        );

        log.info("--------------------------");
        log.info("CREATE FACE PARSE RESPONSE");
        log.info("--------------------------");

        log.info(" -- retrieve face_id in the header response ");
        BioserverCoreManager.face_id = BioserverCoreManager.readheadersResponseAndParseId(response);
        log.info("face_id: "+BioserverCoreManager.face_id);

        log.info("--------------------");
        log.info("GET FACE RESOURCES");
        log.info("---------------------");

        //get face server response
        Response<JsonObject>  faceResponse = BioserverCoreManager.getFace(
                BioserverCoreManager.face_id,
                null,
                null,
                false);

        CreateFaceResponse mappedResponse = mapper.readValue(faceResponse.body().toString(), CreateFaceResponse.class);

        log.info("-----------------------------");
        log.info("GET FACE RESPONSE ASSERTIONS");
        log.info("------------------------------");

        //read image file
        byte[] imageBytes = Files.readAllBytes(Paths.get(ConstantTestEnvironment.RESOURCE_PATH + IMAGE_FACE_1));
        //read json file
        JsonNode jsonNode = mapper.readTree(Paths.get(ConstantTestEnvironment.RESOURCE_PATH+ JSON_FACE_2).toFile());

        faceResourcesResponseAssertions(Response.success(mappedResponse, faceResponse.headers()), null, imageBytes, jsonNode);
        return response;
    }
}
