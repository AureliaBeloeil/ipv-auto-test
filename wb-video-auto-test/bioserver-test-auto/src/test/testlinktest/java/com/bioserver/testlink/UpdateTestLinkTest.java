package com.bioserver.testlink;

import com.bioserver.utils.ConstantTestEnvironment;
import com.bioserver.utils.TestLinkCoreManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class UpdateTestLinkTest {

    static String buildName;
    static boolean isTestLinkEnabled = true;
    Logger log = LoggerFactory.getLogger(getClass());


    @BeforeAll
    static void setUp() throws MalformedURLException {
        TestLinkCoreManager.connect();
        buildName = TestLinkCoreManager.createTestLinkBuild(ConstantTestEnvironment.testlink_projet_name, ConstantTestEnvironment.testlink_plan_name);
    }

    public void runOnTestLink(String testCase, boolean success) {

        if (isTestLinkEnabled) {
            if (success) {
                log.info(" -- Test Case: {}  | Test Success Reported", testCase);
                TestLinkCoreManager.pass(testCase, ConstantTestEnvironment.testlink_projet_name, ConstantTestEnvironment.testlink_plan_name, buildName);
            } else {
                log.info(" -- Test Case: {}  | Test Failed Reported", testCase);
                TestLinkCoreManager.fail(testCase, ConstantTestEnvironment.testlink_projet_name, ConstantTestEnvironment.testlink_plan_name, buildName);
            }
        }

    }
    @Test
    public void readHtmlReports() throws IOException {

        log.info("------------------------");
        log.info("Update TestLink");
        log.info("------------------------");

        List<String> allLines = Files.readAllLines(Paths.get("src/test/resources/testcases.txt"));

        File reports = Paths.get("build/reports/tests/test/classes").toFile();
        File[] reportsList = reports.listFiles();

        log.info("list files: "+reportsList);

        for (int i = 0; i < reportsList.length; i++) {
            if(reportsList[i].isFile()){
                log.info("html file to parse: "+reportsList[i].getName());
                parseHtmlAndUpdateTestlink(reportsList[i], allLines.get(i));
            }
        }

    }

    private void parseHtmlAndUpdateTestlink(File inputFile, String testCaseName) throws IOException {

        Document doc = Jsoup.parse(inputFile, "utf-8");
        List<String> results = doc.select("td[class]").eachText();
        for(int i=0; i<results.size(); i++) {
            String testname = results.get(i);
            if (testname.contains("()")) {
                if(results.get(i+2).equals("passed")) {
                    String testCaseName1 = testname.substring(0, testname.lastIndexOf("("));
                    log.info("Test case to update on Testlink: "+testCaseName1);
                    runOnTestLink(testCaseName1, true);
                } else if(results.get(i+2).equals("failed")) {
                    String testCaseName1 = testname.substring(0, testname.lastIndexOf("("));
                    log.info("Test case to update on Testlink: "+testCaseName1);
                    runOnTestLink(testCaseName1, false);
                } else {
                    //do nothing
                    log.info("check the results state: "+results.get(i+2));
                }
            }
        }
    }

    public void writeOnFile() throws IOException {
        File dir = Paths.get("src/test/resources").toFile();
        File file = new File(dir, "testcases.txt");
        FileWriter fileWriter = new FileWriter(file);
        int SNIndexlength = "Serial_Number".length();
        SNIndexlength = SNIndexlength-3+20;
        fileWriter.write(String.format("%s %20s %20s  \r\n", "Serial_Number", "Name", "Count"));

        List<Data> datas = new ArrayList<Data>();
        datas.add(new Data("001", "getToken", 1));
        datas.add(new Data("002", "createBioSession", 2));
        datas.add(new Data("003", "getBioSessionShouldSuccess", 3));
        datas.add(new Data("004", "createFaceShouldSuccess", 4));
        datas.add(new Data("005", "TEST5", 5));

        for (Data data : datas) {
            fileWriter.write(String.format("%s %"+SNIndexlength+"s %20s  \r\n", data.sNum, data.name, data.count));
        }
        fileWriter.flush();
        fileWriter.close();
    }

    static class Data{
        public String sNum;
        public String name;
        public int count;
        public Data(String no, String name, int count){
            this.sNum = no;
            this.name = name;
            this.count = count;
        }
    }
}
