@Library( "bioserver@")
import com.bioserver.*

node('10.126.237.10') {

    echo "create bioserver lib instance"
    def library = new com.bioserver.setup()

    properties([
            parameters([
                    stringParam(
                            defaultValue: "latest",
                            description: 'get video core image version',
                            name: 'video_core_image_version'
                    ),
                    stringParam(
                            defaultValue: "latest",
                            description: 'get video server image version',
                            name: 'video_server_image_version'
                    ),
                    stringParam(
                            defaultValue: "latest",
                            description: 'get video demo image version',
                            name: 'video_demo_image_version'
                    ),
                    stringParam(
                            defaultValue: "latest",
                            description: 'get video coturn image version',
                            name: 'video_coturn_image_version'
                    ),
                    stringParam(
                            defaultValue: "latest",
                            description: 'get bioserver-video-deploy project version',
                            name: 'biosrv_video_deploy_version'
                    ),
                    stringParam(
                            defaultValue: "latest",
                            description: 'get bioserver-docker project version',
                            name: 'biosrv_docker_version'
                    ),
            ])
    ])

    // test the false value

    def config = [
            'core': params.video_core_image_version,
            'server': params.video_server_image_version,
            'demo': params.video_demo_image_version,
            'coturn': params.video_coturn_image_version,
            'docker': params.biosrv_docker_version,
            'deploy': params.biosrv_video_deploy_version
            ]

    dir("/var/lib/docker/bioServerEnvJenkinsJob") {
        stage("checkout SCM") {
            checkout scm
        }
    }

    dir("/media/nightly/dockerSetup") {
        sh "pwd"

        echo "call makeAllMultiBranch function"
        //def deploy = load 'jenkins/deployments.groovy'
        library.makeAllMultiBranch(config)

    }


    dir("/var/lib/docker/bioServerEnvJenkinsJob") {
        stage("Wait Until Setup finalized") {

            sh "sleep 6"
            //sh(returnStdout: false, script: 'cd ..')

            echo "display actual PATH......."
            def path= sh(returnStdout: true, script: 'pwd').trim()
            echo "Path: "+path

        }

        stage("Get BioServer Status") {

            //ansiColor('xterm') {
            println "---------------------------"
            println "Check BioServer Env Status"
            println "---------------------------"
            try {
                def version = library.getBioServerCoreVersion()

                sh "sudo chmod u+x gradlew"
                //sh "./gradlew test --daemon"
                sh "./gradlew clean assemble test -Denv.version=$version --tests com.bioserver.test.monitoring.GetStatusTest --info --stacktrace"
            } finally {

                //sh "./gradlew  integration --info --stacktrace"

                // update current job file
//            def path = sh(returnStdout: true, script: 'pwd').trim()
//            echo "check Path installation: " + path
//            dir("/home/jenkins/jenkinsWorkspace/workspace/BIOSERVTEST") {
//                //check if file exist
//                def exists = fileExists 'currentjob'
//                if (exists) {
//                    println "currentjob exists ==> delete it before"
//                    sh "rm /home/jenkins/jenkinsWorkspace/workspace/BIOSERVTEST/currentjob"
//                }
//
//                sh "echo $path >> currentjob"
//                sh "cat currentjob"
//            }
//
//            println "publishing tests"
//            try {
//                junit 'build/test-results/test/*.xml'
//                publishHTML(target: [
//                        allowMissing         : false,
//                        alwaysLinkToLastBuild: true,
//                        keepAll              : true,
//                        reportDir            : "build/reports/tests/test",
//                        reportFiles          : 'index.html',
//                        reportName           : "RestApiHtmlTestsReport"
//                ])
//                println " tests published"
//            } finally {
//
//            }

            }

            //}
        }
    }

//    stage("Update Testlink TC") {
//
//        sh "./gradlew  integration --info --stacktrace"
//    }

}