@Library( "bioserver@")
import com.bioserver.*

node('10.126.237.10') {

    dir("/var/lib/docker/bioServerJenkinsJob") {
        stage("checkout SCM") {
            checkout scm
        }


        echo "enable css plugin function"
        def library = new com.bioserver.setup()
        library.enableCSP()

        stage("Wait Until Setup finalized") {

            sh "sleep 5"
            //sh(returnStdout: false, script: 'cd ..')

            echo "display actual PATH......."
            def path= sh(returnStdout: true, script: 'pwd').trim()
            echo "Path: "+path

        }

        stage("Run Rest API Tests") {

            //ansiColor('xterm') {
            try {
                final String version = readFile(file: '/var/lib/docker/bioServerEnvJenkinsJob/currentVersion')
                version = version.trim()
                echo "BioServer Env Version==> "+version

                sh "sudo chmod u+x gradlew"
                //sh "./gradlew test --daemon"
                sh "./gradlew clean assemble test -Denv.version=$version --info --stacktrace"
            } finally {
                println "-------------------------"
                println "Publish Html Reports"
                println "-------------------------"
                //sh "./gradlew  integration --info --stacktrace"

                // update current job file
                def path = sh(returnStdout: true, script: 'pwd').trim()
                echo "check Path installation: " + path

                notify("ACS: Deployed BioServer | Version: 10 on : BIOSRV",
                        "Successfully deployed <B> BioServer</B> <BR/>")
                //step([$class: 'JUnitResultArchiver', allowEmptyResults: true, testResults: '**/build/test-results/test/*.xml'])

                try {
                    junit 'build/test-results/test/*.xml'
                    publishHTML(target: [
                            allowMissing         : false,
                            alwaysLinkToLastBuild: true,
                            keepAll              : true,
                            reportDir            : "build/reports/tests/test",
                            reportFiles          : 'index.html',
                            reportName           : "RestApiHtmlTestsReport"
                    ])
                    println " tests published"


                } finally {

                }

            }
            //}

        }

    }

}

def notify(title, body) {
    def mailRecipients = '870cab85.idemia.com@emea.teams.ms'
    def jobName = currentBuild.fullDisplayName

    emailext body: body,
            mimeType: 'text/html',
            subject: title,
            to: "${mailRecipients}",
            replyTo: "${mailRecipients}"
}