async function importTest(name, path) {
  describe(name, async function () {
    await require(path);
  });
}

//var common = require("./common");

describe("Liveness Brute Force Attack TestSuite", function () {
  beforeEach(function () {
    console.log("running something before each test");
  });

  console.log("---------------------------------------");
  console.log("Liveness Brute Force Attack TestSuite: ");
  console.log("----------------------------------------");
  importTest("bio_fingerprint_01", './tenant1/bio_fingerprint_01');
  importTest("bio_fingerprint_02", './tenant1/bio_fingerprint_02');
  importTest("bio_fingerprint_03", './tenant1/bio_fingerprint_03');
  importTest("bio_fingerprint_04", './tenant1/bio_fingerprint_04');
  importTest("bio_fingerprint_05", './tenant1/bio_fingerprint_05');
  importTest("bio_fingerprint_06", './tenant1/bio_fingerprint_06');
  importTest("bio_fingerprint_07", './tenant1/bio_fingerprint_07');
  importTest("bio_fingerprint_08", './tenant1/bio_fingerprint_08');
  importTest("bio_fingerprint_09", './tenant1/bio_fingerprint_09');
  importTest("bio_fingerprint_10", './tenant1/bio_fingerprint_10');
  importTest("bio_fingerprint_11", './tenant1/bio_fingerprint_11');
  importTest("bio_fingerprint_12", './tenant1/bio_fingerprint_12');

  after(function (done) {
    console.log("after all tests");
    done();
  });
});