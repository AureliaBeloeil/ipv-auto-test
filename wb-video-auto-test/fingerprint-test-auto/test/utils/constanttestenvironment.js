class ConstantTestEnvironment {

  static base_url = "https://10.126.237.10";
  static base_url_ppe = "https://wbs.ppe.xantav.com";
  static video_server_url = this.base_url+"/video-server";
  static video_server_ppe_url = this.base_url_ppe+"/video-server";
  static video_demo_url = this.base_url+"/demo-server-medium/medium-liveness/";
  static no_url = "about:blank";
  static init_liveness_session_path = "/video-server/init-liveness-session/";
  static bio_session_url = "/bioserver-app/v2/bio-sessions";
  static biosrv_api_key = "fp-disabled";//"BIOSRVTEST";
  static biosrv_api_key_tenant1 = "tenant-1";
  static biosrv_api_key_tenant2 = "tenant-2";
  static biosrv_api_key_ppe = "allh6ffb-a208-4bb1-928e-f89021ba2912";
  static correlationId = 'c5a0aec2-1262-41b6-82bd-e9d60da92ef2';
  static callbackURL = 'https://service-provider-site.com/transactions/'+this.correlationId+'/liveness-challenge-result';
  static monitoring_path = "/video-server/v2/monitor";
  static capabilities_path = "/video-server/v2/capabilities";

  static coturn_credentials_path = "/video-server/coturnService?bioSessionId=";
  static wrong_coturn_credentials_path = "/video-server/coturnService=";
  static wrong_biosession_id = "aa5ad8ff";
  static wrong_biosession_id2 = "aa5ad8ff-6146-4849-8fcc-39f29d7a8973";

  static downloadURL = '/video-server/network-speed';
  static latencyURL = '/video-server/network-latency';

  static Mode = {
    F5_1_VID60: {value: 500, name: "F5_1_VID60", code: "A"},
    F5_2_VID75: {value: 600, name: "F5_2_VID75", code: "B"},
    F5_3_VID60: {value: 700, name: "F5_3_VID60", code: "C"},
    F5_4_IDD60: {value: 802, name: "F5_4_IDD60", code: "D"},
    F5_4_IDD75: {value: 804, name: "F5_4_IDD75", code: "E"},
    F5_4_LOW75: {value: 804, name: "F5_4_LOW75", code: "F"}
  };

  static LivenessStatusEnum = {
    SUCCESS: {value: 0, name: "SUCCESS", code: "A"},
    FAILED: {value: 1, name: "FAILED", code: "B"},
    SPOOF: {value: 2, name: "SPOOF", code: "C"},
    SPOOF_JS: {value: 3, name: "SPOOF_JS", code: "D"},
    ERROR: {value: 4, name: "ERROR", code: "E"},
    TIMEOUT: {value: 5, name: "TIMEOUT", code: "F"},
    BLOCKED: {value: 6, name: "BLOCKED", code: "G"}
};

  static LivenessMode = {
    LIVENESS_PASSIVE: {value: 0, name: "LIVENESS_PASSIVE", code: "A"},
    LIVENESS_MEDIUM: {value: 1, name: "LIVENESS_MEDIUM", code: "B"},
    LIVENESS_HIGH: {value: 2, name: "LIVENESS_HIGH", code: "C"},
    MEDIUM: {value: 3, name: "MEDIUM", code: "D"},
    HIGH: {value: 4, name: "HIGH", code: "E"},
    NO_LIVENESS: {value: 5, name: "NO_LIVENESS", code: "F"}
  };

  static SecurityLevel = {
    VERY_LOW: {value: 0, name: "VERY_LOW", code: "A"},
    LOW: {value: 1, name: "LOW", code: "B"},
    MEDIUM: {value: 2, name: "MEDIUM", code: "C"},
    HIGH: {value: 3, name: "HIGH", code: "D"},
    VERY_HIGH: {value: 4, name: "VERY_HIGH", code: "E"},
    VERY_HIGH2: {value: 5, name: "VERY_HIGH2", code: "F"},
    VERY_HIGH3: {value: 6, name: "VERY_HIGH3", code: "G"},
    VERY_HIGH4: {value: 7, name: "VERY_HIGH4", code: "H"},
    VERY_HIGH5: {value: 8, name: "VERY_HIGH5", code: "I"},
    VERY_HIGH6: {value: 9, name: "VERY_HIGH6", code: "J"},
    VERY_HIGH7: {value: 10, name: "VERY_HIGH7", code: "K"}
  };

  static VIDEO_SPOOF = 'liveness_passive_spoof_1.y4m';
  static VIDEO_GENUINE = 'video_hd.y4m';
  static VIDEO_TIMEOUT = 'liveness_passive_timeout.y4m';

  /**
   * TestLink Data
   */
  static testlink_host = "r11-testlink002.mphr11.morpho.com";
  static testink_path = "/tl-ced/lib/api/xmlrpc/v1/xmlrpc.php";
  static testink_api_key = "9a3215648c1fbb1bcff4d227e4e7cb41";
  static testlink_projet_name = "BioServer";
  static testlink_plan_name = "BioServerVideoTPlan";


  /**
   * FingerPrint Variables
   */
  static FIRST_LOCK_TIME = 10000;
  static SECOND_LOCK_TIME = 15000;
  static THIRD_LOCK_TIME = 25000;
  static FORGET_TIME = 110000;
  static MAX_ATTEMPT_STATUS_CODE = 429;
  static MAX_ATTEMPT_ERROR_MSG = "Maximum captures attempt reached";
}
module.exports.ConstantTestEnvironment = ConstantTestEnvironment;

