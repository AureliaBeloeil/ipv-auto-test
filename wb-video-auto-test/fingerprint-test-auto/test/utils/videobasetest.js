const TestLink = require('testlink-xmlrpc');
let chai = require("chai");
let chaiHttp = require("chai-http");
const path = require('path');
var validate = require('uuid-validate');
const { Options } = require('selenium-webdriver/chrome');
const { Builder, By, Key, until } = require('selenium-webdriver');
const { getExtension, getAddHeaderUrl } = require('chrome-modheader');
chai.should();
chai.use(chaiHttp);

// This line allows use with https
//process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
const {ConstantTestEnvironment} = require(path.resolve('test/utils/constanttestenvironment'));
class VideoBaseTest {

  isTestLinkEnabled = false;
  testPlanId = null;
  buildId = null;
  isTestSuite = true;
  init = false;
  testlink = null;
  static bio_session_id = null;
  static mediaStream = null;
  static devicesResult = null;
  driver = null;

//const options = new Options().addExtensions(getExtension());
//   static options = new Options().addArguments(
//     "--use-fake-device-for-media-stream",
//     "--use-fake-ui-for-media-stream",
//     "--allow-running-insecure-content",
//     ['user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36"'],
//     /*"--headless",*/
//     "--disable-gpu",
//     "--no-sandbox",
//     "--allow-insecure-localhost",
//     "--ignore-certificate-errors",
//     "--acceptInsecureCerts",
//     "--use-file-for-fake-video-capture="+path.resolve('test/resources/video_hd.y4m')
//   ).addExtensions(getExtension());

  constructor() {

    this.testlink = new TestLink({
      host: ConstantTestEnvironment.testlink_host,
      secure: true, // Use https, if you are using http, set to false.
      rpcPath: ConstantTestEnvironment.testink_path,
      apiKey: ConstantTestEnvironment.testink_api_key, // The API KEY from TestLink. Get it from user profile.
    });

  }


  async checkConnection() {
    let ping = await this.testlink.sayHello();
    if (ping === "Hello!") {
      console.log("connection established");
    } else {
      throw Promise.reject("Testlink is not connected");
    }
  }
  async createTestLinkBuild() {

    console.log("create new build for testlink ");
    this.testPlanId = await this.testlink.getTestPlanByName({testprojectname: ConstantTestEnvironment.testlink_projet_name, testplanname: ConstantTestEnvironment.testlink_plan_name});
    const time = new Date(Date.now()).toISOString();
    const buildName = "videoServerBuild".concat(time) ;
    console.log("Build Name : ", buildName);
     this.buildId = await this.testlink.createBuild({testplanid: this.testPlanId[0].id, buildname: buildName,
      buildnotes: "create build in order to run tests", active: true, open: true, releasedate: time});
    console.log("Build name successfully created ");
  }

  async runOnTestLink(testcaseid, success) {
    if(this.isTestLinkEnabled) {
      if(success) {
        console.log(" -- Test Case: {}  | Test Success Reported", testcaseid);
        const res = await this.testlink.reportTCResult({testcaseexternalid: testcaseid, testplanid: this.testPlanId[0].id, status: 'p',
          steps: {step_number:1, result: 'p', notes:"test is succeed" } , buildid: this.buildId[0].id, platformname: "BioServer"});
        console.log("reportTCResult : ", res);
        chai.assert.equal(res[0].status, true);
        chai.assert.equal(res[0].message, "Success!");
      } else{
        console.log(" -- Test Case: {}  | Test Failed Reported", testcaseid);
        const res = await this.testlink.reportTCResult({testcaseexternalid: testcaseid, testplanid: this.testPlanId[0].id, status: 'f',
          steps: {step_number:1, result: 'f', notes:"test is failed" } , buildid: this.buildId[0].id, platformname: "BioServer"});
        console.log("reportTCResult : ", res);
        chai.assert.equal(res[0].status, true);
        chai.assert.equal(res[0].message, "Success!");
      }
    }

  }

  static async createBioSession(mode, securityLevel, tenantId) {

    console.log("------------------");
    console.log("Create BioSession ");
    console.log("------------------");

    let response = await chai
      .request(ConstantTestEnvironment.base_url)
      .post(ConstantTestEnvironment.init_liveness_session_path)
      .set('content-type', 'application/json')
      .set('apikey', tenantId)
      .send({'livenessMode': mode, 'numberOfChallenge': 2, 'securityLevel': securityLevel, 'imageStorageEnabled': true, 'ttlSeconds': 300});response.should.have.status(201);
    var location = response.get("location");
    VideoBaseTest.bio_session_id = location.substring(location.lastIndexOf('/') + 1);
    console.log("bio_session_id "+ VideoBaseTest.bio_session_id);

    return VideoBaseTest.bio_session_id;
  }

  static getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  static getOptions(videoName, androidVersion) {

    const agent = "Mozilla/5.0 (Linux; Android "+androidVersion+"; SM-G950F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Mobile Safari/537.36";
    return new Options().addArguments(
      '--use-fake-device-for-media-stream',
      '--use-fake-ui-for-media-stream',
      '--allow-running-insecure-content',
      ['user-agent='+agent],
      "--headless",
      '--disable-gpu',
      '--no-sandbox',
      '--allow-insecure-localhost',
      '--ignore-certificate-errors',
      '--acceptInsecureCerts',
      '--use-file-for-fake-video-capture=' + path.resolve('test/resources/'+videoName)
    )/*.addExtensions(getExtension())*/;
  }

  static async initDriver(options, url) {
    console.log("init selenium browser ");
    this.driver = new Builder()
      .forBrowser('chrome')
      .setChromeOptions(options)
      .build();
    console.log("------------------------");
    console.log("Start Selenium Webdriver");
    console.log("------------------------");
    // runs before each test in this block
    await this.driver.get(getAddHeaderUrl('apikey', ConstantTestEnvironment.biosrv_api_key));
    await this.driver.get(url);
    console.log(" -- Done.");
    return this.driver;
  }

  static async getLivenessChallengeResult(livenessMode) {

    console.log(">> getLivenessChallengeResult()");
    let livenessUrl = ConstantTestEnvironment.bio_session_url+"/"+VideoBaseTest.bio_session_id+"/liveness-challenge-result/"+livenessMode;
    console.log("livenessUrl ==>",  livenessUrl);
    let response = await chai
      .request(ConstantTestEnvironment.base_url)
      .get(livenessUrl)
      .set('apikey', ConstantTestEnvironment.biosrv_api_key)
      .send();
    return response;
  }

  static async getLivenessChallengeResultWithPooling(livenessMode, Slevel, status) {

    var waitingTime = 0;
    for (var pooling = 0; pooling <= 30; pooling++) {

      console.log('----------------------------------');
      console.log('Pooling number ==> ', pooling);
      console.log('----------------------------------');

      let response = await VideoBaseTest.getLivenessChallengeResult(livenessMode);
      console.log('---------------------------------------');
      console.log('Get Liveness Challenge Result Response ');
      console.log('---------------------------------------');
      console.log('httpResponse.statusCode ', response.statusCode);
      if (response.statusCode === 204) {
        console.log('----------------------');
        console.log('wait 2s before retry ');
        console.log('----------------------');
        await VideoBaseTest.sleep(2000);
        waitingTime = waitingTime + 2000;
        console.log('waitingTime ==> ', waitingTime);
      } else if (response.statusCode === 200) {

        console.log('-----------------------------------');
        console.log('retrieve Liveness Challenge Result');
        console.log('-----------------------------------');

        response.should.have.status(200);
        response.body.should.be.a('object');
        console.log('response ' + response.text);

        let livenessStatus = response.body.livenessStatus;
        console.log('Video_Liveness: livenessStatus==> ' + livenessStatus);
        response.body.should.have.property('livenessStatus')
          .equal(status);

        let fakeWebcam = response.body.fakeWebcam;
        console.log('Video_Liveness: fakeWebcam==> ' + fakeWebcam);
        response.body.should.have.property('fakeWebcam')
          .equal(false);

        let actualLivenessMode = response.body.livenessMode;
        console.log('Video_Liveness: livenessMode==> ' + actualLivenessMode);

        let splitExpectedMode = livenessMode.split("/");
        console.log('Expected livenessMode==> ' + splitExpectedMode[0]);
        response.body.should.have.property('livenessMode')
          .equal(splitExpectedMode[0]);

        let numberOfChallenge = response.body.numberOfChallenge;
        console.log('Video_Liveness: numberOfChallenge==> ' + numberOfChallenge);
        response.body.should.have.property('numberOfChallenge')
          .equal(2);

        let securityLevel = response.body.securityLevel;
        console.log('Video_Liveness: securityLevel==> ' + securityLevel);
        response.body.should.have.property('securityLevel')
          .equal(Slevel);

        if(status === 'TIMEOUT') {
          let diagnostic = response.body.diagnostic;
          console.log('Video_Liveness: diagnostic==> ' + diagnostic);
          response.body.should.have.property('diagnostic').equal('No face detected');
        } else{
          let bestImageId = response.body.bestImageId;
          console.log('Video_Liveness: bestImageId==> ' + bestImageId);
          response.body.should.have.property('bestImageId');

          chai.assert.isNotNull(bestImageId);
          chai.assert.equal(validate(bestImageId), true);

        }

        let signature = response.body.signature;
        console.log('Video_Liveness: signature==> ' + signature);
        response.body.should.have.property('signature');
        const decodedJWT = VideoBaseTest.decodingJWT(signature);
        chai.assert.isNotNull(decodedJWT);
        break;

      }

      if (waitingTime >= 70000 || (pooling>= 30 && response.statusCode === 204)) {
        console.log('----------------------------------------');
        console.log(' 30s waitingTime Reached==> Raise Error');
        console.log('----------------------------------------');
        chai.assert.fail('liveness failed');
      }
    }
  }

  static converToLocalTime(serverDate) {

    var dt = new Date(Date.parse(serverDate));
    var localDate = dt;

    var gmt = localDate;
    var min = gmt.getTime() / 1000 / 60; // convert gmt date to minutes
    var localNow = new Date().getTimezoneOffset(); // get the timezone
    // offset in minutes
    var localTime = min - localNow; // get the local time

    var dateStr = new Date(localTime * 1000 * 60);
    dateStr = dateStr.toString("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    return dateStr;
  }

  static sleep(ms) {
    return new Promise((resolve) => {
      console.log('setTimeout >>>');
      setTimeout(resolve, ms);
    });
  }

  static decodingJWT = (token) => {
    console.log('decoding JWT token');
    if(token !== null || token !== undefined){
      const base64String = token.split('.')[1];
      const decodedValue = JSON.parse(Buffer.from(base64String,
                        'base64').toString('ascii'));
      console.log(decodedValue);
      return decodedValue;
    }
    return null;
  }

  static getFaceCaptureOptions() {
    return {
      wspath: '/video-server/engine.io',
      bioserverVideoUrl: ConstantTestEnvironment.base_url,
      rtcConfigurationPath: ConstantTestEnvironment.base_url + '/video-server/coturnService?bioSessionId=' + encodeURIComponent(VideoBaseTest.bio_session_id),
      bioSessionId: VideoBaseTest.bio_session_id,
      trackingFn: (trackingInfo) => {
        console.log('onTracking', trackingInfo);
      },
      errorFn: (error) => {
        console.log('face capture error', error);
      },
      showVideoFile: (recordedVideoFile) => {
        console.log('showVideoFile ', recordedVideoFile);
      },
      showChallengeInstruction: (challengeInstruction) => {
        console.log('challenge instructions', challengeInstruction);
      },
      showChallengeResult: (showChallengeResult) => {
        console.log('call back the backend to retrieve liveness result', showChallengeResult);
        //callback(showChallengeResult)
      }
    };
  }

  static getFaceCaptureOptions2() {
    return {
      wspath: '/video-server/engine.io',
      bioserverVideoUrl: ConstantTestEnvironment.base_url,
      rtcConfigurationPath: ConstantTestEnvironment.base_url + '/video-server/coturnService?bioSessionId=' + encodeURIComponent(VideoBaseTest.bio_session_id),
      bioSessionId: VideoBaseTest.bio_session_id
    };
  }

  initMediaDevicesSuccess(driver) {
    driver.executeAsyncScript(async (callback) =>
      callback(await BioserverVideo.initMediaDevices({
        cameraSelectionGUI: '#cameraSelectID',
        audioSelectionGUI: '#audioSelectID'
      }))
    )
      .then((mediaDevices) => {
        console.log('-------------------------------------------');
        console.log('The list of user camera devices Response: ');
        console.log('------------------------------------------');

        console.log('audioDevices.deviceId ? : ' + mediaDevices.audioDevices[0].deviceId);
        chai.assert.isNotNull(mediaDevices.audioDevices[0].deviceId);
        console.log('audioDevices.label ? : ' + mediaDevices.audioDevices[0].label);
        chai.assert.isNotNull(mediaDevices.audioDevices[0].label);

        console.log('videoDevices.deviceId ? : ' + mediaDevices.videoDevices[0].deviceId);
        chai.assert.isNotNull(mediaDevices.videoDevices[0].deviceId);
        console.log('videoDevices.label ? : ' + mediaDevices.videoDevices[0].label);
        chai.assert.isNotNull(mediaDevices.videoDevices[0].label);
        VideoBaseTest.devicesResult = mediaDevices;

      })
      .catch((e) => {
        console.log('Init Media Devices async finished with errors: #{mediaDevices}' + e);
      });
  }

  async environmentDetectionSuccess(driver) {
    await driver.executeScript('return BioserverEnvironment.detection()')
      .then(function (value) {
        console.log('-------------------------');
        console.log('Env detection Response: ');
        console.log('-------------------------');

        console.log('Env Browser supported ? : ' + value.envDetected.browser.isSupported);
        chai.assert.equal(value.envDetected.browser.isSupported, true);

        console.log('Env Browser supportedList : ' + value.envDetected.browser.supportedList);
        chai.assert.equal(value.envDetected.browser.supportedList.length, 0);

        console.log('Env OS isMobile ? : ' + value.envDetected.os.isMobile);
        chai.assert.equal(value.envDetected.os.isMobile, false);

        console.log('Env OS isSupported ? : ' + value.envDetected.os.isSupported);
        chai.assert.equal(value.envDetected.os.isSupported, true);

        console.log('Env OS supportedList : ' + value.envDetected.os.supportedList);
        chai.assert.equal(value.envDetected.os.supportedList.length, 0);
      });
  }
//TODO REGARDER LE CODE DU GET DEVICE STREAM AVEC NISHANTH OU REDA
  async getDeviceStreamSuccess(driver, width, height) {

    await driver.executeAsyncScript(async function(width, height) {
      var callback = arguments[arguments.length - 1];
      callback(await BioserverVideo.getDeviceStream({
        video:{frameRate:15, width:width, height:height, facingMode:"environment"},
        errorFn: (err) => {
          console.debug("get User Media Stream failed", err);
          callback(err)
        } }))

    }, width, height).then(async function(deviceStream) {

      console.log("-------------------------------------------");
      console.log("The list of user camera devices Response: ");
      console.log("------------------------------------------");

      console.log("MediaStream.resolution : " + deviceStream.resolution);
      //chai.assert.equal(deviceStream.resolution, "1280x720");

      console.log("MediaStream.id : " + deviceStream.id);
      chai.assert.isNotNull(deviceStream.id);

      console.log("MediaStream.active ? : " + deviceStream.active);
      chai.assert.equal(deviceStream.active, true);

      VideoBaseTest.mediaStream = deviceStream;

    }).catch(async (e) => {
      console.log("Get Devices Stream async finished with errors: ", e);
      throw e;
    });
  }

}

module.exports.VideoBaseTest = VideoBaseTest;