/*eslint-disable */
//let chai = require("chai");
//var validate = require('uuid-validate');
//const { Builder, By, Key, until } = require('selenium-webdriver');
let chai = require("chai");
const path = require('path');
var itParam = require('mocha-param');
const {VideoBaseTest} = require(path.resolve('test/utils/videobasetest'));
const {ConstantTestEnvironment} = require(path.resolve('test/utils/constanttestenvironment'));
//const { getExtension, getAddHeaderUrl } = require('chrome-modheader');
var basetest = null;
var driver = null;
var androidVersion = null;

// This line allows use with https
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

function getLivenessMode(mode, securityLevel) {
    return mode+'/'+securityLevel+'?numberOfChallenge=2';
}


describe('Liveness Brute Force Attack TestSuite', () => {


    /**
     * Before class
     */
    before(async function (){

        basetest = new VideoBaseTest();
        if(basetest.isTestSuite && !basetest.init) {
            //await basetest.checkConnection();
            //await basetest.createTestLinkBuild();
            basetest.init = true;
        } else {
            console.log("-------------------------");
            console.log("build is already created ");
            console.log("-------------------------");
        }
        console.log("-------------------------");
        console.log("Browser Initialisation ");
        console.log("-------------------------");
        androidVersion = VideoBaseTest.getRandomInt(9, 50);
    });

    /**
     * after  testcase
     */
    afterEach(function(done) {
        // runs after each test in this block
        console.log("---------------");
        console.log("Close Driver ");
        console.log("---------------");
        driver.quit().then(() => {
            done();
            console.log("Closed.");
        });

    });

    var livenessData =
        [
            { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, counter: 1, status: ConstantTestEnvironment.LivenessStatusEnum.SPOOF.name, video: 'liveness_passive_spoof_1.y4m' },
            { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, counter: 2, status: ConstantTestEnvironment.LivenessStatusEnum.SPOOF.name, video: 'liveness_passive_spoof_1.y4m' },
            { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, counter: 3, status: ConstantTestEnvironment.LivenessStatusEnum.TIMEOUT.name, video: 'liveness_passive_timeout.y4m' },
            { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, counter: 4, status: ConstantTestEnvironment.LivenessStatusEnum.SPOOF.name, video: 'liveness_passive_spoof_1.y4m' },
            { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, counter: 5, status: ConstantTestEnvironment.LivenessStatusEnum.BLOCKED.name, video: 'video_hd.y4m' }
        ];

    describe("Test  Liveness is blocked after 2 SPOOF, 1 TIMEOUT and 1 SPOOF", function () {
        itParam("${value.mode}  ${value.counter} with status = ${value.status} ", livenessData, async function (value) {

            console.log("------------------------------------------------------");
            console.log(" Browser Initialisation ==> Android Version updated: ", androidVersion);
            console.log("-------------------------------------------------------");
            const options = VideoBaseTest.getOptions(value.video, androidVersion);
            driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

            //create bioSession
            const sessionId = await VideoBaseTest.createBioSession(value.mode, ConstantTestEnvironment.SecurityLevel.VERY_LOW.name, ConstantTestEnvironment.biosrv_api_key_tenant1);

            console.log("--------------------------------------");
            console.log("Add Stream to Html Video Element ");
            console.log("-------------------------------------- ");
            await driver.manage().setTimeouts( { implicit: 2000, pageLoad: 2000, script: 100000 } );

            await driver.executeScript(async function attachMediaStream(){
                const mediaStream = await BioserverVideo.getDeviceStream({video:{frameRate:15, width:1280, height:720, facingMode:"environment"}})
                return document.querySelector('video').srcObject=mediaStream;
            });

            console.log("--------------------------------------");
            console.log("start Init Face Capture Client Request ");
            console.log("-------------------------------------- ");

            const url = ConstantTestEnvironment.base_url;

            await driver.executeAsyncScript(async function(url, sessionId) {
                var callback = arguments[arguments.length - 1];
                //Format face capture options request
                const faceCaptureOptions = {
                    wspath: '/video-server/engine.io',
                    bioserverVideoUrl: url,
                    rtcConfigurationPath: url + '/video-server/coturnService?bioSessionId=' + encodeURIComponent(sessionId),
                    bioSessionId: sessionId,
                    trackingFn: (trackingInfo) => {console.log('onTracking', trackingInfo);},
                    errorFn: (error) => {console.log('face capture error', error); callback(error);},
                    showVideoFile: (recordedVideoFile) => {console.log('showVideoFile ', recordedVideoFile);},
                    showChallengeInstruction: (challengeInstruction) => {console.log('challenge instructions', challengeInstruction);},
                    showChallengeResult: (showChallengeResult) => {
                        console.log('call back the backend to retrieve liveness result', showChallengeResult);
                        callback(showChallengeResult); }};

                console.log("-------------------");
                console.log("start Face Capture  ");
                console.log("-------------------");
                const mediaStream = await navigator.mediaDevices.getUserMedia({video: true});
                const captureClient = await BioserverVideo.initFaceCaptureClient(faceCaptureOptions);
                await captureClient.start(mediaStream, false, 'video');

            }, url, sessionId).then(async function(livenessResult) {

                console.log('--------------------------------------------------------');
                console.log('(showChallengeResult) or (errorFn) Callback is triggered ');
                console.log("--------------------------------------------------------");
                console.log("livenessResult ", livenessResult);

                if(value.counter === 5) {
                    console.log("------------------------");
                    console.log("Get Error Fn Result  ");
                    console.log("------------------------");

                    console.log("livenessResult.code : " + livenessResult.code);
                    chai.assert.equal(livenessResult.code, ConstantTestEnvironment.MAX_ATTEMPT_STATUS_CODE);

                    console.log("livenessResult.error : " + livenessResult.error);
                    chai.assert.equal(livenessResult.error, ConstantTestEnvironment.MAX_ATTEMPT_ERROR_MSG);

                    console.log("livenessResult.unlockDateTime : " + livenessResult.unlockDateTime);
                    chai.assert.isNotNull(VideoBaseTest.converToLocalTime(livenessResult.unlockDateTime));

                } else{
                    console.log("--------------------------------");
                    console.log("Get Liveness Challenge Result  ");
                    console.log("--------------------------------");

                    await VideoBaseTest.getLivenessChallengeResultWithPooling(getLivenessMode(value.mode, ConstantTestEnvironment.SecurityLevel.VERY_LOW.name),
                        ConstantTestEnvironment.SecurityLevel.VERY_LOW.name, value.status);
                }
                //upddate testlink
                //await basetest.runOnTestLink("BioServer-TC-421", false);
            }).catch( (e) => {
                console.log(value.mode, " finished with errors: ", e);
                //upddate testlink
                //await basetest.runOnTestLink("BioServer-TC-421", false);
                throw e;
            });

        });
    });
});