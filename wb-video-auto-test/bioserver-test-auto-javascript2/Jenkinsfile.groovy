@Library( "bioserver@")
import com.bioserver.*

node('10.126.237.10') {

    dir("/var/lib/docker/videoServerJenkinsJob") {

        environment {

            PATH = "/usr/local/node-v12.18.0-linux-x64/bin:$PATH"

        }
        //stages {
        stage("checkout SCM") {
            checkout scm
        }

        echo "call makeAllMultiBranch function"
        //def deploy = load 'jenkins/deployments.groovy'
        //library.makeAllMultiBranch("")

        stage("Wait Until Setup finalized") {

            //sh "sleep 5"
            //sh(returnStdout: false, script: 'cd ..')

            echo "display actual PATH......."
            def path = sh(returnStdout: true, script: 'pwd').trim()
            echo "Path: " + path

        }

        stage("Javascript Rest API Tests") {

            echo "PATH is: $PATH"

            //echo "create bioserver lib instance"
            //def library = new com.bioserver.setup()
            //def version = library.getBioServerCoreVersion()
            final String version = readFile(file: '/var/lib/docker/bioServerEnvJenkinsJob/currentVersion')
            version = version.trim()
            echo "BioServer Env Version==> "+version
            //ansiColor('xterm') {
            try {

                withEnv(["PATH+NODEJS=/usr/local/node-v12.18.0-linux-x64/bin"]) {
                    echo "Global PATH = ${env.PATH}"
                    //sh "npm test -- ${version} -- --verbose=true"
                    //sh "npm install"
                    sh "npm run test-jenkins  -- ${version} test/alltestsuites.js"

                    //sh "npm run test:awesome -- ${version} -- --verbose"
                }

            } finally {

                println "publishing tests"
                try {

                    junit 'reports/report.xml'

//                    publishHTML(target: [
//                            allowMissing         : false,
//                            alwaysLinkToLastBuild: true,
//                            keepAll              : true,
//                            reportDir            : "mochawesome-report",
//                            reportFiles          : 'mochawesome.html',
//                            reportName           : "JSHtmlTestsReport"
//                    ])
                    println " tests published"
                } finally {

                }
            }
            //}

            //}
        }

    }

}
