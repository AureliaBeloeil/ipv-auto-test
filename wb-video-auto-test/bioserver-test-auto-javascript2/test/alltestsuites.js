async function importTest(name, path) {
  describe(name, async function () {
    await require(path);
  });
}

//var common = require("./common");

describe("testsuites", function () {
  beforeEach(function () {
    console.log("running something before each test");
  });

  console.log("----------------------------");
  console.log("BioserverVideo WebRtc API: ");
  console.log("----------------------------");
  importTest("BioServer Env detection", './detection/environmentdetectiontest');
  importTest("BioServer InitMediaDevices", './initmediadevices/initmediadevicestest');
  importTest("BioServer GetDeviceStream", './devicestream/devicestreamtest');
  importTest("BioServer InitFaceCapture", './initfacecapture/initfacecaptureclienttest');
  importTest("BioServer Liveness Passive", './livenesspassive/livenesspassivetest');
  importTest("BioServer Liveness Medium", './livenessmedium/livenessmediumtest');
  importTest("BioServer Liveness High", './livenesshigh/livenesshightest');
  importTest("BioServer Network Connectivity Check", './networkCheck/networkchecktest');

  console.log("--------------------------");
  console.log("BioserverVideo Rest API: ");
  console.log("--------------------------");
  importTest("initlivenessSession", './restapitest/initsessiontest');
  importTest("monitoring", './restapitest/monitoringtest');
  importTest("capabilities", './restapitest/capabilitiestest');
  importTest("coturnCredentials", './restapitest/coturncredentialstest');

  after(function (done) {
    console.log("after all tests");
    done();
  });
});