/*eslint-disable */
//let chai = require("chai");
//var validate = require('uuid-validate');
//const { Builder, By, Key, until } = require('selenium-webdriver');
const path = require('path');
var itParam = require('mocha-param');
const {VideoBaseTest} = require(path.resolve('test/utils/videobasetest'));
const {ConstantTestEnvironment} = require(path.resolve('test/utils/constanttestenvironment'));
//const { getExtension, getAddHeaderUrl } = require('chrome-modheader');
var basetest = null;
var driver = null;

// This line allows use with https
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

async function mediaStreamInitProcess() {
  console.log('--------------------------------------');
  console.log('start Environment Detection Request ');
  console.log('--------------------------------------');
  //await basetest.environmentDetectionSuccess(driver);

  console.log('--------------------------------------');
  console.log('start Init Media Devices Request ');
  console.log('--------------------------------------');
  basetest.initMediaDevicesSuccess(driver);

  await driver.sleep(500);
  console.log('--------------------------------------');
  console.log('start Get Device Stream Request ');
  console.log('-------------------------------------- ');
  //await basetest.getDeviceStreamSuccess(driver, 1280, 720);
  await driver.sleep(500);
}

function getLivenessMode(mode, securityLevel) {
  return mode+'/'+securityLevel+'?numberOfChallenge=2';
}

describe('BioServer Video Liveness Passive Test', () => {


  /**
   * Before class
   */
  before(async function (){

    basetest = new VideoBaseTest();
    // basetest.createBioSession(function(){
    //   console.log("BioSessionId.", VideoBaseTest.bio_session_id);
    // });
    if(basetest.isTestSuite && !basetest.init) {
      //await basetest.checkConnection();
      //await basetest.createTestLinkBuild();
      basetest.init = true;
    } else {
      console.log("-------------------------");
      console.log("build is already created ");
      console.log("-------------------------");
    }
  });

  /**
   * Before each testcase
   */
  // beforeEach(async function() {
  //   console.log("init selenium browser ");
  //   driver = new Builder()
  //     .forBrowser('chrome')
  //     .setChromeOptions(VideoBaseTest.options)
  //     .build();
  //   console.log("------------------------");
  //   console.log("Start Selenium Webdriver");
  //   console.log("------------------------");
  //   // runs before each test in this block
  //   //await driver.get(getAddHeaderUrl('apikey', 'BIOSRVTEST'));
  //   await driver.get(ConstantTestEnvironment.video_server_url);
  // });

  /**
   * after each testcase
   */
  afterEach(function(done) {
    // runs after each test in this block
    console.log("---------------");
    console.log("Close Driver ");
    console.log("---------------");
    driver.quit().then(() => {
      done();
      console.log("Closed.");
    });
    //await driver.quit();

  });

  var livenessData =
    [
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_LOW.name, status: ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name, video: 'video_4kuhd.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_LOW.name, status: ConstantTestEnvironment.LivenessStatusEnum.SPOOF.name, video: 'liveness_passive_spoof_1.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.LOW.name, status: ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name, video: 'video_4kuhd.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.LOW.name, status: ConstantTestEnvironment.LivenessStatusEnum.SPOOF.name, video: 'liveness_passive_spoof_1.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.MEDIUM.name, status: ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name, video: 'video_4kuhd.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.MEDIUM.name, status: ConstantTestEnvironment.LivenessStatusEnum.SPOOF.name, video: 'liveness_passive_spoof_1.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.HIGH.name, status: ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name, video: 'video_4kuhd.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.HIGH.name, status: ConstantTestEnvironment.LivenessStatusEnum.SPOOF.name, video: 'liveness_passive_spoof_1.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_HIGH.name, status: ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name, video: 'video_4kuhd.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_HIGH.name, status: ConstantTestEnvironment.LivenessStatusEnum.SPOOF.name, video: 'liveness_passive_spoof_1.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_HIGH2.name, status: ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name, video: 'video_4kuhd.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_HIGH2.name, status: ConstantTestEnvironment.LivenessStatusEnum.SPOOF.name, video: 'liveness_passive_spoof_1.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_HIGH3.name, status: ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name, video: 'video_4kuhd.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_HIGH3.name, status: ConstantTestEnvironment.LivenessStatusEnum.SPOOF.name, video: 'liveness_passive_spoof_1.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_HIGH4.name, status: ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name, video: 'video_4kuhd.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_HIGH4.name, status: ConstantTestEnvironment.LivenessStatusEnum.SPOOF.name, video: 'liveness_passive_spoof_1.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_HIGH5.name, status: ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name, video: 'video_4kuhd.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_HIGH5.name, status: ConstantTestEnvironment.LivenessStatusEnum.SPOOF.name, video: 'liveness_passive_spoof_1.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_HIGH6.name, status: ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name, video: 'video_4kuhd.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_HIGH6.name, status: ConstantTestEnvironment.LivenessStatusEnum.SPOOF.name, video: 'liveness_passive_spoof_1.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_HIGH7.name, status: ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name, video: 'video_4kuhd.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.VERY_HIGH7.name, status: ConstantTestEnvironment.LivenessStatusEnum.SPOOF.name, video: 'liveness_passive_spoof_1.y4m' }
    ];

  describe("Liveness Passive TestSuite", function () {
    itParam("test ${value.mode} with SecurityLevel ${value.securityLevel} and Check status = ${value.status} ", livenessData, async function (value) {
      //chai.expect(value).to.be.a('number');

      console.log("-------------------------");
      console.log("Browser Initialisation ");
      console.log("-------------------------");
      const options = VideoBaseTest.getOptions(value.video);
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      //create bioSession
      const sessionId = await VideoBaseTest.createBioSession(value.mode,value.securityLevel);

      await driver.sleep(2000);
      await mediaStreamInitProcess();

      console.log("--------------------------------------");
      console.log("Add Stream to Html Video Element ");
      console.log("-------------------------------------- ");
      await driver.manage().setTimeouts( { implicit: 2000, pageLoad: 2000, script: 100000 } );

      await driver.executeScript(async function attachMediaStream(){
        const mediaStream = await BioserverVideo.getDeviceStream({video:{frameRate:15, width:1280, height:720, facingMode:"environment"}})
        return document.querySelector('video').srcObject=mediaStream;
      });

      await driver.sleep(2000);
      console.log("--------------------------------------");
      console.log("start Init Face Capture Client Request ");
      console.log("-------------------------------------- ");

      const url = ConstantTestEnvironment.base_url;

      await driver.executeAsyncScript(async function(url, sessionId) {
        var callback = arguments[arguments.length - 1];
        //Format face capture options request
        const faceCaptureOptions = {
          wspath: '/video-server/engine.io',
          bioserverVideoUrl: url,
          rtcConfigurationPath: url + '/video-server/coturnService?bioSessionId=' + encodeURIComponent(sessionId),
          bioSessionId: sessionId,
          trackingFn: (trackingInfo) => {console.log('onTracking', trackingInfo);},
          errorFn: (error) => {console.log('face capture error', error);},
          showVideoFile: (recordedVideoFile) => {console.log('showVideoFile ', recordedVideoFile);},
          showChallengeInstruction: (challengeInstruction) => {console.log('challenge instructions', challengeInstruction);},
          showChallengeResult: (showChallengeResult) => {
            console.log('call back the backend to retrieve liveness result', showChallengeResult);
            callback(showChallengeResult); }};

        console.log("-------------------");
        console.log("start Face Capture  ");
        console.log("-------------------");
        const mediaStream = await navigator.mediaDevices.getUserMedia({video: true});
        const captureClient = await BioserverVideo.initFaceCaptureClient(faceCaptureOptions);
        await captureClient.start(mediaStream, false, 'video');

      }, url, sessionId).then(async function(challengeResult) {

        console.log('-------------------------------------------');
        console.log('showChallengeResult Callback is triggered ');
        console.log("-------------------------------------------");
        console.log("challengeResult ", challengeResult);

        console.log("--------------------------------");
        console.log("Get Liveness Challenge Result  ");
        console.log("--------------------------------");

        await VideoBaseTest.getLivenessChallengeResultWithPooling(getLivenessMode(value.mode, value.securityLevel),
          value.securityLevel, value.status);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-421", false);
      }).catch( (e) => {
        console.log(value.mode, " finished with errors: ", e);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-421", false);
        throw e;
      });

    });
  });
});