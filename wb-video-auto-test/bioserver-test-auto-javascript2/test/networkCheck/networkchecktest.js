/*eslint-disable */


let chai = require("chai");
//const { Options } = require('selenium-webdriver/chrome');
//const { Builder, By, Key, until } = require('selenium-webdriver');
//const { getExtension, getAddHeaderUrl } = require('chrome-modheader');
const path = require('path');

const {VideoBaseTest} = require(path.resolve('test/utils/videobasetest'));
const {ConstantTestEnvironment} = require(path.resolve('test/utils/constanttestenvironment'));

var basetest = null;
var driver = null;
let downloadURL = '/video-server/network-speed';
let latencyURL = '/video-server/network-latency';
let wrong_downloadURL = '/video/network-speed';
let wrong_latencyURL = '/video/network-latency';

// This line allows use with https
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

async function mediaStreamInitProcess() {
  console.log('--------------------------------------');
  console.log('start Environment Detection Request ');
  console.log('--------------------------------------');
  await basetest.environmentDetectionSuccess(driver);

  console.log('--------------------------------------');
  console.log('start Init Media Devices Request ');
  console.log('--------------------------------------');
  basetest.initMediaDevicesSuccess(driver);

  await driver.sleep(500);
  console.log('--------------------------------------');
  console.log('start Get Device Stream Request ');
  console.log('-------------------------------------- ');
  await basetest.getDeviceStreamSuccess(driver, 1280, 720);
  await driver.sleep(500);
}

describe('BioServer Check Network Connectivity Test', () => {

  /**
   * Before class
   */
  before(async function (){

    basetest = new VideoBaseTest();

    if(basetest.isTestSuite && !basetest.init) {
      await basetest.checkConnection();
      await basetest.createTestLinkBuild();
      basetest.init = true;
    } else {
      console.log("-------------------------");
      console.log("build is already created ");
      console.log("-------------------------");
    }
  });

  /**
   * Before each testcase
   */
  // beforeEach(async function() {
  //   console.log("init selenium browser ");
  //   driver = new Builder()
  //     .forBrowser('chrome')
  //     .setChromeOptions(options)
  //     .build();
  //   console.log("------------------------");
  //   console.log("Start Selenium Webdriver");
  //   console.log("------------------------");
  //   // runs before each test in this block
  //   //await driver.get(getAddHeaderUrl('apikey', 'BIOSRVTEST'));
  //   await driver.get(ConstantTestEnvironment.video_demo_url);
  // });

  /**
   * after each testcase
   */
  afterEach(function(done) {
    // runs after each test in this block
    console.log("---------------");
    console.log("Close Driver ");
    console.log("---------------");
    driver.quit().then(() => {
      done();
      console.log("Closed.");
    });
    //await driver.quit();

  });



  /**
   * Test that BioserverNetworkCheck.connectivityMeasure testcase should return success when good connnection is used;.
   */
  describe('bio_connectivity_measure_01', function() {

    it('should return success when good connnection is used', async () => {
      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);
      await driver.executeScript(async function addNetworkCheckScript(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-network-check.js';
        return window.document.head.appendChild(s);
      });
      await driver.sleep(2000);
      console.log('---------------------------------');
      console.log('Send Connectivity Measure Request ');
      console.log("---------------------------------");
      await driver.executeAsyncScript(async function(downloadURL, latencyURL) {
        var callback = arguments[arguments.length - 1];
        await BioserverNetworkCheck.connectivityMeasure({
          downloadURL: downloadURL,
          uploadURL:   downloadURL,
          latencyURL:  latencyURL,
          onNetworkCheckUpdate: (onNetworkCheckUpdate) => {
            console.log("Network Check Results ", onNetworkCheckUpdate);
            callback(onNetworkCheckUpdate);
          }
        })

      }, downloadURL, latencyURL).then(function(str) {
        console.log('---------------------------------');
        console.log('Send Connectivity Measure Request ');
        console.log("---------------------------------");
        console.log("Network Check Results ", str);

        console.log("Network Check Results: goodConnectivity ? ", str.goodConnectivity);
        chai.assert.equal(str.goodConnectivity, true);

        console.log("Network Check Results: latencyMs ? ", str.latencyMs);
        chai.assert.isBelow(str.latencyMs, 300);

        console.log("Network Check Results: upload ? ", str.upload);
        chai.assert.isAbove(str.upload, 400);

        console.log("Network Check Results: download ? ", str.download);
        chai.assert.equal(str.download, false);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-421", false);
      }).catch( (e) => {
        console.log("BioserverNetworkCheck.connectivityMeasure finished with errors: ", e);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-421", false);
        throw e;
      });

      await driver.sleep(2000);
    });
  });

  /**
   * Test that BioserverNetworkCheck.connectivityMeasure testcase should return error when wrong latencyURL is used;
   * TODO TICKET BIO_SRV-1975
   */
  describe('bio_connectivity_measure_02', function() {

    it('should return error when wrong latencyURL is used', async () => {
      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);
      await driver.executeScript(async function addNetworkCheckScript(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-network-check.js';
        return window.document.head.appendChild(s);
      });
      await driver.sleep(2000);

      console.log('---------------------------------');
      console.log('Send Connectivity Measure Request ');
      console.log("---------------------------------");
      //set script timeout
      driver.manage().setTimeouts( { implicit: 2000, pageLoad: 2000, script: 3000 } );

      await driver.executeAsyncScript(async function(downloadURL, wrong_latencyURL) {
        var callback = arguments[arguments.length - 1];
        await BioserverNetworkCheck.connectivityMeasure({
          downloadURL: downloadURL,
          uploadURL:   downloadURL,
          latencyURL:  wrong_latencyURL,
          onNetworkCheckUpdate: (onNetworkCheckUpdate) => {
            console.log("Network Check Results ", onNetworkCheckUpdate);
            //callback(onNetworkCheckUpdate);
          },
          errorFn: (error) => {
            console.log("Network Check Error ", error);
            callback(error);
          }
        })

      }, downloadURL, wrong_latencyURL).then(function(str) {

        console.log("Network Check Error ", str);

        chai.assert.equal(str, "403 (Forbidden)");

        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-422", false);
      }).catch( (e) => {
        console.log("BioserverNetworkCheck.connectivityMeasure finished with errors: ", e);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-422", false);
        throw e;
      });

      await driver.sleep(2000);
    });
  });

  /**
   * Test that BioserverNetworkCheck.connectivityMeasure testcase should return error when latencyURL field is not used;
   * TODO TICKET BIO_SRV-1975
   */
  describe('bio_connectivity_measure_03', function() {

    it('should return error when wrong latencyURL is used', async () => {
      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);
      await driver.executeScript(async function addNetworkCheckScript(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-network-check.js';
        return window.document.head.appendChild(s);
      });
      await driver.sleep(2000);

      console.log('---------------------------------');
      console.log('Send Connectivity Measure Request ');
      console.log("---------------------------------");
      //set script timeout
      driver.manage().setTimeouts( { implicit: 2000, pageLoad: 2000, script: 3000 } );

      await driver.executeAsyncScript(async function(downloadURL) {
        var callback = arguments[arguments.length - 1];
        await BioserverNetworkCheck.connectivityMeasure({
          downloadURL: downloadURL,
          uploadURL:   downloadURL,
          onNetworkCheckUpdate: (onNetworkCheckUpdate) => {
            console.log("Network Check Results ", onNetworkCheckUpdate);
            //callback(onNetworkCheckUpdate);
          },
          errorFn: (error) => {
            console.log("Network Check Error ", error);
            callback(error);
          }
        })

      }, downloadURL).then(function(str) {

        console.log("Network Check Error ", str);

        chai.assert.equal(str, "Bad input params");

        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-425", false);
      }).catch( (e) => {
        console.log("BioserverNetworkCheck.connectivityMeasure finished with errors: ", e);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-425", false);
        throw e;
      });

      await driver.sleep(2000);
    });
  });

  /**
   * Test that BioserverNetworkCheck.connectivityMeasure testcase should return error when wrong uploadURL is used;
   */
  describe('bio_connectivity_measure_06', function() {

    it('should return error when wrong uploadURL is used', async () => {
      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);
      await driver.executeScript(async function addNetworkCheckScript(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-network-check.js';
        return window.document.head.appendChild(s);
      });
      await driver.sleep(2000);

      console.log('---------------------------------');
      console.log('Send Connectivity Measure Request ');
      console.log("---------------------------------");
      await driver.executeAsyncScript(async function(downloadURL, wrong_downloadURL, latencyURL) {
        var callback = arguments[arguments.length - 1];
        await BioserverNetworkCheck.connectivityMeasure({
          downloadURL: downloadURL,
          uploadURL:   wrong_downloadURL,
          latencyURL:  latencyURL,
          onNetworkCheckUpdate: (onNetworkCheckUpdate) => {
            console.log("Network Check Results ", onNetworkCheckUpdate);
            callback(onNetworkCheckUpdate);
          }/*,
          errorFn: (error) => {
            console.log("Network Check Error ", error);
            callback(error);
          }*/
        })
        //callback(res);

      }, downloadURL, wrong_downloadURL, latencyURL).then(function(str) {

        console.log('---------------------------------');
        console.log('Send Connectivity Measure Request ');
        console.log("---------------------------------");
        console.log("Network Check Results ", str);

        console.log("Network Check Results: goodConnectivity ? ", str.goodConnectivity);
        chai.assert.equal(str.goodConnectivity, false);

        console.log("Network Check Results: latencyMs ? ", str.latencyMs);
        chai.assert.isBelow(str.latencyMs, 100);

        console.log("Network Check Results: upload ? ", str.upload);
        chai.assert.equal(str.upload, false);

        console.log("Network Check Results: download ? ", str.download);
        chai.assert.equal(str.download, false);

        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-426", false);
      }).catch( (e) => {
        console.log("BioserverNetworkCheck.connectivityMeasure finished with errors: ", e);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-426", false);
        throw e;
      });

      await driver.sleep(2000);
    });
  });

  /**
   * Test that BioserverNetworkCheck.connectivityMeasure testcase should return error when uploadURL field is not used;
   * TODO TICKET BIO_SRV-1975
   */
  describe('bio_connectivity_measure_07', function() {

    it('should return error when uploadURL field is not used', async () => {
      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);
      await driver.executeScript(async function addNetworkCheckScript(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-network-check.js';
        return window.document.head.appendChild(s);
      });
      await driver.sleep(2000);

      console.log('---------------------------------');
      console.log('Send Connectivity Measure Request ');
      console.log("---------------------------------");
      //set script timeout
      driver.manage().setTimeouts( { implicit: 2000, pageLoad: 2000, script: 3000 } );

      await driver.executeAsyncScript(async function(downloadURL, wrong_downloadURL, latencyURL) {
        var callback = arguments[arguments.length - 1];
        await BioserverNetworkCheck.connectivityMeasure({
          downloadURL: downloadURL,
          latencyURL:  latencyURL,
          onNetworkCheckUpdate: (onNetworkCheckUpdate) => {
            console.log("Network Check Results ", onNetworkCheckUpdate);
            callback(onNetworkCheckUpdate);
          },
          errorFn: (error) => {
            console.log("Network Check Error ", error);
            callback(error);
          }
        })

      }, downloadURL, wrong_downloadURL, latencyURL).then(function(str) {

        console.log("Network Check Error ", str);

        chai.assert.equal(str, "Bad input params");

        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-427", false);
      }).catch( (e) => {
        console.log("BioserverNetworkCheck.connectivityMeasure finished with errors: ", e);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-427", false);
        throw e;
      });

      await driver.sleep(2000);
    });
  });

  /**
   * Test that BioserverNetworkCheck.connectivityMeasure testcase should return error when wrong onNetworkCheckUpdate is used;
   * TODO TICKET BIO_SRV-1975
   */
  describe('bio_connectivity_measure_08', function() {

    it('should return error when wrong onNetworkCheckUpdate is used', async () => {
      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);
      await driver.executeScript(async function addNetworkCheckScript(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-network-check.js';
        return window.document.head.appendChild(s);
      });
      await driver.sleep(2000);

      console.log('---------------------------------');
      console.log('Send Connectivity Measure Request ');
      console.log("---------------------------------");
      //set script timeout
      driver.manage().setTimeouts( { implicit: 2000, pageLoad: 2000, script: 3000 } );

      await driver.executeAsyncScript(async function(downloadURL,  latencyURL) {
        var callback = arguments[arguments.length - 1];
        await BioserverNetworkCheck.connectivityMeasure({
          downloadURL: downloadURL,
          uploadURL:   downloadURL,
          latencyURL:  latencyURL,
          onNetworkCheckUpdate1: (onNetworkCheckUpdate) => {
            console.log("Network Check Results ", onNetworkCheckUpdate);
            callback(onNetworkCheckUpdate);
          },
          errorFn: (error) => {
            console.log("Network Check Error ", error);
            callback(error);
          }
        })

      }, downloadURL, wrong_downloadURL, latencyURL).then(function(str) {

        console.log("Network Check Error ", str);

        chai.assert.equal(str, "Bad input params");

        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-427", false);
      }).catch( (e) => {
        console.log("BioserverNetworkCheck.connectivityMeasure finished with errors: ", e);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-427", false);
        throw e;
      });

      await driver.sleep(2000);
    });
  });

  /**
   * Test that BioserverNetworkCheck.connectivityMeasure testcase should return error when onNetworkCheckUpdate field is not used;
   * TODO TICKET BIO_SRV-1975
   */
  describe('bio_connectivity_measure_09', function() {

    it('should return error when onNetworkCheckUpdate field is not used', async () => {
      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);
      await driver.executeScript(async function addNetworkCheckScript(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-network-check.js';
        return window.document.head.appendChild(s);
      });
      await driver.sleep(2000);

      console.log('---------------------------------');
      console.log('Send Connectivity Measure Request ');
      console.log("---------------------------------");
      //set script timeout
      driver.manage().setTimeouts( { implicit: 2000, pageLoad: 2000, script: 3000 } );

      await driver.executeAsyncScript(async function(downloadURL,  latencyURL) {
        var callback = arguments[arguments.length - 1];
        await BioserverNetworkCheck.connectivityMeasure({
          downloadURL: downloadURL,
          uploadURL:   downloadURL,
          latencyURL:  latencyURL,
          errorFn: (error) => {
            console.log("Network Check Error ", error);
            callback(error);
          }
        })

      }, downloadURL, wrong_downloadURL, latencyURL).then(function(str) {

        console.log("Network Check Error ", str);

        chai.assert.equal(str, "Bad input params");

        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-427", false);
      }).catch( (e) => {
        console.log("BioserverNetworkCheck.connectivityMeasure finished with errors: ", e);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-427", false);
        throw e;
      });

      await driver.sleep(2000);
    });
  });

  /**
   * Test that BioserverNetworkCheck.connectivityMeasure testcase should return bad connectivity when low network connnection is used;
   */
  describe('bio_connectivity_measure_10', function() {

    it('should return bad connectivity when low network connnection is used', async () => {
      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);
      await driver.executeScript(async function addNetworkCheckScript(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-network-check.js';
        return window.document.head.appendChild(s);
      });
      await driver.sleep(2000);

      console.log('---------------------------------');
      console.log('Send Connectivity Measure Request ');
      console.log("---------------------------------");

      driver.manage().setTimeouts( { implicit: 2000, pageLoad: 2000, script: 7000 } );

      await driver.setNetworkConditions({
        offline: false,
        latency: 400, // Additional latency (ms).
        download_throughput: 50 * 1024, // Maximal aggregated download throughput.
        upload_throughput: 1024 // Maximal aggregated upload throughput.
      });

      await driver.executeAsyncScript(async function(downloadURL, latencyURL) {
        var callback = arguments[arguments.length - 1];
        await BioserverNetworkCheck.connectivityMeasure({
          downloadURL: downloadURL,
          uploadURL:   downloadURL,
          latencyURL:  latencyURL,
          onNetworkCheckUpdate: (onNetworkCheckUpdate) => {
            console.log("Network Check Results ", onNetworkCheckUpdate);
            callback(onNetworkCheckUpdate);
          }
        })

      }, downloadURL, latencyURL).then(function(str) {
        console.log('---------------------------------');
        console.log('Send Connectivity Measure Request ');
        console.log("---------------------------------");
        console.log("Network Check Results ", str);

        console.log("Network Check Results: goodConnectivity ? ", str.goodConnectivity);
        chai.assert.equal(str.goodConnectivity, false);

        console.log("Network Check Results: latencyMs ? ", str.latencyMs);
        chai.assert.isAtLeast(str.latencyMs, 40);

        console.log("Network Check Results: upload ? ", str.upload);
        chai.assert.isBelow(str.upload, 20);

        console.log("Network Check Results: download ? ", str.download);
        chai.assert.equal(str.download, false);

        console.log("Network Check Results: message ? ", str.message);
        chai.assert.equal(str.message, "It seems that your network signal is weak");
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-430", false);
      }).catch( (e) => {
        console.log("BioserverNetworkCheck.connectivityMeasure finished with errors: ", e);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-430", false);
        throw e;
      });

      await driver.sleep(2000);
    });
  });

  /**
   * Test that BioserverNetworkCheck.connectivityMeasure testcase should return timeout when no network connnection is used;
   */
  describe('bio_connectivity_measure_11', function() {

    it('should return timeout when no network connnection is used', async () => {
      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);
      await driver.executeScript(async function addNetworkCheckScript(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-network-check.js';
        return window.document.head.appendChild(s);
      });
      await driver.sleep(2000);

      console.log('---------------------------------');
      console.log('Send Connectivity Measure Request ');
      console.log("---------------------------------");

      await driver.setNetworkConditions({
        offline: true,
        latency: 400, // Additional latency (ms).
        download_throughput: 50 * 1024, // Maximal aggregated download throughput.
        upload_throughput: 1024 // Maximal aggregated upload throughput.
      });

      //set script timeout
      driver.manage().setTimeouts( { implicit: 2000, pageLoad: 2000, script: 3000 } );

      await driver.executeAsyncScript(async function(downloadURL, latencyURL) {
        var callback = arguments[arguments.length - 1];
        await BioserverNetworkCheck.connectivityMeasure({
          downloadURL: downloadURL,
          uploadURL:   downloadURL,
          latencyURL:  latencyURL,
          onNetworkCheckUpdate: (onNetworkCheckUpdate) => {
            console.log("Network Check Results ", onNetworkCheckUpdate);
            callback(onNetworkCheckUpdate);
          }
        })

      }, downloadURL, latencyURL).catch( (e) => {
        console.log("BioserverNetworkCheck.connectivityMeasure finished with errors: ", e);
        chai.assert.equal(e.name, "ScriptTimeoutError");
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-431", false);
        //throw e;
      });

      await driver.sleep(2000);
    });
  });

  /**
   * Test that BioserverNetworkCheck.connectivityMeasure testcase should return error when no networkConnectivity parameter is used;
   * TODO REVOIR LES ERRORS RETOURNEES AVEC NISHANTH
   */
  describe('bio_connectivity_measure_12', function() {

    it('should return error when no networkConnectivity parameter is used', async () => {
      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);
      await driver.executeScript(async function addNetworkCheckScript(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-network-check.js';
        return window.document.head.appendChild(s);
      });
      await driver.sleep(2000);

      console.log('---------------------------------');
      console.log('Send Connectivity Measure Request ');
      console.log("---------------------------------");

      //set script timeout
      driver.manage().setTimeouts( { implicit: 2000, pageLoad: 2000, script: 3000 } );

      await driver.executeAsyncScript(async function(downloadURL, latencyURL) {
        var callback = arguments[arguments.length - 1];
        await BioserverNetworkCheck.connectivityMeasure()

      }, downloadURL, latencyURL).then(function(str) {

        console.log("Network Check Error ", str);

        chai.assert.equal(str, "Bad input params");

        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-432", false);
      }).catch( (e) => {
        console.log("BioserverNetworkCheck.connectivityMeasure finished with errors: ", e);
        chai.assert.equal(e.name, "ScriptTimeoutError");
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-432", false);
        //throw e;
      });

      await driver.sleep(2000);
    });
  });


});