/*eslint-disable */
var itParam = require('mocha-param');
let chai = require("chai");
//const { Options } = require('selenium-webdriver/chrome');
//const { Builder, By, Key, until } = require('selenium-webdriver');
//const { getExtension, getAddHeaderUrl } = require('chrome-modheader');
const path = require('path');

const {VideoBaseTest} = require(path.resolve('test/utils/videobasetest'));
const {ConstantTestEnvironment} = require(path.resolve('test/utils/constanttestenvironment'));

var basetest = null;
var driver = null;
let downloadURL = '/video-server/network-speed';
let latencyURL = '/video-server/network-latency';
let wrong_downloadURL = '/video/network-speed';
let wrong_latencyURL = '/video/network-latency';

// This line allows use with https
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

async function mediaStreamInitProcess() {
  console.log('--------------------------------------');
  console.log('start Environment Detection Request ');
  console.log('--------------------------------------');
  await basetest.environmentDetectionSuccess(driver);

  console.log('--------------------------------------');
  console.log('start Init Media Devices Request ');
  console.log('--------------------------------------');
  basetest.initMediaDevicesSuccess(driver);

  await driver.sleep(500);
  console.log('--------------------------------------');
  console.log('start Get Device Stream Request ');
  console.log('-------------------------------------- ');
  await basetest.getDeviceStreamSuccess(driver, 1280, 720);
  await driver.sleep(500);
}

function getLivenessMode(mode, securityLevel) {
  return mode+'/'+securityLevel+'?numberOfChallenge=2';
}

describe('BioServer Check Network Connectivity Test', () => {

  /**
   * Before class
   */
  before(async function (){

    basetest = new VideoBaseTest();

    if(basetest.isTestSuite && !basetest.init) {
      await basetest.checkConnection();
      await basetest.createTestLinkBuild();
      basetest.init = true;
    } else {
      console.log("-------------------------");
      console.log("build is already created ");
      console.log("-------------------------");
    }
  });


  /**
   * after each testcase
   */
  afterEach(function(done) {
    // runs after each test in this block
    console.log("---------------");
    console.log("Close Driver ");
    console.log("---------------");
    driver.quit().then(() => {
      done();
      console.log("Closed.");
    });
    //await driver.quit();

  });



  /**
   * Test that BioserverNetworkCheck.connectivityMeasure testcase should return success when good connnection is used;.
   */
  describe('bio_connectivity_measure_01', function() {

    it('should return success when good connnection is used', async () => {
      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);
      ////https://10.126.237.10/video-server/bioserver-video-api.js
      await driver.executeScript(async function documentCall(){
        function loadError(oError) {
          throw new URIError("The script " + oError.target.src + " didn't load correctly.");
        }

        function loadScriptToHead(url, onloadFunction) {
          var newScript = document.createElement("script");
          newScript.onerror = loadError;
          if (onloadFunction) { newScript.onload = onloadFunction; }
          document.head.appendChild(newScript);
          newScript.src = url;
        }

        return loadScriptToHead("https://10.126.237.10/video-server/bioserver-network-check.js");

        // var s2=window.document.createElement('script');
        // s2.src='https://10.126.237.10/video-server/bioserver-video-api.js';
        // return window.document.head.appendChild(s2);;
      });
      await driver.sleep(2000);


      console.log('---------------------------------');
      console.log('Send Connectivity Measure Request ');
      console.log("---------------------------------");
      await driver.executeAsyncScript(async function(downloadURL, latencyURL) {
        var callback = arguments[arguments.length - 1];
        await BioserverNetworkCheck.connectivityMeasure({
          downloadURL: downloadURL,
          uploadURL:   downloadURL,
          latencyURL:  latencyURL,
          onNetworkCheckUpdate: (onNetworkCheckUpdate) => {
            console.log("Network Check Results ", onNetworkCheckUpdate);
            callback(onNetworkCheckUpdate);
          }
        })

      }, downloadURL, latencyURL).then(function(str) {
        console.log('---------------------------------');
        console.log('Send Connectivity Measure Request ');
        console.log("---------------------------------");
        console.log("Network Check Results ", str);

        console.log("Network Check Results: goodConnectivity ? ", str.goodConnectivity);
        chai.assert.equal(str.goodConnectivity, true);

        console.log("Network Check Results: latencyMs ? ", str.latencyMs);
        chai.assert.isBelow(str.latencyMs, 300);

        console.log("Network Check Results: upload ? ", str.upload);
        chai.assert.isAbove(str.upload, 400);

        console.log("Network Check Results: download ? ", str.download);
        chai.assert.equal(str.download, false);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-421", false);
      }).catch( (e) => {
        console.log("BioserverNetworkCheck.connectivityMeasure finished with errors: ", e);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-421", false);
        throw e;
      });

      await driver.sleep(2000);
    });
  });

  var livenessData =
    [
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.LOW.name, status: ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name, video: 'video_4kuhd.y4m' },
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.MEDIUM.name, status: ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name, video: 'video_4kuhd.y4m'},
      { mode: ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, securityLevel: ConstantTestEnvironment.SecurityLevel.HIGH.name, status: ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name, video: 'video_4kuhd.y4m'}
    ];

  describe("basic mocha test with data", function () {
    itParam("test ${value.mode} with SecurityLevel ${value.securityLevel} and ${value.video} ", livenessData, async function (value) {
      //chai.expect(value).to.be.a('number');
      console.log("init browser options");
      const options = VideoBaseTest.getOptions(value.video);
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.no_url);


      await driver.executeScript(async function documentCall(){
        function loadError(oError) {
          throw new URIError("The script " + oError.target.src + " didn't load correctly.");
        }

        function loadScriptToHead(url, onloadFunction) {
          var newScript = document.createElement("script");
          newScript.onerror = loadError;
          if (onloadFunction) { newScript.onload = onloadFunction; }
          document.head.appendChild(newScript);
          newScript.src = url;
        }

        loadScriptToHead("https://10.126.237.10/video-server/bioserver-environment-api.js");
        loadScriptToHead("https://10.126.237.10/video-server/bioserver-video-ui.js");
        loadScriptToHead("https://10.126.237.10/video-server/bioserver-network-check.js");
        return loadScriptToHead("https://10.126.237.10/video-server/bioserver-video-api.js");

        // var s2=window.document.createElement('script');
        // s2.src='https://10.126.237.10/video-server/bioserver-video-api.js';
        // return window.document.head.appendChild(s2);;
      });
      await driver.sleep(2000);

      const sessionId = await VideoBaseTest.createBioSession(value.mode,value.securityLevel);
      await driver.sleep(2000);
      //const decodedJWT = basetest.decodingJWT("eyJhbGciOiJSUzI1NiJ9.eyJsaXZlbmVzc1N0YXR1cyI6IlNVQ0NFU1MiLCJiZXN0SW1hZ2VJZCI6IjZmZWM2MTFiLTU0NTEtNGVjOC05MDc2LTRhYzMxY2Q4NGUyMyIsImxpdmVuZXNzTW9kZSI6IkxJVkVORVNTX1BBU1NJVkUiLCJudW1iZXJPZkNoYWxsZW5nZSI6MSwic2VjdXJpdHlMZXZlbCI6IkxPVyIsImZha2VXZWJjYW0iOmZhbHNlfQ.U7q-Psr2AFrAMjwTo38CoIwHqyQQjyTuvs8s47lU6vajvzbdnrCeBgdBM4Yz45Db5utHDzJLJVwzcK3KKpTelTxvobSge3gLlETsMaUcT2l0bdavsChNZhVpuZn3WQpZR1d9IK7fcwWNojm1NyAdcLYhUsipufaKqpDop_CnBe9Q7hE5Zvt8fNg5wQ9vXBUq8P7whDoQ96v50x5p0iUrfFZAYDcfBwGrUSxIXDBw1n1PhcYdV29Eqnz1bSJtVbjOYsaGlPdXKYF7mr5UVkdhiU120FaYbf0qukh6x5SCWnQsI2AtbOCtzYNbvmzHUUtQ_jVrK8dzI11nmFYip5_dVYJGJh7MkeoAguNSt_cAwFpy6wEafMc7GkUDjmiPnT_9MsujeT508h3L9MgsdS0tFEMLFwkrmf-0tjO4H8Y2wMYhB4tNUBiigvhLM2FhPynl8qhUFz5N-KRgEaA2YPDohkcgJZbT1x4wKMvHk8YJ9QKNVd6cO5oieoJwyWzafbfsaiUiLmZwZyD-Pfw76G-O9TerKPcUU3rnLJFNcA-sfTxNAGws-oxmgZSjgMASrKW6Sne-KLFMQMmUBWe33Bgi5qbsSnN_OrsWsRY3z77bh6SOUcxVAcJ7frKiDrpTINT6uvusohrnPbBO-JqL1kOZZDZtmXnnMrPIpYGvmyDmjx8")
      await mediaStreamInitProcess();

      console.log("--------------------------------------");
      console.log("Add Stream to Html Video Element ");
      console.log("-------------------------------------- ");

      //await driver.executeScript("let videoTag = document.createElement('video'); videoTag.id='video'; document.body.appendChild(videoTag)");

      await driver.executeScript(async function documentCall(){
        const mediaStream = await BioserverVideo.getDeviceStream({video:{frameRate:15, width:1280, height:720, facingMode:"environment"}})
        //const mediaStream = await BioserverVideo.getDeviceStream();
        //document.createElement('video').srcObject = mediaStream;
        return document.querySelector('video').srcObject=mediaStream;
      });

      await driver.sleep(2000);
      console.log("--------------------------------------");
      console.log("start Init Face Capture Client Request ");
      console.log("-------------------------------------- ");
      //Format face capture options request
      const url = ConstantTestEnvironment.base_url;
      //var callback = arguments[arguments.length - 1];
      //const faceCaptureOptions = VideoBaseTest.getFaceCaptureOptions();

      console.log("-------------------");
      console.log("start Face Capture  ");
      console.log("-------------------");

      await driver.executeAsyncScript(async function(url, sessionId) {

        var callback = arguments[arguments.length - 1];
        const faceCaptureOptions = {
          wspath: '/video-server/engine.io',
          bioserverVideoUrl: url,
          rtcConfigurationPath: url + '/video-server/coturnService?bioSessionId=' + encodeURIComponent(sessionId),
          bioSessionId: sessionId,
          trackingFn: (trackingInfo) => {console.log('onTracking', trackingInfo);},
          errorFn: (error) => {console.log('face capture error', error);},
          showVideoFile: (recordedVideoFile) => {console.log('showVideoFile ', recordedVideoFile);},
          showChallengeInstruction: (challengeInstruction) => {console.log('challenge instructions', challengeInstruction);},
          showChallengeResult: (showChallengeResult) => {
            console.log('call back the backend to retrieve liveness result', showChallengeResult);
            callback(showChallengeResult); }};

        const mediaStream = await navigator.mediaDevices.getUserMedia({video: true});
        const captureClient = await BioserverVideo.initFaceCaptureClient(faceCaptureOptions);
        await captureClient.start(mediaStream, false, 'video');

      }, url, sessionId).then(async function(challengeResult) {

        console.log("challengeResult ", challengeResult);
        await VideoBaseTest.getLivenessChallengeResultWithPooling(getLivenessMode(value.mode, value.securityLevel),
          value.securityLevel, value.status);
        //chai.assert.equal(str.goodConnectivity, true);
      });
      // await driver.executeScript(async function documentCall(faceCaptureOptions){
      //   const mediaStream = await navigator.mediaDevices.getUserMedia({video: true});
      //   //const mediaStream = await BioserverVideo.getDeviceStream();
      //   const captureClient = await BioserverVideo.initFaceCaptureClient(faceCaptureOptions);
      //   return  await captureClient.start(mediaStream, false, 'video');
      // }, faceCaptureOptions);

      await driver.sleep(4000);

      console.log("--------------------------------");
      console.log("Get Liveness Challenge Result  ");
      console.log("--------------------------------");

      await VideoBaseTest.getLivenessChallengeResultWithPooling(getLivenessMode(value.mode, value.securityLevel),
        value.securityLevel, value.status);
    });
  });
});