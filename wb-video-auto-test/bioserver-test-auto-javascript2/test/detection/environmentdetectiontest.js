/*eslint-disable */

let chai = require("chai");
const path = require('path');
const { Options } = require('selenium-webdriver/chrome');
const { Builder, By, Key, until } = require('selenium-webdriver');
//const { getExtension, getAddHeaderUrl } = require('chrome-modheader');

//path.resolve('test/resources/video_hd.y4m')
const {VideoBaseTest} = require(path.resolve('test/utils/videobasetest'));
const {ConstantTestEnvironment} = require(path.resolve('test/utils/constanttestenvironment'));

var basetest = null;

// This line allows use with https
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

function getOptions(userAgent) {
  const opts = new Options().addArguments([userAgent],
    '--headless',
    '--disable-gpu',
    '--no-sandbox',
    '--allow-insecure-localhost',
    '--ignore-certificate-errors',
    '--acceptInsecureCerts');
  const driver = new Builder()
    .forBrowser('chrome')
    .setChromeOptions(opts)
    .build();
  return driver;
}

describe('BioServer Environment Detection Test', () => {

  //const options = new Options().addExtensions(getExtension());


  before(async function (){

    basetest = new VideoBaseTest();
    if(basetest.isTestSuite && !basetest.init) {
      await basetest.checkConnection();
      await basetest.createTestLinkBuild();
      basetest.init = true;
    } else {
      console.log("-------------------------");
      console.log("build is already created ");
      console.log("-------------------------");
    }
  });

  // beforeEach(async function() {
  //
  //   console.log("init selenium browser ");
  //   driver = new Builder()
  //     .forBrowser('chrome')
  //     .setChromeOptions(options)
  //     .build();
  // });

  // afterEach(function(done) {
  //   // runs after each test in this block
  //   console.log("---------------");
  //   console.log("Close Driver ");
  //   console.log("---------------");
  //   driver.quit().then(() => {
  //     done();
  //     console.log("Closed.");
  //   });
  //   //await driver.quit();
  //
  // });


  /**
   * Test that BioserverEnvironment.detection testcase should return the current supported  environment;
   */
  describe('bio_detection_01', function() {
    it('should detect BioServer environment', async () => {

      const driver = getOptions('user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36"');

      await driver.get(ConstantTestEnvironment.no_url);

      await driver.executeScript(async function documentCall(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-environment-api.js';
        return window.document.head.appendChild(s);;
      });

      await driver.sleep(2000);

      await driver.executeScript("return BioserverEnvironment.detection()")
        .then(async function (value) {
          console.log("-------------------------");
          console.log("Env detection Response: ");
          console.log("-------------------------");

          console.log("Env Browser supported ? : " + value.envDetected.browser.isSupported);
          chai.assert.equal(value.envDetected.browser.isSupported, true);

          console.log("Env Browser supportedList : " + value.envDetected.browser.supportedList);
          chai.assert.equal(value.envDetected.browser.supportedList.length, 0);

          console.log("Env OS isMobile ? : " + value.envDetected.os.isMobile);
          chai.assert.equal(value.envDetected.os.isMobile, false);

          console.log("Env OS isSupported ? : " + value.envDetected.os.isSupported);
          chai.assert.equal(value.envDetected.os.isSupported, true);

          console.log("Env OS supportedList : " + value.envDetected.os.supportedList);
          chai.assert.equal(value.envDetected.os.supportedList.length, 0);
          //upddate testlink
          await basetest.runOnTestLink("BioServer-TC-323", true);

        }).catch(async (e) => {
          console.log("Env Detection async finished with errors: #{mediaDevices}", e);
          await basetest.runOnTestLink("BioServer-TC-323", false);
          throw e;
        });

      console.log("---------------");
      console.log("Close Driver ");
      console.log("---------------");
      await driver.quit();

      await driver.sleep(2000);
      // await driver.executeScript("return navigator.userAgent")
      //   .then(function (agent) {
      //     console.log("-------------------------");
      //     console.log("User Agent: " + agent);
      //
      //   });
    });
  });


  /**
   * Test that BioserverEnvironment.detection testcase should return supported os list when current os is not detected;
   */
  describe('bio_detection_03', function() {
    it('should not detect BioServer environment os', async () => {

      const driver = getOptions('user-agent="Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36"');


      await driver.get(ConstantTestEnvironment.no_url);

      await driver.executeScript(async function documentCall(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-environment-api.js';
        return window.document.head.appendChild(s);;
      });
      await driver.sleep(2000);

      await driver.executeScript("return BioserverEnvironment.detection()")
        .then(async function (value) {
          console.log("-------------------------");
          console.log("Env detection Response: ");
          console.log("-------------------------");

          console.log("Env Browser supported ? : " + value.envDetected.browser.isSupported);
          chai.assert.equal(value.envDetected.browser.isSupported, false);

          console.log(value.message);
          chai.assert.equal(value.message, "You seem to be using an unsupported operating system.");

          console.log("Env OS isSupported ? : " + value.envDetected.os.isSupported);
          chai.assert.equal(value.envDetected.os.isSupported, false);

          console.log("Env OS supportedList.length : " + value.envDetected.browser.supportedList.length);
          chai.assert.equal(value.envDetected.os.supportedList.length, 6);

          console.log("envDetected.os.supportedList[0] : " + value.envDetected.os.supportedList[0]);
          chai.assert.equal(value.envDetected.os.supportedList[0], "Windows");

          console.log("envDetected.os.supportedList[1] : " + value.envDetected.os.supportedList[1]);
          chai.assert.equal(value.envDetected.os.supportedList[1], "Mac OS");

          console.log("envDetected.os.supportedList[2] : " + value.envDetected.browser.supportedList[2]);
          chai.assert.equal(value.envDetected.os.supportedList[2], "iOS");

          console.log("envDetected.os.supportedList[3] : " + value.envDetected.os.supportedList[3]);
          chai.assert.equal(value.envDetected.os.supportedList[3], "Android");

          console.log("envDetected.os.supportedList[4] : " + value.envDetected.os.supportedList[4]);
          chai.assert.equal(value.envDetected.os.supportedList[4], "Linux");

          console.log("envDetected.os.supportedList[5] : " + value.envDetected.os.supportedList[5]);
          chai.assert.equal(value.envDetected.os.supportedList[5], "Ubuntu");
          //update testlink
          await basetest.runOnTestLink("BioServer-TC-325", true);


        }).catch(async (e) => {
          console.log("Env Detection async finished with errors: #{mediaDevices}", e);
          await basetest.runOnTestLink("BioServer-TC-325", false);
          throw e;
        });

      console.log("---------------");
      console.log("Close Driver ");
      console.log("---------------");
      await driver.quit();
      await driver.sleep(2000);
    });
  });

  /**
   * Test that BioserverEnvironment.detection testcase should return supported browser list when current browser is not supported;
   */
  describe('bio_detection_04', function() {
    it('should not detect BioServer environment browser', async () => {


      const driver = getOptions('user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Safari/87.0.4280.88 Safari/537.36"');

      await driver.get(ConstantTestEnvironment.no_url);

      await driver.executeScript(async function documentCall(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-environment-api.js';
        return window.document.head.appendChild(s);;
      });
      await driver.sleep(2000);

      await driver.executeScript("return BioserverEnvironment.detection()")
        .then(async function (value) {
          console.log("-------------------------");
          console.log("Env detection Response: ");
          console.log("-------------------------");

          console.log("Env Browser supported ? : " + value.envDetected.browser.isSupported);
          chai.assert.equal(value.envDetected.browser.isSupported, false);

          console.log(value.message);
          chai.assert.equal(value.message, "You seem to be using an unsupported browser.");

          console.log("Env Browser supportedList.length : " + value.envDetected.browser.supportedList.length);
          chai.assert.equal(value.envDetected.browser.supportedList.length, 4);

          console.log("envDetected.browser.supportedList[0] : " + value.envDetected.browser.supportedList[0].name);
          chai.assert.equal(value.envDetected.browser.supportedList[0].minimumVersion, "57");
          chai.assert.equal(value.envDetected.browser.supportedList[0].name, "Chrome");

          console.log("envDetected.browser.supportedList[1] : " + value.envDetected.browser.supportedList[1].name);
          chai.assert.equal(value.envDetected.browser.supportedList[1].minimumVersion, "52");
          chai.assert.equal(value.envDetected.browser.supportedList[1].name, "Firefox");

          console.log("envDetected.browser.supportedList[2] : " + value.envDetected.browser.supportedList[2].name);
          chai.assert.equal(value.envDetected.browser.supportedList[2].minimumVersion, "47");
          chai.assert.equal(value.envDetected.browser.supportedList[2].name, "Opera");

          console.log("envDetected.browser.supportedList[3] : " + value.envDetected.browser.supportedList[3].name);
          chai.assert.equal(value.envDetected.browser.supportedList[3].minimumVersion, "17");
          chai.assert.equal(value.envDetected.browser.supportedList[3].name, "Edge");

          console.log("Env OS isMobile ? : " + value.envDetected.os.isMobile);
          //chai.assert.equal(value.envDetected.os.isMobile, false);

          console.log("Env OS isSupported ? : " + value.envDetected.os.isSupported);
          chai.assert.equal(value.envDetected.os.isSupported, true);

          console.log("Env OS supportedList : " + value.envDetected.os.supportedList);
          chai.assert.equal(value.envDetected.os.supportedList.length, 0);
          //update testlink
          await basetest.runOnTestLink("BioServer-TC-326", true);

        }).catch(async (e) => {
          console.log("Env Detection async finished with errors: #{mediaDevices}", e);
          await basetest.runOnTestLink("BioServer-TC-326", false);
          throw e;
        });

      console.log("---------------");
      console.log("Close Driver ");
      console.log("---------------");
      await driver.quit();
      await driver.sleep(2000);
    });
  });

  /**
   * Test that BioserverEnvironment.detection testcase should return supported browser list when Headless Chrome 74 on Linux is used;
   */
  describe('bio_detection_05', function() {
    it('Headless Chrome 74 on Linux is used', async () => {

      const driver = getOptions('user-agent="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/74.0.3729.157 Safari/537.36"');

      await driver.get(ConstantTestEnvironment.no_url);

      await driver.executeScript(async function documentCall(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-environment-api.js';
        return window.document.head.appendChild(s);;
      });
      await driver.sleep(2000);

      await driver.executeScript("return BioserverEnvironment.detection()")
        .then(async function (value) {
          console.log("-------------------------");
          console.log("Env detection Response: ");
          console.log("-------------------------");

          console.log("Env Browser supported ? : " + value.envDetected.browser.isSupported);
          chai.assert.equal(value.envDetected.browser.isSupported, false);

          console.log(value.message);
          chai.assert.equal(value.message, "You seem to be using an unsupported browser.");

          console.log("Env Browser supportedList.length : " + value.envDetected.browser.supportedList.length);
          chai.assert.equal(value.envDetected.browser.supportedList.length, 3);

          console.log("envDetected.browser.supportedList[0] : " + value.envDetected.browser.supportedList[0].name);
          chai.assert.equal(value.envDetected.browser.supportedList[0].minimumVersion, "57");
          chai.assert.equal(value.envDetected.browser.supportedList[0].name, "Chrome");

          console.log("envDetected.browser.supportedList[1] : " + value.envDetected.browser.supportedList[1].name);
          chai.assert.equal(value.envDetected.browser.supportedList[1].minimumVersion, "52");
          chai.assert.equal(value.envDetected.browser.supportedList[1].name, "Firefox");

          console.log("envDetected.browser.supportedList[2] : " + value.envDetected.browser.supportedList[2].name);
          chai.assert.equal(value.envDetected.browser.supportedList[2].minimumVersion, "47");
          chai.assert.equal(value.envDetected.browser.supportedList[2].name, "Opera");

          console.log("Env OS isMobile ? : " + value.envDetected.os.isMobile);
          //chai.assert.equal(value.envDetected.os.isMobile, false);

          console.log("Env OS isSupported ? : " + value.envDetected.os.isSupported);
          chai.assert.equal(value.envDetected.os.isSupported, true);

          console.log("Env OS supportedList : " + value.envDetected.os.supportedList);
          chai.assert.equal(value.envDetected.os.supportedList.length, 0);
          //update testlink
          await basetest.runOnTestLink("BioServer-TC-475", true);

        }).catch(async (e) => {
          console.log("Env Detection async finished with errors: #{mediaDevices}", e);
          await basetest.runOnTestLink("BioServer-TC-475", false);
          throw e;
        });

      console.log("---------------");
      console.log("Close Driver ");
      console.log("---------------");
      await driver.quit();
      await driver.sleep(2000);
    });
  });

  /**
   * Test that BioserverEnvironment.detection testcase should return success when Os Mac is used;
   */
  describe('bio_detection_06', function() {
    it('should return success when Os Mac is used', async () => {

      const driver = getOptions('user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36"');

      await driver.get(ConstantTestEnvironment.no_url);

      await driver.executeScript(async function documentCall(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-environment-api.js';
        return window.document.head.appendChild(s);;
      });
      await driver.sleep(3000);

      await driver.executeScript("return BioserverEnvironment.detection()")
        .then(async function (value) {
          console.log("-------------------------");
          console.log("Env detection Response: ");
          console.log("-------------------------");

          console.log("Env Browser supported ? : " + value.envDetected.browser.isSupported);
          chai.assert.equal(value.envDetected.browser.isSupported, true);

          console.log("Env Browser supportedList : " + value.envDetected.browser.supportedList);
          chai.assert.equal(value.envDetected.browser.supportedList.length, 0);

          console.log("Env OS isMobile ? : " + value.envDetected.os.isMobile);
          chai.assert.equal(value.envDetected.os.isMobile, false);

          console.log("Env OS isSupported ? : " + value.envDetected.os.isSupported);
          chai.assert.equal(value.envDetected.os.isSupported, true);

          console.log("Env OS supportedList : " + value.envDetected.os.supportedList);
          chai.assert.equal(value.envDetected.os.supportedList.length, 0);
          //upddate testlink
          await basetest.runOnTestLink("BioServer-TC-476", true);

        }).catch(async (e) => {
          console.log("Env Detection async finished with errors: #{mediaDevices}", e);
          await basetest.runOnTestLink("BioServer-TC-476", false);
          throw e;
        });

      console.log("---------------");
      console.log("Close Driver ");
      console.log("---------------");
      await driver.quit();
      await driver.sleep(2000);
    });
  });

  /**
   * Test that BioserverEnvironment.detection testcase should return supported os list when Freebsd os is used;
   */
  describe('bio_detection_07', function() {
    it('should return supported os list when Freebsd os is used', async () => {

      const driver = getOptions('user-agent="Mozilla/5.0 (Syllable; FreeBSD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36"');

      await driver.get(ConstantTestEnvironment.no_url);

      await driver.executeScript(async function documentCall(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-environment-api.js';
        return window.document.head.appendChild(s);;
      });
      await driver.sleep(3000);

      await driver.executeScript("return BioserverEnvironment.detection()")
        .then(async function (value) {
          console.log("-------------------------");
          console.log("Env detection Response: ");
          console.log("-------------------------");

          console.log("Env Browser supported ? : " + value.envDetected.browser.isSupported);
          chai.assert.equal(value.envDetected.browser.isSupported, false);

          console.log(value.message);
          chai.assert.equal(value.message, "You seem to be using an unsupported operating system.");

          console.log("Env OS isSupported ? : " + value.envDetected.os.isSupported);
          chai.assert.equal(value.envDetected.os.isSupported, false);

          console.log("Env OS supportedList.length : " + value.envDetected.browser.supportedList.length);
          chai.assert.equal(value.envDetected.os.supportedList.length, 6);

          console.log("envDetected.os.supportedList[0] : " + value.envDetected.os.supportedList[0]);
          chai.assert.equal(value.envDetected.os.supportedList[0], "Windows");

          console.log("envDetected.os.supportedList[1] : " + value.envDetected.os.supportedList[1]);
          chai.assert.equal(value.envDetected.os.supportedList[1], "Mac OS");

          console.log("envDetected.os.supportedList[2] : " + value.envDetected.browser.supportedList[2]);
          chai.assert.equal(value.envDetected.os.supportedList[2], "iOS");

          console.log("envDetected.os.supportedList[3] : " + value.envDetected.os.supportedList[3]);
          chai.assert.equal(value.envDetected.os.supportedList[3], "Android");

          console.log("envDetected.os.supportedList[4] : " + value.envDetected.os.supportedList[4]);
          chai.assert.equal(value.envDetected.os.supportedList[4], "Linux");

          console.log("envDetected.os.supportedList[5] : " + value.envDetected.os.supportedList[5]);
          chai.assert.equal(value.envDetected.os.supportedList[5], "Ubuntu");
          //update testlink
          await basetest.runOnTestLink("BioServer-TC-477", true);

        }).catch(async (e) => {
          console.log("Env Detection async finished with errors: #{mediaDevices}", e);
          await basetest.runOnTestLink("BioServer-TC-477", false);
          throw e;
        });

      console.log("---------------");
      console.log("Close Driver ");
      console.log("---------------");
      await driver.quit();
      await driver.sleep(2000);
    });
  });

  /**
   * Test that BioserverEnvironment.detection testcase should return Mobile supported;
   * TODO A RAJOUTER SUR TL
   */
  describe('bio_detection_08', function() {
    it('should return Mobile supported', async () => {

      const driver = getOptions('user-agent="Mozilla/5.0 (Linux; Android 9; SM-G950F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Mobile Safari/537.36"');

      await driver.get(ConstantTestEnvironment.no_url);

      await driver.executeScript(async function documentCall(){
        var s=window.document.createElement('script');
        s.src='https://10.126.237.10/video-server/bioserver-environment-api.js';
        return window.document.head.appendChild(s);;
      });
      await driver.sleep(3000);

      await driver.executeScript("return BioserverEnvironment.detection()")
        .then(async function (value) {
          console.log("-------------------------");
          console.log("Env detection Response: ");
          console.log("-------------------------");

          console.log("Env Browser supported ? : " + value.envDetected.browser.isSupported);
          chai.assert.equal(value.envDetected.browser.isSupported, true);

          console.log("Env Browser supportedList : " + value.envDetected.browser.supportedList);
          chai.assert.equal(value.envDetected.browser.supportedList.length, 0);

          console.log("Env OS isMobile ? : " + value.envDetected.os.isMobile);
          chai.assert.equal(value.envDetected.os.isMobile, true);

          console.log("Env OS isSupported ? : " + value.envDetected.os.isSupported);
          chai.assert.equal(value.envDetected.os.isSupported, true);

          console.log("Env OS supportedList : " + value.envDetected.os.supportedList);
          chai.assert.equal(value.envDetected.os.supportedList.length, 0);
          //upddate testlink
          //await basetest.runOnTestLink("BioServer-TC-323", true);

        }).catch(async (e) => {
          console.log("Env Detection async finished with errors: #{mediaDevices}", e);
          //await basetest.runOnTestLink("BioServer-TC-477", false);
          throw e;
        });

      console.log("---------------");
      console.log("Close Driver ");
      console.log("---------------");
      await driver.quit();
      await driver.sleep(2000);
    });
  });
});
//module.exports.environmentdetection = environmentdetection;