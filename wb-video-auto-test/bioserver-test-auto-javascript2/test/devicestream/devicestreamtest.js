/*eslint-disable */


let chai = require("chai");
//const { Options } = require('selenium-webdriver/chrome');
//const { Builder, By, Key, until } = require('selenium-webdriver');
//const { getExtension, getAddHeaderUrl } = require('chrome-modheader');
const path = require('path');

const {VideoBaseTest} = require(path.resolve('test/utils/videobasetest'));
const {ConstantTestEnvironment} = require(path.resolve('test/utils/constanttestenvironment'));

var basetest = null;
var driver = null;

// This line allows use with https
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('BioServer Get Devices Stream Test', () => {

  //const options = new Options().addExtensions(getExtension());

  before(async function (){

    basetest = new VideoBaseTest();
    if(basetest.isTestSuite && !basetest.init) {
      if (basetest.isTestLinkEnabled) {
        await basetest.checkConnection();
        await basetest.createTestLinkBuild();
        basetest.init = true;
      }
    } else {
      console.log("-------------------------");
      console.log("build is already created ");
      console.log("-------------------------");
    }
  });

  // beforeEach(async function() {
  //   console.log("init selenium browser ");
  //   driver = new Builder()
  //     .forBrowser('chrome')
  //     .setChromeOptions(options)
  //     .build();
  //   console.log("------------------------");
  //   console.log("Start Selenium Webdriver");
  //   console.log("------------------------");
  //   // runs before each test in this block
  //   await driver.get(ConstantTestEnvironment.video_server_url);
  // });

  afterEach(function(done) {
    // runs after each test in this block
    console.log("---------------");
    console.log("Close Driver ");
    console.log("---------------");
    driver.quit().then(() => {
      done();
      console.log("Closed.");
    });
    //await driver.quit();

  });



  /**
   * Test that BioserverVideo.getDeviceStream testcase should return media stream when valid request is sent.
   */
  describe('bio_get_device_stream_01', function() {
    it('should return media stream', async () => {

      //https://fr.webcamtests.com/
      ////"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --use-fake-device-for-media-stream --use-file-for-fake-video-capture=C://Users//G597970//Downloads//back_DL.mp4.y4m
      //await driver.get(getAddHeaderUrl('apikey', 'BIOSRVTEST'));

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request
      deviceConstraints= {
        audio:{deviceId:VideoBaseTest.devicesResult.audioDevices[1].deviceId},
        video:{frameRate:15, width:1280, height:720, facingMode:"environment", deviceId:VideoBaseTest.devicesResult.videoDevices[0].deviceId},
        errorFn: (err) => {
          console.debug("get User Media Stream failed", err);
        }
      };

      await driver.executeAsyncScript(async function(deviceConstraints, callback) {

        callback(await BioserverVideo.getDeviceStream(deviceConstraints));

      }, deviceConstraints).then(async (deviceStream) => {

          console.log("-------------------------------------------");
          console.log("The list of user camera devices Response: ");
          console.log("------------------------------------------");

          console.log("MediaStream.resolution : " + deviceStream.resolution);
          chai.assert.equal(deviceStream.resolution, "1280x720");

          console.log("MediaStream.id : " + deviceStream.id);
          chai.assert.isNotNull(deviceStream.id);

          console.log("MediaStream.active ? : " + deviceStream.active);
          chai.assert.equal(deviceStream.active, true);

          console.log("MediaStream.onaddtrack : " + deviceStream.onaddtrack);
          chai.assert.equal(deviceStream.onaddtrack, null);

          console.log("MediaStream.onremovetrack : " + deviceStream.onremovetrack);
          chai.assert.equal(deviceStream.onremovetrack, null);

          console.log("MediaStream.onactive : " + deviceStream.onactive);
          chai.assert.equal(deviceStream.onactive, null);

          console.log("MediaStream.oninactive : " + deviceStream.oninactive);
          chai.assert.equal(deviceStream.oninactive, null);
          await basetest.runOnTestLink("BioServer-TC-337", true);

        })
        .catch(async (e) => {
          console.log("Get Devices Stream async finished with errors: ", e);
          await basetest.runOnTestLink("BioServer-TC-337", false);
          throw e;
        });
    });
  });

  /**
   * Test that BioserverVideo.getDeviceStream testcase should return media stream when video parameter is not sent in the request.
   */
  describe('bio_get_device_stream_03', function() {
    it('should return media stream when video parameter is not sent', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request
      deviceConstraints= {
        audio:{deviceId:VideoBaseTest.devicesResult.audioDevices[1].deviceId},
        errorFn: (err) => {
          console.debug("get User Media Stream failed", err);
        }
      };

      await driver.executeAsyncScript(async function(deviceConstraints, callback) {

        callback(await BioserverVideo.getDeviceStream(deviceConstraints));

      }, deviceConstraints).then(async (deviceStream) => {

        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        console.log("MediaStream.resolution : " + deviceStream.resolution);
        chai.assert.equal(deviceStream.resolution, "1280x720");

        console.log("MediaStream.id : " + deviceStream.id);
        chai.assert.isNotNull(deviceStream.id);

        console.log("MediaStream.active ? : " + deviceStream.active);
        chai.assert.equal(deviceStream.active, true);
        await basetest.runOnTestLink("BioServer-TC-339", true);
      }).catch(async (e) => {
        console.log("Get Devices Stream async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-339", false);
        throw e;
        });
    });
  });

  /**
   * Test that BioserverVideo.getDeviceStream testcase should return media stream when frameRate parameter is not sent in the request.
   */
  describe('bio_get_device_stream_04', function() {
    it('should return media stream when frameRate parameter is not sent', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request
      deviceConstraints= {
        audio:{deviceId:VideoBaseTest.devicesResult.audioDevices[1].deviceId},
        video:{width:1280, height:720, facingMode:"environment", deviceId:VideoBaseTest.devicesResult.videoDevices[0].deviceId},
        errorFn: (err) => {
          console.debug("get User Media Stream failed", err);
        }
      };

      await driver.executeAsyncScript(async function(deviceConstraints, callback) {

        callback(await BioserverVideo.getDeviceStream(deviceConstraints));

      }, deviceConstraints).then(async (deviceStream) => {

        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        console.log("MediaStream.resolution : " + deviceStream.resolution);
        chai.assert.equal(deviceStream.resolution, "1280x720");

        console.log("MediaStream.id : " + deviceStream.id);
        chai.assert.isNotNull(deviceStream.id);

        console.log("MediaStream.active ? : " + deviceStream.active);
        chai.assert.equal(deviceStream.active, true);
        await basetest.runOnTestLink("BioServer-TC-340", true);
      })
        .catch(async (e) => {
          console.log("Get Devices Stream async finished with errors: ", e);
          await basetest.runOnTestLink("BioServer-TC-340", false);
          throw e;
        });
    });
  });

  /**
   * Test that BioserverVideo.getDeviceStream testcase should return media stream when height parameter is not sent in the request.
   */
  describe('bio_get_device_stream_05', function() {
    it('should return media stream when height parameter is not sent', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request
      deviceConstraints= {
        audio:{deviceId:VideoBaseTest.devicesResult.audioDevices[1].deviceId},
        video:{frameRate:15, width:1280, facingMode:"environment", deviceId:VideoBaseTest.devicesResult.videoDevices[0].deviceId},
        errorFn: (err) => {
          console.debug("get User Media Stream failed", err);
        }
      };

      await driver.executeAsyncScript(async function(deviceConstraints, callback) {

        callback(await BioserverVideo.getDeviceStream(deviceConstraints));

      }, deviceConstraints).then(async (deviceStream) => {

        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        console.log("MediaStream.resolution : " + deviceStream.resolution);
        chai.assert.equal(deviceStream.resolution, "1280x720");

        console.log("MediaStream.id : " + deviceStream.id);
        chai.assert.isNotNull(deviceStream.id);

        console.log("MediaStream.active ? : " + deviceStream.active);
        chai.assert.equal(deviceStream.active, true);
        await basetest.runOnTestLink("BioServer-TC-341", true);
      })
        .catch(async (e) => {
          console.log("Get Devices Stream async finished with errors: ", e);
          await basetest.runOnTestLink("BioServer-TC-341", false);
          throw e;
        });
    });
  });

  /**
   * Test that BioserverVideo.getDeviceStream testcase should return media stream when facingMode parameter is not sent in the request.
   */
  describe('bio_get_device_stream_06', function() {
    it('should return media stream when facingMode parameter is not sent', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request
      deviceConstraints= {
        audio:{deviceId:VideoBaseTest.devicesResult.audioDevices[1].deviceId},
        video:{frameRate:15, width:1280, height:720, deviceId:VideoBaseTest.devicesResult.videoDevices[0].deviceId},
        errorFn: (err) => {
          console.debug("get User Media Stream failed", err);
        }
      };

      await driver.executeAsyncScript(async function(deviceConstraints, callback) {

        callback(await BioserverVideo.getDeviceStream(deviceConstraints));

      }, deviceConstraints).then(async (deviceStream) => {

        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        console.log("MediaStream.resolution : " + deviceStream.resolution);
        chai.assert.equal(deviceStream.resolution, "1280x720");

        console.log("MediaStream.id : " + deviceStream.id);
        chai.assert.isNotNull(deviceStream.id);

        console.log("MediaStream.active ? : " + deviceStream.active);
        chai.assert.equal(deviceStream.active, true);
        await basetest.runOnTestLink("BioServer-TC-342", true);
      })
        .catch(async (e) => {
          console.log("Get Devices Stream async finished with errors: ", e);
          await basetest.runOnTestLink("BioServer-TC-342", true);
          throw e;
        });
    });
  });

  /**
   * Test that BioserverVideo.getDeviceStream testcase should return media stream when deviceId parameter is not sent in the request.
   */
  describe('bio_get_device_stream_07', function() {
    it('should return media stream when deviceId parameter is not sent', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request
      deviceConstraints= {
        audio:{deviceId:VideoBaseTest.devicesResult.audioDevices[1].deviceId},
        video:{frameRate:15, width:1280, height:720, facingMode:"environment"},
        errorFn: (err) => {
          console.debug("get User Media Stream failed", err);
        }
      };

      await driver.executeAsyncScript(async function(deviceConstraints, callback) {

        callback(await BioserverVideo.getDeviceStream(deviceConstraints));

      }, deviceConstraints).then(async (deviceStream) => {

        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        console.log("MediaStream.resolution : " + deviceStream.resolution);
        chai.assert.equal(deviceStream.resolution, "1280x720");

        console.log("MediaStream.id : " + deviceStream.id);
        chai.assert.isNotNull(deviceStream.id);

        console.log("MediaStream.active ? : " + deviceStream.active);
        chai.assert.equal(deviceStream.active, true);
        await basetest.runOnTestLink("BioServer-TC-343", true);
      })
        .catch(async (e) => {
          console.log("Get Devices Stream async finished with errors: ", e);
          await basetest.runOnTestLink("BioServer-TC-343", false);
          throw e;
        });
    });
  });

  /**
   * Test that BioserverVideo.getDeviceStream testcase should return media stream when errorFn parameter is not sent in the request.
   */
  describe('bio_get_device_stream_08', function() {
    it('should return media stream when errorFn parameter is not sent', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request
      deviceConstraints= {
        audio:{deviceId:VideoBaseTest.devicesResult.audioDevices[1].deviceId},
        video:{frameRate:15, width:1280, height:720, facingMode:"environment", deviceId:VideoBaseTest.devicesResult.videoDevices[0].deviceId}
      };

      await driver.executeAsyncScript(async function(deviceConstraints, callback) {

        callback(await BioserverVideo.getDeviceStream(deviceConstraints));

      }, deviceConstraints).then(async (deviceStream) => {

        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        console.log("MediaStream.resolution : " + deviceStream.resolution);
        chai.assert.equal(deviceStream.resolution, "1280x720");

        console.log("MediaStream.id : " + deviceStream.id);
        chai.assert.isNotNull(deviceStream.id);

        console.log("MediaStream.active ? : " + deviceStream.active);
        chai.assert.equal(deviceStream.active, true);
        await basetest.runOnTestLink("BioServer-TC-344", true);
      })
        .catch(async (e) => {
          console.log("Get Devices Stream async finished with errors: ", e);
          await basetest.runOnTestLink("BioServer-TC-344", false);
          throw e;
        });
    });
  });


  /**
   * Test that BioserverVideo.getDeviceStream testcase should return media stream when facingMode is set to "user".
   */
  describe('bio_get_device_stream_09', function() {
    it('facingMode is set to "user"', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request
      deviceConstraints= {
        audio:{deviceId:VideoBaseTest.devicesResult.audioDevices[1].deviceId},
        video:{frameRate:15, width:1280, height:720, facingMode:"user", deviceId:VideoBaseTest.devicesResult.videoDevices[0].deviceId},
        errorFn: (err) => {
          console.debug("get User Media Stream failed", err);
        }
      };

      await driver.executeAsyncScript(async function(deviceConstraints, callback) {

        callback(await BioserverVideo.getDeviceStream(deviceConstraints));

      }, deviceConstraints).then(async (deviceStream) => {

        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        console.log("MediaStream.resolution : " + deviceStream.resolution);
        chai.assert.equal(deviceStream.resolution, "1280x720");

        console.log("MediaStream.id : " + deviceStream.id);
        chai.assert.isNotNull(deviceStream.id);

        console.log("MediaStream.active ? : " + deviceStream.active);
        chai.assert.equal(deviceStream.active, true);
        await basetest.runOnTestLink("BioServer-TC-345", true);
      }).catch(async (e) => {
        console.log("Get Devices Stream async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-345", false);
        throw e;
      });
    });
  });

  /**
   * Test that BioserverVideo.getDeviceStream testcase should return media stream when facingMode is set to "environment".
   */
  describe('bio_get_device_stream_10', function() {
    it('facingMode is set to "user"', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request
      deviceConstraints= {
        audio:{deviceId:VideoBaseTest.devicesResult.audioDevices[1].deviceId},
        video:{frameRate:15, width:1280, height:720, facingMode:"environment", deviceId:VideoBaseTest.devicesResult.videoDevices[0].deviceId},
        errorFn: (err) => {
          console.debug("get User Media Stream failed", err);
        }
      };

      await driver.executeAsyncScript(async function(deviceConstraints, callback) {

        callback(await BioserverVideo.getDeviceStream(deviceConstraints));

      }, deviceConstraints).then(async (deviceStream) => {

        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        console.log("MediaStream.resolution : " + deviceStream.resolution);
        chai.assert.equal(deviceStream.resolution, "1280x720");

        console.log("MediaStream.id : " + deviceStream.id);
        chai.assert.isNotNull(deviceStream.id);

        console.log("MediaStream.active ? : " + deviceStream.active);
        chai.assert.equal(deviceStream.active, true);
        await basetest.runOnTestLink("BioServer-TC-346", true);
      }).catch(async (e) => {
        console.log("Get Devices Stream async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-346", false);
        throw e;
      });
    });
  });

  /**
   * Test that BioserverVideo.getDeviceStream testcase should return Unsupported resolution when deviceConstraints set to {width: 1920, height: 1080}.
   */
  describe('bio_get_device_stream_12', function() {
    it('should return Unsupported resolution', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request


      await driver.executeAsyncScript(async function() {

        var callback = arguments[arguments.length - 1];
        deviceConstraints= {
          video:{frameRate:15, width:1920, height:1080, facingMode:"environment"},
          errorFn: (err) => {
            console.debug("get User Media Stream failed", err);
            callback(err)
          }
        };
        await BioserverVideo.getDeviceStream(deviceConstraints)

      }).then(async function(deviceStream) {
        console.log("-----------------------------");
        console.log("Device Stream Error Result: ");
        console.log("-----------------------------");

        console.log("MediaStream.code : " + deviceStream.code);
        chai.assert.equal(deviceStream.code, 1304);

        console.log("MediaStream.error : " + deviceStream.error);

        chai.assert.equal(deviceStream.error, "Resolution is not supported");
        await basetest.runOnTestLink("BioServer-TC-348", true);
      }).catch(async (e) => {
        console.log("Get Devices Stream async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-348", false);
        throw e;
      });
    });
  });

  /**
   * Test that BioserverVideo.getDeviceStream testcase should return unsupported resolution when deviceConstraints set to {width: 3840, height: 2160}.
   */
  describe('bio_get_device_stream_13', function() {
    it('should return unsupported resolution: {width: 3840, height: 2160}', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request


      await driver.executeAsyncScript(async function() {

        var callback = arguments[arguments.length - 1];
        deviceConstraints= {
          video:{frameRate:15, width:3840, height:2160, facingMode:"environment"},
          errorFn: (err) => {
            console.debug("get User Media Stream failed", err);
            callback(err)
          }
        };
        await BioserverVideo.getDeviceStream(deviceConstraints)

      }).then(async function(deviceStream) {
        console.log("-----------------------------");
        console.log("Device Stream Error Result: ");
        console.log("-----------------------------");

        console.log("MediaStream.code : " + deviceStream.code);
        chai.assert.equal(deviceStream.code, 1304);

        console.log("MediaStream.error : " + deviceStream.error);

        chai.assert.equal(deviceStream.error, "Resolution is not supported");
        await basetest.runOnTestLink("BioServer-TC-349", true);
      }).catch(async (e) => {
        console.log("Get Devices Stream async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-349", false);
        throw e;
      });
    });
  });

  /**
   * Test that BioserverVideo.getDeviceStream testcase should return Unsupported Resolution video stream when deviceConstraints set to {width: 640, height: 480}.
   */
  describe('bio_get_device_stream_14', function() {
    it('should return unsupported resolution: {width: 640, height: 480}', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request


      await driver.executeAsyncScript(async function() {

        var callback = arguments[arguments.length - 1];
        deviceConstraints= {
          video:{frameRate:15, width:640, height:480, facingMode:"environment"},
          errorFn: (err) => {
            console.debug("get User Media Stream failed", err);
            callback(err)
          }
        };
        callback(await BioserverVideo.getDeviceStream(deviceConstraints))

      }).then(async function(deviceStream) {
        console.log("-----------------------------");
        console.log("Device Stream Error Result: ");
        console.log("-----------------------------");

        console.log("MediaStream.code : " + deviceStream.code);
        chai.assert.equal(deviceStream.code, 1304);

        console.log("MediaStream.error : " + deviceStream.error);

        chai.assert.equal(deviceStream.error, "Resolution is not supported");
        await basetest.runOnTestLink("BioServer-TC-350", true);
      }).catch(async (e) => {
        console.log("Get Devices Stream async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-350", false);
        throw e;
      });
    });
  });


  /**
   * Test that BioserverVideo.getDeviceStream testcase should return errorFn callback with error code 1304 when errorFn is enabled.
   */
  describe('bio_get_device_stream_26', function() {
    it('should return errorFn callback with error code 1304', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------------------" );
      console.log("start Get Device Stream Request(Wrong Resolution) ");
      console.log("--------------------------------------------------");
      //Format device constraints request
      await driver.executeAsyncScript(async function() {
        var callback = arguments[arguments.length - 1];
        await BioserverVideo.getDeviceStream({video: {width: 720, height: 1280},
          errorFn: (err) => {
            callback(err);
          }})

      }).then(async function(errorStream) {
        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        console.log("errorStream.code : " + errorStream.code);
        chai.assert.equal(errorStream.code, 1304);

        console.log("errorStream.error : " + errorStream.error);
        chai.assert.equal(errorStream.error, "OverconstrainedError: An unexpected error occurred while accessing user device");
        await basetest.runOnTestLink("BioServer-TC-437", true);

      }).catch(async (e) => {
        console.log("Get Devices Stream async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-437", false);
        throw e;
      });
    });
  });

  //TODO TEST NON HD VIDEO (VGA) AND CHECK THE ERROR RESULT
  /**
   * Test that BioserverVideo.getDeviceStream testcase should return unsupported resolution when VGA video format is received.
   */
  describe('bio_get_device_stream_33', function() {
    it('should return unsupported resolution when VGA video format is received', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_vga.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request


      await driver.executeAsyncScript(async function() {

        var callback = arguments[arguments.length - 1];
        deviceConstraints= {
          video:{frameRate:15, width:1280, height:720, facingMode:"environment"},
          errorFn: (err) => {
            console.debug("get User Media Stream failed", err);
            callback(err)
          }
        };
        await BioserverVideo.getDeviceStream(deviceConstraints)

      }).then(async function(deviceStream) {
        console.log("-----------------------------");
        console.log("Device Stream Error Result: ");
        console.log("-----------------------------");

        console.log("MediaStream.code : " + deviceStream.code);
        chai.assert.equal(deviceStream.code, 1304);

        console.log("MediaStream.error : " + deviceStream.error);

        chai.assert.equal(deviceStream.error, "OverconstrainedError: An unexpected error occurred while accessing user device");
        await basetest.runOnTestLink("BioServer-TC-496", true);
      }).catch(async (e) => {
        console.log("Get Devices Stream async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-496", false);
        throw e;
      });
    });
  });
  //TODO TEST  HD VIDEO AND CHECK THE  RESULT IS SUCCEED
  /**
   * Test that BioserverVideo.getDeviceStream testcase should return media stream when HD video format is received.
   */
  describe('bio_get_device_stream_34', function() {
    it('should return media stream when HD video format is received', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request

      await driver.executeAsyncScript(async function() {

        var callback = arguments[arguments.length - 1];
        deviceConstraints= {
          video:{frameRate:15, width:1280, height:720, facingMode:"environment"},
          errorFn: (err) => {
            console.debug("get User Media Stream failed", err);
            callback(err)
          }
        };
        callback(await BioserverVideo.getDeviceStream(deviceConstraints))

      }).then(async (deviceStream) => {

        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        console.log("MediaStream.resolution : " + deviceStream.resolution);
        chai.assert.equal(deviceStream.resolution, "1280x720");

        console.log("MediaStream.id : " + deviceStream.id);
        chai.assert.isNotNull(deviceStream.id);

        console.log("MediaStream.active ? : " + deviceStream.active);
        chai.assert.equal(deviceStream.active, true);
        await basetest.runOnTestLink("BioServer-TC-497", true);
      }).catch(async (e) => {
        console.log("Get Devices Stream async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-497", false);
        throw e;
      });
    });
  });
  //TODO TEST  FHD(1920x1080) VIDEO AND CHECK THE  RESULT IS SUCCEED
  /**
   * Test that BioserverVideo.getDeviceStream testcase should return media stream when FHD video format is received.
   */
  describe('bio_get_device_stream_35', function() {
    it('should return media stream when FHD video format is received', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_fhd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request

      await driver.executeAsyncScript(async function() {

        var callback = arguments[arguments.length - 1];
        deviceConstraints= {
          video:{frameRate:15, width:1280, height:720, facingMode:"environment"},
          errorFn: (err) => {
            console.debug("get User Media Stream failed", err);
            callback(err)
          }
        };
        callback(await BioserverVideo.getDeviceStream(deviceConstraints))

      }).then(async (deviceStream) => {

        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        console.log("MediaStream.resolution : " + deviceStream.resolution);
        chai.assert.equal(deviceStream.resolution, "1280x720");

        console.log("MediaStream.id : " + deviceStream.id);
        chai.assert.isNotNull(deviceStream.id);

        console.log("MediaStream.active ? : " + deviceStream.active);
        chai.assert.equal(deviceStream.active, true);
        await basetest.runOnTestLink("BioServer-TC-498", true);
      }).catch(async (e) => {
        console.log("Get Devices Stream async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-498", false);
        throw e;
      });
    });
  });
  //TODO TEST  4K UHD(3840 × 2160) VIDEO AND CHECK THE  RESULT IS SUCCEED
  /**
   * Test that BioserverVideo.getDeviceStream testcase should return media stream when 4K UHD video format is received.
   */
  describe('bio_get_device_stream_36', function() {
    it('should return media stream when 4K UHD video format is received', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_4kuhd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request

      await driver.executeAsyncScript(async function() {

        var callback = arguments[arguments.length - 1];
        deviceConstraints= {
          video:{frameRate:15, width:1280, height:720, facingMode:"environment"},
          errorFn: (err) => {
            console.debug("get User Media Stream failed", err);
            callback(err)
          }
        };
        callback(await BioserverVideo.getDeviceStream(deviceConstraints))

      }).then(async (deviceStream) => {

        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        console.log("MediaStream.resolution : " + deviceStream.resolution);
        chai.assert.equal(deviceStream.resolution, "1280x720");

        console.log("MediaStream.id : " + deviceStream.id);
        chai.assert.isNotNull(deviceStream.id);

        console.log("MediaStream.active ? : " + deviceStream.active);
        chai.assert.equal(deviceStream.active, true);
        await basetest.runOnTestLink("BioServer-TC-499", true);
      }).catch(async (e) => {
        console.log("Get Devices Stream async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-499", false);
        throw e;
      });
    });
  });

  //TODO TEST  (4096 × 3204) VIDEO AND CHECK THE  RESULT IS SUCCEED
  /**
   * Test that BioserverVideo.getDeviceStream testcase should return media stream when 4096x3204 video format is received.
   */
  describe('bio_get_device_stream_37', function() {
    it('should return media stream when 4096x3204 video format is received', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_4096_2304.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      console.log("--------------------------------------");
      console.log("start Environment Detection Request ");
      console.log("--------------------------------------");
      await basetest.environmentDetectionSuccess(driver);

      console.log("--------------------------------------");
      console.log("start Init Media Devices Request ");
      console.log("--------------------------------------");
      basetest.initMediaDevicesSuccess(driver);

      await driver.sleep(2000);
      console.log("--------------------------------------" );
      console.log("start Get Device Stream Request ");
      console.log("-------------------------------------- ");
      //Format device constraints request

      await driver.executeAsyncScript(async function() {

        var callback = arguments[arguments.length - 1];
        deviceConstraints= {
          video:{frameRate:15, width:1280, height:720, facingMode:"environment"},
          errorFn: (err) => {
            console.debug("get User Media Stream failed", err);
            callback(err)
          }
        };
        callback(await BioserverVideo.getDeviceStream(deviceConstraints))

      }).then(async (deviceStream) => {

        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        console.log("MediaStream.resolution : " + deviceStream.resolution);
        chai.assert.equal(deviceStream.resolution, "1280x720");

        console.log("MediaStream.id : " + deviceStream.id);
        chai.assert.isNotNull(deviceStream.id);

        console.log("MediaStream.active ? : " + deviceStream.active);
        chai.assert.equal(deviceStream.active, true);
        await basetest.runOnTestLink("BioServer-TC-500", false);
      }).catch(async (e) => {
        console.log("Get Devices Stream async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-500", false);
        throw e;
      });
    });
  });


});