/*eslint-disable */
let chai = require("chai");
let chaiHttp = require("chai-http");
const path = require('path');
const {ConstantTestEnvironment} = require(path.resolve('test/utils/constanttestenvironment'));

//assertion style
chai.should();

chai.use(chaiHttp);

var bio_session_id;
var callback = false;


// This line allows use with https
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

function createBioSession(_callback) {

  console.log("------------------");
  console.log("Create BioSession ");
  console.log("------------------");
  //callback = true;

  chai
    .request(ConstantTestEnvironment.base_url)
    .post(ConstantTestEnvironment.init_liveness_session_path)
    .set('content-type', 'application/json')
    .set('apikey', ConstantTestEnvironment.biosrv_api_key)
    .send({'livenessMode': 'LIVENESS_HIGH', 'numberOfChallenge': 2, 'securityLevel': 'HIGH', 'imageStorageEnabled': true,
      'correlationId': ConstantTestEnvironment.correlationId, 'ttlSeconds': 200})
    .end(function(error, response, body) {
      if (error) {
        console.log(error);
        //done(error);
      } else {
        console.log("---------------------");
        console.log("retrieve BioSession ID");
        console.log("---------------------");

        response.should.have.status(201);
        var location = response.get("location");
        bio_session_id = location.substring(location.lastIndexOf('/') + 1);
        console.log("bio_session_id "+ bio_session_id);

        //done(error, response.status);
        //callback = false;
        console.log("call callback() ");
        _callback();
      }
    });
}


describe("Coturn Credentials REST API test", () => {


  /**
   * Test that Get coturn credentials testcase should success and iceServers data.
   * TODO TICKET BIO_SRV-1891
   */

  describe('bio_get_coturn_credentials_01', function() {


    it('should send get coturn credentials to : $path POST', function(done) {

      createBioSession(function(){
        console.log("---------------------------");
        console.log("Coturn Credentials Request ");
        console.log("---------------------------");
        console.log("execute code "+ callback);
        chai
        .request(ConstantTestEnvironment.base_url)
          .get(ConstantTestEnvironment.coturn_credentials_path+bio_session_id)
          .end(function(error, response, body) {
            if (error) {
              console.log(error);
              done(error);
            } else {
              console.log("------------------------------------");
              console.log("retrieve Coturn Credentials Response");
              console.log("------------------------------------");

              response.should.have.status(200);
              response.body.should.be.a('object');

              console.log("response "+ response.text);

              console.log("iceServers ==> urls: "+ response.body.iceServers[0]["urls"]);
              chai.assert.equal(response.body.iceServers[0]["urls"], "turn:10.126.237.10:1194?transport=udp")

              console.log("iceServers ==> username: "+ response.body.iceServers[0]["username"]);
              chai.assert.equal(response.body.iceServers[0]["username"], "mph")

              console.log("iceServers ==> credential: "+ response.body.iceServers[0]["credential"]);
              chai.assert.equal(response.body.iceServers[0]["credential"], "mph")

              done(error, response.status);
            }
          });
      });

    });
  });

  /**
   * Test that Get coturn credentials testcase should return success when wrong api_key is used.
   */

  describe('bio_get_coturn_credentials_02', function() {


    it('should send get coturn credentials to : $path POST', function(done) {

      createBioSession(function(){
        console.log("---------------------------");
        console.log("Coturn Credentials Request ");
        console.log("---------------------------");
        console.log("execute code "+ callback);
        chai
          .request(ConstantTestEnvironment.base_url)
          .get(ConstantTestEnvironment.coturn_credentials_path+bio_session_id)
          .set('apikey', '')
          .end(function(error, response, body) {
            if (error) {
              console.log(error);
              done(error);
            } else {
              console.log("------------------------------------");
              console.log("retrieve Coturn Credentials Response");
              console.log("------------------------------------");

              response.should.have.status(401);
              console.log("Coturn Credentials Error Code: "+ response.status.valueOf());

              done(error, response.status);
            }
          });
      });

    });
  });

  /**
   * Test that Get coturn credentials testcase should return error code 400 when wrong bioSessionId is used.
   * TODO TICKET BIO_SRV-1864
   */

  describe('bio_get_coturn_credentials_03', function() {


    it('should send get coturn credentials to : $path POST', function(done) {

      createBioSession(function(){
        console.log("---------------------------");
        console.log("Coturn Credentials Request ");
        console.log("---------------------------");
        console.log("execute code "+ callback);
        chai
          .request(ConstantTestEnvironment.base_url)
          .get(ConstantTestEnvironment.coturn_credentials_path+ConstantTestEnvironment.wrong_biosession_id)
          .end(function(error, response, body) {
            if (error) {
              console.log(error);
              done(error);
            } else {
              console.log("------------------------------------");
              console.log("retrieve Coturn Credentials Response");
              console.log("------------------------------------");

              response.should.have.status(400);
              console.log("Coturn Credentials Error Code: "+ response.status.valueOf());

              done(error, response.status);
            }
          });
      });

    });
  });

  /**
   * Test that Get coturn credentials testcase should return error code 400 when bioSessionId value is not sent.
   */

  describe('bio_get_coturn_credentials_04', function() {


    it('should send get coturn credentials to : $path POST', function(done) {

      createBioSession(function(){
        console.log("---------------------------");
        console.log("Coturn Credentials Request ");
        console.log("---------------------------");
        console.log("execute code "+ callback);
        chai
          .request(ConstantTestEnvironment.base_url)
          .get(ConstantTestEnvironment.coturn_credentials_path)
          .end(function(error, response, body) {
            if (error) {
              console.log(error);
              done(error);
            } else {
              console.log("------------------------------------");
              console.log("retrieve Coturn Credentials Response");
              console.log("------------------------------------");

              response.should.have.status(400);
              console.log("Coturn Credentials Error Code: "+ response.status.valueOf());

              done(error, response.status);
            }
          });
      });

    });
  });

  /**
   * Test that Get coturn credentials testcase should return error code 404 when bioSession field is not sent.
   */

  describe('bio_get_coturn_credentials_05', function() {


    it('should send get coturn credentials to : $path POST', function(done) {

      createBioSession(function(){
        console.log("---------------------------");
        console.log("Coturn Credentials Request ");
        console.log("---------------------------");
        console.log("execute code "+ callback);
        chai
          .request(ConstantTestEnvironment.base_url)
          .get(ConstantTestEnvironment.wrong_coturn_credentials_path)
          // .set('content-type', 'application/x-www-form-urlencoded')
          // .send({bioSessionId: bio_session_id})
          .end(function(error, response, body) {
            if (error) {
              console.log(error);
              done(error);
            } else {
              console.log("------------------------------------");
              console.log("retrieve Coturn Credentials Response");
              console.log("------------------------------------");

              response.should.have.status(404);
              console.log("Coturn Credentials Error Code: "+ response.status.valueOf());

              done(error, response.status);
            }
          });
      });

    });
  });

})
