/*eslint-disable */
let chai = require("chai");
let chaiHttp = require("chai-http");
const path = require('path');
const {ConstantTestEnvironment} = require(path.resolve('test/utils/constanttestenvironment'));
//assertion style
chai.should();

chai.use(chaiHttp);

// This line allows use with https
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

function getExpectedBioServerVersion() {

  if (process.argv[6].match('mochawesome') != null) {
    console.log('Mochawesome is used : ');
    return '3.14.0-SNAPSHOT'; //FIXME process.argv[7];
  } else if (process.argv[5].match('mocha-jenkins-reporter') != null) {
    console.log('Mocha Jenkins Reporter is used : ');
    return process.argv[8];
  } else {
    console.log('Mocha test is used : ');
    return process.argv[5];
  }
}

describe("Capabilities REST API test", () => {


  /**
   * Test that Get Capabilities testcase should return success and the bioserver instance status.
   */

  describe('bio_get_capabilities_01', function() {

    it('should send get capabilities to : $path POST', function(done) {

      var expectedVersion = getExpectedBioServerVersion();

      console.log("----------------------------");
      console.log('Expected Server version: ',expectedVersion);
      console.log("----------------------------");

      let splitExpectedVersion = expectedVersion.split(".");

      chai
        .request(ConstantTestEnvironment.base_url)
        .get(ConstantTestEnvironment.capabilities_path)
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("----------------------------");
            console.log("Get Capabilities Response");
            console.log("----------------------------");

            response.should.have.status(200);
            response.body.should.be.a('object');

            console.log("<<<<<<<-----------------------");
            console.log("      Video Server Capabilities");
            console.log("<<<<<<<-----------------------");

            response.body.should.have.property('version');
            let version = response.body.version;
            console.log("version "+ version);

            let splittedVideoVersion = version.split(".")
            chai.assert.equal(splittedVideoVersion.length, 3)
            console.log("splittedVideoVersion[0] "+splittedVideoVersion[0]);
            chai.assert.equal(splittedVideoVersion[0], splitExpectedVersion[0])

            console.log("splittedVideoVersion[1] "+splittedVideoVersion[1]);
            chai.assert.equal(splittedVideoVersion[1], splitExpectedVersion[1])

            console.log("splittedVideoVersion[2] "+splittedVideoVersion[2]);
            chai.assert.equal(splittedVideoVersion[2], splitExpectedVersion[2])

            console.log("response "+ response.text);
            var responseObj = JSON.parse(response.text);

            console.log("bioserver-core ==> currentMode "+ responseObj["bioserver-core"]["currentMode"]);
            chai.assert.equal(responseObj["bioserver-core"]["currentMode"][0], ConstantTestEnvironment.Mode.F5_4_IDD75.name)

            console.log("bioserver-core ==> version "+ responseObj["bioserver-core"]["version"]);
            let splittedVersion = responseObj["bioserver-core"]["version"].split(".")
            chai.assert.equal(splittedVersion.length, 3)

            console.log("splittedVersion[0] "+splittedVersion[0]);
            chai.assert.equal(splittedVersion[0], splitExpectedVersion[0])

            console.log("splittedVersion[1] "+splittedVersion[1]);
            chai.assert.equal(splittedVersion[1], splitExpectedVersion[1])

            console.log("splittedVersion[2] "+splittedVersion[2]);
            chai.assert.equal(splittedVersion[2], splitExpectedVersion[2])


            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that Get Capabilities testcase should return error code 401 when wrong api_key is used.
   * TODO TEST A REVOIR ==> apikey nest pas required
   */

  describe('bio_get_capabilities_02', function() {

    it('should send get capabilities to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .get(ConstantTestEnvironment.capabilities_path)
        /*.set('apikey', '')*/
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("----------------------------");
            console.log("Get Capabilities Response");
            console.log("----------------------------");

            response.should.have.status(401);
            console.log("Capabilities Error Code: "+ response.status.valueOf());

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that Get Capabilities testcase should return error code 500 when bioserver instance is down.
   */

  // describe('bio_get_capabilities_03', function() {
  //
  //   //TODO Stop bioserver-core container
  //
  //   it('should send get capabilities to : $path POST', function(done) {
  //
  //     chai
  //       .request(ConstantTestEnvironment.base_url)
  //       .get(ConstantTestEnvironment.capabilities_path)
  //       .set('apikey', ConstantTestEnvironment.biosrv_api_key)
  //       .end(function(error, response, body) {
  //         if (error) {
  //           console.log(error);
  //           done(error);
  //         } else {
  //           console.log("----------------------------");
  //           console.log("Get Capabilities Response");
  //           console.log("----------------------------");
  //
  //           response.should.have.status(500);
  //           console.log("Capabilities Error Code: "+ response.status.valueOf());
  //
  //           done(error, response.status);
  //         }
  //       });
  //   });
  //   //TODO Start bioserver-core container
  // });

})