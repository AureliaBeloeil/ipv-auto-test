/*eslint-disable */
let chai = require("chai");
let chaiHttp = require("chai-http");
//let server = require("../main");

//assertion style
chai.should();
const path = require('path');
chai.use(chaiHttp);

const {ConstantTestEnvironment} = require(path.resolve('test/utils/constanttestenvironment'));

// This line allows use with https
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe("Init liveness Session REST API test", () => {


  /**
   * Test that InitLivenessSession testcase should return success in case a valid request is sent in the request.
   */

  describe('bio_init_liveness_session_01', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        //.send({myparam: 'test'})
        .send({'livenessMode': 'LIVENESS_HIGH', 'numberOfChallenge': 2, 'securityLevel': 'HIGH', 'imageStorageEnabled': true,
          'correlationId': ConstantTestEnvironment.correlationId, 'ttlSeconds': 200, 'callbackURL': ConstantTestEnvironment.callbackURL})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("---------------------");
            console.log("retrieve BioSession ID");
            console.log("---------------------");

            response.should.have.status(201);
            var location = response.get("location");
            let bio_session_id = location.substring(location.lastIndexOf('/') + 1);
            console.log("bio_session_id "+ bio_session_id);

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that InitLivenessSession testcase should return error code 401 when wrong api_key is sent in the request.
   */

  describe('bio_init_liveness_session_02', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', '')
        //.send({myparam: 'test'})
        .send({'livenessMode': 'LIVENESS_HIGH', 'numberOfChallenge': 2, 'securityLevel': 'HIGH', 'imageStorageEnabled': true,
          'correlationId': ConstantTestEnvironment.correlationId, 'ttlSeconds': 200, 'callbackURL': ConstantTestEnvironment.callbackURL})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("----------------------------");
            console.log("retrieve BioSession Response");
            console.log("----------------------------");

            response.should.have.status(401);
            console.log("BioSession Error Code: "+ response.status.valueOf());

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that InitLivenessSession testcase should return error code 400 when wrong livenessMode is sent in the request.
   */

  describe('bio_init_liveness_session_03', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        //.send({myparam: 'test'})
        .send({'livenessMode': 'WRONGLIVENESS', 'numberOfChallenge': 2, 'securityLevel': 'HIGH', 'imageStorageEnabled': true,
          'correlationId': ConstantTestEnvironment.correlationId, 'ttlSeconds': 200, 'callbackURL': ConstantTestEnvironment.callbackURL})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("----------------------------");
            console.log("retrieve BioSession Response");
            console.log("----------------------------");

            response.should.have.status(400);
            console.log("BioSession Error Code: "+ response.status.valueOf());

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that InitLivenessSession testcase should return error code 400 when no livenessMode field is sent in the request.
   */

  describe('bio_init_liveness_session_04', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        //.send({myparam: 'test'})
        .send({'numberOfChallenge': 2, 'securityLevel': 'HIGH', 'imageStorageEnabled': true,
          'correlationId': ConstantTestEnvironment.correlationId, 'ttlSeconds': 200, 'callbackURL': ConstantTestEnvironment.callbackURL})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("----------------------------");
            console.log("retrieve BioSession Response");
            console.log("----------------------------");

            response.should.have.status(400);
            console.log("BioSession Error Code: "+ response.status.valueOf());

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that InitLivenessSession testcase should return success when liveness_passive is used and no numberOfChallenge field is sent in the request.
   * TODO TICKET BIO_SRV-1832
   */

  describe('bio_init_liveness_session_05', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        //.send({myparam: 'test'})
        .send({'livenessMode': 'LIVENESS_PASSIVE', 'securityLevel': 'LOW', 'imageStorageEnabled': true,
          'correlationId': ConstantTestEnvironment.correlationId, 'ttlSeconds': 200, 'callbackURL': ConstantTestEnvironment.callbackURL})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("----------------------------");
            console.log("retrieve BioSession Response");
            console.log("----------------------------");

            response.should.have.status(201);
            var location = response.get("location");
            let bio_session_id = location.substring(location.lastIndexOf('/') + 1);
            console.log("bio_session_id "+ bio_session_id);

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that InitLivenessSession testcase should return success when no securityLevel field is sent in the request.
   * TODO IL FAUT PRECISER QUE CE PARAMETRE EST OPTIONNEL DANS LE DOC
   */

  describe('bio_init_liveness_session_06', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        //.send({myparam: 'test'})
        .send({'livenessMode': 'LIVENESS_PASSIVE', 'numberOfChallenge': 2, 'imageStorageEnabled': true,
          'correlationId': ConstantTestEnvironment.correlationId, 'ttlSeconds': 200, 'callbackURL': ConstantTestEnvironment.callbackURL})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("----------------------------");
            console.log("retrieve BioSession Response");
            console.log("----------------------------");

            response.should.have.status(201);
            var location = response.get("location");
            let bio_session_id = location.substring(location.lastIndexOf('/') + 1);
            console.log("bio_session_id "+ bio_session_id);

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that InitLivenessSession testcase should return error code 400 when wrong securityLevel is sent in the request.
   */

  describe('bio_init_liveness_session_07', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        //.send({myparam: 'test'})
        .send({'livenessMode': 'LIVENESS_PASSIVE', 'securityLevel': 'WRONG', 'numberOfChallenge': 2, 'imageStorageEnabled': true,
          'correlationId': ConstantTestEnvironment.correlationId, 'ttlSeconds': 200, 'callbackURL': ConstantTestEnvironment.callbackURL})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("----------------------------");
            console.log("retrieve BioSession Response");
            console.log("----------------------------");

            response.should.have.status(400);
            console.log("BioSession Error Code: "+ response.status.valueOf());

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that InitLivenessSession testcase should return error code 400 when no imageStorageEnabled field is sent in the request.
   * TODO TICKET BIO_SRV-1833
   */

  describe('bio_init_liveness_session_08', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        //.send({myparam: 'test'})
        .send({'livenessMode': 'LIVENESS_PASSIVE', 'securityLevel': 'LOW', 'numberOfChallenge': 2,
          'correlationId': ConstantTestEnvironment.correlationId, 'ttlSeconds': 200, 'callbackURL': ConstantTestEnvironment.callbackURL})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("----------------------------");
            console.log("retrieve BioSession Response");
            console.log("----------------------------");

            response.should.have.status(400);
            console.log("BioSession Error Code: "+ response.status.valueOf());

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that InitLivenessSession testcase should return success when no  callbackuRL field is sent in the request.
   */

  describe('bio_init_liveness_session_09', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        //.send({myparam: 'test'})
        .send({'livenessMode': 'LIVENESS_HIGH', 'numberOfChallenge': 2, 'securityLevel': 'HIGH', 'imageStorageEnabled': true,
          'correlationId': ConstantTestEnvironment.correlationId, 'ttlSeconds': 200})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("---------------------");
            console.log("retrieve BioSession ID");
            console.log("---------------------");

            response.should.have.status(201);
            var location = response.get("location");
            let bio_session_id = location.substring(location.lastIndexOf('/') + 1);
            console.log("bio_session_id "+ bio_session_id);

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that InitLivenessSession testcase should return success when no correlationId field is sent in the request.
   */

  describe('bio_init_liveness_session_10', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        //.send({myparam: 'test'})
        .send({'livenessMode': 'LIVENESS_HIGH', 'numberOfChallenge': 2, 'securityLevel': 'HIGH',
          'imageStorageEnabled': true, 'ttlSeconds': 200, 'callbackURL': ConstantTestEnvironment.callbackURL})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("---------------------");
            console.log("retrieve BioSession ID");
            console.log("---------------------");

            response.should.have.status(201);
            var location = response.get("location");
            let bio_session_id = location.substring(location.lastIndexOf('/') + 1);
            console.log("bio_session_id "+ bio_session_id);

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that InitLivenessSession testcase should return success when no ttlSeconds field is sent in the request.
   */

  describe('bio_init_liveness_session_11', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        //.send({myparam: 'test'})
        .send({'livenessMode': 'LIVENESS_HIGH', 'numberOfChallenge': 2, 'securityLevel': 'HIGH',
          'correlationId': ConstantTestEnvironment.correlationId, 'imageStorageEnabled': true, 'callbackURL': ConstantTestEnvironment.callbackURL})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("---------------------");
            console.log("retrieve BioSession ID");
            console.log("---------------------");

            response.should.have.status(201);
            var location = response.get("location");
            let bio_session_id = location.substring(location.lastIndexOf('/') + 1);
            console.log("bio_session_id "+ bio_session_id);

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that InitLivenessSession testcase should return error code 405 when wrong method is used in the request.
   */

  describe('bio_init_liveness_session_12', function() {

    // it('should send init liveness to : $path POST', function(done) {
    //
    //   chai
    //     .request(ConstantTestEnvironment.base_url)
    //     .get(ConstantTestEnvironment.init_liveness_session_path)
    //     .set('apikey', ConstantTestEnvironment.biosrv_api_key)
    //     .send({'livenessMode': 'LIVENESS_HIGH', 'numberOfChallenge': 2, 'securityLevel': 'HIGH',
    //       'correlationId': ConstantTestEnvironment.correlationId, 'imageStorageEnabled': true, 'callbackURL': ConstantTestEnvironment.callbackURL})
    //     .end(function(error, response, body) {
    //       if (error) {
    //         console.log(error);
    //         done(error);
    //       } else {
    //         console.log("----------------------------");
    //         console.log("retrieve BioSession Response");
    //         console.log("----------------------------");
    //
    //         response.should.have.status(405);
    //         console.log("BioSession Error Code: "+ response.status.valueOf());
    //
    //         done(error, response.status);
    //       }
    //     });
    // });
  });



  /**
   * Test that InitLivenessSession testcase should return error code 400 when livenessMode is set to
   * Liveness_high and no numberOfChallenge field is sent in the request.
   * TODO TICKET BIO_SRV-1916
   */

  describe('bio_init_liveness_session_13', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        //.send({myparam: 'test'})
        .send({'livenessMode': 'LIVENESS_HIGH', 'securityLevel': 'HIGH', 'imageStorageEnabled': true,
          'correlationId': ConstantTestEnvironment.correlationId, 'ttlSeconds': 200, 'callbackURL': ConstantTestEnvironment.callbackURL})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("----------------------------");
            console.log("retrieve BioSession Response");
            console.log("----------------------------");

            response.should.have.status(400);
            console.log("BioSession Error Code: "+ response.status.valueOf());

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that InitLivenessSession testcase should return success when livenessMode: "NO_LIVENESS" is sent in the request.
   */

  describe('bio_init_liveness_session_14', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        //.send({myparam: 'test'})
        .send({'livenessMode': 'NO_LIVENESS', 'numberOfChallenge': 2, 'securityLevel': 'HIGH', 'imageStorageEnabled': true,
          'correlationId': ConstantTestEnvironment.correlationId, 'ttlSeconds': 200, 'callbackURL': ConstantTestEnvironment.callbackURL})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("---------------------");
            console.log("retrieve BioSession ID");
            console.log("---------------------");

            response.should.have.status(201);
            var location = response.get("location");
            let bio_session_id = location.substring(location.lastIndexOf('/') + 1);
            console.log("bio_session_id "+ bio_session_id);

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that InitLivenessSession testcase should return success when livenessMode: "LIVENESS_MEDIUM" is sent in the request.
   */

  describe('bio_init_liveness_session_15', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        //.send({myparam: 'test'})
        .send({'livenessMode': 'LIVENESS_MEDIUM', 'numberOfChallenge': 2, 'securityLevel': 'HIGH', 'imageStorageEnabled': true,
          'correlationId': ConstantTestEnvironment.correlationId, 'ttlSeconds': 200, 'callbackURL': ConstantTestEnvironment.callbackURL})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("---------------------");
            console.log("retrieve BioSession ID");
            console.log("---------------------");

            response.should.have.status(201);
            var location = response.get("location");
            let bio_session_id = location.substring(location.lastIndexOf('/') + 1);
            console.log("bio_session_id "+ bio_session_id);

            done(error, response.status);
          }
        });
    });
  });

  /**
   * Test that InitLivenessSession testcase should return error code 400 when livenessMode: "LIVENESS_HIGH_TRAINING" is sent in the request.
   */

  describe('bio_init_liveness_session_16', function() {

    it('should send init liveness to : $path POST', function(done) {

      chai
        .request(ConstantTestEnvironment.base_url)
        .post(ConstantTestEnvironment.init_liveness_session_path)
        .set('content-type', 'application/json')
        .set('apikey', ConstantTestEnvironment.biosrv_api_key)
        //.send({myparam: 'test'})
        .send({'livenessMode': 'LIVENESS_HIGH_TRAINING', 'numberOfChallenge': 2, 'securityLevel': 'HIGH', 'imageStorageEnabled': true,
          'correlationId': ConstantTestEnvironment.correlationId, 'ttlSeconds': 200, 'callbackURL': ConstantTestEnvironment.callbackURL})
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("-----------------------------");
            console.log("retrieve BioSession Response");
            console.log("-----------------------------");

            response.should.have.status(400);
            console.log("BioSession Error Code: "+ response.status.valueOf());

            done(error, response.status);
          }
        });
    });
  });

})
