/*eslint-disable */
let chai = require("chai");
let chaiHttp = require("chai-http");
const path = require('path');
const {ConstantTestEnvironment} = require(path.resolve('test/utils/constanttestenvironment'));

//assertion style
chai.should();

chai.use(chaiHttp);

// This line allows use with https
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

function getExpectedBioServerVersion() {

  if (process.argv[6].match('mochawesome') != null) {
    console.log('Mochawesome is used : ');
    return '3.14.0-SNAPSHOT'; //FIXME process.argv[7];
  } else if (process.argv[5].match('mocha-jenkins-reporter') != null) {
    console.log('Mocha Jenkins Reporter is used : ');
    return process.argv[8];
  } else {
    console.log('Mocha test is used : ');
    return process.argv[5];
  }
}

describe("Monitoring REST API test", () => {


  /**
   * Test that Get Status testcase should check the behavior of BioServer instance .
   */

  describe('bio_get_status_01', function() {

    it('should send get status to : $path POST', function(done) {
      var expectedVersion = getExpectedBioServerVersion();

      console.log("----------------------------");
      console.log('Expected Server version: ',expectedVersion);
      console.log("----------------------------");

      let splitExpectedVersion = expectedVersion.split(".");

      chai
        .request(ConstantTestEnvironment.base_url)
        .get(ConstantTestEnvironment.monitoring_path)
        .end(function(error, response, body) {
          if (error) {
            console.log(error);
            done(error);
          } else {
            console.log("----------------------------");
            console.log("retrieve Get Status Response");
            console.log("----------------------------");

            response.should.have.status(200);
            response.body.should.be.a('object');

            console.log("<<<<<<<---------------------");
            console.log("      Video Server Status");
            console.log("<<<<<<<---------------------");

            response.body.should.have.property('version');
            let version = response.body.version;
            console.log("version "+ version);

            let splittedVideoVersion = version.split(".")
            chai.assert.equal(splittedVideoVersion.length, 3)

            console.log("splittedVideoVersion[0] "+splittedVideoVersion[0]);
            chai.assert.equal(splittedVideoVersion[0], splitExpectedVersion[0])

            console.log("splittedVideoVersion[1] "+splittedVideoVersion[1]);
            chai.assert.equal(splittedVideoVersion[1], splitExpectedVersion[1])

            console.log("splittedVideoVersion[2] "+splittedVideoVersion[2]);
            chai.assert.equal(splittedVideoVersion[2], splitExpectedVersion[2])

            response.body.should.have.property('since');
            let since = response.body.since;
            console.log("since "+ since);

            response.body.should.have.property('status');
            let status = response.body.status;
            console.log("status "+ status);

            console.log("      ---------------------");
            console.log("      BioServer Core Status");
            console.log("      ---------------------");

            response.body.should.have.property('bioserver-core');
            //Parse  response to json
            var obj = JSON.parse(response.text);

            console.log("bioserver-core ==> acceptedModes "+ obj["bioserver-core"]["acceptedModes"]);
            chai.assert.equal(obj["bioserver-core"]["acceptedModes"], ConstantTestEnvironment.Mode.F5_4_IDD75.name)

            console.log("bioserver-core ==> status "+ obj["bioserver-core"]["status"]);
            chai.assert.equal(obj["bioserver-core"]["status"], "ok")

            console.log("bioserver-core ==> version "+ obj["bioserver-core"]["version"]);
            let splittedVersion = obj["bioserver-core"]["version"].split(".")
            chai.assert.equal(splittedVersion.length, 3)

            console.log("splittedVersion[0] "+splittedVersion[0]);
            chai.assert.equal(splittedVersion[0], splitExpectedVersion[0])

            console.log("splittedVersion[1] "+splittedVersion[1]);
            chai.assert.equal(splittedVersion[1], splitExpectedVersion[1])

            console.log("splittedVersion[2] "+splittedVersion[2]);
            chai.assert.equal(splittedVersion[2], splitExpectedVersion[2])

            done(error, response.status);
          }
        });
    });
  });

})
