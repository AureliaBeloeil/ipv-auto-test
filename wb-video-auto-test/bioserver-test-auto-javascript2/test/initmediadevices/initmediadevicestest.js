/*eslint-disable */


let chai = require("chai");
const path = require('path');
//const { Options } = require('selenium-webdriver/chrome');
//const { Builder, By, Key, until } = require('selenium-webdriver');
//const { getExtension, getAddHeaderUrl } = require('chrome-modheader');
const {VideoBaseTest} = require(path.resolve('test/utils/videobasetest'));
const {ConstantTestEnvironment} = require(path.resolve('test/utils/constanttestenvironment'));

var basetest = null;
var driver = null;
const  local = false;

// This line allows use with https
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('BioServer Init Media Devices Test', () => {

  before(async function (){

    basetest = new VideoBaseTest();
    if(basetest.isTestSuite && !basetest.init) {
      if (basetest.isTestLinkEnabled) {
        await basetest.checkConnection();
        await basetest.createTestLinkBuild();
        basetest.init = true;
      }
    } else {
      console.log("-------------------------");
      console.log("build is already created ");
      console.log("-------------------------");
    }
  });

  //
  afterEach(function(done) {
    // runs after each test in this block
    console.log("---------------");
    console.log("Close Driver ");
    console.log("---------------");
    driver.quit().then(() => {
      done();
      console.log("Closed.");
    });
    //await driver.quit();

  });



  /**
   * Test that BioserverVideo.initMediaDevices testcase should return video and audio Devices when valid request is processed.
   */
  describe('bio_init_media_devices_01', function() {
    it('should return media devices',  async function ()  {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      await driver.executeAsyncScript(async (callback) =>
        callback(await BioserverVideo.initMediaDevices({cameraSelectionGUI: '#cameraSelectID',audioSelectionGUI: '#audioSelectID', errorFn : (err) =>
          {
            console.debug("Init media devices failed", err);
          }}))
      ).then(async (mediaDevices) =>
      {
        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        if(!local) {
          console.log('audioDevices.deviceId ? : ' + mediaDevices.audioDevices[0].deviceId);
          chai.assert.isNotNull(mediaDevices.audioDevices[0].deviceId);
          console.log('audioDevices.label ? : ' + mediaDevices.audioDevices[0].label);
          chai.assert.isNotNull(mediaDevices.audioDevices[0].label);

          console.log('videoDevices.deviceId ? : ' + mediaDevices.videoDevices[0].deviceId);
          chai.assert.isNotNull(mediaDevices.videoDevices[0].deviceId);
          console.log('videoDevices.label ? : ' + mediaDevices.videoDevices[0].label);
          chai.assert.isNotNull(mediaDevices.videoDevices[0].label);

        } else {
          console.log("audioDevices.deviceId ? : " + mediaDevices.audioDevices[0].deviceId);
          chai.assert.equal(mediaDevices.audioDevices[0].deviceId, "");
          console.log("audioDevices.label ? : " + mediaDevices.audioDevices[0].label);
          chai.assert.equal(mediaDevices.audioDevices[0].label, "Mic: 0");

          console.log("videoDevices.deviceId ? : " + mediaDevices.videoDevices[0].deviceId);
          chai.assert.equal(mediaDevices.videoDevices[0].deviceId, "");
          console.log("videoDevices.label ? : " + mediaDevices.videoDevices[0].label);
          chai.assert.equal(mediaDevices.videoDevices[0].label, "Camera : 1");
        }

        //upddate testlink
        await basetest.runOnTestLink("BioServer-TC-327", true);

      }).catch(async (e) => {
        console.log("Init Media Devices async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-327", false);
        throw e;
      });
    });
  });


  /**
   * Test that BioserverVideo.initMediaDevices testcase should return error code  when wrong #cameraSelectID is used;
   * TODO TICKET BIO_SRV-1884
   * TODO TICKET BIO_SRV-1887
   */
  describe('bio_init_media_devices_02', function() {
    it('should return media error', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      await driver.executeAsyncScript(async function() {
        var callback = arguments[arguments.length - 1];
        await BioserverVideo.initMediaDevices({cameraSelectionGUI: '#1111',audioSelectionGUI: '#audioSelectID', errorFn : (err) =>
          {
            console.debug("Init media devices failed", err);
            callback(err);
          }
        })

      }).then(async function(str) {
        console.log("-------------------------------------------");
        console.log("camera devices Error Response: ");
        console.log("------------------------------------------");
        console.log("media devices error : " + str);

        console.log("media devices error : code " + str.code);
        chai.assert.equal(str.code, 12);

        console.log("media devices error : name " + str.name);
        chai.assert.equal(str.name, "SyntaxError");

        console.log("media devices error : message " + str.message);
        chai.assert.equal(str.message, "Failed to execute 'querySelector' on 'Document': '#1111' is not a valid selector.");

          //upddate testlink
          await basetest.runOnTestLink("BioServer-TC-328", true);

        }).catch(async (e) => {
          console.log("Init Media Devices async finished with errors: ", e);
          await basetest.runOnTestLink("BioServer-TC-328", false);
          throw e;
      });
    });
  });

  /**
   * Test that BioserverVideo.initMediaDevices testcase should return media devices when deviceOptions is not sent.
   */
  describe('bio_init_media_devices_03', function() {
    it('should return media devices when no deviceOptions is sent', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      await driver.executeAsyncScript(async (callback) =>
        callback(await BioserverVideo.initMediaDevices())
      ).then(async (mediaDevices) =>
      {
        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        if(!local) {
          console.log('audioDevices.deviceId ? : ' + mediaDevices.audioDevices[0].deviceId);
          chai.assert.isNotNull(mediaDevices.audioDevices[0].deviceId);
          console.log('audioDevices.label ? : ' + mediaDevices.audioDevices[0].label);
          chai.assert.isNotNull(mediaDevices.audioDevices[0].label);

          console.log('videoDevices.deviceId ? : ' + mediaDevices.videoDevices[0].deviceId);
          chai.assert.isNotNull(mediaDevices.videoDevices[0].deviceId);
          console.log('videoDevices.label ? : ' + mediaDevices.videoDevices[0].label);
          chai.assert.isNotNull(mediaDevices.videoDevices[0].label);

        } else {
          console.log("audioDevices.deviceId ? : " + mediaDevices.audioDevices[0].deviceId);
          chai.assert.equal(mediaDevices.audioDevices[0].deviceId, "");
          console.log("audioDevices.label ? : " + mediaDevices.audioDevices[0].label);
          chai.assert.equal(mediaDevices.audioDevices[0].label, "Mic: 0");

          console.log("videoDevices.deviceId ? : " + mediaDevices.videoDevices[0].deviceId);
          chai.assert.equal(mediaDevices.videoDevices[0].deviceId, "");
          console.log("videoDevices.label ? : " + mediaDevices.videoDevices[0].label);
          chai.assert.equal(mediaDevices.videoDevices[0].label, "Camera : 1");
        }
        //upddate testlink
        await basetest.runOnTestLink("BioServer-TC-329", true);

      }).catch(async (e) => {
        console.log("Init Media Devices async finished with errors: #{mediaDevices}", e);
        //upddate testlink
        await basetest.runOnTestLink("BioServer-TC-329", false);
        throw e;
      });
    });
  });

  /**
   * Test that BioserverVideo.initMediaDevices testcase should return error when html select is used and cameraSelectionGUI  field is not set.
   * TODO TICKET BIO_SRV-1889
   * TODO TICKET BIO_SRV-1890
   *
   */
  describe('bio_init_media_devices_04', function() {
    it('should return error when cameraSelectionGUI  field is not set', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      await driver.executeAsyncScript(async function() {
        var callback = arguments[arguments.length - 1];
        callback(await BioserverVideo.initMediaDevices({test: '#cameraSelectID',audioSelectionGUI: '#audioSelectID', errorFn : (err) =>
          {
            console.debug("Init media devices failed", err);
            callback(err);
          }
        }))

      }).then(async function(str) {
        console.log("-------------------------------------------");
        console.log("camera devices Error Response: ");
        console.log("------------------------------------------");
        console.log("media devices error : " + str);

        console.log("media devices error : code " + str.code);
        chai.assert.equal(str.code, 403);

        console.log("media devices error : name " + str.name);
        chai.assert.equal(str.name, "Forbidden");

        console.log("media devices error : message " + str.message);
        //chai.assert.equal(str.message, "Failed to execute 'querySelector' on 'Document': '#1111' is not a valid selector.");

        //upddate testlink
        await basetest.runOnTestLink("BioServer-TC-330", true);

      }).catch(async (e) => {
        console.log("Init Media Devices async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-330", false);
        throw e;
      });
    });
  });

  /**
   * Test that BioserverVideo.initMediaDevices testcase should return media devices when errorFn is not used in the request.
   *
   */
  describe('bio_init_media_devices_06', function() {
    it('should return media devices when errorFn is not used', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      await driver.executeAsyncScript(async (callback) =>
        callback(await BioserverVideo.initMediaDevices({cameraSelectionGUI: '#cameraSelectID',audioSelectionGUI: '#audioSelectID'}))
      ).then(async (mediaDevices) =>
      {
        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        if(!local) {
          console.log('audioDevices.deviceId ? : ' + mediaDevices.audioDevices[0].deviceId);
          chai.assert.isNotNull(mediaDevices.audioDevices[0].deviceId);
          console.log('audioDevices.label ? : ' + mediaDevices.audioDevices[0].label);
          chai.assert.isNotNull(mediaDevices.audioDevices[0].label);

          console.log('videoDevices.deviceId ? : ' + mediaDevices.videoDevices[0].deviceId);
          chai.assert.isNotNull(mediaDevices.videoDevices[0].deviceId);
          console.log('videoDevices.label ? : ' + mediaDevices.videoDevices[0].label);
          chai.assert.isNotNull(mediaDevices.videoDevices[0].label);

        } else {
          console.log("audioDevices.deviceId ? : " + mediaDevices.audioDevices[0].deviceId);
          chai.assert.equal(mediaDevices.audioDevices[0].deviceId, "");
          console.log("audioDevices.label ? : " + mediaDevices.audioDevices[0].label);
          chai.assert.equal(mediaDevices.audioDevices[0].label, "Mic: 0");

          console.log("videoDevices.deviceId ? : " + mediaDevices.videoDevices[0].deviceId);
          chai.assert.equal(mediaDevices.videoDevices[0].deviceId, "");
          console.log("videoDevices.label ? : " + mediaDevices.videoDevices[0].label);
          chai.assert.equal(mediaDevices.videoDevices[0].label, "Camera : 1");
        }

        //upddate testlink
        await basetest.runOnTestLink("BioServer-TC-332", true);

      }).catch(async (e) => {
        console.log("Init Media Devices async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-332", false);
        throw e;
      });
    });
  });

  /**
   * Test that BioserverVideo.initMediaDevices testcase should return video error when no device camera found.
   *
   */
  // describe('bio_init_media_devices_07', function() {
  //   it('should return video error when no device camera found', async () => {
  //
  //     await driver.executeAsyncScript(async function() {
  //       var callback = arguments[arguments.length - 1];
  //       await BioserverVideo.initMediaDevices({errorFn : (err) =>
  //         {
  //           console.debug("Init media devices failed", err);
  //           callback(err);
  //         }
  //       })
  //
  //     }).then(async function(str) {
  //       console.log("-------------------------------------------");
  //       console.log("camera devices Error Response: ");
  //       console.log("------------------------------------------");
  //       console.log("media devices error : " + str);
  //
  //       console.log("media devices error : code " + str.code);
  //       chai.assert.equal(str.code, 403);
  //
  //       console.log("media devices error : name " + str.name);
  //       chai.assert.equal(str.name, "Forbidden");
  //
  //       console.log("media devices error : message " + str.message);
  //       //chai.assert.equal(str.message, "Failed to execute 'querySelector' on 'Document': '#1111' is not a valid selector.");
  //
  //       //upddate testlink
  //       await basetest.runOnTestLink("BioServer-TC-330", true);
  //
  //     }).catch(async (e) => {
  //       console.log("Init Media Devices async finished with errors: ", e);
  //       await basetest.runOnTestLink("BioServer-TC-330", false);
  //       throw e;
  //     });
  //   });
  // });

  /**
   * Test that BioserverVideo.initMediaDevices testcase should initialize html select element for only user camera devices.
   *
   */
  describe('bio_init_media_devices_08', function() {
    it('should initialize html select element for only user camera devices', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      await driver.executeAsyncScript(async (callback) =>
        callback(await BioserverVideo.initMediaDevices({cameraSelectionGUI: '#cameraSelectID'}))
      ).then(async (mediaDevices) =>
      {
        console.log("-------------------------------------------");
        console.log("The list of user camera devices Response: ");
        console.log("------------------------------------------");

        if(!local) {
          console.log('audioDevices.deviceId ? : ' + mediaDevices.audioDevices[0].deviceId);
          chai.assert.isNotNull(mediaDevices.audioDevices[0].deviceId);
          console.log('audioDevices.label ? : ' + mediaDevices.audioDevices[0].label);
          chai.assert.isNotNull(mediaDevices.audioDevices[0].label);

          console.log('videoDevices.deviceId ? : ' + mediaDevices.videoDevices[0].deviceId);
          chai.assert.isNotNull(mediaDevices.videoDevices[0].deviceId);
          console.log('videoDevices.label ? : ' + mediaDevices.videoDevices[0].label);
          chai.assert.isNotNull(mediaDevices.videoDevices[0].label);

        } else {
          console.log("audioDevices.deviceId ? : " + mediaDevices.audioDevices[0].deviceId);
          chai.assert.equal(mediaDevices.audioDevices[0].deviceId, "");
          console.log("audioDevices.label ? : " + mediaDevices.audioDevices[0].label);
          chai.assert.equal(mediaDevices.audioDevices[0].label, "Mic: 0");

          console.log("videoDevices.deviceId ? : " + mediaDevices.videoDevices[0].deviceId);
          chai.assert.equal(mediaDevices.videoDevices[0].deviceId, "");
          console.log("videoDevices.label ? : " + mediaDevices.videoDevices[0].label);
          chai.assert.equal(mediaDevices.videoDevices[0].label, "Camera : 1");
        }

        //upddate testlink
        await basetest.runOnTestLink("BioServer-TC-334", true);

      }).catch(async (e) => {
        console.log("Init Media Devices async finished with errors: ", e);
        await basetest.runOnTestLink("BioServer-TC-334", false);
        throw e;
      });
    });
  });

});