/*eslint-disable */


let chai = require("chai");
//const { Options } = require('selenium-webdriver/chrome');
//const { Builder, By, Key, until } = require('selenium-webdriver');
//const { getExtension, getAddHeaderUrl } = require('chrome-modheader');
const path = require('path');

const {VideoBaseTest} = require(path.resolve('test/utils/videobasetest'));
const {ConstantTestEnvironment} = require(path.resolve('test/utils/constanttestenvironment'));

var basetest = null;
var driver = null;

// This line allows use with https
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

async function mediaStreamInitProcess() {
  console.log('--------------------------------------');
  console.log('start Environment Detection Request ');
  console.log('--------------------------------------');
  //await basetest.environmentDetectionSuccess(driver);

  console.log('--------------------------------------');
  console.log('start Init Media Devices Request ');
  console.log('--------------------------------------');
  basetest.initMediaDevicesSuccess(driver);

  await driver.sleep(500);
  console.log('--------------------------------------');
  console.log('start Get Device Stream Request ');
  console.log('-------------------------------------- ');
  await basetest.getDeviceStreamSuccess(driver, 1280, 720);
  await driver.sleep(500);
}

function getLivenessMode(mode, securityLevel) {
  return mode+'/'+securityLevel+'?numberOfChallenge=2';
}

describe('BioServer Init and Start Face Capture Test', () => {

  /**
   * Before class
   */
  before(async function (){

    basetest = new VideoBaseTest();

    if(basetest.isTestSuite && !basetest.init) {
      await basetest.checkConnection();
      await basetest.createTestLinkBuild();
      basetest.init = true;
    } else {
      console.log("-------------------------");
      console.log("build is already created ");
      console.log("-------------------------");
    }
  });


  /**
   * after each testcase
   */
  afterEach(function(done) {
    // runs after each test in this block
    console.log("---------------");
    console.log("Close Driver ");
    console.log("---------------");
    driver.quit().then(() => {
      done();
      console.log("Closed.");
    });
    //await driver.quit();

  });



  /**
   * Test that BioserverVideo.initFaceCaptureClient testcase should return videoCaptureClient when valid request is processed.
   */
  describe('bio_init_facecapture_client_01', function() {

    it('should return videoCaptureClient', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      const sessionId = await VideoBaseTest.createBioSession(ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name,ConstantTestEnvironment.SecurityLevel.LOW.name);
      await driver.sleep(2000);

      await mediaStreamInitProcess();

      console.log("--------------------------------------");
      console.log("Add Stream to Html Video Element ");
      console.log("-------------------------------------- ");

      await driver.executeScript(async function attachMediaStream(){
        const mediaStream = await navigator.mediaDevices.getUserMedia({video: true});
        //let videoTag = document.createElement('VIDEO')
        //videoTag.id='video'
        //document.body.appendChild(videoTag)
        //const mediaStream = await BioserverVideo.getDeviceStream();
        //document.createElement('video').srcObject = mediaStream;
        //return document.querySelectorAll('video')[1].srcObject=mediaStream;
        return document.querySelector('video').srcObject=mediaStream;
      });

      await driver.sleep(2000);
      console.log("--------------------------------------");
      console.log("start Init Face Capture Client Request ");
      console.log("-------------------------------------- ");
      //Format face capture options request

      const url = ConstantTestEnvironment.base_url;

      console.log("-------------------");
      console.log("start Face Capture  ");
      console.log("-------------------");

      await driver.executeScript(async function initFaceCapture(sessionId, url){
        var callback = arguments[arguments.length - 1];
        const faceCaptureOptions = {
          wspath:'/video-server/engine.io',
          bioserverVideoUrl:url,
          rtcConfigurationPath:url+'/video-server/coturnService?bioSessionId=' + encodeURIComponent(sessionId),
          bioSessionId:sessionId,
          trackingFn: (trackingInfo) => {
            console.log("onTracking", trackingInfo);
          },
          errorFn: (error) => { console.log("face capture error", error);},
          showVideoFile: (recordedVideoFile) => {console.log("showVideoFile ", recordedVideoFile);},
          showChallengeInstruction: (challengeInstruction) => {console.log("challenge instructions", challengeInstruction);},
          showChallengeResult: (showChallengeResult) => {
            console.log("call back the backend to retrieve liveness result", showChallengeResult);
            callback(showChallengeResult);
          }
        };
        const mediaStream = await navigator.mediaDevices.getUserMedia({video: true});
        //const mediaStream = await BioserverVideo.getDeviceStream();
        const captureClient = await BioserverVideo.initFaceCaptureClient(faceCaptureOptions);
        return  await captureClient.start(mediaStream, false, 'video');
      }, sessionId, url).then(async function(challengeResult) {

        console.log('-------------------------------------------');
        console.log('showChallengeResult Callback is triggered ');
        console.log("-------------------------------------------");
        console.log("challengeResult ", challengeResult);

        console.log("--------------------------------");
        console.log("Get Liveness Challenge Result  ");
        console.log("--------------------------------");

        await VideoBaseTest.getLivenessChallengeResultWithPooling(
          getLivenessMode(ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, ConstantTestEnvironment.SecurityLevel.LOW.name),
          ConstantTestEnvironment.SecurityLevel.LOW.name, ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-421", false);
      }).catch( (e) => {
        console.log(value.mode, " finished with errors: ", e);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-421", false);
        throw e;
      });
    });
  });

  /**
   * Test that BioserverVideo.initFaceCaptureClient testcase should return video error when configuration options is not passed.
   */
  // describe('bio_init_facecapture_client_02', function() {
  //
  //   it('should return video error when configuration options is not passed', async () => {
  //
  //     basetest.createBioSession("LIVENESS_PASSIVE","LOW",function(){
  //       console.log("BioSessionId: ", sessionId);
  //     });
  //     await driver.sleep(2000);
  //
  //     await mediaStreamInitProcess();
  //
  //     console.log("--------------------------------------");
  //     console.log("Add Stream to Html Video Element ");
  //     console.log("-------------------------------------- ");
  //
  //     //await driver.executeScript("let videoTag = document.createElement('video'); videoTag.id='video'; document.body.appendChild(videoTag)");
  //
  //     await driver.executeScript(async function documentCall(){
  //       const mediaStream = await navigator.mediaDevices.getUserMedia({video: true});
  //       return document.querySelector('video').srcObject=mediaStream;
  //     });
  //
  //     await driver.sleep(2000);
  //     console.log("--------------------------------------");
  //     console.log("start Init Face Capture Client Request ");
  //     console.log("-------------------------------------- ");
  //     //Format face capture options request
  //
  //     const  res= await driver.executeScript(async function documentCall(){
  //       const mediaStream = await navigator.mediaDevices.getUserMedia({video: true});
  //       return await BioserverVideo.initFaceCaptureClient();
  //       //return  await captureClient.start(mediaStream, false, 'video');
  //     });
  //
  //     console.log("--------------------------------");
  //     console.log("Get Liveness Challenge Result  ");
  //     console.log("--------------------------------");
  //
  //     await driver.sleep(4000);
  //
  //   });
  // });

  /**
   * Test that BioserverVideo.initFaceCaptureClient testcase should return video error when wrong bioSessionId is used.
   */
  describe('bio_init_facecapture_client_05', function() {

    it('should return video error when wrong bioSessionId is used', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      await VideoBaseTest.createBioSession(ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name,ConstantTestEnvironment.SecurityLevel.LOW.name);
      await driver.sleep(2000);

      await mediaStreamInitProcess();

      console.log("--------------------------------------");
      console.log("Add Stream to Html Video Element ");
      console.log("-------------------------------------- ");

      //await driver.executeScript("let videoTag = document.createElement('video'); videoTag.id='video'; document.body.appendChild(videoTag)");

      await driver.executeScript(async function documentCall(){
        const mediaStream = await navigator.mediaDevices.getUserMedia({video: true});
        return document.querySelector('video').srcObject=mediaStream;
      });

      await driver.sleep(2000);
      console.log("--------------------------------------");
      console.log("start Init Face Capture Client Request ");
      console.log("-------------------------------------- ");
      //Format face capture options request

      console.log("-------------------");
      console.log("start Face Capture  ");
      console.log("-------------------");

      await driver.executeAsyncScript(async function() {
        var callback = arguments[arguments.length - 1];
        const mediaStream = await navigator.mediaDevices.getUserMedia({video: true});
        const captureClient = await BioserverVideo.initFaceCaptureClient({
          wspath:'/video-server/engine.io',
          bioserverVideoUrl:"https://10.126.237.10",
          rtcConfigurationPath:'https://10.126.237.10/video-server/coturnService?bioSessionId=' + encodeURIComponent("aa5ad8ff-6146-4849-8fcc-39f29d7a8973"),
          bioSessionId:"aa5ad8ff-6146-4849-8fcc-39f29d7a8973",
          errorFn: (error) => {
            console.log("face capture error", error);
            callback(error);
          }
        });
        await captureClient.start(mediaStream, false, 'video');

      }).then(function(faceResponse) {

        console.log("-----------------------------");
        console.log("Init face Capture Response: ");
        console.log("-----------------------------");

        console.log("faceResponse.code : " + faceResponse.code);
        chai.assert.equal(faceResponse.code, 404);

        console.log("faceResponse.error : " + faceResponse.error);
        chai.assert.equal(faceResponse.error, "No session found");
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-421", false);
      }).catch( (e) => {
        console.log("initFaceCaptureClient finished with errors: ", e);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-421", false);
        throw e;
      });
    });
  });

  /**
   * Test that BioserverVideo.initFaceCaptureClient testcase should start capture with video record when record label is enabled.
   */
  describe('bio_init_facecapture_client_25', function() {

    it('should start capture with video record', async () => {

      console.log("init browser options");
      const options = VideoBaseTest.getOptions("video_hd.y4m");
      console.log("browser initialisation");
      driver = await VideoBaseTest.initDriver(options, ConstantTestEnvironment.video_server_url);

      const sessionId = await VideoBaseTest.createBioSession(ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name,ConstantTestEnvironment.SecurityLevel.VERY_LOW.name);
      await driver.sleep(2000);

      await mediaStreamInitProcess();

      console.log("--------------------------------------");
      console.log("Add Stream to Html Video Element ");
      console.log("-------------------------------------- ");

      await driver.executeScript(async function attachMediaStream(){
        const mediaStream = await navigator.mediaDevices.getUserMedia({video: true});
        return document.querySelector('video').srcObject=mediaStream;
      });

      await driver.sleep(2000);
      console.log("--------------------------------------");
      console.log("start Init Face Capture Client Request ");
      console.log("-------------------------------------- ");
      //Format face capture options request
      const url = ConstantTestEnvironment.base_url;

      console.log("-------------------");
      console.log("start Face Capture  ");
      console.log("-------------------");

      await driver.executeScript(async function documentCall(sessionId, url){
        var callback = arguments[arguments.length - 1];
        const faceCaptureOptions = {
          wspath:'/video-server/engine.io',
          bioserverVideoUrl:url,
          rtcConfigurationPath:url+'/video-server/coturnService?bioSessionId=' + encodeURIComponent(sessionId),
          bioSessionId:sessionId,
          trackingFn: (trackingInfo) => {
            console.log("onTracking", trackingInfo);
          },
          errorFn: (error) => { console.log("face capture error", error);},
          showVideoFile: (recordedVideoFile) => {console.log("showVideoFile ", recordedVideoFile);},
          showChallengeInstruction: (challengeInstruction) => {console.log("challenge instructions", challengeInstruction);},
          showChallengeResult: (showChallengeResult) => {
            console.log("call back the backend to retrieve liveness result", showChallengeResult);
            callback(showChallengeResult);
          }
        };
        const mediaStream = await navigator.mediaDevices.getUserMedia({video: true});
        const captureClient = await BioserverVideo.initFaceCaptureClient(faceCaptureOptions);
        return  await captureClient.start(mediaStream, true, 'video');
      }, sessionId, url).then(async function(challengeResult) {

        console.log('-------------------------------------------');
        console.log('showChallengeResult Callback is triggered ');
        console.log("-------------------------------------------");
        console.log("challengeResult ", challengeResult);

        console.log("--------------------------------");
        console.log("Get Liveness Challenge Result  ");
        console.log("--------------------------------");

        await VideoBaseTest.getLivenessChallengeResultWithPooling(
          getLivenessMode(ConstantTestEnvironment.LivenessMode.LIVENESS_PASSIVE.name, ConstantTestEnvironment.SecurityLevel.VERY_LOW.name),
          ConstantTestEnvironment.SecurityLevel.VERY_LOW.name, ConstantTestEnvironment.LivenessStatusEnum.SUCCESS.name);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-421", false);
      }).catch( (e) => {
        console.log(value.mode, " finished with errors: ", e);
        //upddate testlink
        //await basetest.runOnTestLink("BioServer-TC-421", false);
        throw e;
      });

    });
  });
});