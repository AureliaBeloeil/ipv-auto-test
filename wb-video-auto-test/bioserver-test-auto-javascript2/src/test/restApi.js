/*eslint-disable */
let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../main");

//assertion style
chai.should();

chai.use(chaiHttp);

describe("REST API test", () => {
  /**
   * get token should return success
   */
  describe("POST  /token-app/v1/authentication", () => {
    it('it should get token ', (done) =>{
      const auth = {
        client_id: "testuser",
        client_secret: "F401MTb6QMNy8VFDMYDJ0dBQZepKDeHbTTCXRGaL+gFOCAomRITeZeJoQG3+CU2YNT2JXVzVgcZWD5LQNASqJA=="
      };
      chai.request("https://10.126.237.10")
        .post("/token-app/v1/authentication")
        .send(auth)
        .end((err, response) => {
          response.should.have.status(201);
          response.body.should.be.a('object');
          response.body.should.have.property('token').isNotNull();
          done();
        });


    });
  })

  /**
   * create biosession should return success
   */
})
