const fetch = require('node-fetch');
const FormData = require('form-data');
const _ = require('lodash');
const config = require('./config');
const debug = require('debug')('front:app:api')


module.exports = {
    getToken: getToken,
    getSession: getSession,
    getLivenessChallengeResult: getLivenessChallengeResult,
    createFace: createFace,
    getFaceImage: getFaceImage,
    doMatch: doMatch,
}
function getToken() {
    if (!config.TOKEN_ENABLED) return;
    debug('>> generate new token ...');
    return fetch(config.TOKEN_URL, {
        method: 'POST',
        body:  'client_id=' + encodeURIComponent(config.TOKEN_CLIENT_ID)
        + '&client_secret=' + encodeURIComponent(config.TOKEN_CLIENT_SECRET),
        headers: _.merge({'content-type': 'application/x-www-form-urlencoded'}, authenticationHeader())
    }).then(response => {
        if (response.status !== 200)  return Promise.reject(response);
        return response.json();
    }).then(function (json) {
        debug('<< generated token ...', json.token);
        return json.token;
    })
}

function getSession(token) {
    return fetch(config.BIOSERVER_CORE_URL + '/bio-sessions', {
        method: 'POST',
        body: JSON.stringify({
            'imageStorageEnabled': true,
            'callbackURL': config.SERVER_PUBLIC_ADDRESS  + config.BASE_PATH + config.LIVENESS_RESULT_CALLBACK_PATH
        }),
        headers: _.merge({'content-type': 'application/json'}, authenticationHeader(token))
    }).then(function (res) {
        if (res.status !== 201) return  Promise.reject(res);
        else return res.headers.get('location').split('/bio-sessions/')[1]
    })
}

function getLivenessChallengeResult(token, sessionId) {
    return fetch(config.BIOSERVER_CORE_URL + '/bio-sessions/' + sessionId + '/liveness-challenge-result', {
        method: 'GET',
        headers: authenticationHeader(token)
    }).then(function (res) {
        if (res.status !== 200) return  Promise.reject(res);
        return res.json();
    })
}


function createFace(token, sessionId, imageFile, imageFaceInfo = {}) {
    const formData = new FormData();
    formData.append('image', imageFile.buffer);
    formData.append('face', imageFaceInfo.buffer);

    return fetch(config.BIOSERVER_CORE_URL + '/bio-sessions/' + sessionId + '/faces', {
        method: 'POST',
        body: formData,
        headers: authenticationHeader(token)
    }).then( res => {
        if (res.status !== 201) return  Promise.reject(res);
        else return res.headers.get('location').split('/faces/')[1]
    }). then( faceId => {
        return fetch(config.BIOSERVER_CORE_URL + '/bio-sessions/' + sessionId + '/faces/' + faceId , {
            method: 'GET',
            headers: _.merge({'content-type': 'application/json'}, authenticationHeader(token))
        })
    }).then( res => {
        if (res.status !== 200) return  Promise.reject(res);
        return res.json();
    });

}
function getFaceImage(token, sessionId, faceId) {
    let url = config.BIOSERVER_CORE_URL + '/bio-sessions/' + sessionId + '/faces/' + faceId + '/image';
    if(config.ENABLE_IMAGE_COMPRESSION){
        url = url + '?compression=true';
    }
    return fetch(url , {
        method: 'GET',
        headers: _.merge({'content-type': 'application/json'}, authenticationHeader(token))
    }).then( res => {
        if (res.status !== 200) return  Promise.reject(res);
        return res.buffer();
    });
}
function doMatch(token, sessionId, referenceFaceId) {
    return fetch(config.BIOSERVER_CORE_URL + '/bio-sessions/' + sessionId + '/faces/' + referenceFaceId + '/matches' , {
        method: 'GET',
        headers: _.merge({'content-type': 'application/json'}, authenticationHeader(token))
    }).then( res => {
        if (res.status !== 200) return  Promise.reject(res);
        return res.json();
    });
}



function authenticationHeader(token) {
    const headers = {};
    if (config.TOKEN_ENABLED) {
        if (token) headers['Authorization'] = 'Bearer ' + token;
    } else {
        headers[config.API_KEY_HEADER] =  config.API_KEY_SECRET;
    }
    return headers;
}