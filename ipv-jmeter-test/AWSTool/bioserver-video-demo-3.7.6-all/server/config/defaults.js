const path = require('path');

module.exports = {
    DEBUG: 'front:*',
    NODE_TLS_REJECT_UNAUTHORIZED: '0',
    BASE_PATH: "/demo-server",
    // used to callback demo-server by WebioServer
    SERVER_PUBLIC_ADDRESS: "https://50.50.1.160:9943",
    // used in callback URL to receive liveness result from WebioServer
    // callbackURL = SERVER_PUBLIC_ADDRESS + BASE_PATH + LIVENESS_RESULT_CALLBACK_PATH
    LIVENESS_RESULT_CALLBACK_PATH: "/liveness-result-callback",

    VIDEO_SERVER_BASE_PATH: "/video-server",
    VIDEO_SERVER_WSPATH: "/engine.io",

    TLS_API_PORT: 9943,
    TLS_CERT_PATH: path.join(__dirname, 'certs/cert.pem'),
    TLS_KEY_PATH: path.join(__dirname, 'certs/key.pem'),

    // BioServer service
    BIOSERVER_CORE_URL: 'https://api.stodeveu.xantav.com/bioserver-app/v2',
    BIOSERVER_VIDEO_URL: 'https://api.stodeveu.xantav.com',

    // Authentication service
    TOKEN_ENABLED: false, // if enabled the wbs tokenApp will be used,
    TOKEN_URL: 'https://50.50.1.160/token-app/v1/authentication',
    TOKEN_CLIENT_ID: 'testuser',
    TOKEN_CLIENT_SECRET: 'F401MTb6QMNy8VFDMYDJ0dBQZepKDeHbTTCXRGaL+gFOCAomRITeZeJoQG3+CU2YNT2JXVzVgcZWD5LQNASqJA==',

    // apiKey conf
    API_KEY_HEADER: 'apikey',
    API_KEY_SECRET: 'ce5f306a-4100-414f-803c-4f15905f688e',

    CODING_QUALITY_THRESHOLD: 100,
    MATCHING_SCORE_THRESHOLD: 3000,
    ENABLE_IMAGE_COMPRESSION:true,
    GIPS_API_KEY_SECRET:'myapikey',
    GIPS_URL:'http://50.50.1.157:8080/gips/rest',
    GIPS_TENANT_ID:'gips' ,
    GIPS_TENANT_ROLE:'RELYING_SERVICE',
    IDPROOFING:false

}
