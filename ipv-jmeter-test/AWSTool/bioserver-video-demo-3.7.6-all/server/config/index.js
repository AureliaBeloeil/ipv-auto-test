const _ = require('lodash')

const defaults = require('./defaults')

const values = _.extend({}, defaults)
// Extend default values with production values


// Extend values with environment variables
_.each(process.env, function (value, key) {
  // Try to parse value in case JSON format is used
  try {
    value = JSON.parse(value)
  } catch (err) {}
    values[key] = value
})

process.env.DEBUG = values.DEBUG
process.env.NODE_TLS_REJECT_UNAUTHORIZED = values.NODE_TLS_REJECT_UNAUTHORIZED || '0'
process.env.UV_THREADPOOL_SIZE = values.UV_THREADPOOL_SIZE  || 10;
module.exports = values
