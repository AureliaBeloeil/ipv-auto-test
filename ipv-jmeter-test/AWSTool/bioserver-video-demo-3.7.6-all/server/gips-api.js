const fetch = require('node-fetch');
const FormData = require('form-data');
const _ = require('lodash');
const config = require('./config');
const debug = require('debug')('front:gips:api')
const context = [{
    "key": "BUSINESS_ID",
    "value": "LOA1P"
}]

const passportFRA = {"jurisdiction": "FRA", "documentType": "PASSPORT", "source": "LIVE_CAPTURE_IMAGE"};

module.exports = {

    getSession: getSession,
    getLivenessChallengeResult: getLivenessChallengeResult,
    createFace: createFace,
    getFaceImage: getFaceImage,

}


/**
 *
 * @returns
 * {
 *   "status": "PROCESSING",
 *   "type": "PORTRAIT",
 *   "id": "3c527efe-6cf2-48d5-b797-36e417b3fc9b",
 *   "sessionId": "c8ce1c32-2bab-411f-8c33-7fb17012a028"
 * }
 */
async function getSession() {
   let identityId = await createIdentity();
    let consent = await postConsent(identityId);
    if(consent) {
        let session = await startVideoCapture(identityId);

        return {
            identityId:session.identityId,
            sessionId:session.sessionId,
            portraitId:session.id
        }

    }
    return null
}

async function getLivenessChallengeResult(session) {
    // {
    //     "status":"NOT_VERIFIED",
    //     "type":"PORTRAIT",
    //     "id":"0eca6486-4623-44a4-b956-1b0eaba17723"
    // }

    // SUCCESS,                  // Challenge(s) request(s) success
    // FAILED,                       // challenge(s) request(s) failed
    // SPOOF,                        // Challenge(s) request(s) spoof detected
    // TIMEOUT,
    // INITIALIZED,
    // IN_PROGRESS,
    // ERROR


   let portraitStatus =  await getStatus(session.identityId, session.portraitId)

    let livenessResult = {
        livenessStatus: 'FAILED',
        livenessMode: '',
        bestImageId: session.portraitId
    };

   if(portraitStatus.errors && portraitStatus.errors.length > 0){
       livenessResult.diagnostic='No face detected';
       return livenessResult;
   }

    if(portraitStatus.status === "NOT_VERIFIED" ){
        livenessResult.livenessStatus='SUCCESS';
    }

    else if(portraitStatus.status === "INVALID" ){
        livenessResult.livenessStatus='SUCCESS';
        livenessResult.matching = false;
    }

    else if(portraitStatus.status === "VERIFIED" ){
        livenessResult.livenessStatus='SUCCESS';
        livenessResult.matching = true;
    } else {
        livenessResult.livenessStatus='FAILED';
    }

    return livenessResult;

}

/**
 * Create new gips identity
 * POST {{url}}/v1/identities
 * response code 200
 * {
 *   "id": "f966c1cc-9cc6-40a8-beec-d433d6a46bbd",
 *   "status": "EXPECTING_INPUT",
 *   "levelOfAssurance": "LOA0",
 *   "creationDateTime": "2019-09-09T10:35:38.681",
 *   "evaluationDateTime": "2019-09-09T10:35:38.686",
 *  "upgradePaths": {
 *  ....
 *  Error code 404/ 401 /
 * @returns identity id
 */

function createIdentity() {
    const formData = new FormData();
    formData.append('context', Buffer.from(context));

    return fetch(config.GIPS_URL + '/v1/identities', {
        method: 'POST',
        body: formData,
        headers: mutipartContentType(authenticationHeader())
    }).then(function (res) {
        if (res.status !== 200) return  Promise.reject(res);
        else return res.json().id
    })
}

/**
 * Post consent
 * POST {{url}}/v1/identities/{{identityId}}/consents
 * response 200
 * [{
 *   "approved": true,
 *	"type": "PORTRAIT",
 *	"validityPeriod": {
 *		"from": "2018-01-01"
 *	}
 * }]
 * @returns  Boolean true / false
 *  Error code 404/ 401 /
 */
function postConsent(identityId) {

    const consent  = [{
        "approved": true,
        "type": "PORTRAIT",
        "validityPeriod": {
            "from": "2019-01-01"
        }
    }]


    return fetch(config.GIPS_URL + '/v1/identities/'+identityId+'/consents', {
        method: 'POST',
        body: consent,
        headers: jsonContentType(authenticationHeader())
    }).then(function (res) {
        if (res.status !== 200) return  Promise.reject(res);
        else {

            let result = res.json()[0];
            if(!result.id || result.type !=consent.type){
                return false;
            }
        }
    })
}


/**
 * Start video capture init session
 * response 200

 * POST {{url}}/v1/identities/{{identityId}}/attributes/portrait/live-capture-video-session
 * @returns
 * {
 *   "status": "PROCESSING",
 *   "type": "PORTRAIT",
 *   "id": "3c527efe-6cf2-48d5-b797-36e417b3fc9b",
 *   "sessionId": "c8ce1c32-2bab-411f-8c33-7fb17012a028"
 * }
 */
function startVideoCapture(identityId) {

    return fetch(config.GIPS_URL + '/v1/identities/'+identityId+'/attributes/portrait/live-capture-video-session', {
        method: 'POST',
        headers: authenticationHeader()
    }).then(function (res) {
        if (res.status !== 200) return  Promise.reject(res);
        else return res.json()
    })
}

/**
 * Get portrait status
 * {{url}}/v1/identities/{{identityId}}/status/{{portraitId}}
 * {
 *   "status": "NOT_VERIFIED",
 *   "type": "PORTRAIT",
 *   "id": "0eca6486-4623-44a4-b956-1b0eaba17723"
 * }
 * @param identityId
 * @param portraitId
 * @returns {*}
 */
function getStatus(identityId, portraitId) {

    return fetch(config.GIPS_URL + '{{url}}/v1/identities/'+identityId+'/status/'+portraitId, {
        method: 'GET',
        headers: authenticationHeader()
    }).then(function (res) {
        if (res.status !== 200) return  Promise.reject(res);
        else return res.json()
    })
}




function createFace( imageFile, identityId) {
    const formData = new FormData();
    formData.append('DocumentCaptureDetails', Buffer.from(passportFRA));
    formData.append('DocumentFront', imageFile.buffer);

    return fetch(config.GIPS_URL +'/v1/identities/'+identityId+'/id-documents/capture', {
        method: 'POST',
        body: formData,
        headers: mutipartContentType(authenticationHeader())
    }).then( res => {
        if (res.status !== 201) return  Promise.reject(res);
        //passport id
        else return res.json()
    });

}
function getFaceImage(identityId) {
    let url = config.GIPS_URL + '/v1/identities/'+identityId+'/attributes/portrait/capture';

    return fetch(url , {
        method: 'GET',
        headers:  authenticationHeader(token)
    }).then( res => {
        if (res.status !== 200) return  Promise.reject(res);
        return res.buffer();
    });
}





function authenticationHeader() {
    const headers = {};
    headers[config.API_KEY_HEADER] =  config.GIPS_API_KEY_SECRET;
    headers['Tenant-Id'] =  config.GIPS_TENANT_ID;
    headers['Tenant-Role'] =  config.GIPS_TENANT_ROLE;
    return headers;
}



function jsonContentType(headers) {

    headers = _.merge({'content-type': 'application/json'},headers)
    return headers;
}



function mutipartContentType(headers) {

    headers = _.merge({'content-type': 'multipart/form-data'},headers)
    return headers;
}