$(document).ready(function () {
    let face1 = {};
    let face2 = {};
    let videoCaptureClient;
    let currentStream;
    let enableLiveness;

    const illuminationOverlay = document.querySelector('#illuminationOverlay')
    const graphicsOverlay = document.querySelector('#graphicsOverlay')
    const faceTrackOverlay = document.querySelector('#faceTrackOverlay')
    const controlledCircle = document.querySelector('#controlledCircle')
    const currentChallengeLine = document.querySelector('#currentChallengeLine')

    const videoContainer = document.querySelector('#face2-video')
    let videoIsFlipped = $('#flipVideo').is(':checked')
    const matching_score_threshold = 3000
    // force the circle to match the oval face
    const defaultFaceOverlayFactor = {
        height: 1,
        width: 0.90
    }



    /********************************
     * Video Capture Process :
     * 1- push face1
     * 2- capture face2 on video stream
     * 3- matching face1 with face2
     *
     *********************************/


    initLivenessSession().then(function (bioSession) {
        showAndScrollTo('#bio_session_panel')
        setText('#bio_session_id', bioSession)
        setText('#bio_session_result', bioSession)
        showAndScrollTo('#face1_panel')
    }).catch(err => {
        console.log('Liveness initialization failed', err)
        setText('#bio_session_result', err)
    })


    $('.panel-success').on('hidden.bs.collapse', toggleIcon)
    $('.panel-success').on('shown.bs.collapse', toggleIcon)

    const env = BioserverEnvironment.detection();
    if (!env.envDetected || !env.envDetected.os.isSupported || !env.envDetected.browser.isSupported) {
        return
    }


    $('#face1_image_file').change(function () {
        const $this = $(this)
        if ($this.val()) {
            $('#face1').prop('disabled', false)
        } else {
            $('#face1').prop('disabled', true)
        }
    })

    // push face1
    $('#face1').click(function (event) {
        const $this = $(this)
        if ($this.is(':enabled')) {
            const id = $this.attr('id')
            startSpinner('#' + id + '_spinner')

            const imageFile = $('#' + id + '_image_file')[0].files[0]
            const faceJsonStr = $('#' + id + '_json').val()
            const sessionId = $('#bio_session_id').text()
            const faceResultSelector = '#' + id + '_result'
            const $image = $('#' + id + '_image')
            const $imagesource = $('#' + id + '_image_source')
            const spinnerSelector = '#' + id + '_spinner'


            createFace(sessionId, imageFile, faceJsonStr)
                .then(function (face) {
                    stopSpinner(spinnerSelector)
                    face1 = face
                    setText('#matches_location', face.url)
                    setText(faceResultSelector, face)
                    $imagesource.attr('src', window.URL.createObjectURL(imageFile))

                    getFaceImage(sessionId, face1.faceId)
                        .then(function (imageByte) {
                            $image.attr('src', window.URL.createObjectURL(imageByte))
                        })

                    showAndScrollTo('#face2_panel')
                    resetGetMatchesPanel(true)
                    return initVideo(sessionId) // will init camera device selection
                })
                .catch(error => {
                    stopSpinner('#face1_spinner')
                    setText('#face1_result', error)
                })
        }
    })

    // select camera device
    $('#deviceId').change(function () {
        const deviceConstraints = {video: {deviceId: $(this).val()}}
        BioserverVideo.getDeviceStream(deviceConstraints)
            .then(stream => {
                currentStream = stream
                updateGUIStream(stream)
            })
            .catch(handleVideoError)
    })
    // enable device torch
    $('#enableTorch').click(function () {
        BioserverVideo.enableCameraFlashLight(currentStream, $(this).is(':checked'))
            .catch(err => {
                handleVideoError(err)
                $('#enableTorch').prop('checked', false)
            })
    })

    // option to flip video
    $('#flipVideo').click(function () {
        if ($(this).is(':checked')) {
            $('#video-container').css('transform', 'scale(-1,1)')
            videoIsFlipped = true
        } else {
            $('#video-container').css('transform', 'scale(1,1)')
            videoIsFlipped = false
        }
    })

    // select camera resolution
    $('#cameraResolution').change(function () {
        const cameraResolution = $('#cameraResolution').val().split('x')
        const deviceConstraints = {
            video: {
                deviceId: $('#deviceId').val(),
                width: cameraResolution[0],
                height: cameraResolution[1]
            }
        }

        BioserverVideo.getDeviceStream(deviceConstraints)
            .then(stream => {
                currentStream = stream
                updateGUIStream(stream)
            })
            .catch(handleVideoError)
    })

    // capture video
    $('#face2').click(function () {
        const $this = $(this)
        $('#record_result').hide() // hide record video result
        $('#liveness_challenge').hide() // hide challenge events
        $('#liveness_result').hide()
        $('#liveness_position').hide()

        $('li', '#video-events').remove() // remove events
        resetGetMatchesPanel()
        if ($this.hasClass('btn-danger')) { // << click on stop capture
            stopCapture()
        } else { // << click on start capture
            // switch button
            // reset GUI elements
            $this.removeClass('btn-primary').addClass('btn-danger')
            $('span', $this).text('Stop Capture face #2')
            // clean previous error
            output(null, 'face2_result')
            // disable video capture options
            $('#video-options').prop('disabled', true)
            startSpinner('#face2_spinner')

            // init highLiveness graphics
            const customUIOptions = {
                controlledPoint: {color: 'DarkTurquoise'},
                challengePoint: {
                    done: {color: 'OrangeRed'},
                    target: {color: 'DarkTurquoise'}
                },
                challengeLines: {
                    done: {color: 'OrangeRed', dashed: false},
                    target: {color: 'DarkTurquoise'}
                }
            }
            //BioserverVideoUI.resetLivenessHighGraphics(customUIOptions);
            BioserverVideoUI.resetLivenessHighGraphics()

            // start capture with user video options
            const recordVideo = $('#record_video').is(':checked')
            enableLiveness = $('#enable_liveness').val()
            const challengeNum = parseInt($('#highLiveness_challanges').val())
            videoCaptureClient.start(currentStream, recordVideo, enableLiveness, challengeNum)
        }
    })

    // change the liveness challenge mode
    $('#enable_liveness').change(function () {
        const small = $(this).next('small')
        if (($(this).val() === 'LIVENESS_HIGH') || ($(this).val() === 'LIVENESS_HIGH_WITH_ILLUMINATION')) {
            small.html('* move closer to the camera, then move the small circle with the movement of your face over the numbered circles')
        } else {
            small.html('* will display challenge instructions to follow')
        }
    })

    function illuminationLoop(colors, i) {

        illuminationOverlay.style.backgroundColor = colors[i]
        illuminationOverlay.style.display = 'block'
        videoCaptureClient.colorDisplayed()

        setTimeout(function () {
            illuminationOverlay.style.display = 'none'
            if (colors[i + 2]) illuminationLoop(colors, i + 2)
        }, colors[i + 1])
    }

    // match face1 with face2
    $('#get_matches_button').click(function () {
        startSpinner('#match_spinner')
        updateMatchResult()
        const bioSessionId = $('#bio_session_id').text()
        const setTextTo = setTextFn('#matches_result2')
        getMatches(bioSessionId, face1.faceId, face2.faceId)
            .then(matchResult => {
                stopSpinner('#match_spinner')
                updateMatchResult(matchResult)
            })
            .catch(setTextTo)
    })

    /****************************
     * * Util functions
     *************************************/



    /** ************************************
     * ** Video Capture CallBack functions **
     * **************************************/
    function onTracking(trackingData) {
        displayUserTooltips(trackingData)
        if (!(trackingData && $('#face2').hasClass('btn-danger'))) {
            return
        }
        if (trackingData.positionInfo) {
            outputEvent(JSON.stringify(trackingData.positionInfo)) // we display challenge instructions
        }
        // in case we receive a face tracking and **capture is in progress**
        if (!(trackingData.facex || trackingData.highLiveness)) {
            updateFaceTracker(false)
            return
        }

        if (trackingData.colorDisplay) {
            illuminationLoop(trackingData.colorDisplay, 0)
        }

        if (trackingData.facex) {
            let faceRect = faceRects(trackingData, {
                width: videoContainer.offsetWidth,
                height: videoContainer.offsetHeight
            })
            updateFaceTracker(true, Math.round(faceRect.x), Math.round(faceRect.y), Math.round(faceRect.w), Math.round(faceRect.h))
        }
        if (trackingData.livenessHigh) {
            BioserverVideoUI.updateLivenessHighGraphics('face2-video', trackingData)
        }

    }


    function displayUserTooltips(trackingData) {
        if (trackingData.distance && $('#liveness_challenge').is(':hidden') && 'NONE' != $('#enable_liveness').val()) {
            output('You are too far away from device. Please move closer', 'distance_information_value')
            $('#liveness_position').show()
        } else {
            $('#liveness_position').hide()
        }
    }

    function onShowVideoFile(path) {
        output(path, 'video_file_name')
        $('#record_result').show()
    }

    function onShowChallengeInstruction(value) {
        output(value, 'liveness_challenge_value')

        $('#liveness_challenge').show()
    }

    function onShowChallengeResult(data) {

        console.log('Liveness Challenge done > requesting result ...');
        challengeInProgress = false;
        const sessionId = $('#bio_session_id').text()
        getLivenessChallengeResult(sessionId)
            .then(function (result) {
                console.log('Liveness Challenge  result ...', result);
                output(result, 'liveness_result_value')
                $('#liveness_challenge').hide()
                $('#liveness_position').hide()

                if (!result || !result.isLivenessSucceeded) {
                    $('#liveness_result').removeClass('alert-success')
                    $('#liveness_result').addClass('alert-danger')
                } else {
                    $('#liveness_result').removeClass('alert-danger')
                    $('#liveness_result').addClass('alert-success')
                }
                $('#liveness_result').show()
                stopCapture()
                console.log('Liveness Challenge  result ..1.');
                output(result, 'face2_result')
                outputEvent(JSON.stringify(result))
                stopSpinner('#face2_spinner')
                face2.faceId = result.bestImageId;
                console.log('Liveness Challenge  result ..1.');
                const sessionId = $('#bio_session_id').text()
                getFaceImage(sessionId, result.bestImageId).then(function (image) {
                    elementById('face2_image').src = window.URL.createObjectURL(image)
                    showAndScrollTo('#match_panel')
                }).catch(handleVideoError)
            })
            .catch(errr => {
                $('#liveness_result').removeClass('alert-success')
                $('#liveness_result').addClass('alert-danger')
                output(errr, 'liveness_result_value')
            });

    }


    function stopCapture() {
        // close peer connection
        if (videoCaptureClient) videoCaptureClient.cancel()
        // switch capture button button
        $('#face2').removeClass('btn-danger').addClass('btn-primary')
        $('span', $('#face2')).text('Capture face #2')
        // enable video capture options
        $('#video-options').prop('disabled', false)
        stopSpinner('#face2_spinner')
        updateFaceTracker(false) // hide tracking circle
        updatehighLivenessTracker(false)
        BioserverVideoUI.resetLivenessHighGraphics()
    }

    function onError(error) {
        console.log(error)
        stopCapture()
        // error is a simple message (eg: "transport close" when server goes down)
        output(error, 'face2_result')
        showAndScrollTo('#face2_result')
    }

    function handleVideoError(error, enableCapture) {
        $('#face2').prop('disabled', enableCapture)
        output(error, 'face2_result')
        showAndScrollTo('#face2_result')
    }

    /*****************************
     * ** Private Util functions **
     * ************************* **/
    function initVideo() {
        BioserverVideo.initMediaDevices({cameraSelectionGUI: '#deviceId'})
            .then(() => {
                const cameraResolution = $('#cameraResolution').val().split('x')
                const deviceConstraints = {
                    video: {
                        // no device id the first time we show the camera to get the default user front camera
                        width: cameraResolution[0],
                        height: cameraResolution[1]
                    }
                }
                return BioserverVideo.getDeviceStream(deviceConstraints)
            })
            .then(stream => {
                currentStream = stream
                updateGUIStream(stream)

                const videoParams = Object.assign({}, window.config, {
                    trackingFn: onTracking,
                    errorFn: onError,
                    showVideoFile: onShowVideoFile,
                    showChallengeInstruction: onShowChallengeInstruction,
                    showChallengeResult: onShowChallengeResult,
                    bioSessionId: $('#bio_session_id').text(),
                    wspath: '/demo-server/wsocket',
                    resolution: $('#cameraResolution').val()
                })
                updateFaceTracker(false)
                updatehighLivenessTracker(false)
                return BioserverVideo.initFaceCaptureClient(videoParams)
            })
            .then(client => {
                videoCaptureClient = client
            })
            .catch(handleVideoError)
    }

    function updateFaceTracker(visible, posX, posY, width, height) {
        if (!visible) {
            faceTrackOverlay.setAttribute('rx', '0')
            faceTrackOverlay.setAttribute('ry', '0')
        } else {
            faceTrackOverlay.setAttribute('cx', posX + width / 2)
            faceTrackOverlay.setAttribute('cy', posY + height / 2)
            faceTrackOverlay.setAttribute('rx', (width / 2) * 0.8)
            faceTrackOverlay.setAttribute('ry', height / 2)
        }
    }

    function addLine(lines, x1, y1, x2, y2, color) {
        let line = document.createElementNS('http://www.w3.org/2000/svg', 'line')
        line.setAttribute('x1', (Math.round(x1)).toString())
        line.setAttribute('y1', (Math.round(y1)).toString())
        line.setAttribute('x2', (Math.round(x2)).toString())
        line.setAttribute('y2', (Math.round(y2)).toString())
        line.setAttribute('stroke', color)
        line.setAttribute('stroke-width', '5')
        line.setAttribute('stroke-dasharray', '10')
        // draw challenge lines
        lines.appendChild(line)
    }

    /**
     *
     * @param points
     * @param x1
     * @param y1
     * @param label
     * @param color
     * @param radius
     * @param videoIsFlipped
     * @param containerWidth
     */
    function addPoint(points, x1, y1, label, color, radius, videoIsFlipped, containerWidth) {
        let challenge = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
        // create challenge circle
        challenge.setAttribute('cx', (Math.round(x1)).toString())
        challenge.setAttribute('cy', (Math.round(y1)).toString())
        challenge.setAttribute('r', Math.round(radius).toString())
        challenge.setAttribute('stroke', 'white')
        challenge.setAttribute('stroke-width', '3')
        challenge.style.fill = color
        points.appendChild(challenge)

        let challengeNbr = document.createElementNS('http://www.w3.org/2000/svg', 'text')
        let x = x1
        if (videoIsFlipped) {
            challengeNbr.setAttribute('style', 'transform: scale(-1, 1);')
            x = containerWidth - x1
        }
        challengeNbr.setAttribute('x', (Math.round(x)).toString())
        challengeNbr.setAttribute('y', (Math.round(y1)).toString())
        challengeNbr.setAttribute('dy', '0.35em')
        challengeNbr.setAttribute('font-family', 'Arial')
        challengeNbr.setAttribute('font-size', radius + 'pt')
        challengeNbr.setAttribute('font-weight', 'bold')
        challengeNbr.setAttribute('text-anchor', 'middle')
        challengeNbr.setAttribute('fill', 'white')
        challengeNbr.textContent = label.toString()
        // draw challenge circles
        points.appendChild(challengeNbr)
    }

    function updatehighLivenessTracker(visible, coefs, highLiveness, videoIsFlipped, enableLiveness) {
        let grpLines = document.createElementNS('http://www.w3.org/2000/svg', 'g')
        let grpPoints = document.createElementNS('http://www.w3.org/2000/svg', 'g')

        if (visible) {
            // if (!videoCaptureElement.hasClass('blure')) videoCaptureElement.addClass('blure'); << resource expensive > cause video lag

            let i = 0
            while (i <= highLiveness.targetChallengeIndex) {
                // create challenge circle
                let colorPoint = 'BlueViolet'
                if (i < highLiveness.targetChallengeIndex) {
                    colorPoint = 'DarkGray'
                }
                // draw challenge circle
                addPoint(grpPoints,
                    highLiveness.challengeCircles[i].x * coefs.coefW,
                    highLiveness.challengeCircles[i].y * coefs.coefH, i + 1,
                    colorPoint,
                    highLiveness.challengeCircles[i].r * coefs.coefW * 0.5,
                    videoIsFlipped, videoContainer.width)

                // create done challenges lines
                if (enableLiveness === 'highLiveness_PATH' && i > 0) {
                    if (i < highLiveness.targetChallengeIndex) {
                        addLine(grpLines,
                            highLiveness.challengeCircles[i - 1].x * coefs.coefW, highLiveness.challengeCircles[i - 1].y * coefs.coefH,
                            highLiveness.challengeCircles[i].x * coefs.coefW, highLiveness.challengeCircles[i].y * coefs.coefH,
                            'DarkGray')
                    }
                    // draw "into-target" challenge line
                    if (i === highLiveness.targetChallengeIndex) {
                        currentChallengeLine.setAttribute('x1', highLiveness.challengeCircles[i - 1].x * coefs.coefW)
                        currentChallengeLine.setAttribute('y1', highLiveness.challengeCircles[i - 1].y * coefs.coefH)
                        currentChallengeLine.setAttribute('x2', highLiveness.controlledCircle.x * coefs.coefW)
                        currentChallengeLine.setAttribute('y2', highLiveness.controlledCircle.y * coefs.coefH)
                        currentChallengeLine.setAttribute('stroke-width', '5')
                    }
                }
                i++
            }
            // draw controlled circle
            controlledCircle.setAttribute('cx', highLiveness.controlledCircle.x * coefs.coefW)
            controlledCircle.setAttribute('cy', highLiveness.controlledCircle.y * coefs.coefH)
            controlledCircle.setAttribute('r', 25 * coefs.coefW)
        } else {
            //videoCaptureElement.removeClass('blure');
            //controlledCircle.setAttribute('r', '0');
            //currentChallengeLine.setAttribute('stroke-width', '0');
        }
        graphicsOverlay.replaceChild(grpLines, graphicsOverlay.firstChild)
        graphicsOverlay.replaceChild(grpPoints, graphicsOverlay.children[graphicsOverlay.children.length - 2])

    }

    // force the circle to match the oval face
    function faceRects(trackingData, videoSize, faceOverlayFactor = defaultFaceOverlayFactor) {

        const coefW = videoSize.width / trackingData.w
        const coefH = videoSize.height / trackingData.h

        return {
            x: trackingData.facex * coefW,
            y: trackingData.facey * coefH,
            w: trackingData.facew * coefW * faceOverlayFactor.width,
            h: trackingData.faceh * coefH * faceOverlayFactor.height
        }
    }

    function responsivehighLiveness(trackingData, videoSize) {
        let r = 0
        if (trackingData.challengeCircles && trackingData.challengeCircles.length()) {
            r = trackingData.challengeCircles[0].r
        }
        const coefW = (videoSize.width) / (trackingData.w + r)
        const coefH = (videoSize.height) / (trackingData.h + r)

        return {
            coefW: coefW,
            coefH: coefH
        }
    }

    function updateGUIStream(stream) {
        let face2Video = document.querySelector('#face2-video')
        try {
            face2Video.srcObject = stream
        } catch (error) {
            // Avoid using this in new browsers, as it is going away.
            face2Video.src = window.URL.createObjectURL(stream)
        }
        $('#face2').prop('disabled', false)
        output(null, 'face2_result')
    }

    function resetGetMatchesPanel(displayMatchPanel) {
        if (!displayMatchPanel) $('#match_panel').hide() // hide matching result
        output(null, 'matches_result2')
        updateMatchResult()
    }

    function updateMatchResult(matchResult) {
        const setTextTo = setTextFn('#matches_result2')
        if (matchResult) {
            if (Array.isArray(matchResult)) {
                const face2MatchResult = matchResult.find(match => {
                    return match.candidate.id === face2.faceId
                })
                if (face2MatchResult && face2MatchResult.score > matching_score_threshold) {
                    $('#get_matches_button').removeClass().addClass('btn btn-success')
                    $('#match_spinner').removeClass().addClass('fa fa-check-square-o')
                    $('#get_matches_info').text('* HIT')
                    setTextTo(face2MatchResult)
                } else {
                    $('#get_matches_button').removeClass().addClass('btn btn-danger')
                    $('#match_spinner').removeClass().addClass('fa fa-close')
                    $('#get_matches_info').text('* NO HIT')
                    setTextTo(face2MatchResult)
                }
            } else setTextTo(matchResult)
        }
        else {
            $('#get_matches_button').removeClass().addClass('btn btn-primary')
            $('#match_spinner').removeClass()
            $('#get_matches_info').text('')
        }
    }

    function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find('.more-less')
            .toggleClass('glyphicon-plus glyphicon-minus')
    }


    function stopSpinner(selector) {
        $(selector).removeClass()
    }

    function startSpinner(selector) {
        $(selector).addClass('fa fa-refresh fa-spin')
    }

    function outputEvent(data) {
        $(elementById('video-events')).prepend('<li class="list-group-item">' + data + ' </li>')
    }

    function output(data, id) {
        elementById(id || 'output').innerHTML = data ? JSON.stringify(data, undefined, 2) : ''
    }


    function elementById(id) {
        return document.getElementById(id)
    }

    function showAndScrollTo(selector) {
        $(selector).show(2000)
        $('html, body').animate({scrollTop: $(selector).offset().top}, 2000)
    }

    function setText(selector, data) {
        if (typeof data === 'string') {
            $(selector).text(data)
        } else {
            $(selector).text(JSON.stringify(data, null, 4))
        }
    }

    function setTextFn(selector) {
        return function (data) {
            setText(selector, data)
        }
    }
})
