

/**
 * init a liveness session (sessionId can be given to avoid creating a new session under bioserver-core)
 * @return sessionId
 */
async function initLivenessSession(sessionId) {
    return new Promise(function (resolve, reject) {
        const xhttp = new window.XMLHttpRequest();
        xhttp.open('GET', '/demo-server/init-liveness-session/' + sessionId, true);
        xhttp.responseType = 'json';
        xhttp.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(xhttp.response)
            } else {
                console.error('initLivenessSession failed');
                reject();
            }
        };
        xhttp.onerror = function () {
            reject();
        }
        xhttp.send();
    })
}

/**
 * retrieve the liveness challenge result from backend
 * @param sessionId
 * @return {isLivenessSucceeded, message}
 */
async function getLivenessChallengeResult(sessionId) {
    return new Promise(function (resolve, reject) {
        const xhttp = new window.XMLHttpRequest();
        xhttp.open('GET', '/demo-server/liveness-challenge-result/' + sessionId, true);
        xhttp.setRequestHeader('Content-type', 'application/json');
        xhttp.responseType = 'json';
        xhttp.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(xhttp.response)
            } else {
                console.error('getLivenessChallengeResult failed');
                reject();
            }
        };
        xhttp.onerror = function () {
            reject();
        }
        xhttp.send();
    })
}

/**
 * Create face via service provider proxy
 * @param sessionId
 * @param imageFile
 * @param faceInfo
 * @returns {Promise<any>}
 */
async function createFace(sessionId, imageFile, faceInfo) {

    return new Promise(function (resolve, reject) {
        const formData = new window.FormData();
        const xhttp = new window.XMLHttpRequest();
        formData.append('image', imageFile);
        formData.append('face', new window.Blob([faceInfo], {type: 'application/json'}));
        xhttp.open('POST', '/demo-server/bio-session/' + sessionId + '/faces', true);
        xhttp.responseType = 'json';
        xhttp.onload = function () {
            if (this.status === 200) {
                resolve(xhttp.response)
            } else {
                console.error('createFace failed');
                reject();
            }
        }
        xhttp.onerror = function () {
            console.error('createFace failed');
            reject(xhttp)
        }
        xhttp.send(formData)
    })
}



/**
 * Get image bytearray  in WBS session via sp proxy
 * @param sessionId
 * @param faceId
 * @returns {Promise<any>}
 */
async function getFaceImage(sessionId, faceId) {

    return new Promise(function (resolve, reject) {
        const xhttp = new window.XMLHttpRequest();
        xhttp.open('GET', '/demo-server/bio-session/' + sessionId + '/faces/' + faceId + '/image', true);
        xhttp.responseType = 'blob';
        xhttp.onload = function () {
            if (this.status === 200) {
                resolve(xhttp.response)
            } else {
                console.error('createFace failed');
                reject();
            }
        }
        xhttp.onerror = function () {
            console.error('createFace failed');
            reject(xhttp)
        }
        xhttp.send()
    })
}

/**
 * Get matching info of two images in WBS session via sp proxy
 * @param sessionId
 * @param referenceFaceId
 * @param candidateFaceId
 * @returns {Promise<any>}
 */

async function getMatches(sessionId, referenceFaceId, candidateFaceId) {

    return new Promise(function (resolve, reject) {
        const xhttp = new window.XMLHttpRequest();
        xhttp.open('GET', '/demo-server/bio-session/' + sessionId + '/faces/' + referenceFaceId + '/matches/' + candidateFaceId, true);
        xhttp.responseType = 'json';
        xhttp.onload = function () {
            if (this.status === 200) {
                resolve(xhttp.response)
            } else {
                console.error('createFace failed');
                reject();
            }
        }
        xhttp.onerror = function () {
            console.error('createFace failed');
            reject(xhttp)
        }
        xhttp.send()
    })
}



async function getStaticFile(url) {

    return new Promise(function (resolve, reject) {
        const xhttp = new window.XMLHttpRequest();
        xhttp.open('GET', url, true);
        xhttp.responseType = 'blob';
        xhttp.onload = function () {
            if (this.status === 200) {
                resolve(xhttp.response)
            } else {
                console.error('retrieve file failed');
                reject();
            }
        }
        xhttp.onerror = function () {
            console.error('retrieve file failed');
            reject(xhttp)
        }
        xhttp.send()
    })
}