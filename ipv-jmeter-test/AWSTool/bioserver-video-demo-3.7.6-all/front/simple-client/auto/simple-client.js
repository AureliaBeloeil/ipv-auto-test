'use strict';
const cameraSelector = document.querySelector('#cameraSelect');
const audioSelector = document.querySelector('#audioSelect');
const videoOutput = document.querySelector('#video');
const captureButton = document.querySelector('#capture');
const sessionNbr = document.querySelector('#sessionNbr');
const stopCaptureButton = document.querySelector('#stop');
const captureResults = document.querySelector('#results');
const imageUrl = document.querySelector('#image_file').src
const faceJsonStr = document.querySelector('#face_json').value
let referenceFile = {}


async function main (){
    let currentMediaStream;
    let faceCaptureClients = new Map();
    const bioserverVideoUrl = ''; // important to set video server url!

    // init session nbr from url param
    const url = new URL(window.location.href);
    const sessionIdParam = url.searchParams.get("sessionId");
    const sessionNbrParam = url.searchParams.get("sessionNbr");
    const autoCapture = url.searchParams.get("auto");
    let enableLiveness = url.searchParams.get("enableLiveness");
    const withMatching = url.searchParams.get("with_matching");
    const withExtraction = url.searchParams.get("with_extraction");
    let trainingMode = 'LIVENESS_HIGH' != enableLiveness ? false : true; // true when HIGH to force the fix of the first point
    if ( 'LIVENESS_MEDIUM'!=enableLiveness && 'LIVENESS_HIGH'!=enableLiveness) {
        enableLiveness = 'NO_LIVENESS';
    }
    if (sessionNbrParam && Number(sessionNbrParam)) {
        sessionNbr.value = sessionNbrParam;
    }

    async function startFaceCapture(pcNumber = 1, enableLiveness, withExtraction, withMatching) {
        // get a token and session from bioserver core
        const sessionId = await initLivenessSession(sessionIdParam).catch( error => {
            console.log(`>> initLivenessSession  FAILED`, error);
        });
        // initialize the face capture client with callbacks
        const faceCaptureOptions = {
            bioserverVideoUrl: bioserverVideoUrl,
            wspath: '/demo-server/wsocket',
            bioSessionId: sessionId,
            trackingFn: (trackingInfo) => {
                //console.log("tracking", trackingInfo);
                const client = faceCaptureClients.get(pcNumber);
                if (!client.firstFrameDate) client.firstFrameDate = trackingInfo.timestamp;
                if (!client.frameNbr) client.frameNbr = 0;
                client.frameNbr++;
                client.lastFrameDate = trackingInfo.timestamp;
            },

            errorFn: (error) => {
                console.log(`>> ${pcNumber} FAILED`, error);
                const result = document.createElement('li');
                const failedClient = faceCaptureClients.get(pcNumber);
                result.innerHTML = `PC-${pcNumber} | FAILED | ${failedClient.sessionId} 
          | ${Math.round((new Date() - failedClient.startDate)/1000)}:s 
          | ${failedClient.frameNbr} frames, (${failedClient.lastFrameDate - failedClient.firstFrameDate}:s) 
          | Error: ${error.error}`;
                if (captureResults.innerHTML.indexOf(`PC-${pcNumber} |`) === -1) {
                    captureResults.insertAdjacentElement('afterbegin', result);
                }
                failedClient.disconnect(true);
            },
            showVideoFile: (recordedVideoFile) => {
                console.log(`>> ${pcNumber} recorded video file`, recordedVideoFile)
            },
            showChallengeInstruction: (challengeInstruction) => {
                console.log(`>> ${pcNumber} challenge instructions`, challengeInstruction)
            },
            showChallengeResult: (challengeResult) => {
               if('LIVENESS_HIGH'==enableLiveness) {
                 console.log(`>> PC ${pcNumber} got challenge result`, challengeResult);
                 const result = document.createElement('li');
                 const successClient = faceCaptureClients.get(pcNumber);
                 let firstDateString = new Date(0);
                 firstDateString.setUTCSeconds(successClient.firstFrameDate);
                 let totalLivenessTime = Math.round((new Date() - successClient.startDate) / 1000);

                 displayResultHighLiveness(result, pcNumber, totalLivenessTime, challengeResult.livenessStatus, successClient, firstDateString)
                 captureResults.insertAdjacentElement('afterbegin', result);
                 successClient.disconnect(true);
               } else {


                getLivenessChallengeResult(sessionId)
                    .then(function (challengeResult) {
                        console.log(`>> PC ${pcNumber} got challenge result`, challengeResult);
                        const result = document.createElement('li');
                        const successClient = faceCaptureClients.get(pcNumber);
                        let firstDateString = new Date(0);
                        firstDateString.setUTCSeconds(successClient.firstFrameDate);
                        let totalLivenessTime = Math.round((new Date() - successClient.startDate) / 1000);


                            if (withExtraction ==='true' && challengeResult.isLivenessSucceeded ) {
                                getFaceImage(sessionId, challengeResult.bestImageId).then(function (bestImage) {
                                    console.log(`>> PC ${pcNumber} got best image result`);
                                    displayResultWithBestImage(result, pcNumber, totalLivenessTime, challengeResult, successClient, firstDateString, bestImage)
					if('' != document.getElementById('face_image').src){
						document.getElementById('face_image').src = window.URL.createObjectURL(bestImage)
					}

                            
                                     successClient.disconnect(true);
                                    captureResults.insertAdjacentElement('afterbegin', result);

                                }).catch(error => {
                                    displayError(result, pcNumber, totalLivenessTime, challengeResult, successClient, firstDateString, error)
                                    captureResults.insertAdjacentElement('afterbegin', result);
                                 successClient.disconnect(true);
                                })
                            }

                            else if (withMatching ==='true' && challengeResult.isLivenessSucceeded) {


                                createFace(sessionId, referenceFile, faceJsonStr)
                                    .then(function (face) {
                                        console.log(`>> PC ${pcNumber} create candidate face `,face );
                                        getMatches(sessionId, challengeResult.bestImageId, face.faceId)
                                            .then(matchResult => {
                                                console.log(`>> PC ${pcNumber} match result `,matchResult );
                                                displayResult(result, pcNumber, totalLivenessTime, challengeResult, successClient, firstDateString, matchResult)
                                                captureResults.insertAdjacentElement('afterbegin', result);
                                                successClient.disconnect(true);
                                            })
                                            .catch(error => {
                                                displayError(result, pcNumber, totalLivenessTime, challengeResult, successClient, firstDateString, error)
                                                captureResults.insertAdjacentElement('afterbegin', result);
                                                successClient.disconnect(true);
                                            })

                                    })
                                    .catch(error => {
                                        displayError(result, pcNumber, totalLivenessTime, challengeResult, successClient, firstDateString, error)
                                        captureResults.insertAdjacentElement('afterbegin', result);
                                        successClient.disconnect(true);
                                    })
                            } else {
                                displayResult(result, pcNumber, totalLivenessTime, challengeResult, successClient, firstDateString)
                                captureResults.insertAdjacentElement('afterbegin', result);
                                successClient.disconnect(true);
                            }





                    })

               }

            }


        };
        console.log('>> initLivenessSession - sessionId: ',sessionId );
        if(sessionId) {
            const client = await BioserverVideo.initFaceCaptureClient(faceCaptureOptions);
            Object.assign(client, {startDate: new Date(), status: 'INIT', sessionId: sessionId});
            faceCaptureClients.set(pcNumber, client);
            // liveness detection : NONE|SLAM|CR2D
            // none => only detect user face
            // SLAM => show instruction to user to follow (turn face left, right ...)
            // CR2D => show two circles to user (with user face movements: get the designed circle over the target circle)
            faceCaptureClients.get(pcNumber).start(currentMediaStream, false, enableLiveness, sessionId, trainingMode); // << no liveness detection and no video storage
        }
    }

    function displayResultWithBestImage(result, pcNumber,totalLivenessTime,  challengeResult, successClient, firstDateString, bestImage){
        let face_id = 'face2_image'+pcNumber 
        displayResult(result, pcNumber,totalLivenessTime,  challengeResult, successClient, firstDateString)
        result.innerHTML = result.innerHTML+`| Img extracted: OK`;
   

    }

    function displayResultHighLiveness(result, pcNumber,totalLivenessTime,  challengeResult, successClient, firstDateString, matchResult){
        let totalTime = Math.round((new Date() - successClient.startDate) / 1000);
        let challengeResultString = challengeResult;
        if (challengeResult == "SUCCESS") {
            challengeResultString = "Liveness succeeded";
        }
        result.innerHTML = `PC-${pcNumber} | ${challengeResultString} | ${successClient.sessionId}
                              |${challengeResult}
                              | ${totalLivenessTime}:s
                              | Total frames: ${successClient.frameNbr}  (${successClient.lastFrameDate - successClient.firstFrameDate}:s)
                              | Total time: ${totalTime}:s
                              | 1st frame: ${firstDateString.toTimeString().split(' ')[0]}`;
    }

    function displayResult(result, pcNumber,totalLivenessTime,  challengeResult, successClient, firstDateString, matchResult){
        let totalTime = Math.round((new Date() - successClient.startDate) / 1000);
        result.innerHTML = `PC-${pcNumber} | ${challengeResult.message} | ${successClient.sessionId} 
                              |${challengeResult.bestImageId}
                              | ${totalLivenessTime}:s 
                              | Total frames: ${successClient.frameNbr}  (${successClient.lastFrameDate - successClient.firstFrameDate}:s) 
                              | Total time: ${totalTime}:s 
                              | 1st frame: ${firstDateString.toTimeString().split(' ')[0]}`;
        if(matchResult){

          result.innerHTML = result.innerHTML+` |Match: [result: ${matchResult.matching}, score: ${matchResult.score}] `;
        }

    }
    function displayError(result, pcNumber,totalLivenessTime,  challengeResult, successClient, firstDateString, error){
        let totalTime = Math.round((new Date() - successClient.startDate) / 1000);
        result.innerHTML = `PC-${pcNumber} | ${challengeResult.message} | ${successClient.sessionId} 
                              |${challengeResult.bestImageId}
                              | ${totalLivenessTime}:s 
                              | Total frames: ${successClient.frameNbr}  (${successClient.lastFrameDate - successClient.firstFrameDate}:s) 
                              | Total time: ${totalTime}:s 
                              | Error: ${error}
                              | 1st frame: ${firstDateString.toTimeString().split(' ')[0]}`;


    }



    async function getStreamAndDisplayIt() {
        // get selected device stream (take a look at optional parameters of this function)
        const deviceConstraints = {
            audio: {deviceId: audioSelector.value},
            video: {deviceId: cameraSelector.value, width: 640, height:480}


        };


        try{
            currentMediaStream = await BioserverVideo.getDeviceStream(deviceConstraints);
            videoOutput.srcObject = currentMediaStream;


            if (autoCapture && autoCapture ==='true') {
                captureButton.click();

            }
        } catch (err) {
            console.log('getStreamAndDisplayIt failed', err);
            alert("Something was wrong , please see the console log");
        }

    }

    try {
        // init camera/mic selection
        await BioserverVideo.initMediaDevices({cameraSelectionGUI: '#cameraSelect',audioSelectionGUI: '#audioSelect'});

        // display the user media stream
        getStreamAndDisplayIt();

        if(withMatching){
            getStaticFile(imageUrl).then(function (face) {
                referenceFile = face;
            })
        }

        // when click on video capture
        captureButton.addEventListener('click', async () => {
            const max = sessionNbr.value;
            console.log(`>> starting ${max} sessions`);
            // clean old clients & results
            captureResults.innerHTML = '';
            faceCaptureClients.forEach(function(client, id) {
                client.disconnect(true);
                console.log(`>>${id} face capture client closed`);
            });
            faceCaptureClients = new Map()
            let i ;
            for(i=1; i<= max; i++) {
                startFaceCapture(i, enableLiveness, withExtraction, withMatching);
            }
        });

        stopCaptureButton.addEventListener('click', () => {
            faceCaptureClients.forEach(function(client, id) {
                client.disconnect(true);
                console.log(`>>${id} face capture client closed`);
            });
        });

        cameraSelector.addEventListener('change', () => getStreamAndDisplayIt());

        audioSelector.addEventListener('change', () => getStreamAndDisplayIt());

    } catch (error) {
        console.log('Something was wrong', error);
        faceCaptureClients.forEach(function(client, id) {
            client.disconnect(true);
            console.log(`>>${id} face capture client closed`);
        });
    }

}

main();
