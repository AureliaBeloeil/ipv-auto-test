let currentClient;
let currentStream;
let idVideoEvent = 1;
let face2 = {}

/********************************
 * Video Capture Process :
 * 1- push face1
 * 2- capture face2 on video stream
 * 3- matching face1 with face2
 *
 *********************************/




function onTracking(trackingData) {
    outputEvent(JSON.stringify(trackingData));
}



function onError(error) {
    $("#faceTracking").hide();
    output(error, 'face2_result')
}

function onShowVideoFile(path) {
    output(path, 'video_file_name');
    $('#record_result').show();
}

function onShowChallengeInstruction(value) {
    output(value, 'liveness_challenge_value')
    $('#liveness_challenge').show()
}

function onShowChallengeResult(value) {
    const sessionId = $('#bio_session_id').text()
    getLivenessChallengeResult(sessionId)
        .then(function (result) {
            console.log('Liveness Challenge  result ...', result);
            output(result, 'liveness_result_value')
            $('#liveness_challenge').hide()
            $('#liveness_position').hide()

            if (!result || !result.isLivenessSucceeded) {
                $('#liveness_result').removeClass('alert-success')
                $('#liveness_result').addClass('alert-danger')
            } else {
                $('#liveness_result').removeClass('alert-danger')
                $('#liveness_result').addClass('alert-success')
            }
            $('#liveness_result').show()

            console.log('Liveness Challenge  result ..1.');
            output(result, 'face2_result')
            outputEvent(JSON.stringify(result))
            currentClient.cancel()
            face2.faceId = result.bestImageId;
            console.log('Liveness Challenge  result ..1.');
            const sessionId = $('#bio_session_id').text()
            getFaceImage(sessionId, result.bestImageId).then(function (image) {
                elementById('face2_image').src = window.URL.createObjectURL(image)
                showAndScrollTo('#match_panel')
            }).catch(handleVideoError)
        })
        .catch(errr => {
            $('#liveness_result').removeClass('alert-success')
            $('#liveness_result').addClass('alert-danger')
            output(errr, 'liveness_result_value')
        });
}

function initVideo(bioSessionId) {
    var currentDeviceId = document.querySelector('#deviceId').value;

    console.log('webcam: ', currentDeviceId)
    const videoParams = Object.assign({}, window.config, {
        trackingFn: onTracking,
        errorFn: onError,
        showVideoFile: onShowVideoFile,
        showChallengeInstruction: onShowChallengeInstruction,
        showChallengeResult: onShowChallengeResult,
        bioSessionId: bioSessionId,
        wspath: '/demo-server/wsocket',
    })
    var urlParams = new URLSearchParams(window.location.search);
    var enableLiveness = urlParams.get('enableLiveness') == 'true';
    var recordVideo = urlParams.get('recordVideo') == 'true';
    console.log("urlParams ", urlParams);
    console.log("recordVideo ", recordVideo);
    console.log("enableLiveness ", enableLiveness);
    BioserverVideo.initMediaDevices()
        .then(() => {
            return BioserverVideo.getDeviceStream()
        })
        .then(stream => {
            currentStream = stream
            elementById('face2-video').srcObject = stream;
            return BioserverVideo.initFaceCaptureClient(videoParams);
        })
        .then(client => {
            currentClient = client
            client.start(currentStream, recordVideo, enableLiveness);
        })
        .catch(handleError);
}

function handleError(error) {
    console.log('navigator.getUserMedia error: ', error);
}
$(document).ready(function() {
    $('.panel-success').on('hidden.bs.collapse', toggleIcon);
    $('.panel-success').on('shown.bs.collapse', toggleIcon);

    initLivenessSession().then(function (bioSession) {
        showAndScrollTo('#bio_session_panel')
        setText('#bio_session_id', bioSession)
        setText('#bio_session_result', bioSession)
        const bioSessionId = $('#bio_session_id').text()
        showAndScrollTo('#face2_panel')
        return initVideo(bioSessionId)
    }).catch(err => {
        console.log('Liveness initialization failed', err)
        setText('#bio_session_result', err)
    })


})

function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}

function stopSpinner(selector) {
    $(selector).removeClass()
}

function outputEvent(data) {

    $(elementById('video-events')).prepend('<li class="list-group-item" id="video-events-"'+idVideoEvent+'>' + data + ' </li>')
    idVideoEvent = idVideoEvent+1;
}

function output(data, id) {
    elementById(id || 'output').innerHTML = JSON.stringify(data, undefined, 2)
}


function elementById(id) {
    return document.getElementById(id)
}

function showAndScrollTo(selector) {
    $(selector).show(2000)
    $('html, body').animate({
        scrollTop: $(selector).offset().top
    }, 2000)
}

function setText(selector, data) {
    if (typeof data === 'string') {
        $(selector).text(data)
    } else {
        $(selector).text(JSON.stringify(data, null, 4))
    }
}

