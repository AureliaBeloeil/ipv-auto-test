/**
 * Check user env support
 * must be ES5 compatible
 **/
if (BioserverEnvironment) {


    var checkNetwork = true;

    var env = BioserverEnvironment.detection();
    var envDetectionPage = document.querySelector('#step-0');
    if (env.envDetected) {
        var browsersDescription = envDetectionPage.querySelector('.browsers-description ');
        var envOS = env.envDetected.os;
        var envBrowser = env.envDetected.browser;
        if (!envOS.isSupported) {
            envDetectionPage.className = envDetectionPage.className.replace('d-none', '');
            envDetectionPage.querySelector('.description').textContent = env.message;
            browsersDescription.textContent = "Please use one of following operating systems for a better experience";
            var osList = envDetectionPage.querySelector('.os-list');
            osList.innerHTML = "";
            for (var osIndex in envOS.supportedList) {
                var osInfo = envOS.supportedList[osIndex];
                var os = document.createElement('div');
                os.className = 'os';
                var osImg = document.createElement('div');
                osImg.id = osInfo.toLowerCase().replace(' ', '-');
                osImg.className = 'os-img';
                var osDesc = document.createElement('span');
                osDesc.innerHTML = osInfo;
                os.appendChild(osImg);
                os.appendChild(osDesc);
                osList.appendChild(os);
            }
            checkNetwork = false;
        } else if (!envBrowser.isSupported) {
            envDetectionPage.className = envDetectionPage.className.replace('d-none', '');
            envDetectionPage.querySelector('.description').textContent = env.message;
            browsersDescription.textContent = "Please use one of following browsers for a better experience";
            var browsersList = envDetectionPage.querySelector('.browsers');
            browsersList.innerHTML = "";
            for (var browserIndex in envBrowser.supportedList) {
                var browserInfo = envBrowser.supportedList[browserIndex];
                var browser = document.createElement('div');
                browser.className = 'browser';
                var browserImg = document.createElement('div');
                browserImg.id = browserInfo.name.toLowerCase().replace(' ', '-');
                browserImg.className = 'browser-img';
                var browserDesc = document.createElement('span');
                browserDesc.innerHTML = browserInfo.name + ' Version ' + browserInfo.minimumVersion + '+';
                browser.appendChild(browserImg);
                browser.appendChild(browserDesc);
                browsersList.appendChild(browser);
            }
            checkNetwork = false;
        } else {
            envDetectionPage.className = envDetectionPage.className.concat(' d-none');
            document.querySelector('#step-1').className = document.querySelector('#step-1').className.replace('d-none', '');
            if (!envOS.isMobile) {
                document.querySelector('main').className = document.querySelector('main').className.concat(' pc');
            }

        }
    } else {
        envDetectionPage.querySelector('.description').textContent = env.message;
        checkNetwork = false;
    }

    if (checkNetwork) {
        // Must test two global variable value
        // Network test has been done
        // var networkTest = false;
        // network connectivity is OK
        // var networkConnectivity = false;

        BioserverEnvironment.connectivityMeasure('/demo-server');

        setTimeout(function () {
            var envDetectionPage = document.querySelector('#step-0-network');
            if(networkTestDone && networkTestStart){

                if (!networkConnectivity.goodConnectivity) {
                    rows = document.querySelectorAll('.step');
                    for(i=0; i< rows.length;i++ ){
                        rows[i].classList.add('d-none');
                    }
                    envDetectionPage.className = envDetectionPage.className.replace('d-none', '');
                    document.querySelector('#step-1').className = document.querySelector('#step-1').className.concat(' d-none');
                }
            } else {
                rows = document.querySelectorAll('.step');
                for(i=0; i< rows.length;i++ ){
                    rows[i].classList.add('d-none');
                }
                envDetectionPage.className = envDetectionPage.className.replace('d-none', '');
                document.querySelector('#step-1').className = document.querySelector('#step-1').className.concat(' d-none');
            }
            envDetectionPage.querySelector('.browsers-description').innerHTML =
                "<p>"+networkConnectivity.message+"</p>"
            +"<p><b>Current upload</b>: "+networkConnectivity.upload+"</p>"
            +"<p><b>Current download</b>: "+networkConnectivity.download+"</p>";

        }, 3000)
    }


}