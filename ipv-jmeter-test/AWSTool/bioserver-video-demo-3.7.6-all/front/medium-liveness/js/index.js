// define html elements
const videoOutput = document.querySelector('#user-video');
const startCaptureButtons = document.querySelectorAll('.start-capture');
const stopCaptureButton = document.querySelector('#stop-capture');
const challengeAnimation = document.querySelector('#challenge-animation');
const headStartPostionOutline = document.querySelector('#center-head-animation');
const moveCloserMsg = document.querySelector('#move-closer-animation');
const illuminationOverlay = document.querySelector('#illumination-overlay');
const selfieInput = document.querySelector('#selfieInput');

let client, videoStream, sessionId, face1;

const urlParams = new URLSearchParams(window.location.search);
const isMatchingEnabled = urlParams.get('enableMatching') === 'true';

const basePath = '/demo-server';

/**
 * 1- init liveness session (from backend)
 * 2- init the communication with the server via webrtc & socket
 * 3- get liveness result (from backend)
 * 4- ask the enduser to push his reference image (post to backend)
 * 5- get the matching result between the best image from webRTC and the reference image
 */
async function init() {
    sessionId = await initLivenessSession().catch(() => stopVideoCaptureAndProcessResult(false));
    if (!sessionId) return;
    // get user camera video (front camera is default)
    videoStream = await BioserverVideo.getDeviceStream().catch(() => stopVideoCaptureAndProcessResult(false));
    if (!videoStream) return;
    // display the video stream
    videoOutput.srcObject = videoStream;
    let challengeInProgress = false;

    // initialize the face capture client with callbacks
    const faceCaptureOptions = {
        wspath: basePath + '/wsocket',
        bioSessionId: sessionId,
        showChallengeInstruction: (challengeInstruction) => {
            challengeInProgress = true;
            // display challenge animation
            challengeAnimation.classList.remove('d-none-fadeout');
            headStartPostionOutline.classList.add('d-none-fadeout');
            moveCloserMsg.classList.add('d-none-fadeout');
        },
        showChallengeResult: async () => {
            console.log('Liveness Challenge done > requesting result ...');
            challengeInProgress = false;
            const result= await getLivenessChallengeResult(sessionId).catch(() => stopVideoCaptureAndProcessResult(false));
            stopVideoCaptureAndProcessResult(result.isLivenessSucceeded, result.message, result.bestImageId);
            if (client) client.disconnect();
        },
        trackingFn: (trackingInfo) => {
            displayInstructionsToUser(trackingInfo, challengeInProgress)
            if (trackingInfo.colorDisplay) {
                displayIlluminationOverlay(trackingInfo.colorDisplay, 0);
            }
        },
        errorFn: (error) => {
            console.log("got error", error);
            challengeInProgress = false;
            stopVideoCaptureAndProcessResult(false);
            if (client) client.disconnect();

        }
    };
    client = await BioserverVideo.initFaceCaptureClient(faceCaptureOptions);
}

startCaptureButtons.forEach(btn => {
    btn.addEventListener('click', async () => {
        document.querySelector('header').classList.add('d-none');
        document.querySelector('main').classList.add('darker-bg');
        await init().catch(() => stopVideoCaptureAndProcessResult(false));
        if (client) setTimeout(() => {
            client.start(videoStream, false, 'LIVENESS_MEDIUM')
        }, 2000);
    })
});

stopCaptureButton.addEventListener('click', async () => {
    stopVideoCaptureAndProcessResult(false, 'Liveness interrupted');
    if (client) client.disconnect();
});

// when next button is clicked go to targeted section
document.querySelectorAll('.step button[data-target]')
    .forEach(btn => btn.addEventListener('click', async () => {
        // d-none all steps
        document.querySelectorAll('.step').forEach(row => row.classList.add('d-none'));
        // display targeted step
        const targetStep = document.querySelector(btn.getAttribute('data-target'));
        targetStep.classList.remove('d-none');

        const targetStepFooter = targetStep.querySelector('.footer');
        if (targetStepFooter) {
            targetStepFooter.classList.add('d-none');
            setTimeout(() => targetStepFooter.classList.remove('d-none'), 3000);
        }
    }))

// gif animations are played only once, this will make them play again
document.querySelectorAll('.reset-animations').forEach(btn => {
    btn.addEventListener('click', () => {
        refreshImgAnimations();
    });
});
// Display first next button of tutorial after 3 sec.
setTimeout(() => document.querySelector('#step-1 .footer').classList.remove('d-none'), 3000);

function refreshImgAnimations() {
    // reload img animations
    document.querySelectorAll('.step > .animation > img').forEach(img => {
        img.src = img.src + '?v=' + Math.random();
    })
}

async function stopVideoCaptureAndProcessResult(success, msg, faceId="") {
    face1=faceId;
    document.querySelector('header').classList.remove('d-none');
    document.querySelector('main').classList.remove('darker-bg');
    challengeAnimation.classList.add('d-none-fadeout');
    moveCloserMsg.classList.add('d-none-fadeout');
    headStartPostionOutline.classList.remove('d-none-fadeout');
    document.querySelectorAll('.step').forEach(step =>  step.classList.add('d-none'));
    if (success) {
        document.querySelector('#step-5').classList.remove('d-none');
        document.querySelectorAll('#step-5 button').forEach(btn => btn.classList.add('d-none'));
        const faceImg = await getFaceImage(sessionId, faceId);
        const bestImg = document.querySelector('#step-5 .animation .best-image');
        bestImg.style.backgroundImage = 'url('+ window.URL.createObjectURL(faceImg) + ')';
        const nextButton = isMatchingEnabled ? 'next-step' : 'reset-step';
        document.querySelector('#step-5 button.' + nextButton).classList.remove('d-none');
    } else {
        document.querySelector('#step-8').classList.remove('d-none');
        if (msg) document.querySelector('#step-8 .description').textContent = msg;
    }
}

function displayInstructionsToUser(trackingInfo, challengeInProgress) {
    if (trackingInfo.distance) { // << user face found but too far from camera
        challengeAnimation.classList.add('d-none-fadeout');
        headStartPostionOutline.classList.add('d-none-fadeout');
        moveCloserMsg.classList.remove('d-none-fadeout');
    } else if (trackingInfo.faceh === 0 && trackingInfo.facew === 0) { // << no face detected
        challengeAnimation.classList.add('d-none-fadeout');
        headStartPostionOutline.classList.remove('d-none-fadeout');
        moveCloserMsg.classList.add('d-none-fadeout');
    } else { // display challenge instruction
        moveCloserMsg.classList.add('d-none-fadeout');
        headStartPostionOutline.classList.add('d-none-fadeout');
        if (challengeInProgress) {
            challengeAnimation.classList.remove('d-none-fadeout');
        }
    }
}

function displayIlluminationOverlay(colors, i) {
    // show illumination. overlay
    illuminationOverlay.style.backgroundColor = colors[i];
    illuminationOverlay.classList.remove('d-none');
    if (client) client.colorDisplayed();
    // switch illumination overlay color
    setTimeout(function () {
        illuminationOverlay.classList.add('d-none');
        if (colors[i + 2]) displayIlluminationOverlay(colors, i + 2);
    }, colors[i + 1]);
}

async function pushFaceAndDoMatch(selfieImage) {
    try {
        const face2 = await createFace(sessionId, selfieImage);
        const matches = await getMatches(sessionId, face1, face2.faceId);
        document.querySelectorAll('.step').forEach(step =>  step.classList.add('d-none'));
        if (matches.matching === 'ok') {
            document.querySelector('#step-7 .description').innerHTML = 'Matching succeeded <br> score: ' + matches.score;
            document.querySelector('#step-7').classList.remove('d-none');
        } else {
            document.querySelector('#step-8').classList.remove('d-none');
            if (matches.score) document.querySelector('#step-8 .description').innerHTML = 'Matching failed <br> score: ' + matches.score;
        }
        console.log(matches);
    } catch (e) {
        console.error(e);
        stopVideoCaptureAndProcessResult(false);
    }
}


/**
 * init a liveness session
 * @return sessionId
 */
async function initLivenessSession() {
    return new Promise(function (resolve, reject) {
        const xhttp = new window.XMLHttpRequest();
        xhttp.open('GET', basePath + '/init-liveness-session', true);
        xhttp.responseType = 'json';
        xhttp.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(xhttp.response)
            } else {
                console.error('initLivenessSession failed')  ;
                reject();
            }
        };
        xhttp.onerror = function () {
            reject();
        }
        xhttp.send();
    })
}

/**
 * retrieve the liveness challenge result from backend
 * @param sessionId
 * @return {isLivenessSucceeded, message}
 */
async function getLivenessChallengeResult(sessionId) {
    return new Promise(function (resolve, reject) {
        const xhttp = new window.XMLHttpRequest();
        xhttp.open('GET', basePath + '/liveness-challenge-result/'+sessionId, true);
        xhttp.setRequestHeader('Content-type', 'application/json');
        xhttp.responseType = 'json';
        xhttp.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(xhttp.response)
            } else {
                console.error('getLivenessChallengeResult failed')  ;
                reject();
            }
        };
        xhttp.onerror = function () {
            reject();
        }
        xhttp.send();
    })
}

async function createFace(sessionId, imageFile, faceInfo = '{"imageType" : "SELFIE","friendlyName" : "selfie", "imageRotationEnabled":"true"}') {

    return new Promise(function (resolve, reject) {
        const formData = new window.FormData();
        const xhttp = new window.XMLHttpRequest();
        formData.append('image', imageFile);
        formData.append('face', new window.Blob([faceInfo], {type: 'application/json'}));
        xhttp.open('POST', basePath + '/bio-session/' + sessionId + '/faces', true);
        xhttp.responseType = 'json';
        xhttp.onload = function () {
            document.getElementById("loading").classList.add('d-none');
            if (this.status === 200) {
                resolve(xhttp.response)
            } else {
                console.error('createFace failed')  ;
                reject();
            }
        }
        xhttp.onerror = function () {
            console.error('createFace failed')  ;
            reject(xhttp)
        }
        xhttp.send(formData);
        document.getElementById("loading").classList.remove('d-none');
    })
}

async function getFaceImage(sessionId, faceId) {

    return new Promise(function (resolve, reject) {
        const xhttp = new window.XMLHttpRequest();
        xhttp.open('GET', basePath + '/bio-session/' + sessionId + '/faces/'+ faceId + '/image', true);
        xhttp.responseType = 'blob';
        xhttp.onload = function () {
            if (this.status === 200) {
                resolve(xhttp.response)
            } else {
                console.error('createFace failed')  ;
                reject();
            }
        }
        xhttp.onerror = function () {
            console.error('createFace failed')  ;
            reject(xhttp)
        }
        xhttp.send()
    })
}
async function getMatches(sessionId, referenceFaceId, candidateFaceId) {

    return new Promise(function (resolve, reject) {
        const xhttp = new window.XMLHttpRequest();
        xhttp.open('GET', basePath + '/bio-session/' + sessionId + '/faces/'+ referenceFaceId + '/matches/' + candidateFaceId, true);
        xhttp.responseType = 'json';
        xhttp.onload = function () {
            if (this.status === 200) {
                resolve(xhttp.response)
            } else {
                console.error('createFace failed')  ;
                reject();
            }
        }
        xhttp.onerror = function () {
            console.error('createFace failed')  ;
            reject(xhttp)
        }
        xhttp.send()
    })
}


document.querySelector('#takeMyPickture').addEventListener('click', () => {
    selfieInput.click();
});
selfieInput.addEventListener('change', (e) => {
    pushFaceAndDoMatch(e.target.files[0])
});