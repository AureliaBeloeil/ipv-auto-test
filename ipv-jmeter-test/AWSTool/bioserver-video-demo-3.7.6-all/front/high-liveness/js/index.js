// define html elements
const videoOutput = document.querySelector('#user-video');
const tutorialVideoPlayer = document.querySelector('#video-player');
const stopCaptureButton = document.querySelector('#stop-capture');
const stopTutorialButton = document.querySelector('#stop-tutorial');
const switchCameraButton = document.querySelector('#switch-camera');
const headStartPostionOutline = document.querySelector('#center-head-animation');
const headRotationAnimation = document.querySelector('#head-rotation-animation');
const moveCloserMsg = document.querySelector('#move-closer-animation');
const loadingChallenge = document.querySelector('#loading-challenge');
const illuminationOverlay = document.querySelector('#illumination-overlay');
const bestImgElement = document.querySelector('#step-liveness-ok .best-image');
const selfieInput = document.querySelector('#selfieInput');

let client, // let start & stop face capture
    videoStream, // user video camera stream
    videoMediaDevices, // list of user camera devices
    sessionId, // current sessionId
    resetSession, // reset the current liveness session when using real mode (use the same session for training attempts)
    bestImageId, // best image captured from user video stream
    bestImageURL, // best image url (in memory window.URL.createObjectURL)
    positiveMsgDisplayed,
    currentDeviceIndex, // index of the camera currently used in face capture
    cameraPermissionAlreadyAsked;

const urlParams = new URLSearchParams(window.location.search); // let you extract params from url
const isMatchingEnabled = urlParams.get('enableMatching') === 'true';

const basePath = '/demo-server';

/**
 * 1- init liveness session (from backend)
 * 2- init the communication with the server via webrtc & socket
 * 3- get liveness result (from backend)
 * 4- [Optional] ask the end user to push his reference image (post to backend)
 * 5- [Optional] get the matching result between the best image from webRTC and the reference image
 */
async function init(options = {}){
    initLivenessDesign(options.trainingMode);
    // get user camera video (front camera is default)
    videoStream = await BioserverVideo.getDeviceStream({video:  {deviceId: options.deviceId}})
        .catch((e) => {
            let msg = 'Failed to get camera device stream';
            if (e.name && e.name.indexOf('NotAllowed') > -1) {
                msg = 'Access to camera is required to do liveness detection';
            }
            stopVideoCaptureAndProcessResult(false, msg)
        });
    if (!videoStream) return;
    // display the video stream
    videoOutput.srcObject = videoStream;

    // request a sessionId from backend (if we are in training mode or switching camera we use the same session)
    if (!sessionId || resetSession && !options.switchCamera) {
        sessionId = await initLivenessSession().catch(() => stopVideoCaptureAndProcessResult(false, 'Failed to initialize session'));
    }
    if (!sessionId) return;

    // initialize the face capture client with callbacks
    let challengeInProgress = false;
    const faceCaptureOptions = {
        wspath: basePath + '/wsocket',
        bioSessionId: sessionId,
        showChallengeInstruction: (challengeInstruction) => {
            console.log('##################### challenge inst', challengeInstruction)
            challengeInProgress = true;
            switchCameraButton.classList.add('d-none');
            // display challenge
            let options = {
                challengePoint :{
                    target: {image: './img/challengeAnimation.gif'}, // on hover challenge image
                    done: {image: './img/checkMark2.gif'}, // on done challenge image
                }
            };
            options = {}; // remove this line to see custom animations during liveness challenge
            BioserverVideoUI.resetLivenessHighGraphics(options);
        },
        showChallengeResult: async (trainingChallengeResult) => { // << we get the results only in training mode, for real mode ask backend to retrieve results from WBServer
            console.log('Liveness Challenge done > requesting result ...', trainingChallengeResult);
            challengeInProgress = false;
            BioserverVideoUI.resetLivenessHighGraphics();
            headRotationAnimation.classList.add('d-none-fadeout');
            loadingChallenge.classList.remove('d-none-fadeout');
            if (trainingChallengeResult && trainingChallengeResult.livenessStatus) {
                let msg = 'Timeout has expired', isLivenessSucceeded;
                if (trainingChallengeResult.livenessStatus === 'SUCCESS') {
                    msg = 'Liveness succeeded';
                    isLivenessSucceeded = true;
                }
                stopVideoCaptureAndProcessResult(isLivenessSucceeded, msg, '', options.trainingMode);
            } else {
                const result= await getLivenessChallengeResult(sessionId).catch(() => stopVideoCaptureAndProcessResult(false, 'Failed to retrieve liveness results'));
                if (result) stopVideoCaptureAndProcessResult(result.isLivenessSucceeded, result.message, result.bestImageId, options.trainingMode);
            }
            if (client) client.disconnect();
        },
        trackingFn: (trackingInfo)=>{
            displayInstructionsToUser(trackingInfo, challengeInProgress, options.trainingMode);
            if (trackingInfo.colorDisplay) {
                displayIlluminationOverlay(trackingInfo.colorDisplay, 0);
            }
            if (trackingInfo.livenessHigh) {
                BioserverVideoUI.updateLivenessHighGraphics('user-video', trackingInfo);
            }
        },
        errorFn: (error) => {
            console.log("got error", error);
            challengeInProgress = false;
            stopVideoCaptureAndProcessResult(false, 'Sorry, there was an issue.');
            if (client) client.disconnect();

        }
    };
    client = await BioserverVideo.initFaceCaptureClient(faceCaptureOptions);
}

stopCaptureButton.addEventListener('click', async () => {
    resetLivenessDesign();
    if (client) client.disconnect();
});

switchCameraButton.addEventListener('click', async () => {
    if (client) {
        try {
            switchCameraButton.classList.add('d-none');
            // retrieve user cameras
            if (!videoMediaDevices) {
                const mediaDevices = await BioserverVideo.initMediaDevices();
                videoMediaDevices = mediaDevices.videoDevices.map(d => {return d.deviceId});
            }
            if (!videoMediaDevices || videoMediaDevices.length === 1) { // << we do not switch camera if only 1 camera found
                switchCameraButton.classList.remove('d-none');
                return;
            }
            client.disconnect();
            // training mode enabled ?
            const trainingMode = document.querySelector('#step-liveness').classList.contains('training-mode');
            resetLivenessDesign();
            console.log('video devices: ',{videoMediaDevices});
            const videoTrack = videoStream.getVideoTracks()[0];
            const settings = videoTrack.getSettings();
            console.log('video track settings: ',{settings});
            let currentDeviceId = settings.deviceId; // not all browsers support this
            const index = currentDeviceId ? videoMediaDevices.indexOf(currentDeviceId): currentDeviceIndex ? currentDeviceIndex+1 : 0;
            if (videoMediaDevices.length > 1) {
                if (index < videoMediaDevices.length - 1) {
                    currentDeviceIndex = index + 1;
                } else {
                    currentDeviceIndex = 0;
                }
            }
            // get next camera id (loop over user cameras)
            currentDeviceId = videoMediaDevices[currentDeviceIndex];
            await init({deviceId: currentDeviceId, trainingMode:trainingMode, switchCamera: true});
            setTimeout( () => {
                client.start(videoStream, false, 'LIVENESS_HIGH',2, trainingMode);
                switchCameraButton.classList.remove('d-none');
            }, 2000);
        } catch (e) {
            stopVideoCaptureAndProcessResult(false, 'Failed to switch camera');
        }

    }
});

stopTutorialButton.addEventListener('click', async () => {
    tutorialVideoPlayer.pause();
});

// when next button is clicked go to targeted step
document.querySelectorAll('*[data-target]')
    .forEach( btn => btn.addEventListener('click', async () => {
        // d-none all steps
        document.querySelectorAll('.step').forEach(row => row.classList.add('d-none'));
        // display targeted step
        let targetStepId = btn.getAttribute('data-target');
        if (targetStepId === '#step-tutorial') {
            // start playing tutorial video (from the beginning)
            tutorialVideoPlayer.currentTime = 0;
            tutorialVideoPlayer.play();
        } else
            if (targetStepId === '#step-liveness' ) { // << if client clicks on start capture or start training
            const trainingMode = btn.classList.contains('start-training-capture');
            if (!cameraPermissionAlreadyAsked) {// << display the camera access permission step the first time only
                cameraPermissionAlreadyAsked = true; // TODO: use localStorage ??
                targetStepId = '#step-access-permission';
                // when client accepts camera permission access > we redirect it to the desired mode (training mode/ real mode)
                document.querySelector(targetStepId + ' button').classList
                    .add(trainingMode ? 'start-training-capture':'start-capture');
            } else {
                document.querySelector('#step-liveness').classList.remove('d-none');
                await init({trainingMode: trainingMode}).catch(() => stopVideoCaptureAndProcessResult(false));
                if (client) setTimeout( () => {
                    client.start(videoStream, false, 'LIVENESS_HIGH', 2, trainingMode);
                    switchCameraButton.classList.remove('d-none');
                    }, 2000);
                return;
            }
        }
        const targetStep = document.querySelector(targetStepId);
        targetStep.classList.remove('d-none');
    }));
document.querySelector('#step-liveness .tutorial').addEventListener('click', async () => {
  if (client) {
     resetLivenessDesign();
     client.disconnect();
  }
});
// gif animations are played only once, this will make them play again
document.querySelectorAll('.reset-animations').forEach(btn => {
    btn.addEventListener('click', () => {
        refreshImgAnimations();
    });
});
function refreshImgAnimations() {
    // reload only gif animations
    document.querySelectorAll('.step > .animation > img').forEach(img => {
        const gifAnimation = img.src.split('?')[0];
        if (gifAnimation.endsWith('.gif')) img.src = gifAnimation+ '?v=' + Math.random();
    })
}
async function stopVideoCaptureAndProcessResult(success, msg, faceId="", trainingMode){
    bestImageId=faceId;
    // we reset the session when we finished the liveness check real session
    resetSession = sessionId && !document.querySelector('#step-liveness').classList.contains('training-mode');
    resetLivenessDesign();
    document.querySelectorAll('.step').forEach(step =>  step.classList.add('d-none'));
    if (trainingMode) {
        if (success) {
            document.querySelector('#step-liveness-tr-ok').classList.remove('d-none');
        } else {
            document.querySelector('#step-liveness-tr-ko').classList.remove('d-none');
        }
    } else {
        if (success) {
            document.querySelector('#step-liveness-ok').classList.remove('d-none');
            document.querySelectorAll('#step-liveness-ok button').forEach(btn => btn.classList.add('d-none'));
            const faceImg = await getFaceImage(sessionId, faceId);
            bestImageURL = window.URL.createObjectURL(faceImg);
            bestImgElement.style.backgroundImage = 'url('+ bestImageURL + ')';
            const nextButton = isMatchingEnabled ? 'next-step' : 'reset-step';
            document.querySelector('#step-liveness-ok button.' + nextButton).classList.remove('d-none');
        } else if (msg && (msg.indexOf('Timeout')> -1 || msg.indexOf('failed') >-1)) {
            document.querySelector('#step-liveness-failed').classList.remove('d-none');
        } else {
            document.querySelector('#step-liveness-ko').classList.remove('d-none');
            if (msg) document.querySelector('#step-liveness-ko .description').textContent = msg;
        }
    }
}
/**
 * prepare video capture elements
 * @param trainingModeEnabled
 */
function initLivenessDesign(trainingModeEnabled){
    document.querySelector('header').classList.add('d-none');
    document.querySelector('main').classList.add('darker-bg');
    if (trainingModeEnabled) document.querySelector('#step-liveness').classList.add('training-mode');
    switchCameraButton.classList.add('d-none');
    moveCloserMsg.classList.add('d-none-fadeout');
    headRotationAnimation.classList.add('d-none-fadeout');
    loadingChallenge.classList.add('d-none-fadeout');
    headStartPostionOutline.classList.remove('d-none-fadeout');
    positiveMsgDisplayed = null;
}
/**
 * reset video capture elements at the end of the process
 */
function resetLivenessDesign() {
    BioserverVideoUI.resetLivenessHighGraphics();
    document.querySelector('header').classList.remove('d-none');
    document.querySelector('main').classList.remove('darker-bg');
    document.querySelector('#step-liveness').classList.remove('training-mode');
    if (bestImageURL) window.URL.revokeObjectURL(bestImageURL); // free memory
    bestImgElement.style.backgroundImage = null;
    switchCameraButton.classList.add('d-none');
}
/**
 * display messages to user during capture (eg: move closer, center your face ...)
 * @param trackingInfo face tracking info
 * @param challengeInProgress challenge has started?
 * @param trainingMode training mode enabled ?
 */
let lastChallengeIndex = -1;
function displayInstructionsToUser(trackingInfo, challengeInProgress, trainingMode){
    const graphicChallengePoints = document.querySelector('#wbs-graphics-overlay');
    if (trackingInfo.distance){ // << user face found but too far from camera
        if (graphicChallengePoints) graphicChallengePoints.classList.add('d-none');
        headStartPostionOutline.classList.add('d-none-fadeout');
        moveCloserMsg.classList.remove('d-none-fadeout');
        headRotationAnimation.classList.add('d-none-fadeout');
        loadingChallenge.classList.add('d-none-fadeout');
    } else if (trackingInfo.faceh === 0 && trackingInfo.facew === 0){ // << no face detected
        if (graphicChallengePoints) graphicChallengePoints.classList.add('d-none');
        headStartPostionOutline.classList.remove('d-none-fadeout');
        moveCloserMsg.classList.add('d-none-fadeout');
        headRotationAnimation.classList.add('d-none-fadeout');
        loadingChallenge.classList.add('d-none-fadeout');
    } else { // display challenge instruction
        moveCloserMsg.classList.add('d-none-fadeout');
        headStartPostionOutline.classList.add('d-none-fadeout');
        if (challengeInProgress && graphicChallengePoints) {
            graphicChallengePoints.classList.remove('d-none');
            loadingChallenge.classList.add('d-none-fadeout');
            if (trackingInfo.livenessHigh) {
                const mssg = headRotationAnimation.querySelector('.video-msg');
                if (trackingInfo.livenessHigh.targetChallengeIndex === 0) { // first point at the left of the user
                    if(trainingMode) headRotationAnimation.style.backgroundImage = 'url(./img/Center-Side-Right.gif)';
                    else headRotationAnimation.style.backgroundImage = '';
                    mssg.textContent = 'Rotate your head to move the line.';
                    mssg.classList.remove('bg-green');
                    mssg.classList.add('bg-blueviolet');
                    headRotationAnimation.classList.remove('d-none-fadeout');
                } else if (!positiveMsgDisplayed
                    && trackingInfo.livenessHigh.targetChallengeIndex !== lastChallengeIndex){
                    lastChallengeIndex = trackingInfo.livenessHigh.targetChallengeIndex;
                    headRotationAnimation.style.backgroundImage = '';
                    let remainingChallengeNbr = 'one';
                    switch (Object.keys(trackingInfo.livenessHigh.challengeCircles).length - lastChallengeIndex + 1) {
                        case 2 : remainingChallengeNbr = 'one more point'; break;
                        case 3 : remainingChallengeNbr = 'two more points'; break;
                        case 4 : remainingChallengeNbr = 'three more points'; break;
                        case 5 : remainingChallengeNbr = 'four more points'; break;
                        case 6 : remainingChallengeNbr = 'five more points'; break;
                        default: remainingChallengeNbr = 'one more points';
                    }
                    mssg.textContent =  `Good job! Only ${remainingChallengeNbr} to go.`;
                    mssg.classList.remove('bg-blueviolet');
                    mssg.classList.add('bg-green');
                    headRotationAnimation.classList.remove('d-none-fadeout');
                    positiveMsgDisplayed = setTimeout(()=> {
                        headRotationAnimation.classList.add('d-none-fadeout'); positiveMsgDisplayed = false;
                    }, 3000)
                }

            }
        } else loadingChallenge.classList.remove('d-none-fadeout');
    }
}
// not used
function displayIlluminationOverlay (colors, i) {
    // show illumination. overlay
    illuminationOverlay.style.backgroundColor = colors[i];
    illuminationOverlay.classList.remove('d-none');
    if (client) client.colorDisplayed();
    // switch illumination overlay color
    setTimeout(function () {
        illuminationOverlay.classList.add('d-none');
        if (colors[i + 2]) displayIlluminationOverlay(colors, i + 2);
    }, colors[i + 1]);
}
/**
 * init a liveness session
 * @return sessionId
 */
async function initLivenessSession() {
    console.log('init liveness session');
    return new Promise(function (resolve, reject) {
        const xhttp = new window.XMLHttpRequest();
        xhttp.open('GET', basePath + '/init-liveness-session', true);
        xhttp.responseType = 'json';
        xhttp.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(xhttp.response)
            } else {
                console.error('initLivenessSession failed')  ;
                reject();
            }
        };
        xhttp.onerror = function () {
            reject();
        }
        xhttp.send();
    })
}
/**
 * retrieve the liveness challenge result from backend (via polling)
 * @param sessionId
 * @param maxAttempts
 * @param interval
 * @return {isLivenessSucceeded, message}
 */
async function getLivenessChallengeResult(sessionId, maxAttempts = 10, interval = 1000) {

    return new Promise(function (resolve, reject) {
        const xhttp = new window.XMLHttpRequest();
        xhttp.open('GET', basePath + '/liveness-challenge-result/'+sessionId+'/?polling=true', true);
        xhttp.setRequestHeader('Content-type', 'application/json');
        xhttp.responseType = 'json';
        xhttp.onload = () => {
            if (xhttp.status) {
                if (xhttp.status === 200) {
                    resolve(xhttp.response)
                } else if (maxAttempts) { // >> polling
                    console.log('getLivenessChallengeResult retry ...', maxAttempts);
                    return new Promise(r => setTimeout(r, interval))
                        .then( () => {
                            resolve(getLivenessChallengeResult(sessionId, maxAttempts -1));
                        });
                } else {
                    console.error('getLivenessChallengeResult failed, max retries reached');
                    reject();
                }
            }

        };

        xhttp.onerror = function (e) {
            reject();
        }
        xhttp.send();
    })
}
/**
 * send another image to match with video best image
 * @param selfieImage
 * @return {Promise<void>}
 */
async function pushFaceAndDoMatch(selfieImage) {
    try {
        const face2 = await createFace(sessionId, selfieImage);
        const matches = await getMatches(sessionId, bestImageId, face2.faceId);
        document.querySelectorAll('.step').forEach(step =>  step.classList.add('d-none'));
        if (matches.matching === 'ok') {
            document.querySelector('#step-selfie-ok .description').innerHTML = 'Matching succeeded <br> score: ' + matches.score;
            document.querySelector('#step-selfie-ok').classList.remove('d-none');
        } else {
            document.querySelector('#step-selfie-ko').classList.remove('d-none');
            if (matches.score) {
                document.querySelector('#step-selfie-ko .description').innerHTML = 'Matching failed <br> score: ' + matches.score || '';
            }
        }
        console.log(matches);
    } catch (e) {
        console.error(e);
        document.querySelectorAll('.step').forEach(step =>  step.classList.add('d-none'));
        document.querySelector('#step-selfie-ko').classList.remove('d-none');
        document.querySelector('#step-selfie-ko .description').innerHTML = 'Matching failed';
    }
}
// TODO: move faceInfo in backend side
async function createFace(sessionId, imageFile, faceInfo = '{"imageType" : "SELFIE","friendlyName" : "selfie", "imageRotationEnabled":"true"}') {

    return new Promise(function (resolve, reject) {
        const formData = new window.FormData();
        const xhttp = new window.XMLHttpRequest();
        formData.append('image', imageFile);
        formData.append('face', new window.Blob([faceInfo], {type: 'application/json'}));
        xhttp.open('POST', basePath + '/bio-session/' + sessionId + '/faces', true);
        xhttp.responseType = 'json';
        xhttp.onload = function () {
            document.getElementById("loading").classList.add('d-none');
            if (this.status === 200) {
                resolve(xhttp.response)
            } else {
                console.error('createFace failed')  ;
                reject();
            }
        }
        xhttp.onerror = function () {
            console.error('createFace failed')  ;
            reject(xhttp)
        }
        xhttp.send(formData);
        document.getElementById("loading").classList.remove('d-none');
    })
}
async function getFaceImage(sessionId, faceId) {

    return new Promise(function (resolve, reject) {
        const xhttp = new window.XMLHttpRequest();
        xhttp.open('GET', basePath + '/bio-session/' + sessionId + '/faces/'+ faceId + '/image', true);
        xhttp.responseType = 'blob';
        xhttp.onload = function () {
            if (this.status === 200) {
                resolve(xhttp.response)
            } else {
                console.error('createFace failed')  ;
                reject();
            }
        }
        xhttp.onerror = function () {
            console.error('createFace failed')  ;
            reject(xhttp)
        }
        xhttp.send()
    })
}
async function getMatches(sessionId, referenceFaceId, candidateFaceId) {

    return new Promise(function (resolve, reject) {
        const xhttp = new window.XMLHttpRequest();
        xhttp.open('GET', basePath + '/bio-session/' + sessionId + '/faces/'+ referenceFaceId + '/matches/' + candidateFaceId, true);
        xhttp.responseType = 'json';
        xhttp.onload = function () {
            if (this.status === 200) {
                resolve(xhttp.response)
            } else {
                console.error('createFace failed')  ;
                reject();
            }
        }
        xhttp.onerror = function () {
            console.error('createFace failed')  ;
            reject(xhttp)
        }
        xhttp.send()
    })
}

document.querySelector('#takeMyPickture').addEventListener('click', () => {
    selfieInput.click();
});
selfieInput.addEventListener('change', (e) => {
    pushFaceAndDoMatch(e.target.files[0])
});