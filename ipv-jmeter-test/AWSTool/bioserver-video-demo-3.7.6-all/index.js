const start = Date.now();
const wbsApi = require('./server/wbs-api');
const gipsApi = require('./server/gips-api');
const express = require('express');
const config = require('./server/config');
const https = require('https');
const fs = require('fs');
const serveStatic = require('serve-static');
const httpProxy = require('http-proxy');
const multer  = require('multer'); // for files upload
const storage = multer.memoryStorage(); // use in memory
const upload = multer({ storage: storage }); // a temp directory could be used instead
const debug = require('debug')('front:app:server');
const app = express();

let token, livenessResults = {}; // TODO use some distributed cache

// load certs from config folder
const server = https.createServer({
    key: fs.readFileSync(config.TLS_KEY_PATH),
    cert: fs.readFileSync(config.TLS_CERT_PATH)
}, app).listen(config.TLS_API_PORT, () => {
    debug('Backend server started - https://localhost:' + config.TLS_API_PORT + config.BASE_PATH);
    debug('Total starting time: ', Date.now() - start , 'ms')
});


// forward socket requests to wbserver
app.all(config.BASE_PATH + '/wsocket', function (req, res) {
    proxy.web(req, res, {
        target: getWsTarget(req),
        ignorePath: true
    });
});

app.get(config.BASE_PATH + '/bioserver-environment-api.js', function (req, res) {
    proxy.web(req, res, {
        target: config.BIOSERVER_VIDEO_URL + config.VIDEO_SERVER_BASE_PATH + '/bioserver-environment-api.js',
        ignorePath: true
    });
});

app.get(config.BASE_PATH + '/apidoc', function (req, res) {
    proxy.web(req, res, {
        target: config.BIOSERVER_VIDEO_URL + config.VIDEO_SERVER_BASE_PATH + '/apidoc',
        ignorePath: true
    });
});

app.get(config.BASE_PATH + '/bioserver-video-api.js', function (req, res) {
    proxy.web(req, res, {
        target: config.BIOSERVER_VIDEO_URL + config.VIDEO_SERVER_BASE_PATH + '/bioserver-video-api.js',
        ignorePath: true
    });
});
app.get(config.BASE_PATH + '/bioserver-video-ui.js', function (req, res) {
    proxy.web(req, res, {
        target: config.BIOSERVER_VIDEO_URL + config.VIDEO_SERVER_BASE_PATH + '/bioserver-video-ui.js',
        ignorePath: true
    });
});


app.get(config.BASE_PATH + '/networktest.zip', function (req, res) {
    proxy.web(req, res, {
        target: config.BIOSERVER_VIDEO_URL + config.VIDEO_SERVER_BASE_PATH + '/networktest.zip',
        ignorePath: true
    });
});

app.post(config.BASE_PATH + '/networkspeed', function (req, res) {
    proxy.web(req, res, {
        target: config.BIOSERVER_VIDEO_URL + config.VIDEO_SERVER_BASE_PATH + '/networkspeed',
        ignorePath: true
    });
});


// serve demos
debug('Aivalable web applications:');
app.use(config.BASE_PATH , serveStatic('front/home'));
debug('Home page => /');
app.use(config.BASE_PATH + '/medium-liveness', serveStatic('front/medium-liveness'));
debug('Medium liveness => /medium-liveness');
app.use(config.BASE_PATH + '/high-liveness', serveStatic('front/high-liveness'));
debug('high liveness => /high-liveness');
app.use(config.BASE_PATH + '/technical-demo', serveStatic('front/simple-client'));
debug('technical demo page => /technical-demo');

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded({extended: true}));
// Parse JSON bodies (as sent by API clients)
app.use(express.json());

// create the proxy
const proxy = new httpProxy.createProxyServer({
    target: config.BIOSERVER_VIDEO_URL,
    ssl: {
        key: fs.readFileSync(config.TLS_KEY_PATH),
        cert: fs.readFileSync(config.TLS_CERT_PATH)
    },
    changeOrigin: true,
    ws: true,
    secure: false
});
if(proxy && proxy.options.target) {
    debug('Middleware server created - Target server:', proxy.options.target)
}

proxy.on('error', async (error, req, res) => {
    debug('got error', error);
    res.writeHead(500, {
        'Content-Type': 'text/plain'
    });
    res.end('Something went wrong with your request.');
});

// -------------------------------------
// -- Add authentication headers
// -- on outgoing proxy requests to wbs
// -------------------------------------
//
proxy.on('proxyReq', function(proxyReq, req, res, options) {
    if (config.TOKEN_ENABLED) {
        proxyReq.setHeader('authorization','Bearer ' + token);
    } else {
        proxyReq.setHeader(config.API_KEY_HEADER, config.API_KEY_SECRET);
    }
});
proxy.on('proxyRes', function (proxyRes, req, res) {
    // in case we want to debug response
    // debug('Got proxy response from the target url:', req.url, JSON.stringify(proxyRes.headers, true, 2));
});
server.on('upgrade', (req, socket, head) => {
    if (config.TOKEN_ENABLED) {
        req.headers['authorization'] =  'Bearer ' + token;
    } else {
        req.headers[config.API_KEY_HEADER] =  config.API_KEY_SECRET;
    }
    proxy.ws(req, socket, head, {
        target: getWsTarget(req),
        ignorePath: true
    });
});

/**                     **
 * -------------------- **
 * --  http endpoints   **
 * -------------------- **
 *                      **/
//
// init liveness challenge session (do not create session if sessionId parameter is present)
//
app.get(config.BASE_PATH + '/init-liveness-session/:sessionId?', async (req, res) => {
    // retrieve token and new session
    try {

        let sessionId = req.params.sessionId;

        if (config.IDPROOFING && (!sessionId || sessionId == 'null')) {
            debug('<< sessionId not present, calling getSession..from gips api.');
            session  = await gipsApi.getSession();
            sessionId = session.sessionId;
            livenessResults[sessionId]=
                {status: 'PENDING',
                identityId:session.identityId,
                portraitId:session.portraitId};
        } else {

            if (!sessionId || sessionId == 'null') {
                debug('<< sessionId not present, calling getSession...');
                token = await wbsApi.getToken();
                sessionId = await wbsApi.getSession(token);
            } else {
                debug('<< sessionId present, avoid calling getSession to use : ', sessionId);
            }
            livenessResults[sessionId] = {status: 'PENDING'};

        }

        debug(sessionId, 'init-liveness-session', livenessResults[sessionId]);
        res.json(sessionId);

    } catch(e) {
        debug('init-liveness-session failed', e);
        debug('init-liveness-session failed', e.status, e.statusText);
        res.status(e.status ? e.status : 500).json({error: 'init-liveness-session failed'});
    }
});
//
// Receive liveness challenge result from WebioServer
//
app.post(config.BASE_PATH + config.LIVENESS_RESULT_CALLBACK_PATH, async (req, res) => {
    const sessionId = req.body && req.body.sessionId;
    if (!sessionId) {
        const err = {error: 'Missing mandatory param sessionId'};
        debug('Failed to request liveness result with error:', err);
        res.status(400).send(err);
    } else {

        debug('<< liveness result available for sessionID: ', sessionId);
        res.status(204).send();
        const livenessResult = await wbsApi.getLivenessChallengeResult(token, sessionId).catch(err => {
            debug('Failed to request liveness result with error:', err);
        });
        debug(sessionId, 'Request liveness result', livenessResult);
        livenessResults[sessionId] = livenessResult;
        //setTimeout( () => {livenessResults[sessionId] = livenessResult}, 5000);
    }
});
//
// Get liveness challenge result (polling from client)
//
app.get(config.BASE_PATH + '/liveness-challenge-result/:sessionId', async (req, res) => {
    // retrieve sessionId
    let sessionId = req.params.sessionId;
    let polling = req.query.polling;
    debug(sessionId, '> retrieve liveness-challenge result', {polling});
    if (!sessionId) {
        const error = {error: 'Missing mandatory param sessionId'};
        debug('< get liveness-challenge failed', error);
        res.status(400).json(error);
    } else {
        try {
            let livenessResult = livenessResults[sessionId];
            if (!livenessResult) {
                debug(sessionId, '< No liveness-challenge associated to this session');
                res.status(404).send(); // unknown sessionID
            } else if (polling && livenessResult.status === 'PENDING') {
                debug(sessionId, '< Waiting for liveness result callback ');
                res.status(204).send(); // tell client that no liveness result is available for now
            } else {
                if (!polling) {
                    debug(sessionId, '> No callback done, retrieve directly liveness challenge results');
                         if(config.IDPROOFING){
                        livenessResult = await gipsApi.getLivenessChallengeResult(livenessResults[sessionId].identityId , livenessResults[sessionId].portraitId )
                        Object.assign( livenessResult, livenessResults[sessionId] );

                    } else {
                             livenessResult = await wbsApi.getLivenessChallengeResult(token, sessionId);

                    }

                }
                delete livenessResults[sessionId];
                debug(sessionId, '< Got liveness-challenge result', {livenessResult});
                let result = {isLivenessSucceeded: false, message: 'Something was wrong'};
                if (livenessResult.livenessStatus === 'SUCCESS') {
                    result.message = 'Liveness succeeded';
                    result.isLivenessSucceeded = true;
                    result.bestImageId = livenessResult.bestImageId;
                } else if (livenessResult.livenessStatus === 'SPOOF') {
                    result.message = 'Liveness failed';
                } else if (livenessResult.livenessStatus === 'TIMEOUT') {
                    result.message = 'Timeout has expired';
                } else if (livenessResult.livenessStatus === 'ERROR') {
                    result.message = 'Something was wrong';
                }
                res.send(result);
            }
        } catch(e) {
            debug(sessionId, 'get liveness-challenge failed', e.status, e.statusText, e);
            res.status(e.status ? e.status : 500).json({error: 'get liveness-challenge failed'});
        }
    }
});
//
// Create face
//
app.post(config.BASE_PATH + '/bio-session/:bioSessionId/faces', upload.any(),
    async (req, res) => {
        try {

            let image = req.files[0];
            let face = req.files[1];
            if (req.files[0].fieldname !== 'image') {
                image = req.files[1];
                face = req.files[0];
            }
            const sessionId = req.params.bioSessionId;
            if(config.IDPROOFING){

                let livenessResult = livenessResults[sessionId];

                const faceResult = await gipsApi.createFace(image, livenessResult.identityId);

            } else {
                const faceResult = await wbsApi.createFace(token, sessionId, image, face);
                debug(sessionId, 'create face result', faceResult);
                if (faceResult.quality >= config.CODING_QUALITY_THRESHOLD) res.json({faceId: faceResult.id});
                else res.status(400).send();
            }
        } catch (e) {
            debug(req.params.bioSessionId, 'create face failed', e.status, e.statusText);
            res.status(e.status ? e.status : 500).send();
        }
    }
);
//
// get face image
//
app.get(config.BASE_PATH + '/bio-session/:bioSessionId/faces/:faceId/image', async (req, res) => {
    try {
        const sessionId = req.params.bioSessionId;
        const faceId = req.params.faceId;
        const faceImg = await wbsApi.getFaceImage(token, sessionId, faceId);
        debug(sessionId, 'get a face img for faceID', faceId);
        res.status(200).send(faceImg);
    } catch (e) {
        debug(req.params.bioSessionId, 'get face image failed', e.status, e.statusText);
        res.status(e.status ? e.status : 500).send();
    }
});
//
// get matches
//
app.get(config.BASE_PATH + '/bio-session/:bioSessionId/faces/:referenceFaceId/matches/:candidateFaceId', async (req, res) => {
    try {

        if(config.IDPROOFING){

            getLivenessChallengeResult(session)

         res.json(Object.assign({matching: "ko", score: Math.round(match.score)}));


        } else {

            const referenceFaceId = req.params.referenceFaceId;
            const candidateFaceId = req.params.candidateFaceId;
            const sessionId = req.params.bioSessionId;
            const matchResult = await wbsApi.doMatch(token, sessionId, referenceFaceId);
            debug(sessionId, 'get matches result', matchResult);
            // check matching score ...
            const match = matchResult.find(m => m.candidate.id === candidateFaceId);
            // client should not see all results (like score) ...
            if (match && match.score >= config.MATCHING_SCORE_THRESHOLD) res.json(Object.assign({
                matching: "ok",
                score: Math.round(match.score)
            }));
            else res.json(Object.assign({matching: "ko", score: Math.round(match.score)}));

        }


    } catch (e) {
        debug(req.params.bioSessionId, 'get matches failed', e.status, e.statusText);
        res.status(e.status ? e.status : 500).send();
    }
});

//
// stream video tutorial
//
app.use(config.BASE_PATH + '/video', serveStatic('assets', {
    maxAge: '300000'
}));


function getWsTarget(req) {
    return config.BIOSERVER_VIDEO_URL
        + config.VIDEO_SERVER_BASE_PATH
        + config.VIDEO_SERVER_WSPATH
        + '/?' + req.url.split('?')[1];
}
