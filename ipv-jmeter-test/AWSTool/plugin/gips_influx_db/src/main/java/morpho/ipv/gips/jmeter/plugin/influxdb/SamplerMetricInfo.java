package morpho.ipv.gips.jmeter.plugin.influxdb;

import org.apache.jmeter.visualizers.backend.SamplerMetric;

/**
 * Created by g578661 on 24/09/2019.
 */
public class SamplerMetricInfo {

    private String threadName;
    private SamplerMetric samplerMetric;

    public SamplerMetricInfo(String threadName, SamplerMetric samplerMetric) {
        this.threadName = threadName;
        this.samplerMetric = samplerMetric;
    }

    public String getThreadName() {
        return threadName;
    }

    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }

    public SamplerMetric getSamplerMetric() {
        return samplerMetric;
    }

    public void setSamplerMetric(SamplerMetric samplerMetric) {
        this.samplerMetric = samplerMetric;
    }
}
