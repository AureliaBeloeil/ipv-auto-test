package morpho.ipv.gips.jmeter.plugin.influxdb;

import org.apache.jmeter.samplers.SampleResult;

/**
 * Created by g578661 on 25/09/2019.
 */
public class GipsSampleResult extends SampleResult {

    private long realTime;

    public GipsSampleResult (SampleResult sampleResult, long realTime) {
        super (sampleResult);
        this.realTime = realTime;
    }

    @Override
    public long getTime () {
        return realTime;
    }
}
