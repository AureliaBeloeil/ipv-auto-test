package morpho.ipv.gips.jmeter.plugin.influxdb;

/**
 * Created by g578661 on 19/09/2019.
 */
public class ScenarioMetric {

    private long duration;

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
