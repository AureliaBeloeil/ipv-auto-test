import groovy.json.JsonSlurper;

def jsonResponse = null
JsonSlurper JSON = new JsonSlurper ()
//get path of proof file
String proofFile = args[1]+"responses-report/"+args[0]+"_proof.form-data"
//get path of verificationAndEvaluationDetails file
String verificationAndEvaluationDetails = "verificationAndEvaluationDetails.json"

def zip = new java.util.zip.ZipFile(proofFile)

//Parse proof and take OCR service list from verificationAndEvaluationDetails.json file
zip.entries().findAll { !it.directory }.each {
	println it.name
	if (it.name.endsWith("verificationAndEvaluationDetails.json")) {
		jsonResponse = JSON.parseText(zip.getInputStream(it).text);
	}
}

//put jsonResponse in props properties
props.put(args[0], jsonResponse)

